﻿using System.Diagnostics.Contracts;
using System.Linq;
using Roslyn.Compilers.CSharp;
using ContractGenerator.Contracts;

namespace ContractGenerator.Classification
{
    public static class Negator
    {
        /// <summary>
        /// Attempts to apply the negations found inside the expression.
        /// If the expression cannot be negated, the original expression is returned.
        /// Currently, only simple unary and binary expressions can be negated.
        /// </summary>
        /// <param name="expression">the expression</param>
        /// <returns>the expression with its negations applied, or the original expression if the 
        /// negations could not be applied</returns>
        public static ExpressionSyntax TryApplyNegation(ExpressionSyntax expression)
        {
            Contract.Requires(expression != null);
            Contract.Ensures(Contract.Result<ExpressionSyntax>() != null);

            expression = expression.UnwrapParentheses();

            if (expression.IsNegation())
            {
                var tryToNegate = expression.GetNegationTerm().UnwrapParentheses();

                if (tryToNegate.IsNegation())
                {                
                    // This covers the case of a double negation, such as !(!(x)).
                    // In such a case we drop the 2 outermost negations ands try to negate the inner expression.
                    return TryApplyNegation(tryToNegate.GetNegationTerm());
                }
                else if (tryToNegate is BinaryExpressionSyntax)
                {
                    // Try to negate the binary expression.
                    var negatedBinaryExpression = TryNegate(tryToNegate);

                    if (negatedBinaryExpression != null)
                    {
                        return negatedBinaryExpression;
                    }
                }
                else if (tryToNegate is IdentifierNameSyntax || tryToNegate is InvocationExpressionSyntax)
                {
                    return Syntax.ParseExpression("!" + tryToNegate.ToFullString());
                }
            }
            return expression;
        }

        /// <summary>
        /// Attempts to negate the expression.
        /// If the expression could not be negated, null is returned.
        /// </summary>
        /// <param name="expression">the expression to negate</param>
        /// <returns>the negated expression, or <c>null</c> if the expression could not be negated</returns>
        public static ExpressionSyntax TryNegate(ExpressionSyntax expression)
        {
            Contract.Requires(expression != null);

            expression = expression.UnwrapParentheses();

            var binaryExpression = expression as BinaryExpressionSyntax;
            if (binaryExpression != null)
            {
                SyntaxKind negatedExpressionKind = NegateOperator(binaryExpression.Kind);

                if (negatedExpressionKind != SyntaxKind.None)
                {
                    BinaryExpressionSyntax negatedExpression =
                        Syntax.BinaryExpression(negatedExpressionKind, binaryExpression.Left, binaryExpression.Right);
                    return negatedExpression.FixBinaryExpressionWhitespace();
                }
            }
            else if (expression.Kind == SyntaxKind.LogicalNotExpression)
            {
                expression = (expression.ChildNodes().ElementAt(0) as ExpressionSyntax).UnwrapParentheses();
                return expression;
            }
            else if (expression is IdentifierNameSyntax || expression is InvocationExpressionSyntax)
            {
                return Syntax.ParseExpression("!" + expression.ToFullString());
            }

            return null;
        }

        /// <summary>
        /// Tests if an expression represents a negation. A negation is of the form !(..) or not(..).
        /// </summary>
        /// <param name="expression">the expression</param>
        /// <returns>true if the expression represents a negation</returns>
        private static bool IsNegation(this ExpressionSyntax expression)
        {
            Contract.Requires(expression != null);
            Contract.Ensures(Contract.Result<bool>().Implies(expression.Kind == SyntaxKind.LogicalNotExpression || expression is InvocationExpressionSyntax));

            if(expression.Kind == SyntaxKind.LogicalNotExpression)
            {
                return true;
            }

            if(expression is InvocationExpressionSyntax)
            {
                var invocation = expression as InvocationExpressionSyntax;
                if(invocation.Expression.ToString().Equals("not"))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the expression being negated in a negation expression.
        /// </summary>
        /// <param name="negation">the negation expression</param>
        /// <returns>the term being negated in the negation</returns>
        private static ExpressionSyntax GetNegationTerm(this ExpressionSyntax negation)
        {
            Contract.Requires(negation.IsNegation());
            Contract.Requires(negation.Kind == SyntaxKind.LogicalNotExpression || negation is InvocationExpressionSyntax);
            Contract.Ensures(Contract.Result<ExpressionSyntax>() != null);

            if(negation.Kind == SyntaxKind.LogicalNotExpression)
            {
                return negation.ChildNodes().ElementAt(0) as ExpressionSyntax;
            }
            else
            {
                Contract.Assert(negation is InvocationExpressionSyntax);
                var invocation = negation as InvocationExpressionSyntax;
                Contract.Assert(invocation.ArgumentList.Arguments.Count() == 1);
                return invocation.ArgumentList.Arguments[0].Expression;
            }
        }

        public static SyntaxKind NegateOperator(SyntaxKind kind)
        {
            switch (kind)
            {
                // Negate tokens.
                case SyntaxKind.GreaterThanEqualsToken:
                    return SyntaxKind.LessThanToken;
                case SyntaxKind.GreaterThanToken:
                    return SyntaxKind.LessThanEqualsToken;
                case SyntaxKind.LessThanEqualsToken:
                    return SyntaxKind.GreaterThanToken;
                case SyntaxKind.LessThanToken:
                    return SyntaxKind.GreaterThanEqualsToken;
                case SyntaxKind.ExclamationEqualsToken:
                    return SyntaxKind.EqualsEqualsToken;
                case SyntaxKind.EqualsEqualsToken:
                    return SyntaxKind.ExclamationEqualsToken;

                // Negate expressions.
                case SyntaxKind.GreaterThanOrEqualExpression:
                    return SyntaxKind.LessThanExpression;
                case SyntaxKind.GreaterThanExpression:
                    return SyntaxKind.LessThanOrEqualExpression;
                case SyntaxKind.LessThanOrEqualExpression:
                    return SyntaxKind.GreaterThanExpression;
                case SyntaxKind.LessThanExpression:
                    return SyntaxKind.GreaterThanOrEqualExpression;
                case SyntaxKind.NotEqualsExpression:
                    return SyntaxKind.EqualsExpression;
                case SyntaxKind.EqualsExpression:
                    return SyntaxKind.NotEqualsExpression;
            }

            return SyntaxKind.None;
        }

        public static SyntaxKind ReverseOperator(SyntaxKind kind)
        {
            switch (kind)
            {
                // Reverse tokens.
                case SyntaxKind.GreaterThanEqualsToken:
                    return SyntaxKind.LessThanEqualsToken;
                case SyntaxKind.GreaterThanToken:
                    return SyntaxKind.LessThanToken;
                case SyntaxKind.LessThanEqualsToken:
                    return SyntaxKind.GreaterThanEqualsToken;
                case SyntaxKind.LessThanToken:
                    return SyntaxKind.GreaterThanToken;
                case SyntaxKind.ExclamationEqualsToken:
                    return SyntaxKind.ExclamationEqualsToken;
                case SyntaxKind.EqualsEqualsToken:
                    return SyntaxKind.EqualsEqualsToken;

                // Reverse expressions.
                case SyntaxKind.GreaterThanOrEqualExpression:
                    return SyntaxKind.LessThanOrEqualExpression;
                case SyntaxKind.GreaterThanExpression:
                    return SyntaxKind.LessThanExpression;
                case SyntaxKind.LessThanOrEqualExpression:
                    return SyntaxKind.GreaterThanOrEqualExpression;
                case SyntaxKind.LessThanExpression:
                    return SyntaxKind.GreaterThanExpression;
                case SyntaxKind.NotEqualsExpression:
                    return SyntaxKind.NotEqualsExpression;
                case SyntaxKind.EqualsExpression:
                    return SyntaxKind.EqualsExpression;
            }

            return SyntaxKind.None;
        }
    }
}

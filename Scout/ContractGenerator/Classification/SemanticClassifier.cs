﻿using System;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Linq;
using Roslyn.Compilers.CSharp;

namespace ContractGenerator.Classification
{
    public enum ImplicationType
    {
        Generated,
        Wild,
        NotAnImplication
    }

    public static class SemanticClassifier
    {
        private class ContractCategory
        {
            public string Name { get; private set; }
            public Func<ContractKind, ExpressionSyntax, bool> Rule { get; private set; }

            public ContractCategory(string name, Func<ExpressionSyntax, bool> rule)
            {
                this.Name = name;
                this.Rule = (k, e) => rule(e);
            }

            public ContractCategory(string name, Func<ContractKind, ExpressionSyntax, bool> rule)
            {
                this.Name = name;
                this.Rule = rule;
            }
        }

        private static readonly ContractCategory[] categories =
        {
            new ContractCategory("Nullness", IsNullnessCheck),
            new ContractCategory("Null/Blank", IsNonNullOrEmptyCheck),
            new ContractCategory("Non-Empty", c => IsSizeCheck(c) && IsPositiveCheck(c)),
            new ContractCategory("Lower/Upper Bound", c => IsBoundedCheck(c) && !IsSizeCheck(c)),
            new ContractCategory("Indicator", IsIndicator),
            new ContractCategory("Frame Condition", IsFrameCondition),
            new ContractCategory("Return Value", c => IsBoolResult(c) && !IsGetterSetter(c)),
            new ContractCategory("Bounds Check", IsBoundsCheck),
            new ContractCategory("Constant", c => IsConstantCheck(c)), //&& !IsSizeCheck(c)),
            new ContractCategory("Implication", IsImplication),
            new ContractCategory("Getter/Setter", (k,c) => k.HasFlag(ContractKind.Ensures) && IsGetterSetter(c)),
            new ContractCategory("State Update", IsStateUpdate),
            new ContractCategory("Membership", ContainsCheck),
            new ContractCategory("Expr. Comparison", c => IsExprComparison(c) || IsComparisonCheck(c) || IsSpecialCaseExprComparison(c))
        };


        /// <summary>
        /// Classifies a contract by its semantic type.
        /// </summary>
        /// <param name="contract">the contract to classify</param>
        /// <returns>the category of classification, or the empty string if the contract could not be classified</returns>
        public static string Classify(ProgramContract contract)
        {
            Contract.Requires(contract != null);
            return Classify(contract.Implication.Consequent, contract.Kind);
        }

        public static string Classify(string contract, ContractKind contractKind = ContractKind.Requires)
        {
            Contract.Requires(!string.IsNullOrEmpty(contract));

            var expression = Syntax.ParseExpression(contract);

            if(expression is InvocationExpressionSyntax)
            {
                var invocation = expression as InvocationExpressionSyntax;

                if(invocation.Expression.ToString().Equals("Contract.ForAll"))
                {
                    if (invocation.ArgumentList.Arguments.Count == 2)
                    {
                        expression = (invocation.ArgumentList.Arguments[1].Expression as SimpleLambdaExpressionSyntax).Body as ExpressionSyntax;
                    }
                }
            }

            foreach (ContractCategory category in categories)
            {
                if (category.Rule(contractKind, expression))
                {
                    if (category.Name.Equals("Implication"))
                    {
                        // Classify implications by the classification of the consequent.
                        return Classify(new Implication(expression).Consequent, contractKind);
                    }
                    else
                    {
                        return category.Name;
                    }
                }
            }
            return "Uncategorized";
        }

        public static ImplicationType GetImplicationType(ExpressionSyntax expr)
        {
            ImplicationType type;
            IsImplication(expr, out type);
            return type;
        }

        /// <summary>
        /// Returns <tt>true</tt> if <paramref name="expr"/> encodes an implication; currently that the expression
        /// is logical-or expression.
        /// </summary>
        private static bool IsImplication(ExpressionSyntax expr, out ImplicationType type)
        {
            if (expr.ToFullString().Contains(".Implies"))
            {
                type = ImplicationType.Generated;
                return true;
            }
            else if (expr is BinaryExpressionSyntax && expr.Kind == SyntaxKind.LogicalOrExpression)
            {
                type = ImplicationType.Wild;
                return true;
            }

            type = ImplicationType.NotAnImplication;
            return false;
        }

        public static bool IsImplication(ExpressionSyntax expr)
        {
            ImplicationType type;
            return IsImplication(expr, out type);
        }

        public static bool IsIndicatorMember(string ident)
        {
            return new[] { "Is", "Can", "Has" }.Any(prefix => ident.StartsWith(prefix) && ident.Length > prefix.Length && char.IsUpper(ident[prefix.Length]));
        }

        public static bool IsIndicator(ExpressionSyntax expr)
        {
            if (expr is IdentifierNameSyntax)
            {
                // If we just have a lone boolean, consider it to be an indicator variable regardless of the name
                // var ident = ((IdentifierNameSyntax)expr).ToString();
                return true;
            }
            else if (expr is MemberAccessExpressionSyntax)
            {
                // If we just have a lone boolean, consider it to be an indicator variable regardless of the name
                // var ident = ((MemberAccessExpressionSyntax)expr).Name.ToString();
                return true;
            }
            else if (expr is InvocationExpressionSyntax)
            {
                var call = (InvocationExpressionSyntax)expr;

                // Check for instance methods.
                if (call.ArgumentList.Arguments.Count == 0)
                {
                    return IsIndicator(call.Expression);
                }
                else if (call.ArgumentList.Arguments.Count == 1)
                {
                    if (call.Expression is MemberAccessExpressionSyntax)
                    {
                        // Check that the member owner appears to be a static class
                        // XXX: how can we prevent this from catching too much?
                        return char.IsUpper(((MemberAccessExpressionSyntax)call.Expression).Expression.ToString()[0]);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else if (expr is PrefixUnaryExpressionSyntax && expr.Kind == SyntaxKind.LogicalNotExpression)
            {
                return IsIndicator(((PrefixUnaryExpressionSyntax)expr).Operand);
            }
            else
            {
                return false;
            }
        }

        public static bool IsContractResult(ExpressionSyntax expr)
        {
            return expr is InvocationExpressionSyntax && ((InvocationExpressionSyntax)expr).Expression.ToString().StartsWith("Contract.Result<");
        }

        public static bool IsGetterSetter(ExpressionSyntax expr)
        {
            Predicate<ExpressionSyntax> chk = e => e is MemberAccessExpressionSyntax || e is IdentifierNameSyntax ||
                IsContractResult(e) || e.Kind == SyntaxKind.ThisExpression;

            if (expr is BinaryExpressionSyntax && expr.Kind == SyntaxKind.EqualsExpression)
            {
                var b = (BinaryExpressionSyntax)expr;
                return chk(b.Left) && chk(b.Right);
            }
            else if (expr is InvocationExpressionSyntax)
            {
                var i = (InvocationExpressionSyntax)expr;

                // Handle ReferenceEquals(Contract.Result<>, x) case
                if (i.Expression.ToString().Equals("ReferenceEquals"))
                {
                    return IsContractResult(i.ArgumentList.Arguments[0].Expression) || IsContractResult(i.ArgumentList.Arguments[1].Expression);
                }

                if (i.Expression is MemberAccessExpressionSyntax)
                {
                    var m = (MemberAccessExpressionSyntax)i.Expression;
                    return m.Name.ToString().Equals("Equals") && chk(m.Expression) && chk(i.ArgumentList.Arguments[0].Expression);
                }

                return false;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns <c>true</c> if <paramref name="oldValue"/> is the pre-state expression for <paramref name="expr"/>.
        /// </summary>
        public static bool IsOldValue(ExpressionSyntax oldValue, ExpressionSyntax expr)
        {
            if (oldValue is InvocationExpressionSyntax)
            {
                var i = (InvocationExpressionSyntax)oldValue;
                return i.Expression.ToString().Equals("Contract.OldValue") && i.ArgumentList.Arguments[0].Expression.IsEquivalentTo(expr);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns <tt>true</tt> iff <paramref name="expr"/> relates an expression to its pre-state.
        /// </summary>
        public static bool IsStateUpdate(ExpressionSyntax expr)
        {
            if (expr is BinaryExpressionSyntax)
            {
                var b = (BinaryExpressionSyntax)expr;

                var containsOld = from e in b.Right.DescendantNodes().OfType<ExpressionSyntax>()
                                  where IsOldValue(e, b.Left)
                                  select e;

                // RHS can be LHS's pre-state as long as it's not an equality.
                return (!IsOldValue(b.Right, b.Left) || expr.Kind != SyntaxKind.EqualsExpression) && containsOld.Any();
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns <tt>true</tt> iff <paramref name="expr"/> states that an expression has not been modified.
        /// </summary>
        public static bool IsFrameCondition(ExpressionSyntax expr)
        {
            if (expr is BinaryExpressionSyntax && expr.Kind == SyntaxKind.EqualsExpression)
            {
                var b = (BinaryExpressionSyntax)expr;
                return IsOldValue(b.Left, b.Right) || IsOldValue(b.Right, b.Left);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns <tt>true</tt> iff <paramref name="expr"/> is a binary expression over two non-literals.
        /// </summary>
        public static bool IsExprComparison(ExpressionSyntax expr)
        {
            if (expr is BinaryExpressionSyntax && expr.Kind != SyntaxKind.LogicalAndExpression)
            {
                var b = (BinaryExpressionSyntax)expr;
                return !(IsFrameCondition(expr) || IsStateUpdate(expr) || IsImplication(expr)) &&
                       !(b.Right is LiteralExpressionSyntax || b.Left is LiteralExpressionSyntax);
            }
            else
            {
                return false;
            }
        }

        public static bool IsNonEmptyCheck(ExpressionSyntax expr)
        {
            if (expr is InvocationExpressionSyntax)
            {
                var i = (InvocationExpressionSyntax)expr;
                if (i.Expression is MemberAccessExpressionSyntax)
                {
                    var m = (MemberAccessExpressionSyntax)i.Expression;
                    return m.Name.ToString().Equals("Any");
                }

                return false;
            }
            else
            {
                return false;
            }
        }

        public static bool IsSizeCheck(ExpressionSyntax expr)
        {
            if (expr is BinaryExpressionSyntax)
            {
                var b = (BinaryExpressionSyntax)expr;
                return IsCollectionSizeExpr(b.Left) || IsCollectionSizeExpr(b.Right);
            }
            else
            {
                return false;
            }
        }

        public static bool IsGreaterThanCheck(ExpressionSyntax expr, int val, bool strict)
        {
            var gtKind = strict ? SyntaxKind.GreaterThanExpression : SyntaxKind.GreaterThanOrEqualExpression;
            var ltKind = strict ? SyntaxKind.LessThanExpression : SyntaxKind.LessThanOrEqualExpression;


            if (expr is BinaryExpressionSyntax)
            {
                var b = (BinaryExpressionSyntax)expr;

                if (b.Kind == gtKind)
                {
                    return b.Right.ToString().Equals(val.ToString(CultureInfo.InvariantCulture));
                }
                else if (b.Kind == ltKind)
                {
                    return b.Left.ToString().Equals(val.ToString(CultureInfo.InvariantCulture));
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool IsBoundedCheck(ExpressionSyntax expr)
        {
            if (expr is BinaryExpressionSyntax)
            {
                var b = (BinaryExpressionSyntax)expr;

                //Predicate<ExpressionSyntax> isRegular = e => !(e is InvocationExpressionSyntax) || IsContractResult(e);

                switch (expr.Kind)
                {
                    case SyntaxKind.GreaterThanOrEqualExpression:
                    case SyntaxKind.GreaterThanExpression:
                    case SyntaxKind.LessThanExpression:
                    case SyntaxKind.LessThanOrEqualExpression:
                        return (b.Left is LiteralExpressionSyntax || b.Right is LiteralExpressionSyntax); //&&
                               //(isRegular(b.Left) && isRegular(b.Right));

                    default:
                        return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool IsPositiveCheck(ExpressionSyntax expr)
        {
            return IsGreaterThanCheck(expr, 0, true) || IsGreaterThanCheck(expr, 1, false);
        }

        public static bool IsCollectionSizeExpr(ExpressionSyntax expr)
        {
            return expr is MemberAccessExpressionSyntax &&
                    new[] { "Count", "Length", "VertexCount" }.Any(m => ((MemberAccessExpressionSyntax)expr).Name.ToString().Equals(m));
        }

        public static bool IsBoundsCheck(ExpressionSyntax expr)
        {
            if (expr is BinaryExpressionSyntax && expr.Kind == SyntaxKind.LessThanExpression)
            {
                var b = (BinaryExpressionSyntax)expr;
                return IsCollectionSizeExpr(b.Right);
            }
            else if (expr is BinaryExpressionSyntax && expr.Kind == SyntaxKind.LogicalAndExpression)
            {
                var b = (BinaryExpressionSyntax)expr;
                return IsGreaterThanCheck(b.Left, 0, false) && IsBoundsCheck(b.Right);
            }
            else
            {
                return false;
            }
        }

        public static bool IsNullnessCheck(ExpressionSyntax expr)
        {
            if (expr is BinaryExpressionSyntax && (expr.Kind == SyntaxKind.NotEqualsExpression || expr.Kind == SyntaxKind.EqualsExpression))
            {
                var cond = (BinaryExpressionSyntax)expr;
                return cond.Left.Kind == SyntaxKind.NullLiteralExpression || cond.Right.Kind == SyntaxKind.NullLiteralExpression;
            }
            else if (expr is InvocationExpressionSyntax)
            {
                // include special expressions from the subject programs
                var helperMethods = new string[]
                {
                  "EnumerableContract.ElementsNotNull",
                  "cce.NonNull",
                  "cce.NonNullElements",
                  "cce.NonNullDictionaryAndValues",
                };

                bool isNullHelper = helperMethods.Any(m => ((InvocationExpressionSyntax)expr).Expression.ToString().Equals(m));

                if (isNullHelper)
                {
                    return true;
                }
                else
                {
                    string expression = expr.ToFullString();
                    return expression.Contains("Contract.ForAll") && expression.Contains(" != null");
                }
            }
            else if (expr is PrefixUnaryExpressionSyntax && expr.Kind == SyntaxKind.LogicalNotExpression)
            {
                // Check for !ReferenceEquals

                var inner = ((PrefixUnaryExpressionSyntax)expr).Operand;
                if (inner is InvocationExpressionSyntax)
                {
                    var method = (InvocationExpressionSyntax)inner;
                    Predicate<int> isNull = x => method.ArgumentList.Arguments[x].ToString().Equals("null");
                    return SimpleMethodName(method).Equals("ReferenceEquals") && (isNull(0) || isNull(1));
                }
                return false;
            }
            else
            {
                return false;
            }
        }

        public static string SimpleMethodName(InvocationExpressionSyntax expr)
        {
            if (expr.Expression is IdentifierNameSyntax)
            {
                return expr.Expression.ToString();
            }
            else if (expr.Expression is MemberAccessExpressionSyntax)
            {
                return ((MemberAccessExpressionSyntax)expr.Expression).Name.ToString();
            }
            else if (expr.Expression is GenericNameSyntax)
            {
                return expr.Expression.ToString();
            }
            else
            {
                return expr.Expression.ToString();
            }
        }

        public static bool ContainsCheck(ExpressionSyntax expr)
        {
            if (expr is InvocationExpressionSyntax)
            {
                var i = (InvocationExpressionSyntax)expr;

                if (i.ArgumentList.Arguments.Count == 1)
                {
                    // include some special methods from QuickGraph
                    return new[] { "Contains", "ContainsVertex", "ContainsEdge" }.Any(n => n.Equals(SimpleMethodName(i)));
                }
                else
                {
                    return false;
                }
            }
            else if (expr is PrefixUnaryExpressionSyntax && expr.Kind == SyntaxKind.LogicalNotExpression)
            {
                return ContainsCheck(((PrefixUnaryExpressionSyntax)expr).Operand);
            }
            else
            {
                return false;
            }
        }

        public static bool IsNonNullOrEmptyCheck(ExpressionSyntax expr)
        {
            if (expr is InvocationExpressionSyntax)
            {
                var method = ((InvocationExpressionSyntax)expr).Expression;
                return method.ToString().Equals("string.IsNullOrEmpty") ||
                       method.ToString().Equals("string.IsNullOrWhiteSpace") ||
                       method.ToString().Equals("String.IsNullOrEmpty") ||
                       method.ToString().Equals("String.IsNullOrWhiteSpace");

            }
            else if (expr is PrefixUnaryExpressionSyntax && expr.Kind == SyntaxKind.LogicalNotExpression)
            {
                return IsNonNullOrEmptyCheck(((PrefixUnaryExpressionSyntax)expr).Operand);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns a predicate that detects <paramref name="predicate"/> in <tt>Enumerable.All</tt> and <tt>Contract.ForAll</tt> methods.
        /// </summary>
        public static Func<ContractKind, ExpressionSyntax, bool> CreateEnumerablePredicate(Func<ContractKind, ExpressionSyntax, bool> predicate)
        {
            return (kind, expr) =>
            {
                if (expr is InvocationExpressionSyntax)
                {
                    var i = (InvocationExpressionSyntax)expr;

                    if (new[] { "Enumerable.All", "Contract.Forall" }.Any(n => n.Equals(i.Expression.ToString())))
                    {
                        var d = i.ArgumentList.Arguments[1].Expression;
                        if (d is SimpleLambdaExpressionSyntax)
                        {
                            var l = (SimpleLambdaExpressionSyntax)d;
                            return l.Body is ExpressionSyntax && predicate(kind, (ExpressionSyntax)l.Body);
                        }

                        return false;
                    }
                }
                return false;
            };
        }

        public static bool IsBoolResult(ExpressionSyntax expr)
        {
            if (expr is BinaryExpressionSyntax && expr.Kind == SyntaxKind.EqualsExpression)
            {
                var b = (BinaryExpressionSyntax)expr;

                if (b.Left is InvocationExpressionSyntax)
                {
                    var ie = (InvocationExpressionSyntax)b.Left;
                    return ie.Expression.ToString() == "Contract.Result<bool>" || ie.Expression.ToString() == "Contract.Result<int>";
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        private static bool IsStringEqualityWithLiteral(ExpressionSyntax expr)
        {
            if(expr is InvocationExpressionSyntax)
            {
                var invocation = expr as InvocationExpressionSyntax;
                var memberAccess = invocation.ChildNodes().ElementAt(0) as MemberAccessExpressionSyntax;
                var arguments = invocation.ArgumentList.Arguments;

                if (memberAccess.Name.ToString().Equals("Equals") && 
                    arguments.Count == 1 &&
                    arguments[0].Expression is LiteralExpressionSyntax)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsConstantCheck(ExpressionSyntax expr)
        {
            if(IsStringEqualityWithLiteral(expr))
            {
                return true;
            }
            if (expr is InvocationExpressionSyntax)
            {
                return ((InvocationExpressionSyntax)expr).Expression.ToString().Equals("Contract.Result<bool>");
            }
            else if (expr is PrefixUnaryExpressionSyntax && expr.Kind == SyntaxKind.LogicalNotExpression)
            {
                return IsConstantCheck(((PrefixUnaryExpressionSyntax)expr).Operand);
            }
            else if (expr is BinaryExpressionSyntax && expr.Kind == SyntaxKind.EqualsExpression)
            {
                var b = (BinaryExpressionSyntax)expr;
                return b.Right is LiteralExpressionSyntax || b.Left is LiteralExpressionSyntax;
            }
            else
            {
                return false;
            }
        }

        public static bool IsComparisonCheck(ExpressionSyntax expr)
        {
            if (expr.Kind == SyntaxKind.LessThanExpression || expr.Kind == SyntaxKind.LessThanOrEqualExpression)
            {
                var b = (BinaryExpressionSyntax)expr;
                return b.Left is InvocationExpressionSyntax && (SimpleMethodName((InvocationExpressionSyntax)b.Left).Equals("Compare"))
                       && b.Right is LiteralExpressionSyntax;
            }
            else if (expr is PrefixUnaryExpressionSyntax && expr.Kind == SyntaxKind.LogicalNotExpression)
            {
                var n = (PrefixUnaryExpressionSyntax)expr;

                return n.Operand is InvocationExpressionSyntax && (SimpleMethodName((InvocationExpressionSyntax)n.Operand).Equals("Equals"));

            }
            else
            {
                return false;
            }
        }

        public static bool IsSpecialCaseExprComparison(ExpressionSyntax expr)
        {
            return expr.ToFullString().Contains(".OneOf(");
        }
    }
}

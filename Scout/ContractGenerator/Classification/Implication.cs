﻿using System.Diagnostics.Contracts;
using System.Text.RegularExpressions;
using Roslyn.Compilers.CSharp;
using ContractGenerator.Contracts;
using System.Diagnostics;

namespace ContractGenerator.Classification
{
    public class Implication
    {
        [ContractInvariantMethod]
        private void Invariants()
        {
            Contract.Invariant(!string.IsNullOrEmpty(Antecedent));
            Contract.Invariant(AntecedentExpression != null);
            Contract.Invariant(!string.IsNullOrEmpty(Consequent));
            Contract.Invariant(ConsequentExpression != null);
            Contract.Invariant(!string.IsNullOrEmpty(AsContract));
            Contract.Invariant((!DaikonComponentsUnset).Implies(!DaikonAntecedent.IsEmpty() && !DaikonConsequent.IsEmpty()));
            Contract.Invariant(DaikonComponentsUnset.Implies(DaikonAntecedent.IsEmpty() || DaikonConsequent.IsEmpty()));
        }

        public static string TrueAntecedent = "true";

        private static string impliesTemplate = "({0}).Implies({1})";

        private static string daikonImpliesTemplate = "{0} ==> {1}";

        /// <summary>
        /// The implication antecedent.
        /// If the consequent is always true, the antecedent is "true".
        /// </summary>
        public string Antecedent { get; private set; }

        /// <summary>
        /// The Roslyn expression representing the antecedent.
        /// </summary>
        public ExpressionSyntax AntecedentExpression { get; private set; }

        /// <summary>
        /// The implication consequent.
        /// </summary>
        public string Consequent { get; private set; }

        /// <summary>
        /// The Roslyn expression representing the consequent.
        /// </summary>
        public ExpressionSyntax ConsequentExpression { get; private set; }

        /// <summary>
        /// Cached contract.
        /// </summary>
        private string contract;

        /// <summary>
        /// This implication as a contract.
        /// </summary>
        public string AsContract
        {
            get
            {
                if (IsUniversal)
                {
                    return Consequent;
                }
                else
                {
                    if (contract == null)
                    {
                        contract = string.Format(impliesTemplate, Antecedent, Consequent);
                    }

                    return contract;
                }
            }
        }

        /// <summary>
        /// The daikon antecedent.
        /// May be empty.
        /// </summary>
        public string DaikonAntecedent { get; private set; }

        /// <summary>
        /// The daikon consequent.
        /// May be empty.
        /// </summary>
        public string DaikonConsequent { get; private set; }

        /// <summary>
        /// Cached daikon contract.
        /// </summary>
        private string daikonContract;

        /// <summary>
        /// This implication as a daikon contract.
        /// </summary>
        public string AsDaikonContract
        {
            get
            {
                if (DaikonComponentsUnset)
                {
                    return string.Empty;
                }
                else if (IsUniversal)
                {
                    return DaikonConsequent;
                }
                else
                {
                    if (daikonContract == null)
                    {
                        daikonContract = string.Format(daikonImpliesTemplate, DaikonAntecedent, DaikonConsequent);
                    }

                    return daikonContract;
                }
            }
        }

        /// <summary>
        /// True if the DaikonAntecedent or DaikonConsequent are not set (they are empty).
        /// This indicates there was an error processing the daikon contract.
        /// </summary>
        public bool DaikonComponentsUnset
        {
            get
            {
                return DaikonAntecedent.IsEmpty() || DaikonConsequent.IsEmpty(); 
            }
        }

        /// <summary>
        /// <c>True</c> if the antecedent is always true.
        /// </summary>
        public bool IsUniversal
        {
            get
            {
                return Antecedent.Equals(TrueAntecedent);
            }
        }

        /// <summary>
        /// Tests if an expression is a generated implication in c-sharp format.
        /// An example match:
        /// (this.x != null).Implies(() => this.y > 3)
        /// </summary>
        private static readonly Regex GeneratedImplies = new Regex("\\((.*)\\).Implies\\((.*)\\)");
        
        /// <summary>
        /// Tests if an expression is a generated implication in daikon format.
        /// An example match:
        /// (this.x != null) ==> (this.y > 3)
        /// </summary>
        private static readonly Regex GeneratedDaikonImplies = new Regex("(.*)\\s+==>\\s+(.*)");

        /// <summary>
        /// Creates a new implication with both c-sharp and daikon components.
        /// </summary>
        /// <param name="contract">the c-sharp contract</param>
        /// <param name="daikon">the daikon contract</param>
        public Implication(string contract, string daikon)
        {
            Contract.Requires(!string.IsNullOrEmpty(contract));
            Contract.Requires(!string.IsNullOrEmpty(daikon));
            var contractExpression = Syntax.ParseExpression(contract);
            SetComponents(contractExpression);
            SetDaikonComponents(daikon);
        }

        /// <summary>
        /// Creates a new implication with with only c-sharp components (no daikon components).
        /// </summary>
        /// <param name="contract">the c-sharp contract</param>
        public Implication(string contract)
        {
            Contract.Requires(!string.IsNullOrEmpty(contract));
            Contract.Ensures(DaikonComponentsUnset);
            var contractExpression = Syntax.ParseExpression(contract);
            SetComponents(contractExpression);
            DaikonAntecedent = string.Empty;
            DaikonConsequent = string.Empty;
        }

        /// <summary>
        /// Creates a new implication with with only c-sharp components (no daikon components).
        /// </summary>
        /// <param name="contractExpression">the c-sharp Roslyn expression</param>
        public Implication(ExpressionSyntax contractExpression)
        {
            Contract.Requires(contractExpression != null);
            Contract.Ensures(DaikonComponentsUnset);
            SetComponents(contractExpression);
            DaikonAntecedent = string.Empty;
            DaikonConsequent = string.Empty;
        }

        /// <summary>
        /// Sets Antecedent and Consequent for the given c# contract expression.
        /// </summary>
        /// <param name="expression"></param>
        private void SetComponents(ExpressionSyntax expression)
        {
            Contract.Requires(expression != null);

            this.contract = null;
            this.daikonContract = null;

            string antecedent;
            string consequent;
            ImplicationType type = SemanticClassifier.GetImplicationType(expression);

            if (type == ImplicationType.NotAnImplication)
            {
                antecedent = TrueAntecedent;
                consequent = expression.ToFullString();
            }
            else if (type == ImplicationType.Generated)
            {
                Match match = GeneratedImplies.Match(expression.ToFullString());
                antecedent = match.Groups[1].Value.Trim();
                consequent = match.Groups[2].Value.Trim();
            }
            else
            {
                BinaryExpressionSyntax binaryExpression = (BinaryExpressionSyntax)expression;
                ExpressionSyntax left = Negator.TryNegate(binaryExpression.Left);
                ExpressionSyntax right = binaryExpression.Right;
                antecedent = left.ToFullString().Trim();
                consequent = right.ToFullString().Trim();
            }

            var antecedentExpression = Syntax.ParseExpression(antecedent).Simplify();
            this.AntecedentExpression = antecedentExpression;
            this.Antecedent = antecedentExpression.ToFullString().Trim().Remove("this.");

            var consequentExpression = Syntax.ParseExpression(consequent).Simplify();
            this.ConsequentExpression = consequentExpression;
            this.Consequent = consequentExpression.ToFullString().Trim().Remove("this.");
            // Note: daikon splitter conditions remove "this", so we also remove it.
        }

        /// <summary>
        /// Sets DaikonAntecedent and DaikonConsequent given the daikon contract string.
        /// This method requires Antecedent and Consequent to already be set.
        /// 
        /// If the daikon components cannot be set, DaikonAntecedent and DaikonConsequent will be set to empty.
        /// </summary>
        /// <param name="daikon">the daikon contract.</param>
        private void SetDaikonComponents(string daikon)
        {
            Contract.Requires(!string.IsNullOrEmpty(Antecedent));
            Contract.Requires(!string.IsNullOrEmpty(Consequent));
            Contract.Requires(!string.IsNullOrEmpty(daikon));

            string antecedent = string.Empty;
            string consequent = string.Empty;

            if (IsUniversal)
            {
                antecedent = TrueAntecedent;
                consequent = daikon;
            }
            else
            {
                var generated = GeneratedDaikonImplies.Match(daikon);
                if (generated.Success)
                {
                    antecedent = generated.Groups[1].Value;
                    consequent = generated.Groups[2].Value;
                }
            }

            antecedent = antecedent.Replace("return", "_return").Replace("not", "!").Remove("this.");
            consequent = consequent.Replace("return", "_return").Replace("not", "!").Remove("this.").Replace("<==>", "==");

            var antecedentExpression = Syntax.ParseExpression(antecedent);
            var consequentExpression = Syntax.ParseExpression(consequent);

            // Try to process the Daikon antecedent as c#.
            if (antecedent.Equals(antecedentExpression.ToFullString()))
            {
                DaikonAntecedent = antecedentExpression.Simplify().ToFullString();
            }
            else
            {
                DaikonAntecedent = antecedent;
            }

            // Try to process the Daikon consequent as c#.
            if (consequent.Equals(consequentExpression.ToFullString()))
            {
                DaikonConsequent = consequentExpression.Simplify().ToFullString();
            }
            else
            {
                DaikonConsequent = consequent;
            }

            DaikonAntecedent = DaikonAntecedent.Replace("_return", "return").Trim();
            DaikonConsequent = DaikonConsequent.Replace("_return", "return").Replace("  ", " ").Trim();
        }

        /// <summary>
        /// Tests if another implication is the opposite of this.
        /// Two implications are opposites if they have the same consequent but their
        /// antecedents are negations of one another.
        /// </summary>
        /// <param name="other">the other implication</param>
        /// <returns>true if the other implication is opposite of this</returns>
        public bool IsOpposite(Implication other)
        {
            Contract.Requires(other != null);

            if (!IsUniversal)
            {
                var otherAntecedentNegate = Negator.TryNegate(other.AntecedentExpression);

                if (otherAntecedentNegate != null &&
                    otherAntecedentNegate.ToFullString().Equals(Antecedent) &&
                    other.Consequent.Equals(Consequent))
                {
                    return true;
                }
            }

            return false;
        }
    }
}

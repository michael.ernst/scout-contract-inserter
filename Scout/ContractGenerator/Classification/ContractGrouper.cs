﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace ContractGenerator.Classification
{
    // Data structure representing one level of contract grouping.
    using OneGrouping = Dictionary<ContractKind, SortedDictionary<string, List<ProgramContract>>>;
    // Data structure representing two levels of contract grouping.
    using TwoGroupings = Dictionary<ContractKind, Dictionary<string, SortedDictionary<string, List<ProgramContract>>>>;

    /// <summary>
    /// Groups lists of contracts by various properties. 
    /// </summary>
    public static class ContractGrouper
    {
        #region Public Methods

        /// <summary>
        /// Groups the contracts of <paramref name="element"/> by kind, then by variable.
        /// </summary>
        /// <param name="element">the element whose contracts to group</param>
        /// <returns>a mapping from kind to variable to lists of contracts</returns>
        [Pure]
        public static OneGrouping GroupByVariable(ProgramElement element)
        {
            Contract.Requires(element != null);
            Contract.Ensures(Contract.Result<OneGrouping>() != null);

            // Maps contract kind -> variable names -> list of contracts
            var result = new OneGrouping();
            // Maps contract kind -> list of contracts
            var groupByKind = GroupByKind(element.Contracts);

            // Group each kind of contract by their variables.
            result[ContractKind.Requires] = GroupByVariable(groupByKind[ContractKind.Requires], element.Parameters);
            result[ContractKind.Ensures] = GroupByVariable(groupByKind[ContractKind.Ensures], element.Parameters);
            result[ContractKind.Invariant] = GroupByVariable(groupByKind[ContractKind.Invariant], element.Parameters);

            return result;
        }

        /// <summary>
        /// Groups the contracts of <paramref name="element"/> by kind, then by semantic type.
        /// </summary>
        /// <param name="element">the element whose contracts to group</param>
        /// <returns>a mapping from kind to semantic type to lists of contracts</returns>
        [Pure]
        public static OneGrouping GroupBySemanticType(ProgramElement element)
        {
            Contract.Requires(element != null);
            Contract.Ensures(Contract.Result<OneGrouping>() != null);

            // Maps contract kind -> variable names -> list of contracts
            var result = new OneGrouping();
            // Maps contract kind -> list of contracts
            var groupByKind = GroupByKind(element.Contracts);

            // Group each kind of contract by their variables.
            result[ContractKind.Requires] = GroupBySemanticType(groupByKind[ContractKind.Requires]);
            result[ContractKind.Ensures] = GroupBySemanticType(groupByKind[ContractKind.Ensures]);
            result[ContractKind.Invariant] = GroupBySemanticType(groupByKind[ContractKind.Invariant]);

            return result;
        }

        [Pure]
        public static TwoGroupings GroupByAntecedentThenVariable(ProgramElement element)
        {
            return GroupByAntecedent(element, true);
        }

        [Pure]
        public static TwoGroupings GroupByAntecedentThenSemanticType(ProgramElement element)
        {
            return GroupByAntecedent(element, false);   
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Groups <paramref name="contracts"/> by the groupings defined by <paramref name="groups"/>.
        /// </summary>
        /// <typeparam name="T">the grouping parameter</typeparam>
        /// <param name="contracts">the contracts to group</param>
        /// <param name="groups">maps a contract to a set of groups that contracts belongs to</param>
        /// <returns>a mapping from <typeparamref name="T"/> to lists of contracts</returns>
        [Pure]
        private static Dictionary<T, List<ProgramContract>> Group<T>(IEnumerable<ProgramContract> contracts, Func<ProgramContract, HashSet<T>> groups)
        {
            Contract.Requires(contracts != null);
            Contract.Requires(groups != null);
            Contract.Ensures(Contract.Result<Dictionary<T, List<ProgramContract>>>() != null);

            var dict = new Dictionary<T, List<ProgramContract>>();

            foreach (ProgramContract contract in contracts)
            {
                foreach (var group in groups(contract))
                {
                    if (!dict.ContainsKey(group)) dict[group] = new List<ProgramContract>();
                    dict[group].Add(contract);
                }
            }

            return dict;
        }

        [Pure]
        private static Dictionary<T, List<ProgramContract>> Group<T>(IEnumerable<ProgramContract> contracts, Func<ProgramContract, T> group)
        {
            Contract.Requires(contracts != null);
            Contract.Requires(group != null);
            Contract.Ensures(Contract.Result<Dictionary<T, List<ProgramContract>>>() != null);
            return Group(contracts, contract => new HashSet<T> {@group(contract)});
        }

        /// <summary>
        /// Groups the contracts of <paramref name="contracts"/> by kind. 
        /// Every value of <see cref="ContractKind"/> has a non-null key in the resulting dictionary.
        /// </summary>
        /// <param name="contracts">the contracts to group</param>
        /// <returns>a mapping from kind to lists of contracts</returns>
        [Pure]
        private static Dictionary<ContractKind, List<ProgramContract>> GroupByKind(IEnumerable<ProgramContract> contracts)
        {
            Contract.Requires(contracts != null);
            Contract.Ensures(Contract.Result<Dictionary<ContractKind, List<ProgramContract>>>() != null);

            // Group contracts by their kind.
            var dict = Group(contracts, contract => contract.Kind);

            // Make sure a key exists for each contract kind.
            foreach (ContractKind kind in Enum.GetValues(typeof(ContractKind)))
            {
                if (!dict.ContainsKey(kind))
                {
                    dict[kind] = new List<ProgramContract>();
                }
            }

            return dict;
        }

        /// <summary>
        /// Groups a list of contracts by the variables they contain.
        /// </summary>
        /// <param name="contracts">the contracts to group</param>
        /// <param name="parameters">the parameters of the method the contracts belong to (required for sorting the variables)</param>
        /// <returns>a mapping from variable string to lists of contracts</returns>
        [Pure]
        private static SortedDictionary<string, List<ProgramContract>> GroupByVariable(IEnumerable<ProgramContract> contracts, IEnumerable<Parameter> parameters)
        {
            Contract.Requires(contracts != null);
            Contract.Requires(parameters != null);
            Contract.Ensures(Contract.Result<SortedDictionary<string, List<ProgramContract>>>() != null);

            var dict = Group(contracts, contract => new HashSet<string>(contract.GroupVariables));

            // The keys of dict are variables. 
            // Sort the variables such that the order is 'this', param1, param2, ..., paramN, Contract.Result.
            return new SortedDictionary<string, List<ProgramContract>>(dict, new VariableComparer(parameters));
        }

        /// <summary>
        /// Groups a list of contracts by their semantic types (Nullness, Indicator, Implication etc.)
        /// </summary>
        /// <param name="contracts">the contracts to group</param>
        /// <returns>a mapping from semantic type string to lists of contracts</returns>
        [Pure]
        private static SortedDictionary<string, List<ProgramContract>> GroupBySemanticType(IEnumerable<ProgramContract> contracts)
        {
            Contract.Requires(contracts != null);
            Contract.Ensures(Contract.Result<SortedDictionary<string, List<ProgramContract>>>() != null);

            var dict = Group(contracts, SemanticClassifier.Classify);
            return new SortedDictionary<string, List<ProgramContract>>(dict);
        }

        /// <summary>
        /// Groups a list of contracts by their implication antecedent. If a contract is not an implication (has no antecedent),
        /// then it is categorized into the 'Always' category.
        /// </summary>
        /// <param name="contracts">the contracts to group</param>
        /// <returns>a mapping from implication antecedent to lists of contracts</returns>
        [Pure]
        private static Dictionary<string, List<ProgramContract>> GroupByAntecedent(IEnumerable<ProgramContract> contracts)
        {
            Contract.Requires(contracts != null);
            var byAntecedent = Group(contracts, contract => contract.Implication.Antecedent);
            // Merge similar antecedents.
            // For example, there could be a key for "x > y" and "y < x". We want these to be grouped under the same node.
            Func<string, string, bool> similarButUnequalTest = (first, second) =>
                !first.Equals(second) && ExpressionComparison.AreEquivalentBinaryExpressions(first, second);
            var merged = byAntecedent.MergeKeys(similarButUnequalTest);
            // var mergedAndSorted = new SortedDictionary<string, List<ProgramContract>>(merged, new AntecedentComparer());
            // return mergedAndSorted;
            return merged;
        }

        /// <summary>
        /// Groups the contracts of <paramref name="element"/> by kind, then antecedent, then variable/semantic type.
        /// </summary>
        /// <param name="element">the element whose contracts to group</param>
        /// <param name="groupByVariable">if <c>true</c> second level grouping is group by variable, otherwise it is semantic type</param>
        /// <returns>a mapping from kind to antecedent to variable/semantic type to list of contracts</returns>
        [Pure]
        private static TwoGroupings GroupByAntecedent(ProgramElement element, bool groupByVariable)
        {
            var dict = new TwoGroupings();

            // 1: Group by kind.
            var byKind = GroupByKind(element.Contracts);

            foreach (ContractKind kind in Enum.GetValues(typeof(ContractKind)))
            {
                dict[kind] = new Dictionary<string, SortedDictionary<string, List<ProgramContract>>>();
             
                // 2: Group by antecedent.
                var byAntecedent = GroupByAntecedent(byKind[kind]);

                foreach (string antecedent in byAntecedent.Keys)
                {
                    // 3: Group by variable or semantic.
                    if (groupByVariable)
                    {
                        dict[kind][antecedent] = GroupByVariable(byAntecedent[antecedent], element.Parameters);
                    }
                    else
                    {
                        dict[kind][antecedent] = GroupBySemanticType(byAntecedent[antecedent]);
                    }
                }
 
            }

            return dict;
        }

        #endregion

        private class VariableComparer : IComparer<string>
        {
            private readonly List<string> words;

            public VariableComparer(IEnumerable<Parameter> parameters)
            {
                words = new List<string>();
                words.Add("this");
                foreach (var p in parameters)
                {
                    words.Add(p.Name);
                }
                words.Add("Contract.Result");
            }

            private static int wordCompare(string a, string b, string word)
            {
                bool aword = a.StartsWith(word);
                bool bword = b.StartsWith(word);
                if (aword && bword) return String.Compare(a, b, StringComparison.Ordinal);
                if (aword) return -1;
                if (bword) return 1;
                return Int32.MaxValue;
            }

            public int Compare(string a, string b)
            {
                foreach (var word in words)
                {
                    int comp = wordCompare(a, b, word);
                    if (comp != Int32.MaxValue)
                    {
                        return comp;
                    }
                }
                return String.Compare(a, b, StringComparison.Ordinal);
            }
        }

        private class AntecedentComparer : IComparer<string>
        {
            public AntecedentComparer()
            {
            }

            public int Compare(string a, string b)
            {
                if (a.Equals("true")) return 1;
                return String.Compare(a, b);
            }
        }
    }
}

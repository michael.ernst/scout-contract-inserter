﻿using Roslyn.Compilers.CSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractGenerator.Classification
{
    public static class ExpressionComparison
    {
        /// <summary>
        /// Returns true if <paramref name="first"/> is similar to <paramref name="second"/>.
        /// Two expressions are said to be similar if they are both binary expressions 
        /// with the same left and right expressions but a different comparison type.
        /// Implications with the same antecedent and similar binary expressions as consequents
        /// are also considered similar.
        /// For example, a > b is similar to a >= b, but not similar to a > c.
        /// </summary>
        /// <param name="first">the first c# expression</param>
        /// <param name="second">the second c# expression</param>
        /// <returns>True if the expressions are similar</returns>
        public static bool AreSimilar(string first, string second)
        {
            Contract.Requires(!string.IsNullOrEmpty(first));
            Contract.Requires(!string.IsNullOrEmpty(second));

            var firstImplication = new Implication(first);
            var secondImplication = new Implication(second);

            return AreSimilarImplications(firstImplication, secondImplication);
        }

        public static bool AreSimilarImplications(Implication first, Implication second)
        {
            if (first.Antecedent.Equals(second.Antecedent))
            {
                return AreSimilarBinaryExpressions(
                    firstExpression: Syntax.ParseExpression(first.Consequent),
                    secondExpression: Syntax.ParseExpression(second.Consequent));
            }

            return false;
        }

        /// <summary>
        /// Tests if two binary expressions are equivalent.
        /// For example, x > y is equivalent to y < x.
        /// </summary>
        /// <param name="first">the first expression</param>
        /// <param name="second">the second expression</param>
        /// <returns>true if the two binary expressions are equal</returns>
        public static bool AreEquivalentBinaryExpressions(string first, string second)
        {
            Contract.Requires(!string.IsNullOrEmpty(first));
            Contract.Requires(!string.IsNullOrEmpty(second));

            var firstExpression = Syntax.ParseExpression(first) as BinaryExpressionSyntax;
            var secondExpression = Syntax.ParseExpression(second) as BinaryExpressionSyntax;

            if (firstExpression == null || secondExpression == null)
            {
                return false;
            }

            return firstExpression.Left.IsEquivalentTo(secondExpression.Right) &&
                   firstExpression.Right.IsEquivalentTo(secondExpression.Left) &&
                   firstExpression.OperatorToken.Kind == Negator.ReverseOperator(secondExpression.OperatorToken.Kind);
        }

        private static bool AreSimilarBinaryExpressions(ExpressionSyntax firstExpression, ExpressionSyntax secondExpression)
        {
            if (firstExpression is BinaryExpressionSyntax && secondExpression is BinaryExpressionSyntax)
            {
                var firstBinary = (BinaryExpressionSyntax)firstExpression;
                var secondBinary = (BinaryExpressionSyntax)secondExpression;

                // Check if the left and right expressions are equivalent
                if (firstBinary.Left.IsEquivalentTo(secondBinary.Left) &&
                   firstBinary.Right.IsEquivalentTo(secondBinary.Right) &&
                   firstBinary.OperatorToken.Kind != secondBinary.OperatorToken.Kind)
                {
                    return true;
                }
            }

            return false;
        }
    }
}

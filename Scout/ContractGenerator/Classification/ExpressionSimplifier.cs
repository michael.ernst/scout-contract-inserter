﻿using Roslyn.Compilers.CSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractGenerator.Classification
{
    public static class ExpressionSimplifier
    {
        public static ExpressionSyntax SimplifyBooleanEquality(ExpressionSyntax expression)
        {
            if (expression is BinaryExpressionSyntax)
            {
                var binaryExpression = expression as BinaryExpressionSyntax;

                if ((binaryExpression.Right.ToString().Equals("true") && binaryExpression.OperatorToken.Kind == SyntaxKind.EqualsEqualsToken) ||
                   (binaryExpression.Right.ToString().Equals("false") && binaryExpression.OperatorToken.Kind == SyntaxKind.ExclamationEqualsToken))
                {
                    return binaryExpression.Left;
                }
                else if ((binaryExpression.Right.ToString().Equals("false") && binaryExpression.OperatorToken.Kind == SyntaxKind.EqualsEqualsToken) ||
                    (binaryExpression.Right.ToString().Equals("true") && binaryExpression.OperatorToken.Kind == SyntaxKind.ExclamationEqualsToken))
                {
                    return Syntax.ParseExpression("!" + binaryExpression.Left.ToString());
                }
            }

            return expression;
        }

        public static ExpressionSyntax SimplifyNegationForIdentifiers(ExpressionSyntax expression)
        {
            expression = expression.UnwrapParentheses();

            if (expression.Kind == SyntaxKind.LogicalNotExpression)
            {
                var child = expression.ChildNodes().ElementAt(0);
                if (child.Kind == SyntaxKind.ParenthesizedExpression)
                {
                    var innerChildren = child.ChildNodes().ToList();
                    if (innerChildren.Count == 1)
                    {
                        var innerInnerChild = innerChildren.ElementAt(0);
                        if (innerInnerChild is IdentifierNameSyntax || innerInnerChild is MemberAccessExpressionSyntax)
                        {
                            return Syntax.ParseExpression("!" + innerInnerChild.ToString());
                        }
                    }
                }
            }

            return expression;
        }

        public static ExpressionSyntax Simplify(this ExpressionSyntax expression)
        {
            expression = Negator.TryApplyNegation(expression);
            expression = SimplifyBooleanEquality(expression);
            expression = SimplifyNegationForIdentifiers(expression);

            if (expression is BinaryExpressionSyntax)
            {
                var binaryExpression = expression as BinaryExpressionSyntax;
                var left = binaryExpression.Left.Simplify();
                var right = binaryExpression.Right.Simplify();

                var fixedBinaryExpression =
                    Syntax.ParseExpression(
                    left.ToFullString() +
                    binaryExpression.OperatorToken.ToFullString() +
                    right.ToFullString()) as BinaryExpressionSyntax;

                return fixedBinaryExpression.FixBinaryExpressionWhitespace() as ExpressionSyntax;
            }

            return expression;
        }
    }
}

﻿using Roslyn.Compilers.CSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractGenerator.Classification
{
    public static class RoslynExtensions
    {
        public static ExpressionSyntax UnwrapParentheses(this ExpressionSyntax expression)
        {
            // Unwrap parenthesized expressions.
            while (expression.Kind == SyntaxKind.ParenthesizedExpression)
            {
                expression = expression.ChildNodes().ToList()[0] as ExpressionSyntax;
            }

            return expression;
        }

        public static BinaryExpressionSyntax FixBinaryExpressionWhitespace(this BinaryExpressionSyntax binaryExpression)
        {
            return AddTrailingWhitespaceToOperator(AddLeadingWhitespaceToOperator(binaryExpression));
        }

        private static BinaryExpressionSyntax AddLeadingWhitespaceToOperator(BinaryExpressionSyntax binaryExpression)
        {
            string leftWhitespace = binaryExpression.Left.GetTrailingTrivia().ToFullString() +
                                    binaryExpression.OperatorToken.LeadingTrivia.ToFullString();

            bool addLeftWhitespace = string.IsNullOrEmpty(leftWhitespace);

            if (addLeftWhitespace)
            {
                // Add leading whitespace to the operator token.
                SyntaxToken newOperatorToken =
                    binaryExpression.OperatorToken.WithLeadingTrivia(Syntax.TriviaList(Syntax.Space));
                // Replace the operator token.
                return binaryExpression.WithOperatorToken(newOperatorToken);
            }
            else
            {
                return binaryExpression;
            }
        }

        private static BinaryExpressionSyntax AddTrailingWhitespaceToOperator(BinaryExpressionSyntax binaryExpression)
        {
            string rightWhitespace = binaryExpression.OperatorToken.TrailingTrivia.ToFullString() +
                         binaryExpression.Right.GetLeadingTrivia().ToFullString();

            bool addRightWhitespace = string.IsNullOrEmpty(rightWhitespace);

            if (addRightWhitespace)
            {
                // Add trailing whitespace to the operator token.
                SyntaxToken newOperatorToken =
                    binaryExpression.OperatorToken.WithTrailingTrivia(Syntax.TriviaList(Syntax.Space));
                // Replace the operator token.
                return binaryExpression.WithOperatorToken(newOperatorToken);
            }
            else
            {
                return binaryExpression;
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;
using System.Collections.ObjectModel;

namespace ContractGenerator
{
    /// <summary>
    /// Filters a <see cref="ProgramContract"/> based on the logic of its invariant (such as its variables). 
    /// </summary>
    public class ContractFilter
    {
        /// <summary>
        /// The <see cref="ProgramElement"/> this filter applies to.
        /// </summary>
        public ProgramElement Element { get; private set; }
        
        /// <summary>
        /// The invariant type filtered-out by this filter.
        /// </summary>
        public InvariantType InvariantType { get; private set; }
        
        /// <summary>
        /// The variable names this filtered-out by this filter.
        /// </summary>
        public HashSet<string> Variables { get; private set; }

        /// <summary>
        /// The contracts filtered-out by this filter.
        /// </summary>
        private readonly List<ProgramContract> filteredContracts;

        /// <summary>
        /// A read-only view of the <see cref="ProgramContract"/> filtered by this filter. 
        /// </summary>
        public ReadOnlyCollection<ProgramContract> FilteredContracts
        {
            get
            {
                return filteredContracts.AsReadOnly();
            }
        }

        /// <summary>
        /// A comma-separated list of the variables that this filter filters. 
        /// </summary>
        public string FilteredVariablesString
        {
            get
            {
                return string.Join(", ", Variables);
            }
        }

        public ContractFilter(ProgramElement method, InvariantType invariantType)
            : this(method, invariantType, new string[] { }) { }

        public ContractFilter(ProgramElement method, InvariantType invariantType, IEnumerable<string> variables)
        {
            Contract.Requires(method != null);

            Element = method;
            InvariantType = invariantType;
            Variables = new HashSet<string>();
            filteredContracts = new List<ProgramContract>();

            foreach (string variable in variables)
            {
                Variables.Add(variable);
            }
        }

        /// <summary>
        /// Clears the filtered contracts list. 
        /// </summary>
        public void ClearFilteredContracts()
        {
            filteredContracts.Clear();
        }

        /// <summary>
        /// Adds <paramref name="contract"/> to the filtered list. NOTE: this does
        /// not imply <c>FilterContract(<paramref name="contract"/>) == true</c>.
        /// </summary>
        /// <param name="contract">the contract to add to the filtered list</param>
        public void AddFilteredContract(ProgramContract contract)
        {
            Contract.Requires(contract != null);
            filteredContracts.Add(contract);
        }

        /// <summary>
        /// Returns <c>true</c> if the contract is filtered, <c>false</c> otherwise.
        /// </summary>
        /// <param name="contract">the contract</param>
        /// <returns><c>true</c> if the contract is filtered, <c>false</c> otherwise.</returns>
        [Pure]
        public bool Filters(ProgramContract contract)
        {
            Contract.Requires(contract != null);

            if (InvariantType == InvariantType.TypeOf)
            {
                return contract.IsTypeOfInvariant;
            }

            int requiredNumberOfVariables = 1;
            if (InvariantType == InvariantType.Binary || InvariantType == InvariantType.Ternary)
            {
                if (contract.InvariantType != this.InvariantType)
                {
                    return false;
                }
                requiredNumberOfVariables = 2;
            }

            int found = 0;
            foreach (string variable in Variables)
            {
                if (found >= requiredNumberOfVariables)
                {
                    break;
                }

                if (contract.Variables.Contains(variable))
                {
                    found++;
                }
            }

            return found >= requiredNumberOfVariables;
        }

        /// <summary>
        /// Activate this filter for <see cref="Element"/>.
        /// </summary>
        public void Apply()
        {
            Element.AddFilter(this);
        }

        /// <summary>
        /// Deactivate this filter for <see cref="Element"/>.
        /// </summary>
        public void Remove()
        {
            Contract.Requires(Element.Filters.Contains(this));
            Contract.Ensures(!Element.Filters.Contains(this));
            Element.RemoveFilter(this);
        }
    }
}

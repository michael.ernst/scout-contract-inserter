﻿using System;
using System.Runtime.Serialization;

namespace ContractGenerator
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2240:ImplementISerializableCorrectly"), Serializable]
    public class ContractGenerationException : Exception
    {
        public string LogFilePath { get; set; }

        public ContractGenerationException()
        { }

        public ContractGenerationException(string message)
        : base(message) { }

        public ContractGenerationException(string format, params object[] args)
        : base(string.Format(format, args)) { }
    
        public ContractGenerationException(string message, Exception innerException)
        : base(message, innerException) { }
    
        public ContractGenerationException(string format, Exception innerException, params object[] args)
        : base(string.Format(format, args), innerException) { }

        protected ContractGenerationException(SerializationInfo info, StreamingContext context)
        : base(info, context) { }
    }
}

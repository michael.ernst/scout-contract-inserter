﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using ContractGenerator.Classification;
using ContractGenerator.Contracts;

namespace ContractGenerator
{
    /// <summary>
    /// Represents a single requires, ensures, or invariant contract. 
    /// </summary>
    public sealed class ProgramContract
    {
        #region Events

        public event EventHandler<ContractModifiedArgs> ContractModified;

        /// <summary>
        /// Raises the contract modified event.
        /// A contract is considered modified when it is inserted/deleted.
        /// </summary>
        /// <param name="e">event information - contains the contract and insertion boolean</param>
        private void OnContractModified(ContractModifiedArgs e)
        {
            EventHandler<ContractModifiedArgs> handler = ContractModified;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion

        /// <summary>
        /// The unique identifer for this contract.
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// The text of this contract as a Code Contract expression.
        /// </summary>
        public string CodeContractText
        {
            get
            {
                return GetCodeContractExpression(Implication.AsContract, Kind);
            }
        }

        /// <summary>
        /// The text of this contract.
        /// </summary>
        public string ContractText
        {
            get
            {
                return Implication.AsContract;
            }
        }

        /// <summary>
        /// Gets the Code Contract text of this contract with white-space removed.
        /// </summary>
        public string CodeContractTextNoWhitespace
        {
            get
            {
                return CodeContractText.StripWhitespace();
            }
        }

        /// <summary>
        /// The Daikon text of this contract.
        /// </summary>
        public string DaikonContractText
        {
            get
            {
                return Implication.AsDaikonContract;
            }
        }

        /// <summary>
        /// The Daikon invariant type class of this contract.
        /// </summary>
        public string DaikonInvariantType { get; private set; }

        /// <summary>
        /// Backing field for <see cref="GroupVariables"/>.
        /// </summary>
        private readonly string[] groupVariables;

        /// <summary>
        /// The collection of variables to group this contract by when grouping by variable.
        /// This is not the same as <see cref="Variables"/>. For example, <see cref="Variables"/>
        /// may contain "Contract.OldValue(s)", while this would contain just "s".
        /// </summary>
        public IEnumerable<string> GroupVariables
        {
            get
            {
                return groupVariables;
            }
        }

        /// <summary>
        /// Backing field for <see cref="Variables"/>.
        /// </summary>
        private readonly string[] variables;

        /// <summary>
        /// The collection of all variables referenced by this contract. 
        /// </summary>
        public IEnumerable<string> Variables
        {
            get
            {
                return variables;
            }
        }

        /// <summary>
        /// The program element this contract belongs to.
        /// </summary>
        public ProgramElement ContainingElement { get; private set; }

        /// <summary>
        /// The invariant type (unary, binary etc.) of this contract.
        /// </summary>
        public InvariantType InvariantType { get; private set; }

        /// <summary>
        /// The kind (requires, ensures, or invariant) of this contract.
        /// </summary>
        public ContractKind Kind { get; set; }

        /// <summary>
        /// Backing field for <see cref="IsUserWritten"/>.
        /// </summary>
        private bool isUserWritten;

        /// <summary>
        /// True if the contract is user-written, i.e. it was present during the original parse.
        /// </summary>
        public bool IsUserWritten
        {
            get
            {
                return isUserWritten;
            }
            set
            {
                if (value)
                {
                    inserted = true;
                }

                isUserWritten = value;
            }
        }

        /// <summary>
        /// Backing field for <see cref="Inserted"/>.
        /// </summary>
        private bool inserted;

        /// <summary>
        /// True if this contract is currently inserted as a Code Contract in the source code.
        /// </summary>
        public bool Inserted
        {
            get
            {
                return inserted;
            }
            private set
            {
                if (value != inserted)
                {
                    OnContractModified(value
                        ? new ContractModifiedArgs(this, true)
                        : new ContractModifiedArgs(this, false));
                    inserted = value;
                }
            }
        }

        /// <summary>
        /// True if this contract is trivial (invalid/redundant).
        /// Trivial contracts should not be displayed or counted.
        /// </summary>
        public bool IsTrivial { get; set; }

        /// <summary>
        /// The number of invariant filters filtering this contract. 
        /// </summary>
        public int FilteredBy { get; set; } 

        /// <summary>
        /// True if this contract is currently filtered.
        /// </summary>
        public bool IsFiltered
        {
            get { return FilteredBy > 0; }
        }

        /// <summary>
        /// Determines the order this should contract should appear in when added to its containing element.
        /// A contract with a smaller index gets inserted before a contract with a larger index.
        /// </summary>
        public int InsertionIndex { get; set; }

        /// <summary>
        /// True if this is a type-of invariant.
        /// </summary>
        public bool IsTypeOfInvariant
        {
            get
            {
                return ContractText.Contains("typeof(") || ContractText.Contains("GetType(");
            }
        }

        /// <summary>
        /// The semantic classification of this contract (Nullness, Indicator etc.)
        /// </summary>
        public string SemanticClassification { get; private set; }

        /// <summary>
        /// The set of contracts with the same <see cref="ContainingElement"/> that are similar to this.
        /// 
        /// Two expressions are said to be similar if they are both binary expressions 
        /// with the same left and right expressions but a different comparison type.
        /// Implications with the same antecedent and similar binary expressions as consequents
        /// are also similar.
        /// 
        /// For example, a > b is similar to a >= b, but not similar to a > c.
        /// </summary>
        public HashSet<ProgramContract> SimilarContracts { get; private set; }

        /// <summary>
        /// This contract represented as an implication.
        /// </summary>
        public Implication Implication { get; private set; }

        /// <summary>
        /// True if this contract requires the contract extension library to be referenced.
        /// </summary>        
        public bool RequiresExtensionLibraryReference
        {
            get
            {
                // Put most common calls first.
                return ContractText.Contains(".Implies") ||
                       ContractText.Contains(".OneOf") ||
                       ContractText.Contains(".IsSubsequence") ||
                       ContractText.Contains(".IsPowerOfTwo") ||
                       ContractText.Contains(".Pow") ||
                       ContractText.Contains(".GCD") ||
                       ContractText.Contains(".LexLT") ||
                       ContractText.Contains(".LexLTE") ||
                       ContractText.Contains(".LexGT") ||
                       ContractText.Contains(".LexGTE");
            }
        }

        /// <summary>
        /// True if this contract requires a reference to System.Linq.
        /// </summary>
        public bool RequiresLinqReference
        {
            get
            {
                return ContractText.Contains(".Count");
            }
        }

        /// <summary>
        /// True if this contract requires a reference to System.
        /// </summary>
        public bool RequiresSystemReference
        {
            get
            {
                return ContractText.Contains("Double.");
            }
        }

        /// <summary>
        /// The original, unparsed, contract in c# form.
        /// </summary>
        private string originalContract;

        /// <summary>
        /// The original, unparsed, contract in Daikon form.
        /// </summary>
        private string originalDaikonContract;

        private static string requiresTemplate = "Contract.Requires({0});";
        private static string ensuresTemplate = "Contract.Ensures({0});";
        private static string invariantTemplate = "Contract.Invariant({0});";

        [ContractInvariantMethod]
        private void Invariants()
        {
            Contract.Invariant(Implication != null);
            Contract.Invariant(!string.IsNullOrEmpty(originalContract));
            Contract.Invariant(!string.IsNullOrEmpty(originalDaikonContract));
            Contract.Invariant(!string.IsNullOrEmpty(CodeContractText));
            Contract.Invariant(!string.IsNullOrEmpty(ContractText));
            Contract.Invariant(!string.IsNullOrEmpty(DaikonContractText) || IsTrivial);
            Contract.Invariant(!string.IsNullOrEmpty(DaikonInvariantType));
            Contract.Invariant(!string.IsNullOrEmpty(SemanticClassification));
            Contract.Invariant(ContainingElement != null);
            Contract.Invariant(variables != null);
            Contract.Invariant(groupVariables != null);
            Contract.Invariant(variables.Any());
            Contract.Invariant(groupVariables.Any());
            Contract.Invariant(InsertionIndex >= 0);
            Contract.Invariant(FilteredBy >= 0);
            Contract.Invariant(Kind != ContractKind.None);
            Contract.Invariant(InvariantType != InvariantType.BinaryAndTernary && InvariantType != InvariantType.TypeOf);
        }

        public ProgramContract(int id, string contract, string daikonContract, string daikonInvariantType, 
            IEnumerable<string> groupVars, IEnumerable<string> vars, ContractKind kind, ProgramElement parent, int index)
        {
            this.originalContract = contract;
            this.originalDaikonContract = daikonContract;

            contract = FixCSharp(Fix(contract));
            daikonContract = Fix(daikonContract);
            this.Implication = new Implication(contract, daikonContract);

            this.Id = id;
            this.ContainingElement = parent;
            this.DaikonInvariantType = daikonInvariantType;
            this.InvariantType = daikonInvariantType.GetInvariantType();
            this.Kind = kind;
            this.IsUserWritten = false;
            this.InsertionIndex = index;
            this.inserted = false;
            this.FilteredBy = 0;
            this.SemanticClassification = SemanticClassifier.Classify(contract, kind);
            this.SimilarContracts = new HashSet<ProgramContract>();

            var groupVariablesTemp = new HashSet<string>();
            var variablesTemp = new HashSet<string>();

            foreach (var variable in groupVars)
            {
                groupVariablesTemp.Add(Fix(variable));
            }

            foreach (var variable in vars)
            {
                variablesTemp.Add(Fix(variable));
            }

            this.groupVariables = groupVariablesTemp.ToArray();
            this.variables = variablesTemp.ToArray();

            this.IsTrivial = IsInvalidContract(ContractText, DaikonContractText) || Implication.DaikonComponentsUnset;
        }

        private string GetCodeContractExpression(string contract, ContractKind kind)
        {
            string template;
            if (kind == ContractKind.Requires) template = requiresTemplate;
            else if (kind == ContractKind.Ensures) template = ensuresTemplate;
            else template = invariantTemplate;
            return string.Format(template, contract);
        }

        private bool IsInvalidContract(string contract, string daikonContract)
        {
            return contract.Contains("oneOf.java.jpp");
        }

        private string FixCSharp(string contract)
        {
            // TODO: Fix bug where pure method calls in generated contracts have their parentheses missing.
            // In the splitter file, pure method calls have their parentheses omitted so Daikon can properly
            // resolve expressions with pure method calls (I do not remember all  the details of when this
            // is the case). As a consequence, generated contracts with some pure method calls have their
            // parenthesis missing, causing invalid c# statements. Some extra information is required to deduce
            // when to add the parenthesis back.
            return contract;
        }

        private string Fix(string contract)
        {
            contract = FixDaikonArtifacts(contract);
            contract = Extensions.FixGenericArtifacts(contract);
            contract = ShortenTypeNames(contract);
            return contract;
        }

        private string ShortenTypeNames(string contract)
        {
            string result = contract;
            // System:
            result = result.Remove("System.Collections.Generic.");
            result = result.Remove("System.Threading.Tasks.");
            result = result.Remove("System.Text.RegularExpressions.");
            return result;
        }
        
        private string FixDaikonArtifacts(string daikonContract)
        {
            string result = daikonContract;
            result = result.Replace("<==>", "==");
            result = result.Replace("has only one value", "!= null");
            result = result.Replace("java.lang.Math.max", "max");
            result = result.Replace("java.lang.Math.min", "min");
            result = result.Replace("plume.MathMDE.gcd", "gcd");
            result = result.Replace("java.lang.Class", "object");
            return result;
        }

        /// <summary>
        /// Inserts this contract into the source code.
        /// </summary>
        public void Insert()
        {
            if (!this.inserted)
            {
                this.ContainingElement.InsertContract(this);
                this.inserted = true;

                // Mark similar contracts as inserted.
                foreach (ProgramContract contract in this.SimilarContracts)
                {
                    contract.inserted = true;
                }
            }
        }

        /// <summary>
        /// Deletes this contract and all similar contracts from the source code.
        /// </summary>
        public void Delete()
        {
            if (this.inserted)
            {
                this.ContainingElement.DeleteContract(this);
                this.inserted = false;

                // Also delete all similar contracts.
                foreach (ProgramContract contract in this.SimilarContracts)
                {
                    contract.Delete();
                }
            }
        }

        public override string ToString()
        {
            return DaikonContractText;
        }
    }
}

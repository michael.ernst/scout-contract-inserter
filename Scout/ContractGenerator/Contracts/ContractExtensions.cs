﻿using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;

namespace ContractGenerator.Contracts
{
    public static class ContractExtensions
    {
        [Pure]
        public static bool Implies(this bool antecedent, bool consequent)
        {
            Contract.Ensures(Contract.Result<bool>() == (!antecedent || consequent));
            return !antecedent || consequent;
        }

        [Pure]
        public static bool OneOf<T>(this T x, T first, params T[] rest)
        {
            Contract.Requires(x != null);
            Contract.Requires(rest != null);
            Contract.Ensures(Contract.Result<bool>() == (x.Equals(first) || Contract.Exists(rest, e => x.Equals(e))));
            return x.Equals(first) || rest.Any(e => x.Equals(e));
        }
    }
}

﻿using System;

namespace ContractGenerator
{
    /// <summary>
    /// Event arguments for the filter modified event.
    /// A filter modified event is raised when the filters of a <see cref="ProgramContract"/> are modified. 
    /// </summary>
    public class FilterModifiedArgs : EventArgs
    {
        /// <summary>
        /// The modified filter.
        /// </summary>
        public ContractFilter Filter { get; private set; }

        /// <summary>
        /// True if the filter was added, false otherwise.
        /// </summary>
        public bool Added { get; private set; }

        public FilterModifiedArgs(ContractFilter filter, bool added)
        {
            Filter = filter;
            Added = added;
        }
    }

    /// <summary>
    /// Event arguments for the contract modified event. 
    /// A contract modified event is raised when a <see cref="ProgramContract"/> is inserted or deleted. 
    /// </summary>
    public class ContractModifiedArgs : EventArgs
    {
        /// <summary>
        /// The modified contract.
        /// </summary>
        public ProgramContract Contract { get; private set; }

        /// <summary>
        /// The new insertion action of the contract. 
        /// </summary>
        public bool Inserted { get; private set; }

        public ContractModifiedArgs(ProgramContract contract, bool inserted)
        {
            Contract = contract;
            Inserted = inserted; 
        }
    }
}

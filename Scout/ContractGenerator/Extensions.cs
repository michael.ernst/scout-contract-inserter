﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnvDTE;
using System.Text.RegularExpressions;
using EnvDTE80;
using System.Diagnostics.Contracts;

namespace ContractGenerator
{

    public static class Extensions
    {
        static readonly Regex contractOldValue = new Regex("Contract.OldValue");
        static readonly Regex contractReturn = new Regex("Contract.Result<(.*)>\\(\\)");

        [Pure]
        public static string ImitateDaikon(string csharpContract)
        {
            string fake = csharpContract;
            fake = contractOldValue.Replace(fake, "orig");
            fake = contractReturn.Replace(fake, "return");    
            return fake;
        }

        /// <summary>
        /// Finds the index of the first space character (' ') in the type name 
        /// that is not inside generic brackets.
        /// </summary>
        /// <param name="typeName">the type name</param>
        /// <returns>the index of the first space character not inside generic brackets</returns>
        [Pure]
        public static int IndexOfFirstSpaceNotInGenericBrackets(string typeName)
        {
            int depth = 0;

            for (int i = 0; i < typeName.Length; i++)
            {
                if (typeName[i] == '<') depth++;
                else if (typeName[i] == '>') depth--;
                else if (typeName[i] == ' ' && depth == 0) return i;
            }

            return -1;
        }


        /// <summary>
        /// Returns the number of generic parameters for <paramref name="sourceTypeName"/>.
        /// </summary>
        /// <param name="sourceTypeName">the type name</param>
        /// <returns>The number of generic parameters for <paramref name="sourceTypeName"/></returns>
        [Pure]
        public static int GenericParameterCount(this string sourceTypeName)
        {
            Contract.Requires(sourceTypeName != null);
            Contract.Ensures(Contract.Result<int>() >= 0);

            string type = sourceTypeName.Split('.').Last();

            int si = type.IndexOf('<');
            if (si < 0)
            {
                return 0;
            }
            else
            {
                int count = 1;

                si += 1;
                int ei = type.LastIndexOf('>');
                int depth = 0;

                for (; si < ei; si++)
                {
                    if (type[si] == '<') depth++;
                    else if (type[si] == '>') depth--;
                    else if (type[si] == ',' && depth == 0) count++;
                }

                return count;
            }
        }

        /// <summary>
        /// Adds the number of generic arguments to the type name.
        /// </summary>
        /// <param name="sourceTypeName">the type name</param>
        /// <returns>the type name with the number of generic arguments added</returns>
        [Pure]
        public static string AddGenericParameterCount(this string sourceTypeName)
        {
            int genericCount = sourceTypeName.GenericParameterCount();

            if (genericCount == 0)
            {
                return sourceTypeName;
            }
            else
            {
                int si = sourceTypeName.IndexOf("<", System.StringComparison.Ordinal);
                string first = sourceTypeName.Substring(0, si);
                string second = sourceTypeName.Substring(si, sourceTypeName.Length - si);
                return first + "`" + genericCount + second;
            }
        }

        /// <summary>
        /// Returns the name with generics removed.
        /// </summary>
        /// <param name="typeName"></param>
        /// <example><c>List&lt;int&gt;</c> becomes <c>List</c></example>
        /// <returns>the name with generics removed</returns>
        [Pure]
        public static string RemoveGenerics(this string typeName)
        {
            Contract.Requires(!string.IsNullOrEmpty(typeName));
            Contract.Ensures(!string.IsNullOrEmpty(Contract.Result<string>()));

            string r = typeName;
            // Use an iteration counter to avoid infinite loops. 
            // Infinite loops were occuring when parsing the operator >, >=, <, <= methods.
            int iterations = 0;

            while (r.Contains(">") && iterations < 10)
            {
                int index = r.IndexOf('>');
                r = Regex.Replace(r.Substring(0, index + 1), "(<.*>)", "") + r.Substring(index + 1, r.Length - index - 1);
                iterations++;
            }
            return r;
        }

        public static string FixGenericArtifacts(string contract)
        {
            Contract.Requires(!string.IsNullOrEmpty(contract));
            Contract.Ensures(!string.IsNullOrEmpty(contract));
            Contract.Ensures(Contract.Result<string>().Length <= contract.Length);
            Contract.Ensures(!Contract.Result<string>().Contains('`'));

            var characters = contract.ToCharArray();
            int depth = 0;
            bool replaceNextEndBracket = false;

            for (int i = 0; i < characters.Length - 2; i++)
            {
                char first = characters[i];
                char second = characters[i + 1];
                char third = characters[i + 2];

                if (first == '`' && third == '[')
                {
                    characters[i + 2] = '<';
                    i += 3;
                    depth++;
                    replaceNextEndBracket = true;
                }
                else if (third == '[')
                {
                    replaceNextEndBracket = false;
                }
                else if (third == ']')
                {
                    if (replaceNextEndBracket)
                    {
                        characters[i + 2] = '>';
                        depth--;
                    }
                    replaceNextEndBracket = depth != 0;
                }
            }

            string fix = new string(characters);
            fix = fix.Replace("`1", string.Empty);
            fix = fix.Replace("`2", string.Empty);
            fix = fix.Replace("`3", string.Empty);
            return fix;
        }


        /// <summary>
        /// Gets the signature of a <see cref="CodeClass"/>.
        /// </summary>
        /// <param name="clazz">the class</param>
        /// <returns>the signature of <paramref name="clazz"/></returns>
        [Pure]
        public static Signature Signature(this CodeClass clazz)
        {
            return new Signature(clazz.FullName, new List<Parameter>());
        }

        /// <summary>
        /// Gets the signature of a <see cref="CodeFunction"/>.
        /// </summary>
        /// <param name="method">the method</param>
        /// <returns>the signature of <paramref name="method"/></returns>
        [Pure]
        public static Signature Signature(this CodeFunction method)
        {
            return new Signature(method.FullName, method.GetParameters().ToList());
        }

        /// <summary>
        /// Gets the signature of a <see cref="CodeProperty"/>
        /// </summary>
        /// <param name="property">the property</param>
        /// <returns>the signature of <paramref name="property"/></returns>
        [Pure]
        public static Signature Signature(this CodeProperty property)
        {
            return new Signature(property.FullName, new List<Parameter>());
        }

        /// <summary>
        /// Gets the signature of a <see cref="CodeType"/>.
        /// </summary>
        /// <param name="type">the type</param>
        /// <returns>the signature of <paramref name="type"/></returns>
        [Pure]
        public static Signature Signature(this CodeType type)
        {
            return new Signature(type.FullName, new List<Parameter>());
        }

        /// <summary>
        /// Returns the <c>typeof</c> expression for the given type. For example, <c>typeof(Dictionary&lt;K,V&gt;)</c> 
        /// would return <c>typeof(Dictionary&lt;,&gt;)</c>.
        /// </summary>
        /// <param name="typeRef">The Visual Studio code model type</param>
        /// <returns>the <c>typeof</c> expression for the given type.</returns>
        [Pure]
        public static string TypeOfExpression(this CodeType typeRef)
        {
            return string.Format("typeof({0})", typeRef.TypeOfArgument());
        }

        /// <summary>
        /// Returns the <c>typeof</c> argument for the given type. For example, <c>typeof(Dictionary&lt;K,V&gt;)</c> 
        /// would return <c>Dictionary&lt;,&gt;</c>.
        /// </summary>
        /// <param name="typeRef">The Visual Studio code model type</param>
        /// <returns>the <c>typeof</c> argument for the given type.</returns>
        [Pure]
        public static string TypeOfArgument(this CodeType typeRef)
        {
            int genericArgCnt = typeRef.FullName.GenericParameterCount();

            if (genericArgCnt == 0)
            {
                return typeRef.Name;
            }
            else
            {
                return string.Format("{0}<{1}>", typeRef.Name.RemoveGenerics(), ",".Repeat(genericArgCnt - 1));
            }
        }

        public static bool TypeEquals(this IEnumerable<Parameter> parameters, IEnumerable<Parameter> other)
        {
            var firstTypes = parameters.Select(param => param.QualifiedTypeName);
            var secondTypes = other.Select(param => param.QualifiedTypeName);
            return firstTypes.SequenceEqual(secondTypes);
        }

        /// <summary>
        /// The default name to use when creating a contract class for this type.
        /// NOTE: this does not include the generic parameters.
        /// </summary>
        /// <param name="typeRef">The Visual Studio code model type</param>
        [Pure]
        public static string DefaultContractClassName(this CodeType typeRef)
        {
            return typeRef.Name.RemoveGenerics() + "Contract";
        }

        /// <summary>
        /// Returns the name of the Daikon object invariant program point for <paramref name="classRef"/>.
        /// </summary>
        /// <param name="classRef">The Visual Studio code model class</param>
        /// <returns>the name of the Daikon object invariant program point for <paramref name="classRef"/></returns>
        [Pure]
        public static string DaikonObjectInvariantName(this CodeClass classRef)
        {
            Contract.Requires(classRef != null);
            return ReflectiveName(classRef.FullName);
        }

        /// <summary>
        /// Returns the name of the default constructor for <paramref name="classRef"/>.
        /// </summary>
        /// <param name="classRef">The Visual Studio code model class</param>
        /// <returns>the name of the default constructor for <paramref name="classRef"/></returns>
        [Pure]
        public static string DefaultConstructorQualifiedName(this CodeClass classRef)
        {
            Contract.Requires(classRef != null);
            return classRef.ReflectiveFullName() + "." + classRef.ReflectiveName();
        }

        /// <summary>
        /// Returns the name of the default constructor for <paramref name="classRef"/>.
        /// </summary>
        /// <param name="classRef">The Visual Studio code model class</param>
        /// <returns>the name of the default constructor for <paramref name="classRef"/></returns>
        [Pure]
        public static string DefaultConstructorFullName(this CodeClass classRef)
        {
            Contract.Requires(classRef != null);
            return classRef.ReflectiveFullName() + "." + ReflectiveName(classRef.Name);
        }

        /// <summary>
        /// Returns the name of the default constructor for <paramref name="classRef"/>.
        /// </summary>
        /// <param name="classRef">The Visual Studio code model class</param>
        /// <returns>the name of the default constructor for <paramref name="classRef"/></returns>
        [Pure]
        public static string DefaultConstructorName(this CodeClass classRef)
        {
            Contract.Requires(classRef != null);
            return ReflectiveName(classRef.Name);
        }

        /// <summary>
        /// Returns the full name of <paramref name="classRef"/> using .NET reflection-style naming.
        /// </summary>
        /// <param name="classRef">The Visual Studio code model class</param>
        /// <returns> Returns the full name of <paramref name="classRef"/> using .NET reflection-style naming</returns>
        [Pure]
        public static string ReflectiveFullName(this CodeClass classRef)
        {
            Contract.Requires(classRef != null);
            return ReflectiveName(classRef.FullName);
        }

        /// <summary>
        /// Returns the name of <paramref name="classRef"/> using .NET reflection-style naming.
        /// </summary>
        /// <param name="classRef">The Visual Studio code model class</param>
        /// <returns> Returns the name of <paramref name="classRef"/> using .NET reflection-style naming</returns>
        [Pure]
        public static string ReflectiveName(this CodeClass classRef)
        {
            Contract.Requires(classRef != null);
            return ReflectiveName(classRef.Name);
        }

        /// <summary>
        /// Returns the full name of <paramref name="typeRef"/> using .NET reflection-style naming.
        /// </summary>
        /// <param name="typeRef">The Visual Studio code model type</param>
        /// <returns> Returns the full name of <paramref name="typeRef"/> using .NET reflection-style naming</returns>
        [Pure]
        public static string ReflectiveFullName(this CodeType typeRef)
        {
            Contract.Requires(typeRef != null);
            return ReflectiveName(typeRef.FullName);
        }

        /// <summary>
        /// Returns the full name of <paramref name="interfaceRef"/> using .NET reflection-style naming.
        /// </summary>
        /// <param name="interfaceRef">The Visual Studio code model interface</param>
        /// <returns> Returns the full name of <paramref name="interfaceRef"/> using .NET reflection-style naming</returns>
        [Pure]
        public static string ReflectiveFullName(this CodeInterface interfaceRef)
        {
            Contract.Requires(interfaceRef != null);
            return ReflectiveName(interfaceRef.FullName);
        }

        /// <summary>
        /// Returns the full name of <paramref name="methodRef"/> using .NET reflection-style naming.
        /// </summary>
        /// <param name="methodRef">The Visual Studio code model method</param>
        /// <param name="containingType">The containing Visual Studio code model type</param>
        /// <returns> Returns the full name of <paramref name="methodRef"/> using .NET reflection-style naming</returns>
        [Pure]
        public static string ReflectiveFullName(CodeFunction methodRef, CodeType containingType)
        {
            Contract.Requires(methodRef != null);
            Contract.Requires(containingType != null);

            if (methodRef.FunctionKind == vsCMFunction.vsCMFunctionConstructor)
            {
                var qualifiedType = ReflectiveFullName(containingType);
                return qualifiedType + "." + qualifiedType.Split('.').Last();
            }
            else if (methodRef.FunctionKind == vsCMFunction.vsCMFunctionPropertyGet)
            {
                return ReflectiveFullName(containingType) + "." + ReflectiveName(methodRef.Name) + ".get";
            }
            else if (methodRef.FunctionKind == vsCMFunction.vsCMFunctionPropertySet)
            {
                return ReflectiveFullName(containingType) + "." + ReflectiveName(methodRef.Name) + ".set";
            }
            else
            {
                return ReflectiveFullName(containingType) + "." + ReflectiveName(methodRef.Name);
            }          
        }

        /// <summary>
        /// Returns the name of <paramref name="methodRef"/> using .NET reflection-style naming.
        /// </summary>
        /// <param name="methodRef">The Visual Studio code model method</param>
        /// <param name="containingType">The containing Visual Studio code model type</param>
        /// <returns>Returns the name of <paramref name="methodRef"/> using .NET reflection-style naming</returns>
        [Pure]
        public static string ReflectiveName(CodeFunction methodRef, CodeType containingType)
        {
            Contract.Requires(methodRef != null);
            Contract.Requires(containingType != null);

            if (methodRef.FunctionKind == vsCMFunction.vsCMFunctionConstructor)
            {
                var qualifiedType = ReflectiveFullName(containingType);
                return qualifiedType.Split('.').Last();
            }
            else
            {
                return ReflectiveName(methodRef.Name);
            }
        }

        /// <summary>
        /// Returns the full name of <paramref name="propertyRef"/> using .NET reflection-style naming.
        /// </summary>
        /// <param name="propertyRef">The Visual Studio code model property</param>
        /// <param name="containingType">The containing Visual Studio code model type</param>
        /// <returns> Returns the full name of <paramref name="propertyRef"/> using .NET reflection-style naming</returns>
        [Pure]
        public static string ReflectiveFullName(this CodeProperty propertyRef, CodeType containingType)
        {
            Contract.Requires(propertyRef != null);
            Contract.Requires(containingType != null);
            return ReflectiveFullName(containingType) + "." + ReflectiveName(propertyRef.Name);
        }

        /// <summary>
        /// Returns the name of <paramref name="propertyRef"/> using .NET reflection-style naming.
        /// </summary>
        /// <param name="propertyRef">The Visual Studio code model property</param>
        /// <param name="containingType">The containing Visual Studio code model type</param>
        /// <returns> Returns the name of <paramref name="propertyRef"/> using .NET reflection-style naming</returns>
        [Pure]
        public static string ReflectiveName(this CodeProperty propertyRef, CodeType containingType)
        {
            Contract.Requires(propertyRef != null);
            Contract.Requires(containingType != null);
            return ReflectiveName(propertyRef.Name);
        }

        [Pure]
        private static string ReflectiveName(string typeName)
        {
            var x = typeName.GenericParameterCount();
            return x == 0 ? typeName.RemoveGenerics() : (typeName.RemoveGenerics() + "`" + x);
        }

        [Pure]
        private static string CodeFunctionDisplayName(CodeFunction _method)
        {
            Contract.Requires(_method != null);
            Contract.Ensures(!string.IsNullOrEmpty(Contract.Result<string>()));

            var method = (CodeFunction2)_method;

            if (method.IsClassInvariantMethod())
            {
                // Hack attack.
                string remove = "." + method.FullName.Split('.').Last() + "-";
                string withoutCtorName = (method.FullName + "-").Replace(remove, string.Empty);
                return withoutCtorName;
            }
            if (method.FunctionKind == vsCMFunction.vsCMFunctionPropertyGet)
            {
                return method.FullName + ".get()";
            }
            else if (method.FunctionKind == vsCMFunction.vsCMFunctionPropertySet)
            {
                return method.FullName + ".set()";
            }
            else
            {
                return method.FullName;
            }
        }

        [Pure]
        public static string CodeElementDisplayName(CodeElement element)
        {
            Contract.Requires(element != null);
            Contract.Ensures(!string.IsNullOrEmpty(Contract.Result<string>()));

            if (element.Kind == vsCMElement.vsCMElementFunction)
            {
                return CodeFunctionDisplayName(element as CodeFunction);
            }
            else
            {
                return element.FullName;
            }
        }

        /// <summary>
        /// Returns <c>true</c> if the statement is a Code Contract statement, i.e. it begins 
        /// with <c>Contract.Requires</c>, <c>Contract.Ensures</c>, or <c>Contract.Invariant</c>.
        /// </summary>
        /// <param name="statement">The C# statement</param>
        /// <returns><c>true</c> iff the statement is a Code Contract statement</returns>
        [Pure]
        public static bool IsContractStatement(this string statement)
        {
            return statement.StartsWith("Contract.Requires") || 
                statement.StartsWith("Contract.Ensures") || 
                statement.StartsWith("Contract.Invariant");
        }

        [Pure]
        public static string XmlDocKey(this ContractKind pp)
        {
            if (pp == ContractKind.Requires) return "requires";
            else if (pp == ContractKind.Ensures) return "ensures";
            else if (pp == ContractKind.Invariant) return "invariant";
            else return "arbitrary";
        }

        /// <summary>
        /// Returns the Contract Inserter model of the parameters of <paramref name="method"/>.
        /// </summary>
        /// <param name="method">The Visual Studio code model method</param>
        /// <returns>The Contract Inserter model of the method's parameters</returns>
        [Pure]
        public static IEnumerable<Parameter> GetParameters(this CodeFunction method)
        {
            Contract.Requires(method != null);
            Contract.Ensures(Contract.Result<IEnumerable<Parameter>>() != null);
            Contract.Ensures(Contract.Result<IEnumerable<Parameter>>().Count() == method.Parameters.Count);

            var parameters = new List<Parameter>();
            foreach (CodeParameter parameter in method.Parameters)
            {
                CodeTypeRef paramType = parameter.Type;
                vsCMTypeRef typeKind = paramType.TypeKind;
                string sourceName;

                if (typeKind == vsCMTypeRef.vsCMTypeRefArray) 
                {
                    // Handle arrays and jagged arrays.
                    sourceName = GetParameterTypeForArray(paramType);
                }
                else if (typeKind == vsCMTypeRef.vsCMTypeRefOther)
                {
                    // Generic, non-array parameter.
                    sourceName = paramType.AsString.Trim();
                }
                else
                {
                    // Non-generic, non-array parameter.
                    sourceName = paramType.CodeType.FullName;
                }

                // Add an extra space after commas. 
                // This is required because it is how Celeriac handles generic type parameters.
                sourceName = sourceName.Replace(",", ", ");
                parameters.Add(new Parameter(sourceName, parameter.FullName));
            }
            return parameters;
        }

        /// <summary>
        /// Returns the type name as a string of <paramref name="arrayType"/>.
        /// </summary>
        /// <param name="arrayType">the reference to the array type (arrayType.TypeKind == vsCMTypeRef.vsCMTypeRefArray)</param>
        /// <returns>a string representing the type of the array</returns>
        /// <remarks>
        /// This is recursive in order to account for jagged arrays, such as T[][][], or int[][].
        /// </remarks>
        [Pure]
        public static string GetParameterTypeForArray(CodeTypeRef arrayType)
        {
            Contract.Requires(arrayType.TypeKind == vsCMTypeRef.vsCMTypeRefArray);
            Contract.Ensures(!string.IsNullOrEmpty(Contract.Result<string>()));

            vsCMTypeRef typeKind = arrayType.ElementType.TypeKind;

            if (typeKind == vsCMTypeRef.vsCMTypeRefOther)
            {
                // Generic array, such as T[].
                return arrayType.ElementType.AsString + "[]";
            }
            else if (typeKind == vsCMTypeRef.vsCMTypeRefArray)
            {
                // For jagged arrays, such as T[][][] or int[][], just compute next level of array recursively.
                return GetParameterTypeForArray(arrayType.ElementType) + "[]";
            }
            else
            {
                // Regular array, such as int[].
                return arrayType.ElementType.CodeType.FullName + "[]";
            }
        }

        [Pure]
        public static bool HasGetter(this CodeProperty property)
        {
            try
            {
                return property.Getter != null;
            }
            catch
            {
                return false;
            }
        }

        [Pure]
        public static bool HasAutoImplementedGetter(this CodeProperty property)
        {
            if(property.HasGetter())
            {
                var location = new SourceLocation(property.Getter);
                return location.Source.Contains("return");
            }

            return false;
        }

        [Pure]
        public static bool HasSetter(this CodeProperty property)
        {
            try
            {
                return property.Setter != null;
            }
            catch
            {
                return false;
            }
        }


        [Pure]
        public static bool HasAutoImplementedSetter(this CodeProperty property)
        {
            if (property.HasSetter())
            {
                var location = new SourceLocation(property.Setter);
                return location.Source.Contains("value");
            }

            return false;
        }

        [Pure]
        public static bool IsAutoImplemented(this CodeProperty property)
        {
            return property.HasAutoImplementedGetter() && property.HasAutoImplementedSetter();
        }

        /// <summary>
        /// Returns <c>true</c> iff <paramref name="method"/> is an contract invariant method. That is, 
        /// the method has the <c>ContractInvariantMethod</c> attribute.
        /// </summary>
        /// <param name="method">a non-null method</param>
        /// <returns><c>true</c> iff <paramref name="method"/> is an object invariant method</returns>
        [Pure]
        public static bool IsClassInvariantMethod(this CodeFunction method)
        {
            Contract.Requires(method != null);
            return method.Attributes.Cast<CodeElement2>().Any(attr => attr.FullName.Equals("System.Diagnostics.Contracts.ContractInvariantMethodAttribute"));
        }

        [Pure]
        public static bool IsClassInvariantMethod(this CodeElement element)
        {
            return element.Kind == vsCMElement.vsCMElementFunction && IsClassInvariantMethod(element as CodeFunction);
        }

        /// <summary>
        /// The default name to use when creating a contract class for this interface.
        /// NOTE: this does not include the generic parameters.
        /// </summary>
        /// <param name="interfaceRef">The Visual Studio code model interface</param>
        [Pure]
        public static string DefaultContractClassName(this CodeInterface interfaceRef)
        {
            return interfaceRef.Name.RemoveGenerics() + "Contract";
        }

        [Pure]
        public static string GetTypeConstraintString(this CodeType typeRef)
        {
            var headerStart = typeRef.GetStartPoint(vsCMPart.vsCMPartHeader);
            var bodyStart = typeRef.GetStartPoint(vsCMPart.vsCMPartBody);
            var headerText = headerStart.CreateEditPoint().GetText(bodyStart);

            int paramsEnd = headerText.IndexOf(')');
            int typeConstraintStart = headerText.IndexOf("where", paramsEnd + 1, System.StringComparison.Ordinal);
            int headerEnd = headerText.LastIndexOf('{');

            return typeConstraintStart < 0 ? string.Empty : headerText.Substring(typeConstraintStart, headerEnd - typeConstraintStart).Trim();
        }

        /// <summary>
        /// Returns the generic parameters string for the type, or the empty string if the
        /// type is not generic.
        /// </summary>
        /// <example>List&lt;T&gt; would return &lt;T&gt;</example>
        /// <param name="typeRef"></param>
        /// <returns>
        /// the generic parameters string for the type, or the empty string if the
        /// type is not generic
        /// </returns>
        [Pure]
        public static string GetGenericParameterString(this CodeType typeRef)
        {
            int s = typeRef.FullName.IndexOf('<');
            return s < 0 ? string.Empty : typeRef.FullName.Substring(s, typeRef.FullName.LastIndexOf('>') - s + 1);
        }

        /// <summary>
        /// Returns <c>true</c> iff <paramref name="clazz"/> is a contract class.
        /// </summary>
        /// <param name="clazz">a pointer to the class</param>
        /// <returns><c>true</c> iff <paramref name="clazz"/> is a contract class, <c>false</c> otherwise</returns>
        [Pure]
        public static bool IsContractClass(this CodeClass clazz)
        {
            return clazz.Attributes.Cast<CodeAttribute>().Any(attribute => attribute.Name.Equals("ContractClassFor"));
        }

        [Pure]
        public static ContractKind GetContractKind(this string str)
        {
            Contract.Requires(str != null);
            if (str.StartsWith("Contract.Requires") || str.Contains("ENTER")) return ContractKind.Requires;
            else if (str.StartsWith("Contract.Ensures") || str.Contains("EXIT")) return ContractKind.Ensures;
            else if (str.StartsWith("Contract.Invariant") || str.Contains("OBJECT"))  return ContractKind.Invariant;
            else return ContractKind.None;
        }

        [Pure]
        public static InvariantType GetInvariantType(this string invariantType)
        {
            Contract.Requires(!string.IsNullOrEmpty(invariantType));

            invariantType = invariantType.ToLower();
            if (invariantType.Contains("ternary")) return InvariantType.Ternary;
            else if (invariantType.Contains("unary")) return InvariantType.Unary;
            else if (invariantType.Contains("binary")) return InvariantType.Binary;
            else if (invariantType.Contains("all")) return InvariantType.All;

            throw new ArgumentException(
                string.Format("No InvariantType enumeration of type '{0}'", invariantType));
        }

        /// <summary>
        /// Returns a new dictionary that merges the lists of keys that are similar. 
        /// </summary>
        /// <typeparam name="T">the key type</typeparam>
        /// <typeparam name="W">the value list element type</typeparam>
        /// <param name="dict">the dictionary to merge</param>
        /// <param name="mergeKeys">function that returns true if two keys should be merged</param>
        /// <returns>the merged dictionary</returns>
        [Pure]
        public static Dictionary<T, List<W>> MergeKeys<T, W>(this Dictionary<T, List<W>> dict, Func<T, T, bool> mergeKeys)
        {
            Contract.Requires(dict != null);
            Contract.Requires(Contract.ForAll(dict.Values, value => value != null));
            Contract.Requires(mergeKeys != null);
            Contract.Ensures(Contract.Result<Dictionary<T, List<W>>>() != null);
            Contract.Ensures(Contract.Result<Dictionary<T, List<W>>>().Keys.Count <= dict.Keys.Count);
            Contract.Ensures(Contract.Result<Dictionary<T, List<W>>>().Values.Select(value => value.Count).Sum() == 
                dict.Values.Select(value => value.Count).Sum());

            var merge = new Dictionary<T, List<W>>();
            var keys = new List<T>(dict.Keys);
            var added = new HashSet<int>();

            for (int i = 0; i < keys.Count; i++)
            {
                T first = keys[i];
                bool merged = false;

                for (int j = 0; j < keys.Count; j++)
                {
                    T second = keys[j];

                    if (mergeKeys(first, second) && !added.Contains(i) && !added.Contains(j))
                    {
                        var combined = new List<W>();
                        combined.AddRange(dict[first]);
                        combined.AddRange(dict[second]);
                        merge[first] = combined;
                        added.Add(i);
                        added.Add(j);
                        merged = true;
                        continue;
                    }
                }

                if (!merged && !added.Contains(i))
                {
                    merge[first] = dict[first];
                    added.Add(i);
                }
            }

            return merge;
        }
    }
}

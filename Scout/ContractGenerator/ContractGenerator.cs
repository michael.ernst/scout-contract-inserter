﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Reflection;
using EnvDTE;
using EnvDTE80;

namespace ContractGenerator
{
    public enum GenerateStatus
    {
        NothingGenerated,
        Building,
        ParsingSource,
        Seal,
        Celeriac,
        DaikonCalculate,
        DaikonPrint,
        ParsingContracts,
        Error,
        Success,
        Other,
    }

    public class GenerateStatusEvent : EventArgs
    {
        public GenerateStatus Status { get; private set; }
        public string Message { get; private set; }

        public GenerateStatusEvent(GenerateStatus status, string message)
        {
            this.Status = status;
            this.Message = message;
        }
    }

    public class ContractGenerator
    {
        /// <summary>
        /// Name of the log file.
        /// </summary>
        private const string LogPath = "inserter.error.log";

        /// <summary>
        /// True to create splitter files for parsed source files and pass them to Daikon.
        /// </summary>
        private const bool CreateSplitters = true;

        /// <summary>
        /// True to parse purity information for methods marked as pure and auto-implemented properties.
        /// </summary>
        private const bool ParsePurityInfo = true;

        #region Events

        // Clients subscribe to this event to be notified of contract generation progress.
        public event EventHandler<GenerateStatusEvent> GenerateStatusChange;

        private void UpdateStatus(GenerateStatus status, string message)
        {
            EventHandler<GenerateStatusEvent> handler = GenerateStatusChange;
            if (handler != null)
            {
                handler(this, new GenerateStatusEvent(status, message));
            }
        }

        // Forward generation status updates from the front end loader.
        private void FrontEndLoaderGenerateStatusChange(object sender, GenerateStatusEvent status)
        {
            UpdateStatus(status.Status, status.Message);
        }

        #endregion

        /// <summary>
        /// Generates Microsoft Code Contracts for <paramref name="project"/>.
        /// </summary>
        /// <param name="solution">the solution that contains the project being generated for</param>
        /// <param name="project">the project to generate contracts for</param>
        /// <param name="loaderFiles">user supplied contract, invariant, or trace files</param>
        /// <param name="projectModel">the output model with generated contracts</param>
        public void Generate(Solution solution, Project project, LoaderFiles loaderFiles, out ProjectModel projectModel)
        {
            Contract.Requires(solution != null);
            Contract.Requires(project != null);
            Contract.Requires(loaderFiles != null);

            projectModel = null; 
            string executingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Contract.Assert(!string.IsNullOrEmpty(executingDirectory));
            string celeriacDirectory = Path.Combine(executingDirectory, "binaries", "celeriac");
            string celeriacExecutable = Path.Combine(celeriacDirectory, Resources.CeleriacExecutable);

            if (!File.Exists(celeriacExecutable))
            {
                // The celeriac executable should always exist, even if it will not be used during generation.
                ErrorAndExit(Resources.CeleriacExecutableNotFound);
            }

            string celeriacHome = Environment.GetEnvironmentVariable(Resources.CeleriacEnvironmentVariable, EnvironmentVariableTarget.User);
            if (celeriacHome == null)
            {
                Environment.SetEnvironmentVariable(Resources.CeleriacEnvironmentVariable, celeriacDirectory, EnvironmentVariableTarget.User);
            }

            // Try to build the solution the project is in.
            UpdateStatus(GenerateStatus.Building, Resources.StatusBuildingSolution);
            solution.SolutionBuild.Build(true);

            if (solution.SolutionBuild.LastBuildInfo != 0)
            {
                // Build failed.
                ErrorAndExit(Resources.SolutionBuildFailedError);
                return;
            }

            string projectDirectory = GetProjectDirectory(project);
            string executablePath = GetProjectExecutable(project);
            string executableDirectory = GetProjectExecutableFolder(project);
            // string splitterDirectory = Path.Combine(projectDirectory, "splitters");

            if(projectDirectory == null || !Directory.Exists(projectDirectory))
            {
                ErrorAndExit(string.Format("Could not find project directory: {0}", projectDirectory));
            }

            if (executablePath == null || !File.Exists(executablePath))
            {
                ErrorAndExit(string.Format("Could not find project output file: {0}", executablePath));
            }

            if (executableDirectory == null || !Directory.Exists(executableDirectory))
            {
                ErrorAndExit(string.Format("Could not find project output directory: {0}", executableDirectory));
            }

            LoaderConfiguration configuration = new LoaderConfiguration(loaderFiles, celeriacExecutable, projectDirectory, executableDirectory, executablePath);

            // Build succeeded.
            UpdateStatus(GenerateStatus.ParsingSource, Resources.StatusParsingSolution);

            /*
            // Delete all splitter files from previous run.
            if (CreateSplitters && Directory.Exists(splitterDirectory))
            {
                foreach (FileInfo splitterFile in new DirectoryInfo(splitterDirectory).GetFiles())
                {
                    splitterFile.Delete();
                }

                Directory.Delete(splitterDirectory);
            }
                */

            // Build the project model.
            ProjectModel generatedProjectModel = BuildProjectModel(project);

            if(generatedProjectModel == null)
            {
                ErrorAndExit(string.Format("Could not parse project model for project"));
            }

            /*
            // Add the generated splitter files to the configuration (if splitters were generated).
            if (CreateSplitters && Directory.Exists(splitterDirectory))
            {
                foreach (string splitterFile in Directory.GetFiles(splitterDirectory))
                {
                    configuration.SplitterFiles.Add(splitterFile);
                }
            }
                */

            bool success;
            string message;
            string logPath;

            using (Log log = new Log(Path.Combine(projectDirectory, LogPath)))
            {
                logPath = log.FilePath;

                using (FrontEndLoader loader = new FrontEndLoader(configuration, generatedProjectModel.PureMethods, log))
                {
                    loader.GenerateStatusChange += FrontEndLoaderGenerateStatusChange;
                    var result = loader.CreateContractsFile();
                    success = result.Item1;
                    message = result.Item2;
                }
            }

            if (success)
            {
                UpdateStatus(GenerateStatus.ParsingContracts, Resources.StatusParsingContracts);
                // If success, message is the path to the contract file to parse.

                string contractsFile = message;
                Dictionary<Signature, DaikonElement> contractMapping = null;

                try
                {
                    contractMapping = ContractParser.ParseInvariants(contractsFile);
                }
                catch(Exception e)
                {
                    projectModel = null;
                    ErrorAndExit(string.Format("Error parsing contracts file:\n{0}\n\nStack trace:\n{1}", e.Message, e.StackTrace));
                }

                try
                {
                    generatedProjectModel.CreateMethodContracts(contractMapping);
                }
                catch(Exception e)
                {
                    ErrorAndExit(string.Format("Error linking generated contracts to project:\n{0}\n\nStack trace:\n{1}", e.Message, e.StackTrace));
                }

                foreach (ProgramElement method in generatedProjectModel.ProgramElements)
                {
                    method.PostProcess();
                }

                projectModel = generatedProjectModel;
                UpdateStatus(GenerateStatus.Success, Resources.StatusSuccess);
            }
            else
            {
                string errorMessage = string.Format(Resources.GenerationFailedError, message);
                ErrorAndExit(errorMessage, logPath);
            }
        }

        /// <summary>
        /// Construct the <see cref="ProjectModel"/> for <paramref name="project"/>.
        /// </summary>
        /// <param name="project">the Visual Studio project</param>
        /// <returns>The Contract Inserter code model of the project.</returns>
        private ProjectModel BuildProjectModel(Project project)
        {
            Contract.Requires(project != null);

            string projectName = project.Name;
            string projectExecutablePath = GetProjectExecutable(project);
            string projectDirectory = GetProjectDirectory(project);

            ProjectModel model = null;

            try
            {
                model = new ProjectModel(projectName, projectExecutablePath);
                ProjectItems target = project.ProjectItems;

                // Recursively build the model. Target is the base Visual Studio code element for projectName. 
                foreach (ProjectItem item in target)
                {
                    BuildProjectItemModel(projectDirectory, item, model);
                }

                // Link interfaces to their respective contract classes.
                foreach (var type in model.Types)
                {
                    foreach (var contractClass in model.ContractClasses)
                    {
                        Dictionary<Signature, Signature> links;
                        type.LinkContractClass(contractClass, out links);
                        model.AddLinks(links);
                    }
                }
            }
            catch (FileNotFoundException fileNotFound)
            {
                throw new FileNotFoundException(
                    string.Format("Error locating file \"{0}\". Does project \"{1}\" output an executable", fileNotFound.Message, model.ProjectName),
                    fileNotFound);
            }
            catch (Exception exception)
            {
                throw new ContractGenerationException(exception.Message, exception);
            }

            return model;
        }

        /// <summary>
        /// Recursively populate <paramref name="model"/> from <paramref name="projectItem"/>.
        /// </summary>
        /// <param name="projectDirectory">The project directory</param>
        /// <param name="projectItem">The project item to build the model from</param>
        /// <param name="model">The current model</param>
        private void BuildProjectItemModel(string projectDirectory, ProjectItem projectItem, ProjectModel model)
        {
            VSFileType type = GuidToVSType.Convert(projectItem.Kind);
            string file = projectItem.Name;

            if (type == VSFileType.PhysicalFile)
            {
                if (file.Length > 3 && file.Substring(projectItem.Name.Length - 3, 3) == ".cs")
                {
                    string sourceFilePath = (string)projectItem.Properties.Item("FullPath").Value;
                    string sourceRelativePath = sourceFilePath.Replace(projectDirectory, string.Empty);

                    UpdateStatus(GenerateStatus.ParsingSource,
                        string.Format("Parsing '{0}'", sourceRelativePath));

                    if (CreateSplitters)
                    {
                        if (!string.IsNullOrEmpty(sourceFilePath))
                        {
                            string splitterDirectory = Path.Combine(projectDirectory, "splitters");
                            string splitterFile = Path.Combine(splitterDirectory, Path.ChangeExtension(projectItem.Name, "spinfo"));
                            Directory.CreateDirectory(Path.Combine(projectDirectory, "splitters"));
                            Splitters.SplitterGenerator.GenerateSplitters(sourceFilePath, splitterFile);
                        }
                    }

                    BuildCodeFileModel(projectItem.FileCodeModel, model);                   
                }

                if (projectItem.ProjectItems != null)
                {
                    foreach (ProjectItem child in projectItem.ProjectItems)
                    {
                        BuildProjectItemModel(projectDirectory, child, model);
                    }
                }
            }
            else if(type == VSFileType.PhysicalFolder || type == VSFileType.VirtualFolder)
            {
                if (projectItem.ProjectItems != null)
                {
                    foreach (ProjectItem child in projectItem.ProjectItems)
                    {
                        BuildProjectItemModel(projectDirectory, child, model);
                    }
                }
            }
            else
            {
                throw new ContractGenerationException(
                    string.Format("File \"{0}\" has an unrecognized type of \"{1}\".", projectItem.Name, projectItem.Kind));
            }
        }

        /// <summary>
        /// Populate the model from a C# source file.
        /// </summary>
        /// <param name="sourceFile">The file to build the model from</param>
        /// <param name="model">The current model</param>
        private void BuildCodeFileModel(FileCodeModel sourceFile, ProjectModel model)
        {
            foreach (CodeElement2 element in sourceFile.CodeElements)
            {
                BuildCodeElementModel(element, model, null, null);
            }
        }

        /// <summary>
        /// Populate the mode with types, methods, and properties in <paramref name="element"/>.
        /// </summary>
        /// <param name="element">The element to visit.</param>
        /// <param name="model">The current model</param>
        /// <param name="containingNamespace">The surrounding namespace</param>
        /// <param name="containingType">The surrounding type</param>
        private void BuildCodeElementModel(CodeElement2 element, ProjectModel model, CodeNamespace/*?*/ containingNamespace, CodeType/*?*/ containingType)
        {
            Contract.Requires(element != null);
            Contract.Requires(model != null);
            Contract.Requires(containingType == null || containingType is CodeClass || containingType is CodeInterface);

            if (element.Kind == vsCMElement.vsCMElementNamespace)
            {
                foreach (CodeElement2 child in element.Children)
                {
                    BuildCodeElementModel(child, model, element as CodeNamespace, null);
                }
            }
            else if (element.Kind == vsCMElement.vsCMElementClass)
            {
                var clazz = element as CodeClass;

                if (clazz.IsContractClass())
                {
                    model.ContractClasses.Add(ContractClass.Wrap(clazz));      
                }
                else // Implementation class.
                {
                    // Add the implementation class. This also aggregates partial classes together into one unit. 
                    model.Types.Add(new Class(clazz));
                    foreach (CodeElement2 child in element.Children)
                    {
                        BuildCodeElementModel(child, model, containingNamespace, element as CodeType);
                    }
                }
            }
            else if (element.Kind == vsCMElement.vsCMElementInterface)
            {
                model.Types.Add(new Interface(element as CodeInterface));
                foreach (CodeElement2 child in element.Children)
                {
                    BuildCodeElementModel(child, model, containingNamespace, element as CodeType);
                }                
            }
            else if (element.Kind == vsCMElement.vsCMElementFunction || element.Kind == vsCMElement.vsCMElementProperty)
            {
                Contract.Assert(containingType != null);

                if (ParsePurityInfo)
                {
                    PurityEntry  purityString = ParsePurityString(containingNamespace, containingType, element, model.ProjectExecutablePath);

                    if (purityString != null)
                    {
                        model.PureMethods.Add(purityString);
                    }
                }
            }
        }

        /// <summary>
        /// Creates a purity string of the form "[namespace.class],[fully qualified assembly name];[function/property name]"
        /// used in the Celeriac purity file. 
        /// </summary>
        /// <param name="codeNamespace">The namespace of the function/property</param>
        /// <param name="containingType">The containing </param>
        /// <param name="projectOutputPath">The path to the project executable. Used to get the assembly name.</param>
        /// <param name="methodOrProperty">The function/property</param>
        /// <returns>The purity string if a pure attribute is found, else <c>null</c></returns>
        private static PurityEntry ParsePurityString(CodeNamespace codeNamespace, CodeType containingType, CodeElement2 methodOrProperty, string projectOutputPath)
        {
            Contract.Requires(projectOutputPath != null);
            Contract.Requires(containingType != null);
            Contract.Requires(methodOrProperty != null);
            Contract.Requires(methodOrProperty.Kind == vsCMElement.vsCMElementFunction || methodOrProperty.Kind == vsCMElement.vsCMElementProperty);

            string assemblyName = GetAssemblyName(projectOutputPath);
            if (assemblyName == null)
            {
                throw new ContractGenerationException(
                    string.Format("Could not parse the assembly name of binary \"{0}\"", projectOutputPath));
            }

            const string pureAttribute = "System.Diagnostics.Contracts.PureAttribute";
            string methodName = null;

            if (methodOrProperty.Kind == vsCMElement.vsCMElementFunction) 
            {
                var m = methodOrProperty as CodeFunction;

                // Celeriac does not accept pure methods that have paramters or return void. 
                // ToString and GetHashCode already accounted for.
                if (m.GetParameters().Any() && 
                    m.Name != "ToString" && m.Name != "GetHashCode" &&
                    m.Type.TypeKind != vsCMTypeRef.vsCMTypeRefVoid)
                {
                    foreach (CodeElement2 attr in m.Attributes)
                    {
                        if (attr.FullName.Equals(pureAttribute))
                        {
                            methodName = methodOrProperty.Name;
                        }
                    }
                }
            }     
            else if (methodOrProperty.Kind == vsCMElement.vsCMElementProperty) 
            {
                bool pure = false; // TODO: Can combine cases into one. No use for this variable. 
                var p = methodOrProperty as CodeProperty;
                SourceLocation fsProp = new SourceLocation(p.StartPoint, p.EndPoint);
                SourceLocation fsGetter = new SourceLocation(p.Getter.StartPoint, p.Getter.EndPoint);

                // TODO: A bug in the EnvDTE namespace prevents detection of attributes on getters/setters.
                foreach (CodeElement2 attr in p.Attributes)
                {
                    if (attr.FullName.Equals(pureAttribute))
                    {
                        methodName = "get_" + p.Getter.Name;
                        pure = true;
                    }
                }

                if (!pure)
                {
                    // This tests if the getter is auto-implemented (its source is equal to the source of the overall property).
                    // TODO: Is there a simpler way to test for auto-implementation of getters?
                    if (fsProp.Source.Equals(fsGetter.Source))
                    {
                        methodName = "get_" + p.Getter.Name;
                    }
                }
            }

            string typeName = codeNamespace == null ? containingType.Name : codeNamespace.Name + "." + containingType.Name;

            return methodName != null 
                ? new PurityEntry(typeName, assemblyName, methodName) 
                : null;
        }

        /// <summary>
        /// Gets the full assembly name of the output file designated by <paramref name="assemblyPath"/>.
        /// </summary>
        /// <param name="assemblyPath">Absolute path of tproject output file</param>
        /// <returns>The full assembly name of <paramref name="assemblyPath"/>, else <c>null</c></returns>
        private static string GetAssemblyName(string assemblyPath)
        {
            try
            {
                AssemblyName assemblyName = AssemblyName.GetAssemblyName(assemblyPath);
                return assemblyName.FullName;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the path to the output file of <paramref name="project"/>. May be an .exe or .dll
        /// </summary>
        /// <param name="project">The project whose output file path to retrieve</param>
        /// <returns>The absolute path to the output file of <paramref name="project"/>, else <c>null</c></returns>
        private static string GetProjectExecutable(Project project)
        {
            try
            {
                Configuration config = project.ConfigurationManager.ActiveConfiguration;
                string projectFolder = Path.GetDirectoryName(project.FileName);
                string outputPath = (string)config.Properties.Item("OutputPath").Value;
                string assemblyFileName = (string)project.Properties.Item("AssemblyName").Value;
                int outputType = (int)project.Properties.Item("OutputType").Value;
                if (outputType == 2) // Output type value of 2 indicates a .dll 
                {
                    assemblyFileName += ".dll";
                }
                else
                {
                    assemblyFileName += ".exe";
                }
                return Path.Combine(new[] { projectFolder, outputPath, assemblyFileName });
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the path to the output file folder of <paramref name="project"/>. 
        /// The output file folder is the folder the project output (such as an executable) resides in.
        /// </summary>
        /// <param name="project">The project whose output file folder path to retrieve</param>
        /// <returns>The absolute path to the output file folder of <paramref name="project"/>, else <c>null</c></returns>
        private static string GetProjectExecutableFolder(Project project)
        {
            try
            {
                Configuration config = project.ConfigurationManager.ActiveConfiguration;
                string projectFolder = Path.GetDirectoryName(project.FileName);
                string outputPath = (string)config.Properties.Item("OutputPath").Value;
                return Path.Combine(new[] { projectFolder, outputPath });
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the full project directory path.
        /// </summary>
        /// <param name="project">the project</param>
        /// <returns>The full project directory path</returns>
        private static string GetProjectDirectory(Project project)
        {
            return project.Properties.Item("FullPath").Value.ToString();
        }

        /// <summary>
        /// Mark the generation process as completed and raise an <see cref="ContractGenerationException"/>.
        /// </summary>
        /// <param name="message">The error message to display to the user.</param>
        /// <param name="logFilePath">The path to the error log.</param>
        private void ErrorAndExit(string message, string logFilePath = "")
        {
            UpdateStatus(GenerateStatus.Error, Resources.StatusError);
            var exception = new ContractGenerationException(message);
            exception.LogFilePath = logFilePath;
            throw exception;
        }
    }
}

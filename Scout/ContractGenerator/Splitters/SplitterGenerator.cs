﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using Roslyn.Compilers.CSharp;

namespace ContractGenerator.Splitters
{
    public static class SplitterGenerator
    {
        private const string REPLACE_SECTION = "REPLACE";
        private const string PPT_SECTION = "PPT_NAME {0}";

        public static void GenerateSplittersForDirectory(string directory, string outputDirectory = "C:\\Users\\Forrest\\Desktop\\splitters")
        {
            foreach(var file in Directory.GetFiles(directory))
            {
                if(file.EndsWith(".cs"))
                {
                    string name = file.Replace(directory, string.Empty).Replace("\\", string.Empty);
                    string splitterFile = Path.ChangeExtension(Path.Combine(outputDirectory, name), "spinfo");
                    GenerateSplitters(file, splitterFile);
                }
            }

            foreach(var dir in Directory.GetDirectories(directory))
            {
                GenerateSplittersForDirectory(dir);
            }
        }

        public static void ConcatSplittersForDirectory(string directory = "C:\\Users\\Forrest\\Desktop\\splitters")
        {
            var output = new StringBuilder();

            foreach (var file in Directory.GetFiles(directory))
            {
                if (file.EndsWith(".spinfo"))
                {
                    string text = File.ReadAllText(file);
                    output.Append(text);
                    output.AppendLine();
                }
            }

            File.WriteAllText(Path.Combine(directory, "all-splitters.spinfo"), output.ToString());
        }

        /// <summary>
        /// Generates a Daikon splitter file for a c-sharp source file.
        /// </summary>
        /// <param name="sourceFile">the path to the source file</param>
        /// <param name="splitterFile">the path to the output splitter file</param>
        /// <returns>the contents of the .spinfo file</returns>
        public static string GenerateSplitters(string sourceFile, string splitterFile = null)
        {
            Contract.Requires(sourceFile != null);
            Contract.Ensures(Contract.Result<string>() != null);

            string[] inputFiles = new string[1];
            inputFiles[0] = sourceFile;
            return GenerateFromSourceFiles(inputFiles, splitterFile);
        }

        private static string GenerateFromSourceFiles(string[] sourceFiles, string splitterFile)
        {
            Contract.Requires(sourceFiles != null);
            Contract.Requires(sourceFiles.Any());
            Contract.Ensures(Contract.Result<string>() != null);

            var replaceStatements = new Dictionary<string, string>();
            var programPointStatements = new Dictionary<string, HashSet<string>>();

            // First pass - generate replace statement section information.
            foreach (string sourceFile in sourceFiles)
            {
                var syntaxTree = SyntaxTree.ParseFile(sourceFile);
                GenerateReplaceStatements(syntaxTree, replaceStatements);
            }
            
            // Second pass - generate program point section information. 
            foreach (string sourceFile in sourceFiles)
            {
                var syntaxTree = SyntaxTree.ParseFile(sourceFile);
                GenerateProgramPoints(syntaxTree, programPointStatements, replaceStatements);
            }

            string spinfoContent = GenerateSplitterFileContent(programPointStatements, replaceStatements);

            if (!string.IsNullOrEmpty(splitterFile))
            {
                // Create the splitter file if specified.
                if(!Directory.Exists(Path.GetDirectoryName(splitterFile)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(splitterFile));
                }
                File.WriteAllText(splitterFile, spinfoContent);
            }

            return spinfoContent;
        }

        private static void GenerateReplaceStatements(SyntaxTree sourceTree, Dictionary<string, string> replaceStatements)
        {
            Contract.Requires(sourceTree != null);
            Contract.Requires(replaceStatements != null);

            var replaceWalker = new ReplaceStatementWalker(replaceStatements);
            replaceWalker.Visit(sourceTree.GetRoot());
        }

        private static void GenerateProgramPoints(SyntaxTree sourceTree, Dictionary<string, HashSet<string>> programPointStatements, 
            Dictionary<string, string> replaceStatements)
        {
            Contract.Requires(sourceTree != null);
            Contract.Requires(programPointStatements != null);

            var programPointWalker = new ProgramPointWalker(sourceTree, programPointStatements, replaceStatements);
            programPointWalker.Visit(sourceTree.GetRoot());
        }

        /// <summary>
        /// Creates the content of a splitter file given the program point statements and replace statements.
        /// </summary>
        /// <param name="programPointStatements">the program point statements</param>
        /// <param name="replaceStatements">the replace statements</param>
        /// <returns>the splitter file text content</returns>
        private static string GenerateSplitterFileContent(Dictionary<string, HashSet<string>> programPointStatements, Dictionary<string, string> replaceStatements)
        {
            Contract.Requires(programPointStatements != null);
            Contract.Requires(replaceStatements != null);

            StringBuilder spinfo = new StringBuilder();

            // Create the program point section.
            foreach (var pptName in programPointStatements.Keys)
            {
                HashSet<string> conditions = programPointStatements[pptName];
                if (conditions.Count != 0)
                {
                    string pptLine = string.Format(PPT_SECTION, pptName);
                    spinfo.AppendLine(pptLine);
                    foreach (string condition in conditions)
                    {
                        string _condition = condition.StripNewLinesAndExcessWhitespace();
                        spinfo.AppendLine(_condition);
                    }
                    spinfo.AppendLine();
                }
            }

            // Create the replace section. 
            if (replaceStatements.Keys.Count != 0)
            {
                spinfo.AppendLine(REPLACE_SECTION);

                foreach (var procedure in replaceStatements.Keys)
                {
                    string replacement = replaceStatements[procedure];
                    string _replacement = replacement.StripNewLinesAndExcessWhitespace();
                    spinfo.AppendLine(procedure);
                    spinfo.AppendLine(_replacement);
                }
            }

            return spinfo.ToString();
        }
    }
}

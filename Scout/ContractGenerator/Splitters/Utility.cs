﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Roslyn.Compilers.CSharp;

namespace ContractGenerator.Splitters
{
    public static class Utility
    {
        // Returns the expressions that make up statement. If statement is not a binary expression, then the 
        // only returned expression is statement, otherwise the binary expression is broken up recursively into its statements. 
        public static IEnumerable<ExpressionSyntax> FlattenConditionStatement(ExpressionSyntax statement)
        {
            Contract.Requires(statement != null);
            Contract.Ensures(Contract.Result<IEnumerable<ExpressionSyntax>>() != null);
            Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<ExpressionSyntax>>(), expression => expression != null));
            // If statement is not a binary expression, only 1 expression should be returned:
            Contract.Ensures(statement as BinaryExpressionSyntax != null || Contract.Result<IEnumerable<ExpressionSyntax>>().Count() == 1);

            List<ExpressionSyntax> statementExpressions = new List<ExpressionSyntax>();
            FlattenConditionStatement(statement, statementExpressions);
            return statementExpressions;
        }

        // Helper to ^^
        private static void FlattenConditionStatement(ExpressionSyntax expression, List<ExpressionSyntax> expressions)
        {
            var binaryExpression = expression as BinaryExpressionSyntax;
            if (binaryExpression != null)
            {
                FlattenConditionStatement(binaryExpression.Left, expressions);
                FlattenConditionStatement(binaryExpression.Right, expressions);
                return;
            }
            expressions.Add(expression);
        }

        /// <summary>
        /// Returns a string that represents the expression. Any sub-expression that is a method invocation of
        /// zero arguments has its ending parentheses removed.
        /// 
        /// this.Test() -> this.Test
        /// this.Test() && x > 50 -> this.Test && x > 50
        /// this.GetTest().Test() -> this.GetTest().Test
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static string FixConditionStatement(ExpressionSyntax expression)
        {
            var modifiedCondition = new StringBuilder();
            FixConditionStatement(expression, modifiedCondition);
            return modifiedCondition.ToString();
        }

        // Helper to ^^
        private static void FixConditionStatement(ExpressionSyntax expression, StringBuilder condition)
        {
            var binaryExpression = expression as BinaryExpressionSyntax;
            if (binaryExpression != null)
            {
                FixConditionStatement(binaryExpression.Left, condition);
                condition.Append(binaryExpression.OperatorToken.ToFullString());
                FixConditionStatement(binaryExpression.Right, condition);
                return;
            }

            // Remove parentheses from zero-arg method invocations.
            string exp = expression.ToFullString().Replace("()", string.Empty);
            condition.Append(exp);
        }

        // Returns true if the method is a one line return statement and sets returnCondition to syntax object representing the condition.
        public static bool IsBodyOneLineReturnStatement(BlockSyntax body, out ExpressionSyntax returnCondition)
        {
            Contract.Requires(body != null);
            Contract.Ensures(Contract.Result<bool>() || Contract.ValueAtReturn(out returnCondition) == null);

            var children = body.ChildNodes().ToList();
            bool oneLineReturn = children.Count == 1 && // There is a single child.
                                 children[0].Kind == SyntaxKind.ReturnStatement && // That is a return statement.
                                 children[0].ChildNodes().Count() == 1; // And actually returns a value (non-void).

            if (oneLineReturn)
            {
                // Get the return statement expression. The first child of the body is the return statement. The first child
                // of the return statement is the return condition. 
                returnCondition = (ExpressionSyntax)body.ChildNodes().First().ChildNodes().First();
            }
            else
            {
                returnCondition = null;
            }

            return oneLineReturn;
        }

        /// <summary>
        /// Gets the fully qualified name of a method (the program point point name for the method).
        /// </summary>
        /// <param name="methodNode">the method</param>
        /// <returns>the fully qualified method name</returns>
        public static string GetFullyQualifiedMethodName(MethodDeclarationSyntax methodNode)
        {
            Contract.Requires(methodNode != null);
            Contract.Ensures(!string.IsNullOrEmpty(Contract.Result<string>()));


            string pptName = string.Empty;

            // Build the program point name for this method by tracing back up the class/namespace tree.
            var hierarchy = methodNode.Ancestors()
                                 .Where(ancestor => ancestor.Kind == SyntaxKind.NamespaceDeclaration || ancestor.Kind == SyntaxKind.ClassDeclaration)
                                 .Reverse();

            foreach (SyntaxNode node in hierarchy)
            {
                if (node.Kind == SyntaxKind.NamespaceDeclaration)
                {
                    NamespaceDeclarationSyntax namespaceDeclaration = (NamespaceDeclarationSyntax)node;
                    pptName += namespaceDeclaration.Name + ".";
                }
                else
                {
                    ClassDeclarationSyntax classDeclaration = (ClassDeclarationSyntax)node;
                    int genericCount = GetNumberOfGenericClassParameters(classDeclaration);

                    if (genericCount == 0)
                    {
                        pptName += classDeclaration.Identifier + ".";
                    }
                    else
                    {
                        pptName += classDeclaration.Identifier + "`" + genericCount + ".";
                    }
                }
            }

            pptName += methodNode.Identifier.ToFullString();
            return pptName;
        }

        public static int GetNumberOfGenericClassParameters(ClassDeclarationSyntax clazz)
        {
            var typeParameterList = clazz.ChildNodes().FirstOrDefault(
                child => child.Kind == SyntaxKind.TypeParameterList);

            if (typeParameterList == null)
            {
                return 0;
            }
            else
            {
                return typeParameterList.ToFullString().Split(',').Length;
            }
        }

        /// <summary>
        /// Gets the fully qualified name of a property (the program point point name for the property).
        /// </summary>
        /// <param name="propertyNode">the property</param>
        /// <returns>the fully qualified property name</returns>
        public static string GetFullyQualifiedPropertyName(PropertyDeclarationSyntax propertyNode)
        {
            Contract.Requires(propertyNode != null);
            Contract.Ensures(!string.IsNullOrEmpty(Contract.Result<string>()));
            // TODO: This code is almost identical to GetFullyQualifiedMethodName.

            string pptName = string.Empty;

            // Build the program point name for this method by tracing back up the class/namespace tree.
            var hierarchy = propertyNode.Ancestors()
                                 .Where(ancestor => ancestor.Kind == SyntaxKind.NamespaceDeclaration || ancestor.Kind == SyntaxKind.ClassDeclaration)
                                 .Reverse();

            foreach (SyntaxNode node in hierarchy)
            {
                if (node.Kind == SyntaxKind.NamespaceDeclaration)
                {
                    NamespaceDeclarationSyntax namespaceDeclaration = (NamespaceDeclarationSyntax)node;
                    pptName += namespaceDeclaration.Name + ".";
                }
                else
                {
                    ClassDeclarationSyntax classDeclaration = (ClassDeclarationSyntax)node;
                    pptName += classDeclaration.Identifier + ".";
                }
            }

            pptName += propertyNode.Identifier.ToFullString().StripNewLinesAndExcessWhitespace();
            return pptName;          
        }

        // Remove new lines and excess whitespace from a string.
        public static string StripNewLinesAndExcessWhitespace(this string str)
        {
            return Regex.Replace(str.Replace(Environment.NewLine, " "), @"\s+", " ").Trim();
        }

        /// <summary>
        /// Gets the fully qualified name of every pure method defined in a Celeriac purity file.
        /// </summary>
        /// <param name="purityFile">the path to the purity file</param>
        /// <returns>a list of fully qualified methods that are pure</returns>
        public static IEnumerable<string> ParsePureMethods(string purityFile)
        {
            Contract.Ensures(Contract.Result<IEnumerable<string>>() != null);

            List<string> pureMethodNames = new List<string>();

            if (!String.IsNullOrEmpty(purityFile) && File.Exists(purityFile))
            {
                string[] purityEntries = File.ReadAllLines(purityFile);

                foreach (string purityEntry in purityEntries)
                {
                    // A purity entry has the form:
                    // {qualified_class_name},{assembly_name},{version},{culture},{public_key_token};{method_name}

                    try
                    {
                        string[] split = purityEntry.Split(';');
                        // Remove "get_" from pure properties.
                        string methodName = split[1].Replace("get_", string.Empty);
                        string qualifiedClassName = split[0].Split(',')[0];
                        pureMethodNames.Add(qualifiedClassName + "." + methodName);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        Debug.WriteLine(
                            string.Format("Malformed purity entry: {0}", purityEntry));
                    }
                }
            }
            else
            {
                Debug.WriteLine(
                    string.Format("Could not find purity file \"{0}\"", purityFile));
            }

            return pureMethodNames;
        }
    }
}

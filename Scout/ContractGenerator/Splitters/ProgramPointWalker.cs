﻿using System.Collections.Generic;
using Roslyn.Compilers.CSharp;

namespace ContractGenerator.Splitters
{
    internal sealed class ProgramPointWalker : SyntaxWalker
    {
        private static readonly string[] illegalConditionStatements =
        {
            //" is ",
            //"new ",
            //"cce.NonNull",
            //".Unify(",
            //"//",
            //".FromInt",
            //"FindBoolAttribute",
            //".Contains"
        };

        private static readonly Dictionary<string, string> conditionReplacements = new Dictionary<string,string>
        {
	        //{"Equals", "equals"},
	        //{"StartsWith", "startsWith"},
	        //{"EndsWith", "endsWith"},
        };

        /// <summary>
        /// Maps program point names to a list of conditions to split on within the program point.
        /// </summary>
        private readonly Dictionary<string, HashSet<string>> programPointStatements;

        /// <summary>
        /// Maps method calls to their replacement one line return statement.
        /// </summary>
        private Dictionary<string, string> replaceStatements;

        /// <summary>
        /// The name of the current program point being visited. 
        /// </summary>
        private string currentPpt;

        /// <summary>
        /// The syntax tree of the source code.
        /// </summary>
        private SyntaxTree sourceTree;

        public ProgramPointWalker(SyntaxTree sourceTree, Dictionary<string, HashSet<string>> programPointStatements, Dictionary<string, string> replaceStatements)
        {
            this.sourceTree = sourceTree;
            this.currentPpt = null;
            this.programPointStatements = programPointStatements;
            this.replaceStatements = replaceStatements;
        }

        public override void VisitMethodDeclaration(MethodDeclarationSyntax methodNode)
        {
            string pptName = Utility.GetFullyQualifiedMethodName(methodNode: methodNode);
            this.currentPpt = pptName;

            if (!programPointStatements.ContainsKey(pptName))
            {
                programPointStatements[pptName] = new HashSet<string>();
            }

            base.VisitMethodDeclaration(methodNode);
        }

        public override void VisitWhileStatement(WhileStatementSyntax node)
        {
            ExtractConditionStatement(node.Condition);
            base.VisitWhileStatement(node);
        }

        public override void VisitIfStatement(IfStatementSyntax node)
        {
            ExtractConditionStatement(node.Condition);
            base.VisitIfStatement(node);
        }


        public override void VisitConditionalExpression(ConditionalExpressionSyntax node)
        {
            ExtractConditionStatement(node.Condition);
            base.VisitConditionalExpression(node);
        }

        private void ExtractConditionStatement(ExpressionSyntax conditionStatement)
        {
            if (this.currentPpt == null)
            {
                return;
            }

            string modifiedCondition = Utility.FixConditionStatement(conditionStatement);

            foreach (string statementToReplace in conditionReplacements.Keys)
            {
                if (modifiedCondition.Contains(statementToReplace))
                {
                    modifiedCondition =
                        modifiedCondition.Replace(statementToReplace, conditionReplacements[statementToReplace]);
                }
            }

            bool isIllegal = false;
            foreach (string invalid in illegalConditionStatements)
            {
                if (modifiedCondition.Contains(invalid))
                {
                    isIllegal = true;
                }
            }

            if (modifiedCondition.Equals("true"))
            {
                isIllegal = true;
            }

            if (!isIllegal)
            {
                this.programPointStatements[this.currentPpt].Add(modifiedCondition);
            }
        }
    }
}

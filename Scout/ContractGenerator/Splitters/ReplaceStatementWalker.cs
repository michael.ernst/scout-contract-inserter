﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Roslyn.Compilers.CSharp;

namespace ContractGenerator.Splitters
{
    /// <summary>
    /// Walks the source tree to discover replace statements for a Daikon splitter file.
    /// Replace statements are generated for methods and getter properties that are only return statements. 
    /// </summary>
    public class ReplaceStatementWalker : SyntaxWalker
    {
        private readonly Dictionary<string, string> replaceStatements;

        public ReplaceStatementWalker(Dictionary<string, string> replaceStatements)
        {
            Contract.Requires(replaceStatements != null);
            this.replaceStatements = replaceStatements;
        }

        public static bool ValidReplaceIdentifier(string replaceIdentifier)
        {
            return !replaceIdentifier.Contains("=") &&
                   !replaceIdentifier.Contains("this ") &&
                   !replaceIdentifier.Contains(">") &&
                   !replaceIdentifier.Contains("<") &&
                   !replaceIdentifier.Contains(" is ") &&
                   !replaceIdentifier.Contains("new ");

        }

        // Visit methods.
        public override void VisitMethodDeclaration(MethodDeclarationSyntax methodNode)
        {
            // methodNode.Body will be null for interface methods with no implementation.
            if (methodNode.Body != null)
            {
                // We do not need to generate replace statements for pure methods defined in the Celeriac purity file. 
                string qualifiedMethodIdentifier = Utility.GetFullyQualifiedMethodName(methodNode);

                // Create a splitter REPLACE statement only if the method body is a one line return. 
                ExpressionSyntax returnCondition;
                if (Utility.IsBodyOneLineReturnStatement(methodNode.Body, out returnCondition))
                {
                    string methodIdentifier = methodNode.Identifier.ToFullString();
                    string parameterIdentifier = methodNode.ParameterList.ToFullString().StripNewLinesAndExcessWhitespace();
                    string replaceIdentifer = methodIdentifier + parameterIdentifier;

                    if(ValidReplaceIdentifier(replaceIdentifer))
                    {
                        AddReplaceStatement(replaceIdentifer, Utility.FixConditionStatement(returnCondition));
                    }
                }
            }
        }

        // Visit accessors of properties.
        public override void VisitAccessorDeclaration(AccessorDeclarationSyntax node)
        {
            // We only care about getter properties. 
            if (node.Keyword.Kind == SyntaxKind.GetKeyword)
            {
                if (node.Parent.Parent is PropertyDeclarationSyntax) // see below why we need this check.
                {
                    PropertyDeclarationSyntax property = (PropertyDeclarationSyntax)node.Parent.Parent;
                    string qualifiedPropertyName = Utility.GetFullyQualifiedPropertyName(property);


                    string propertyIdentifier = property.Identifier.ToFullString().StripNewLinesAndExcessWhitespace() + "()";

                    if (ValidReplaceIdentifier(propertyIdentifier))
                    {

                        // node.Body is null when the getter is automatically generated. 
                        if (node.Body == null)
                        {
                            // this.replaceStatements[propertyIdentifier] = propertyIdentifier;
                        }
                        else
                        {
                            ExpressionSyntax returnExpression;
                            if (Utility.IsBodyOneLineReturnStatement(node.Body, out returnExpression))
                            {
                                string _return = Utility.FixConditionStatement(returnExpression);
                                AddReplaceStatement(propertyIdentifier, _return);
                            }
                        }
                    }

                }
                else
                {
                    // node.Parent.Parent is IndexerDeclarationSyntax.
                    // Example from Microsoft Boogie when this case occurs:
                    //     public new ProcedureSummaryEntry/*!*/ this[int i] {
                    //        get {
                    //            Contract.Requires(0 <= i && i < Count);
                    //            Contract.Ensures(Contract.Result<ProcedureSummaryEntry>() != null);
                    //            return cce.NonNull((ProcedureSummaryEntry/*!*/)base[i]);
                    //       }
                }
            }
            base.VisitAccessorDeclaration(node);
        }

        private void AddReplaceStatement(string statement, string replacement)
        {
            if (ValidReplaceIdentifier(statement) &&
               ValidReplaceIdentifier(replacement) &&
               !statement.Equals(replacement))
            {
                this.replaceStatements[statement] = replacement;
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnvDTE;
using EnvDTE80;
using System.Diagnostics.Contracts;
using System.IO;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace ContractGenerator
{
    /// <summary>
    /// Represents a method parameter with a qualified type and name. Two parameters are equivalent if their
    /// qualified type and name are the same.
    /// </summary>
    /// <remarks>
    /// Unlike other Contract Inserter model entities, parameters are not associated with an
    /// entity in the Visual Studio code model.
    /// </remarks>
    [Serializable]
    public class Parameter
    {
        /// <summary>
        /// The fully qualified type of the parameter, such as <c>System.Int32</c> or <c>System.Array[]</c>.
        /// </summary>
        public string QualifiedTypeName { get; private set; }
        
        /// <summary>
        /// The short type name of the parameter (i.e., without namespace or containing class).
        /// </summary>
        public string SimpleTypeName { get; private set; }

        /// <summary>
        /// The name of the parameter.
        /// </summary>
        public string Name { get; private set; }

        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(QualifiedTypeName != null);
            Contract.Invariant(Name != null);
            Contract.Invariant(SimpleTypeName != null);
        }

        public Parameter(string qualifiedType, string name)
        {
            Contract.Requires(!string.IsNullOrEmpty(qualifiedType));
            Contract.Requires(!string.IsNullOrEmpty(name));

            QualifiedTypeName = qualifiedType;
            SimpleTypeName = qualifiedType.Split('.').Last();
            Name = name;
        }

        /// <summary>
        /// Returns <c>true</c> if <paramref name="other"/> has the same qualified type and name.
        /// </summary>
        /// <param name="other">another parameter</param>
        /// <returns><c>true</c> if <paramref name="other"/> has the same qualified type and name</returns>
        public bool Equals(Parameter other)
        {
            return QualifiedTypeName.Equals(other.QualifiedTypeName) && Name.Equals(other.Name);
        }

        public override bool Equals(object obj)
        {
            Parameter p = obj as Parameter;
            if (p == null)
            {
                return false;
            }
            return this.Equals(p);
        }

        public override int GetHashCode()
        {
            return QualifiedTypeName.GetHashCode() ^ Name.GetHashCode();
        }

        public override string ToString()
        {
            return QualifiedTypeName + " " + Name;
        }
    }

    /// <summary>
    /// An immutable representation of a method signature; two signatures are equivalent if the method name 
    /// and parameters are equivalent.
    /// </summary>
    [Serializable]
    public class Signature
    {
        public string QualifiedName { get; private set; }
        public ReadOnlyCollection<Parameter> Parameters { get; private set; }

        public Signature(string qualifiedName, ReadOnlyCollection<Parameter> parameters)
        {
            this.QualifiedName = qualifiedName;
            this.Parameters = new ReadOnlyCollection<Parameter>(parameters);
        }

        public Signature(string qualifiedName, List<Parameter> parameters)
            : this (qualifiedName, new ReadOnlyCollection<Parameter>(parameters))
        {
            // NOP
        }
        
        public override int GetHashCode()
        {
            int pHash = 19;
            foreach (var p in Parameters)
            {
                pHash = pHash * 31 + p.GetHashCode();
            }
            return QualifiedName.GetHashCode() ^ pHash;
        }

        public override bool Equals(object obj)
        {
            Signature method = obj as Signature;
            if (method == null)
            {
                return false;
            }

            return this.Equals(method);
        }

        /// <summary>
        /// Returns <c>true</c> iff the method names and parameters are equal.
        /// </summary>
        /// <param name="method">the other method, or <c>null</c></param>
        /// <returns><c>true</c> iff the method names and parameters are equal</returns>
        [Pure]
        public bool Equals(Signature method)
        {
            if (method == null)
            {
                return false;
            }
            else
            {
                return this.QualifiedName.Equals(method.QualifiedName) && Parameters.SequenceEqual(method.Parameters);
            }
        }

        public override string ToString()
        {
            return QualifiedName + "(" + string.Join(", ", Parameters.Select(p => p.ToString())) + ")";
        }
    }

    /// <summary>
    /// Represents a method in the project. Includes references to the containing type, as well
    /// as the corresponding contract method and interface methods, if any.
    /// </summary>
    public class Method
    {
        /// <summary>
        /// Reference to the corresponding method in the Visual Studio code model.
        /// </summary>
        public CodeFunction MethodRef { get; private set; }

        /// <summary>
        /// Reference to the corresponding contract class method in the Visual Studio code model, or <c>null</c>.
        /// </summary>
        public CodeFunction ContractMethodRef { get; set; }

        /// <summary>
        /// Reference to the corresponding interface method in the Visual Studio code model, or <c>null</c>.
        /// </summary>
        public CodeFunction InterfaceMethodRef { get; set; }

        /// <summary>
        /// Reference to the Visual Studio code model method this method overrides, or <c>null</c>.
        /// </summary>
        public CodeFunction OverrideMethodRef { get; private set; }

        /// <summary>
        /// The method source code.
        /// </summary>
        public SourceLocation MethodSource
        {
            get
            {
                return new SourceLocation(MethodRef);
            }
        }

        /// <summary>
        /// The source code of the corresponding contract class, or <c>null</c>.
        /// </summary>
        public SourceLocation ContractMethodSource
        {
            get
            {
                if (ContractMethodRef == null) return null;
                return new SourceLocation(ContractMethodRef);
            }
        }

        /// <summary>
        /// The source code of the corresponding interface, or <c>null</c>.
        /// </summary>
        public SourceLocation InterfaceMethodSource
        {
            get
            {
                if (InterfaceMethodRef == null) return null;
                return new SourceLocation(InterfaceMethodRef);
            }
        }

        /// <summary>
        /// The qualified name of the method, including the qualified name of the containing type.
        /// </summary>
        public string QualifiedName { get; private set; }

        /// <summary>
        /// The simple name of the method. Includes generics information, but not the qualified name
        /// of the containing type.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The method's parameters.
        /// </summary>
        public ReadOnlyCollection<Parameter> Parameters { get; private set; }

        /// <summary>
        /// <c>true</c> iff the method is abstract.
        /// </summary>
        public bool IsAbstract
        {
            get
            {
                try
                {
                    return MethodRef.MustImplement;
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// True if <paramref name="overrideRef"/> overrides <paramref name="baseRef"/>.
        /// </summary>
        [Pure]
        private static bool IsOverrideMatch(CodeFunction2 overrideRef, CodeFunction2 baseRef)
        {
            if (baseRef.CanOverride ||
                baseRef.MustImplement ||
                baseRef.OverrideKind != vsCMOverrideKind.vsCMOverrideKindNone)
            {
                if (baseRef.Name.Equals(overrideRef.Name) &&
                    baseRef.GetParameters().TypeEquals(overrideRef.GetParameters()) &&
                    overrideRef.FunctionKind == baseRef.FunctionKind)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Finds the method that <paramref name="methodRef"/> overrides.
        /// </summary>
        /// <param name="methodRef">the override method</param>
        /// <param name="containingType">the type that contains the overriden method</param>
        /// <returns>the overridden method, or null if no overriden is found</returns>
        [Pure]
        private static CodeFunction FindOverrideRef(CodeFunction2 methodRef, CodeType containingType)
        {
            try
            {
                if (methodRef.OverrideKind != vsCMOverrideKind.vsCMOverrideKindNone)
                {
                    foreach (CodeElement _base in containingType.Bases)
                    {
                        foreach (CodeElement2 e in _base.Children)
                        {
                            if (e.Kind == vsCMElement.vsCMElementFunction)
                            {
                                var baseMethod = e as CodeFunction2;

                                if (IsOverrideMatch(methodRef, baseMethod))
                                {
                                    return baseMethod;
                                }
                            }
                            else if (e.Kind == vsCMElement.vsCMElementProperty)
                            {
                                var property = e as CodeProperty;
                                Contract.Assert(property != null);

                                if (property.HasGetter())
                                {
                                    if (IsOverrideMatch(methodRef, property.Getter as CodeFunction2))
                                    {
                                        return property.Getter;
                                    }
                                }

                                if (property.HasSetter())
                                {
                                    if (IsOverrideMatch(methodRef, property.Setter as CodeFunction2))
                                    {
                                        return property.Setter;
                                    }
                                }
                            }
                        }
                    }

                    // We did not find the method one base class up. Try again one level higher, but only check the first base.
                    if (containingType.Bases.Count > 0)
                    {
                        CodeElement firstBase = containingType.Bases.Item(1); // Index is 1-based, not 0-based.
                        if (firstBase.IsCodeType)
                        {
                            return FindOverrideRef(methodRef, firstBase as CodeType);
                        }
                    }
                }
            }
            catch(NotImplementedException) { }

            return null;       
        }

        /// <summary>
        /// True if <paramref name="interfaceRef"/> is the interface method that
        /// <paramref name="implementedRef"/> implements.
        /// </summary>
        [Pure]
        private static bool IsInterfaceMatch(CodeFunction2 implementedRef, CodeFunction2 interfaceRef)
        {
            if (interfaceRef.MustImplement)
            {
                if (implementedRef.Name.Equals(interfaceRef.Name) &&
                    interfaceRef.GetParameters().TypeEquals(implementedRef.GetParameters()) &&
                    implementedRef.FunctionKind == interfaceRef.FunctionKind)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Finds the method that <paramref name="methodRef"/> implements.
        /// </summary>
        /// <param name="methodRef">the implemented method</param>
        /// <param name="containingType">the type that contains the implemented method</param>
        /// <returns>the interface method, or null if no corresponding interface method is found</returns>
        [Pure]
        private static CodeFunction FindInterfaceRef(CodeFunction2 methodRef, CodeType containingType)
        {
            try
            {
                if (containingType.Kind == vsCMElement.vsCMElementClass)
                {
                    CodeClass containingClass = containingType as CodeClass;
                    Contract.Assert(containingClass != null);

                    foreach (CodeElement _interface in containingClass.ImplementedInterfaces)
                    {
                        foreach (CodeElement2 e in _interface.Children)
                        {
                            if (e.Kind == vsCMElement.vsCMElementFunction)
                            {
                                var baseMethod = e as CodeFunction2;

                                if (IsInterfaceMatch(methodRef, baseMethod))
                                {
                                    return baseMethod;
                                }
                            }
                            else if (e.Kind == vsCMElement.vsCMElementProperty)
                            {
                                var property = e as CodeProperty;
                                Contract.Assert(property != null);

                                if (property.HasGetter())
                                {
                                    if (IsInterfaceMatch(methodRef, property.Getter as CodeFunction2))
                                    {
                                        return property.Getter;
                                    }
                                }

                                if (property.HasSetter())
                                {
                                    if (IsInterfaceMatch(methodRef, property.Setter as CodeFunction2))
                                    {
                                        return property.Setter;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (NotImplementedException)
            {

            }

            return null;     
        }

        public Method(CodeFunction methodRef, CodeType containingType)
        {
            Contract.Requires(methodRef != null);
            Contract.Requires(containingType != null);

            this.MethodRef = methodRef;
            this.ContractMethodRef = null;
            this.QualifiedName = Extensions.ReflectiveFullName(methodRef, containingType);
            this.Name = Extensions.ReflectiveName(methodRef, containingType);
            this.Parameters = new ReadOnlyCollection<Parameter>(new List<Parameter>(methodRef.GetParameters()));
            this.OverrideMethodRef = FindOverrideRef(methodRef as CodeFunction2, containingType);
            this.InterfaceMethodRef = FindInterfaceRef(methodRef as CodeFunction2, containingType);
        }

        // No MethodRef or InterfaceRef, just a ContractMethodRef.
        public Method(CodeType containingType, CodeFunction contractMethodRef)
        {
            Contract.Requires(containingType != null);
            Contract.Requires(contractMethodRef != null);

            this.ContractMethodRef = contractMethodRef;

            this.QualifiedName = Extensions.ReflectiveFullName(contractMethodRef, containingType);
            this.Name = Extensions.ReflectiveName(contractMethodRef, containingType);
            this.Parameters = new ReadOnlyCollection<Parameter>(new List<Parameter>(contractMethodRef.GetParameters()));
        }

        [Pure]
        public static Method PropertyGetter(CodeProperty propertyRef, CodeType containingType)
        {
            var m = new Method(propertyRef.Getter, containingType);
            return m;
        }

        [Pure]
        public static Method PropertySetter(CodeProperty propertyRef, CodeType containingType)
        {
            var m = new Method(propertyRef.Setter, containingType);
            return m;
        }

        /// <summary>
        /// The signature of the method using the qualified name for the method
        /// </summary>
        public Signature Signature
        {
            get
            {
                return new Signature(QualifiedName, Parameters);
            }
        }

        public override string ToString()
        {
            return Signature.ToString();
        }
    }

    /// <summary>
    /// An immutable descriptor of a C# Property using the property's getter and setter.
    /// </summary>
    public struct PropertyMethodDescriptor
    {
        public readonly Signature getter;
        public readonly Signature setter;

        public PropertyMethodDescriptor(Signature getter, Signature setter)
        {
            this.getter = getter;
            this.setter = setter;
        }

        public PropertyMethodDescriptor(Method getter, Method setter)
        {
            this.getter = getter != null ? getter.Signature : null;
            this.setter = setter != null ? setter.Signature : null;
        }
    }

    /// <summary>
    /// Represents a property in the project. Includes references to the corresponding contract method 
    /// and interface methods, if any. 
    /// </summary>
    public class Property
    {
        /// <summary>
        /// Reference to the corresponding property in the Visual Studio code model.
        /// </summary>
        public CodeProperty PropertyRef { get; private set; }

        /// <summary>
        /// Reference to the corresponding contract class method in the Visual Studio code model, or <c>null</c>.
        /// </summary>
        public CodeProperty ContractPropertyRef { get; set; }

        /// <summary>
        /// The source code of the property.
        /// </summary>
        public SourceLocation PropertySource
        {
            get
            {
                return new SourceLocation(PropertyRef);
            }
        }

        /// <summary>
        /// The source code of the corresponding contract class property, or <c>null</c>.
        /// </summary>
        public SourceLocation ContractPropertySource
        {
            get
            {
                if (ContractPropertyRef == null) return null;
                return new SourceLocation(ContractPropertyRef);
            }
        }

        /// <summary>
        /// The qualified name of the property.
        /// </summary>
        public string QualifiedName { get; private set; }

        /// <summary>
        /// The simple name of the property.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The corresponding getter method, or <c>null</c>.
        /// </summary>
        public Method Getter { get; private set; }

        /// <summary>
        /// The corresponding setter method, or <c>null</c>.
        /// </summary>
        public Method Setter { get; private set; }

        public Property(CodeProperty propertyRef, CodeType containingType)
        {
            Contract.Requires(propertyRef != null);
            Contract.Requires(containingType != null);

            this.PropertyRef = propertyRef;
            this.ContractPropertyRef = null;
            this.QualifiedName = Extensions.ReflectiveFullName(propertyRef, containingType);
            this.Name = Extensions.ReflectiveName(propertyRef, containingType);

            this.Getter = (propertyRef.Getter != null) ? Method.PropertyGetter(propertyRef, containingType) : null;
            this.Setter = (propertyRef.Setter != null) ? Method.PropertySetter(propertyRef, containingType) : null;
        }

        /// <summary>
        /// The <see cref="PropertyMethodDescriptor"/> for the property.
        /// </summary>
        public PropertyMethodDescriptor MethodDescriptor
        {
            get
            {
                return new PropertyMethodDescriptor(Getter, Setter);
            }
        }

        public override string ToString()
        {
            return this.QualifiedName;
        }
    }

    /// <summary>
    /// Represents a contract class for an interface. In C#, a contract class is an
    /// abstract class that is associated with an interface via the <c>ContractClassFor</c> attribute.
    /// </summary>
    public class ContractClass
    {
        /// <summary>
        /// A reference to the contract class in the Visual Studio code model.
        /// </summary>
        public CodeClass ContractClassRef { get; private set; }

        /// <summary>
        /// A reference to the interface in the Visual Studio code model for which the contract class provides contracts.
        /// </summary>
        public CodeType TypeRef { get; private set; }

        /// <summary>
        /// The source code of the contract class.
        /// </summary>
        public SourceLocation ContractSource
        {
            get
            {
                return new SourceLocation(ContractClassRef);
            }
        }

        /// <summary>
        /// The source code of the interface the contract class provides contracts for.
        /// </summary>
        public SourceLocation TypeSource
        {
            get
            {
                return new SourceLocation(TypeRef);
            }
        }

        /// <summary>
        /// A mapping from method signatures to the corresponding methods.
        /// </summary>
        public Dictionary<Signature, Method> Methods { get; private set; }

        /// <summary>
        /// A mapping from property descriptors to the corresponding properties.
        /// </summary>
        public Dictionary<PropertyMethodDescriptor, Property> Properties { get; private set; }

        /// <summary>
        /// The object invariant method, or <c>null</c>.
        /// </summary>
        public Method InvariantMethod { get; private set; }

        /// <summary>
        /// Create a <see cref="ContractClass"/> model for the given <c>CodeClass</c>.
        /// 
        /// NOTE: this method does not set <see cref="Interface.ContractClass"/> for the corresponding <see cref="Interface"/>; 
        /// you must use the <see cref="Interface.LinkContractClass"/> method.
        /// </summary>
        /// <param name="contractClass">The Visual Studio class model</param>
        /// <returns>The newly created <see cref="ContractClass"/></returns>
        public static ContractClass Wrap(CodeClass contractClass)
        {
            Contract.Requires(contractClass != null);
            Contract.Requires(contractClass.IsContractClass());
            Contract.Requires(contractClass.IsAbstract);
            Contract.Ensures(Contract.Result<ContractClass>() != null);

            // The typeof contract class should match the interface/class it inherits/derives from. 
            string interfaceName = GetTypeForName(contractClass);

            // Check interfaces
            foreach (CodeElement interfaceRef in contractClass.ImplementedInterfaces)
            {
                var baseType = interfaceRef as CodeType;
                if (baseType.TypeOfArgument().Equals(interfaceName))
                {
                    return new ContractClass(contractClass, baseType);
                }
            }

            // Check base classes
            foreach (CodeElement interfaceRef in contractClass.Bases)
            {
                var baseType = interfaceRef as CodeType;
                if (baseType.TypeOfArgument().Equals(interfaceName))
                {
                    return new ContractClass(contractClass, baseType);
                }
            }

            throw new ContractGenerationException("Could not find matching interface or base class for contract class " + contractClass.FullName);
        }

        /// <summary>
        /// Returns the name of the interface for which the contract class provides contracts. The name
        /// is taken from the attribute value of the <c>ContractClassFor</c> attribute.
        /// </summary>
        /// <param name="contractClass">the contract class</param>
        /// <returns>the name of the interface for which the contract class provides contracts.</returns>
        private static string GetTypeForName(CodeClass contractClass)
        {
            Contract.Requires(contractClass != null);
            Contract.Requires(contractClass.IsContractClass());
            Contract.Ensures(!string.IsNullOrEmpty(Contract.Result<string>()));

            foreach (CodeAttribute attribute in contractClass.Attributes)
            {
                if (attribute.Name.Equals("ContractClassFor"))
                {
                    string value = attribute.Value;
                    value = value.Replace("typeof(", string.Empty);
                    value = value.Replace(")", string.Empty);
                    return value;
                }
            }

            throw new ApplicationException("Could not locate the ContractClassFor attribute for the given contract class");
        }     

        /// <summary>
        /// Construct a contract class model for contract class <paramref name="contractClassRef"/> providing
        /// contracts for <paramref name="typeRef"/>. 
        /// 
        /// NOTE: this method does not set <see cref="Interface.ContractClass"/> for the corresponding <see cref="Interface"/>; 
        /// you must use the <see cref="Interface.LinkContractClass"/> method.
        /// </summary>
        /// <param name="contractClassRef">The Visual Studio model contract class</param>
        /// <param name="typeRef">The Visual Studio model interface</param>
        private ContractClass(CodeClass contractClassRef, CodeType typeRef)
        {
            this.ContractClassRef = contractClassRef;
            this.TypeRef = typeRef;
            this.Methods = new Dictionary<Signature, Method>();
            this.Properties = new Dictionary<PropertyMethodDescriptor, Property>();
            this.InvariantMethod = null;

            Func<CodeFunction, bool> IsImplementationMethod = m => typeRef is CodeClass && !m.MustImplement;

            // Create a model for each method and property
            foreach (CodeElement2 member in contractClassRef.Children)
            {
                if (member.Kind == vsCMElement.vsCMElementFunction)
                {
                    CodeFunction mRef = member as CodeFunction;

                    Method m = new Method(mRef, contractClassRef as CodeType);
                    this.Methods.Add(m.Signature, m);

                    if (mRef.IsClassInvariantMethod())
                    {
                        this.InvariantMethod = m;
                    }
                    
                } 
                else if (member.Kind == vsCMElement.vsCMElementProperty)
                {
                    Property p = new Property(member as CodeProperty, contractClassRef as CodeType);
                    this.Properties.Add(p.MethodDescriptor, p);
                }
            }

            // For each method/property in the interface or abstract class, set a reference from the contract class
            // method to the interface method.
            foreach (CodeElement2 typeMember in TypeRef.Members)
            {
                if (typeMember.Kind == vsCMElement.vsCMElementFunction)
                {
                    if (IsImplementationMethod(typeMember as CodeFunction))
                    {
                        continue;
                    }

                    var name = Extensions.ReflectiveName(typeMember as CodeFunction, TypeRef);
                    var m = Methods.Values.FirstOrDefault(e => e.Name.Equals(name));

                    if (m != null)
                    {
                        // User-defined contract classes might not have contract methods for every method
                        // NEED to create the method in this case?
                        m.InterfaceMethodRef = typeMember as CodeFunction;
                    }
                }
                else if (typeMember.Kind == vsCMElement.vsCMElementProperty)
                {
                    var typeProperty = typeMember as CodeProperty;
                    Contract.Assert(typeProperty != null);

                    if ((!typeProperty.HasGetter() || IsImplementationMethod(typeProperty.Getter))
                        && (!typeProperty.HasSetter() || IsImplementationMethod(typeProperty.Setter)))
                    {
                        continue;
                    }

                    var name = Extensions.ReflectiveName(typeMember as CodeProperty, TypeRef);
                    var p = Properties.Values.FirstOrDefault(e => e.Name.Equals(name));

                    if (p != null)
                    {
                        // User-defined contract classes might not have contract methods for every property
                        // NEED to create the method in this case?
                        p.ContractPropertyRef = typeMember as CodeProperty;
                    }
                }
            }
        }

        public override string ToString()
        {
            return ContractClassRef.Name + "[Contract for: " + TypeRef.Name + "]";
        }
    }

    public abstract class TypeModel
    {
        /// <summary>
        /// The superclasses and interfaces of the type.
        /// </summary>
        public List<CodeElement> DerivedFrom { get; private set; }

        /// <summary>
        /// The qualified name of the type. Uses .NET reflection style to match the types output by Celeriac.
        /// </summary>
        public string QualifiedName { get; private set; }

        /// <summary>
        /// The methods contained in the type.
        /// </summary>
        public List<Method> Methods { get; private set; }

        /// <summary>
        /// The properties contained in the type.
        /// </summary>
        public List<Property> Properties { get; private set; }

        /// <summary>
        /// The underlying Visual Studio type
        /// </summary>
        public CodeType TypeRef { get; private set; }

        /// <summary>
        /// The corresponding contract class, or <c>null</c>.
        /// </summary>
        public ContractClass ContractClass { get; private set; }

        /// <summary>
        /// A reference to the contract class in the Visual Studio code model.
        /// </summary>
        public CodeClass ContractClassRef
        {
            get
            {
                if (ContractClass != null)
                {
                    return ContractClass.ContractClassRef;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// <c>true</c> if the type has generic parameters.
        /// </summary>
        public abstract bool IsGeneric { get; }

        /// <summary>
        /// The object invariant methods.
        /// </summary>
        public List<Method> InvariantMethods { get; private set; }

        protected TypeModel(CodeType typeRef)
        {
            Contract.Requires(typeRef != null);

            this.TypeRef = typeRef;
            this.QualifiedName = typeRef.ReflectiveFullName();
            this.DerivedFrom = new List<CodeElement>();
            this.Methods = new List<Method>();
            this.InvariantMethods = new List<Method>();
            this.Properties = new List<Property>();       
            
            foreach (CodeElement c in typeRef.Bases)
            {
                this.DerivedFrom.Add(c);
            }

            foreach (CodeElement2 e in typeRef.Children)
            {
                if (e.Kind == vsCMElement.vsCMElementFunction)
                {
                    AddMethod(e as CodeFunction, typeRef);
                }
                else if (e.Kind == vsCMElement.vsCMElementProperty)
                {
                    AddProperty(e as CodeProperty, typeRef);
                }
            }
        }

        /// <summary>
        /// Add a method to this type.
        /// </summary>
        /// <param name="methodToAdd">The method to add</param>
        /// <param name="typeRef">The parent type</param>
        public void AddMethod(CodeFunction methodToAdd, CodeType typeRef)
        {
            Method newMethod = new Method(methodToAdd, typeRef);
            this.Methods.Add(newMethod);

            if (methodToAdd.IsClassInvariantMethod())
            {
                if (InvariantMethods == null)
                {
                    InvariantMethods = new List<Method>();
                }
                InvariantMethods.Add(newMethod);
            }
        }

        /// <summary>
        /// Add a parameter to this type.
        /// </summary>
        /// <param name="parameterToAdd">The parameter to add</param>
        /// <param name="typeRef">The parent type</param>
        public void AddProperty(CodeProperty parameterToAdd, CodeType typeRef)
        {
            this.Properties.Add(new Property(parameterToAdd, typeRef));
        }

        /// <summary>
        /// Add methods, properties, superclass, and interface information from type definition <paramref name="otherType"/>
        /// for this type.
        /// </summary>
        /// <param name="otherType">The other type definition.</param>
        public void AddTypeInformation(TypeModel otherType)
        {
            Contract.Requires(otherType != null);
            Contract.Requires(QualifiedName.Equals(otherType.QualifiedName));

            Methods.AddRange(otherType.Methods);
            Properties.AddRange(otherType.Properties);
            DerivedFrom.AddRange(otherType.DerivedFrom.Where(t => !t.FullName.Equals("System.Object")));
        }

        /// <summary>
        /// If the <paramref name="contractClass"/> is a contract class for this type, links methods (properties)
        /// from this interface to the corresponding contract class methods (properties). Returns <c>true</c> iff 
        /// <paramref name="contractClass"/> is a contract class for this interface.
        /// </summary>
        /// <param name="contractClass">the contract class</param>
        /// <param name="createdLinks">the method/properties that were linked. Will be <c>null</c> if return is false.</param>
        /// <returns>
        ///     <c>true</c> iff <paramref name="contractClass"/> is a contract class for this interface.
        /// </returns>
        public bool LinkContractClass(ContractClass contractClass, out Dictionary<Signature, Signature> createdLinks)
        {
            Contract.Requires(contractClass != null);

            // Check if contractClass is the contract class for this interface.
            if (!QualifiedName.Equals(contractClass.TypeRef.ReflectiveFullName()))
            {
                createdLinks = null;
                return false;
            }

            var links = new Dictionary<Signature, Signature>();
            links.Add(contractClass.ContractClassRef.Signature(), contractClass.TypeRef.Signature());
            this.ContractClass = contractClass;

            foreach (Method declaration in Methods)
            {
                if (this is Class && !declaration.IsAbstract)
                {
                    continue;
                }

                var contractClassMethod = contractClass.Methods.Values.FirstOrDefault(e => e.Name.Equals(declaration.Name));
                bool match = contractClassMethod != null && contractClassMethod.Parameters.SequenceEqual(declaration.Parameters);

                if (match)
                {
                    declaration.ContractMethodRef = contractClassMethod.MethodRef;
                    // Link from contract class method to declaration method.
                    links.Add(contractClassMethod.Signature, declaration.Signature);
                }
            }

            foreach (Property declaration in Properties)
            {
                if ((declaration.Getter == null || !declaration.Getter.IsAbstract)
                    && (declaration.Setter == null || !declaration.Setter.IsAbstract))
                {
                    continue;
                }

                var contractClassProperty = contractClass.Properties.Values.FirstOrDefault(e => e.Name.Equals(declaration.Name));

                if (contractClassProperty != null)
                {
                    declaration.ContractPropertyRef = contractClassProperty.PropertyRef;

                    if (contractClassProperty.Getter != null)
                    {
                        Contract.Assert(declaration.Getter != null);
                        declaration.Getter.ContractMethodRef = contractClassProperty.Getter.MethodRef;
                        // Link from contract property getter to declaration property getter.
                        links.Add(contractClassProperty.Getter.Signature, declaration.Getter.Signature);
                    }

                    if (contractClassProperty.Setter != null)
                    {
                        Contract.Assert(declaration.Setter != null);
                        declaration.Setter.ContractMethodRef = contractClassProperty.Setter.MethodRef;
                        // Link from contract property setter to declaration property setter.
                        links.Add(contractClassProperty.Setter.Signature, declaration.Setter.Signature);
                    }
                }
            }

            createdLinks = links;
            return true;
        }

        public override string ToString()
        {
            return QualifiedName;
        }
    }

    public class Interface : TypeModel
    {
        /// <summary>
        /// A reference to the class in the Visual Studio code model.
        /// </summary>
        public CodeInterface InterfaceRef { get; private set; }

        public SourceLocation InterfaceSource
        {
            get
            {
                return InterfaceRef == null ? null : new SourceLocation(InterfaceRef);
            }
        }

        public override bool IsGeneric
        {
            get
            {
                var codeInterface = InterfaceRef as CodeInterface2;
                return codeInterface != null && codeInterface.IsGeneric;
            }
        }

        public Interface(CodeInterface interfaceRef)
             : base(interfaceRef as CodeType)
        {
            Contract.Requires(interfaceRef != null);
            this.InterfaceRef = interfaceRef;
        }
    }

    /// <summary>
    /// Represents a class.
    /// </summary>
    public class Class : TypeModel
    {
        /// <summary>
        /// A reference to the class in the Visual Studio code model.
        /// </summary>
        public CodeClass ClassRef { get; private set; }

        /// <summary>
        /// The source code of the class.
        /// </summary>
        public SourceLocation ClassSource
        {
            get
            {
                return new SourceLocation(ClassRef);
            }
        }

        public bool IsAbstract
        {
            get
            {
                return ClassRef.IsAbstract;
            }
        }

        public override bool IsGeneric
        {
            get
            {
                var codeClass = ClassRef as CodeClass2;
                return codeClass != null && codeClass.IsGeneric;
            }
        }

        public Class(CodeClass classRef)
            : base(classRef as CodeType)
        {
            Contract.Requires(classRef != null);

            this.ClassRef = classRef;

            foreach (CodeElement i in classRef.ImplementedInterfaces)
            {
                this.DerivedFrom.Add(i);
            }
        }
    }

    /// <summary>
    /// A collection of  <see cref="TypeModel"/>; contains no duplicates, if a type
    /// is added multiple times, its member information is added to the existing entry.
    /// </summary>
    public class TypeCollection : IEnumerable<TypeModel>
    {
        private readonly List<TypeModel> types;

        public IEnumerator<TypeModel> GetEnumerator()
        {
            return types.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Create an empty list of types.
        /// </summary>
        public TypeCollection()
        {
            types = new List<TypeModel>();
        }

        /// <summary>
        /// Add a type to the collection. If a type with the same qualified name is already
        /// in the collection, the information is added to that type.
        /// </summary>
        /// <param name="type">the type to add</param>
        public void Add(TypeModel type)
        {
            foreach (TypeModel t in types)
            {
                if (t.QualifiedName.Equals(type.QualifiedName))
                {
                    t.AddTypeInformation(type);
                    return;
                }
            }
            types.Add(type);
        }
    }

    /// <summary>
    /// Represents an entry in the purity file passed to Celeriac. Each entry in the file has the form:
    /// "[namespace.class],[fully qualified assembly name];[function/property name]"
    /// </summary>
    public class PurityEntry
    {
        /// <summary>
        /// The simple name of the type.
        /// </summary>
        public string ClassQualifiedName { get; private set; }

        /// <summary>
        /// The qualified name of the assembly.
        /// </summary>
        public string AssemblyQualifiedName { get; private set; }

        /// <summary>
        /// The name of the pure method.
        /// </summary>
        public string MethodName { get; private set; }

        // Matches a purity entry.
        private static readonly Regex purityEntryRegex = new Regex("([\\w.]*),(.*);(.*)");

        /// <summary>
        /// Creates a purity file located at <paramref name="path"/> with the <paramref name="entries"/>
        /// as its content.
        /// </summary>
        /// <param name="path">the path to create the purity file</param>
        /// <param name="entries">the content of the purity file</param>
        public static void CreatePurityFile(string path, IEnumerable<PurityEntry> entries)
        {
            using (StreamWriter file = new StreamWriter(path)) 
            {
                foreach (var e in entries)
                {
                    file.WriteLine(e.ToString());
                }
            }
        }

        /// <summary>
        /// Reads the purity file located at <paramref name="path"/>.
        /// </summary>
        /// <param name="path">the path of the purity file</param>
        /// <returns>the purity entries of the purity file</returns>
        public static HashSet<PurityEntry> ReadPurityFile(string path)
        {
            HashSet<PurityEntry> purityEntries = new HashSet<PurityEntry>();

            if(File.Exists(path))
            {
                string[] entries = File.ReadAllLines(path);
                foreach (string entry in entries)
                {
                    PurityEntry _entry = FromString(entry);
                    if (entry != null)
                    {
                        purityEntries.Add(_entry);
                    }
                }
            }

            return purityEntries;
        }

        /// <summary>
        /// Initializes a purity entry from a string.
        /// </summary>
        /// <param name="purityEntry">the purity entry string</param>
        /// <returns>the newly initialized purity entry, or null if the purity string could not be parsed</returns>
        public static PurityEntry FromString(string purityEntry)
        {
            Contract.Requires(!string.IsNullOrEmpty(purityEntry));

            var match = purityEntryRegex.Match(purityEntry);
            if(match.Success)
            {
                string classQualifiedName = match.Groups[1].Value;
                string assemblyQualifiedName = match.Groups[2].Value.Trim();
                string methodName = match.Groups[3].Value;
                return new PurityEntry(classQualifiedName, assemblyQualifiedName, methodName);
            }
            return null;
        }

        public PurityEntry(string classQualifiedName, string assemblyQualifiedName, string methodName)
        {
            this.ClassQualifiedName = classQualifiedName;
            this.AssemblyQualifiedName = assemblyQualifiedName;
            this.MethodName = methodName;
        }

        public override string ToString()
        {
            return ClassQualifiedName + ", " + AssemblyQualifiedName + ";" + MethodName;
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            PurityEntry other = (PurityEntry)obj;
            if (other == null)
            {
                return false;
            }

            return this.ToString().Equals(other.ToString());
        }
    }
}

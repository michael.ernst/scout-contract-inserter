﻿using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Threading;

namespace ContractGenerator
{
    /// <summary>
    /// The results of running a process.
    /// </summary>
    public class RunProcessResults
    {
        public bool Success { get; private set; }
        public string StdOut { get; private set; }
        public string StdError { get; private set; }
        public Exception Exception { get; private set; }

        public RunProcessResults(bool success, string stdOut, string stdErr, Exception exception)
        {
            Contract.Requires(stdOut != null);
            Contract.Requires(stdErr != null);
            this.Success = success;
            this.StdOut = stdOut;
            this.StdError = stdErr;
            this.Exception = exception;
        }
    }

    /// <summary>
    /// Runs a process. 
    /// </summary>
    [ComVisible(false)]
    public sealed class ProcessRunner : Process
    {
        public string Command { get; private set; }
        public string Name { get; private set; }
        private string arguments;

        /// <summary>
        /// Creates a process to run the executable.
        /// </summary>
        /// <param name="processName">the name of the process</param>
        /// <param name="baseExecutable">the executable to run</param>
        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        public ProcessRunner(string processName, string baseExecutable)
        {
            Contract.Requires(baseExecutable != null);

            StartInfo.CreateNoWindow = true;
            StartInfo.RedirectStandardOutput = true;
            StartInfo.RedirectStandardError = true;
            StartInfo.UseShellExecute = false;
            StartInfo.FileName = baseExecutable;
            this.Command = baseExecutable + " ";
            this.Name = processName;
        }

        /// <summary>
        /// Adds an argument.
        /// </summary>
        /// <param name="argument">the argument to add</param>
        public void AddArgument(string argument)
        {
            Contract.Requires(argument != null);

            this.arguments += argument + " ";
            this.Command += argument + " ";
        }

        public void AddArgument(IConvertible flag, string value)
        {
            Contract.Requires(flag != null);
            Contract.Requires(value != null);

            var argument = EnumArgument.GetArgument(flag, value);
            Contract.Assume(argument != null, "Flag " + flag + " with value '" + value + "' has no argument");
            AddArgument(argument);
        }

        public void AddArgument(IConvertible flag)
        {
            Contract.Requires(flag != null);

            var argument = EnumArgument.GetArgument(flag);
            Contract.Assume(argument != null, "Flag " + flag + " has no argument");
            AddArgument(argument);
        }

        /// <summary>
        /// Runs the process.
        /// </summary>
        /// <returns>If success, returns false and the standard output. If failure, returns true and an error message</returns>
        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        public RunProcessResults RunProcess()
        {
            StartInfo.Arguments = arguments;
            LoaderStreamReader stdout = null;
            LoaderStreamReader stderr = null;
            string stdErrorText = string.Empty;
            string stdOutText = string.Empty;

            try
            {
                Start();
                PriorityClass = ProcessPriorityClass.High;

                stdout = new LoaderStreamReader(StandardOutput);
                stderr = new LoaderStreamReader(StandardError);
                Thread stdoutThread = new Thread(stdout.Go);
                Thread stderrThread = new Thread(stderr.Go);
                stdoutThread.Start();
                stderrThread.Start();
                stdoutThread.Join();
                stderrThread.Join();
                WaitForExit();
                stdErrorText = stderr.Text;
                stdOutText = stdout.Text;
            }
            catch (Exception exception)
            {
                return new RunProcessResults(false, stdOutText, stdErrorText, exception);
            }
            finally
            {
                if (stdout != null)
                {
                    stdout.Close();
                }

                if (stderr != null)
                {
                    stderr.Close();
                }
            }

            return new RunProcessResults(true, stdOutText, stdErrorText, null);
        }

        /// <summary>
        /// Stream reader that avoids deadlocks when reading from standard error and output simultaneously. 
        /// </summary>
        private class LoaderStreamReader
        {
            private readonly StreamReader sr;
            public string Text { get; private set; }

            public LoaderStreamReader(StreamReader sr)
            {
                Contract.Requires(sr != null);
                this.sr = sr;
            }

            public void Go()
            {
                Text = sr.ReadToEnd();
            }

            public void Close()
            {
                if (sr != null)
                {
                    sr.Dispose();
                }
            }
        }
    }
}

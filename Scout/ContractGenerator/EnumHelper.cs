﻿using System;
using System.Globalization;
using System.Reflection;
using System.Diagnostics.Contracts;

namespace ContractGenerator
{
    public interface IAttribute<out T>
    {
        T Value { get; }
    }

    /// <summary>
    /// An attribute that holds an integer representing the number of required arguments. 
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class NumArgsAttribute : Attribute, IAttribute<int>
    {
        private readonly int numberOfArguments;

        public NumArgsAttribute(int value)
        {
            this.numberOfArguments = value;
        }

        public int Value
        {
            get { return this.numberOfArguments; }
        }
    }

    /// <summary>
    /// An attribute that holds a string representing the text of a command line flag. 
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class FlagAttribute : Attribute, IAttribute<string>
    {
        private readonly string flag;

        public FlagAttribute(string value)
        {
            this.flag = value;
        }

        public string Value
        {
            get { return this.flag; }
        }
    }
   
    /// <summary>
    /// Helper class used by the FrontEndLoader to manage the construction of command line arguments. 
    /// </summary>
    public static class EnumArgument
    {
        /// <summary>
        /// Retrieves the value of an attribute off of an enumeration flag.
        /// </summary>
        /// <typeparam name="T">The attribute type</typeparam>
        /// <typeparam name="R">The value type</typeparam>
        /// <param name="enum">The enumeration flag that has attribute <typeparamref name="T"/> with value of type <typeparamref name="R"/></param>
        /// <returns>The value of type <typeparamref name="T"/> of attribute <typeparamref name="R"/> off of <paramref name="enum"/></returns>
        private static R GetAttributeValue<T, R>(IConvertible @enum)
        {
            R attributeValue = default(R);

            if (@enum != null)
            {
                FieldInfo fi = @enum.GetType().GetField(@enum.ToString(CultureInfo.InvariantCulture));

                if (fi == null) return attributeValue;
                T[] attributes = fi.GetCustomAttributes(typeof(T), false) as T[];

                if (attributes == null || attributes.Length <= 0) return attributeValue;
                IAttribute<R> attribute = attributes[0] as IAttribute<R>;

                if (attribute != null)
                {
                    attributeValue = attribute.Value;
                }
            }

            return attributeValue;
        }

        /// <summary>
        /// Gets the command line argument designated by <paramref name="flag"/> and <paramref name="value"/>.
        /// Ensures that <paramref name="flag"/> expects a single value. 
        /// </summary>
        /// <param name="flag">The enumeration flag (contains a NumArgsAttribute and FlagAttribute)</param>
        /// <param name="value">The vaue of the argument</param>
        /// <returns>The command line argument (flag text + value)</returns>
        public static string GetArgument(IConvertible flag, string value)
        {
            Contract.Requires(flag != null);
            Contract.Requires(value != null);
            int numArgs = EnumArgument.GetAttributeValue<NumArgsAttribute, int>(flag);
            string description = EnumArgument.GetAttributeValue<FlagAttribute, string>(flag);
            if (numArgs != 1)
            {
                throw new ArgumentException(
                    string.Format("Flag {0} requires {1} argument(s) when 1 argument was supplied", flag, numArgs));
            }
            return description + value;
        }

        /// <summary>
        /// Gets the command line argument designated by <paramref name="flag"/>.
        /// Ensures that <paramref name="flag"/> expects no values. 
        /// </summary>
        /// <param name="flag">The enumeration flag (contains a NumArgsAttribute and FlagAttribute)</param>
        /// <returns>The command line argument (the flag text)</returns>
        public static string GetArgument(IConvertible flag)
        {
            Contract.Requires(flag != null);
            int numArgs = EnumArgument.GetAttributeValue<NumArgsAttribute, int>(flag);
            string description = EnumArgument.GetAttributeValue<FlagAttribute, string>(flag);
            if (numArgs != 0)
            {
                throw new ArgumentException(
                    string.Format("Flag {0} requires {1} argument(s)", flag, numArgs));
            }
            return description;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;
using System.Collections.ObjectModel;
using ContractGenerator.Classification;

namespace ContractGenerator
{
    public enum ContractKind 
    { 
        Ensures, 
        Requires, 
        Invariant, 
        None 
    };

    public enum InvariantType
    {
        All,
        Unary,
        Binary,
        Ternary,
        BinaryAndTernary,
        TypeOf
    }

    /// <summary>
    /// A program element is a collection of program contracts for a single method
    /// (property getters and setters are also considered methods).
    /// 
    /// For every method with generated contracts, there is an associated program element.
    /// 
    /// In some cases, the method may not exist yet in the source code. This can be true
    /// for object invariant methods and implicit default constructors.
    /// When a contract is inserted for a missing method, it is created in the source code
    /// (so it is no longer missing).
    /// 
    /// Interface and abstract methods are also a special case, because contracts
    /// cannot be added to such methods. Instead, contracts are added to a special
    /// class called a contract class, which implements the interface or abstract class
    /// and provides contracts for the type. It is important to make the distinction
    /// between the interface/abstract method, and the contract class method because
    /// there is no associated program element for the contract class method, only the
    /// interface/abstract method.
    /// </summary>
    public class ProgramElement
    {
        public const string MSG_CANNOT_INSERT_GENERIC_CONTRACT_CLASS = "Creation of contract classes for generic interfaces is not supported; to add contracts, manually add a contract class and rerun this tool.";

        #region Object Invariants

        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(project != null);
            Contract.Invariant(parent != null);
            Contract.Invariant(Contracts != null);
            Contract.Invariant(Parameters != null);
            Contract.Invariant(Contract.ForAll(Contracts, contract => contract != null));
            Contract.Invariant(Contract.ForAll(Parameters, parameter => parameter != null));
            Contract.Invariant(preCount >= 0);
            Contract.Invariant(postCount >= 0);
            Contract.Invariant(invariantCount >= 0);
            Contract.Invariant(invariantCount == 0 || (preCount == 0) && (postCount == 0));
            Contract.Invariant(stateCheck());
        }

        /// <summary>
        /// If CreateInvariantMethod or CreateDefaultConstructor is true, then Method and Property must be null.
        /// Otherwise either one of Method or Property must not be null. 
        /// </summary>
        /// <returns>
        /// <c>true</c> if the above condition holds, <c>false</c> otherwise
        /// </returns>
        [Pure]
        private bool stateCheck()
        {
            if (CreateInvariantMethod || CreateDefaultConstructor)
            {
                if (method != null || property != null)
                {
                    return false;
                }
            }
            else
            {
                if (method == null && property == null)
                {
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region Events 

        /// <summary>
        /// Raised when the filters of this element are modified via
        /// <see cref="ProgramElement.AddFilter"/> or <see cref="ProgramElement.RemoveFilter"/>
        /// </summary>
        public event EventHandler<FilterModifiedArgs> FiltersModified;

        /// <summary>
        /// Raised when a contract is inserted or deleted via
        /// <see cref="ProgramElement.InsertContract"/> or <see cref="ProgramElement.DeleteContract"/>
        /// </summary>
        public event EventHandler<ContractModifiedArgs> ContractModified;

        /// <summary>
        /// Raises the filter modified event.
        /// </summary>
        /// <param name="e">event information, contains the modified filter</param>
        private void OnFilterModified(FilterModifiedArgs e)
        {
            EventHandler<FilterModifiedArgs> handler = FiltersModified;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises the contract modified event.
        /// </summary>
        /// <param name="e">event information</param>
        private void OnContractModified(ContractModifiedArgs e)
        {
            EventHandler<ContractModifiedArgs> handler = ContractModified;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Handle the contract modified event of contained ProgramContracts.
        /// Attached to the ContractModified event of each ProgramContract as it is inserted into this.  
        /// </summary>
        /// <param name="sender">The ProgramContract that has been modified</param>
        /// <param name="e">event information, contains the modified contract and its modification</param>
        /// <remarks>
        /// Propogates event.
        /// </remarks>
        private void HandleContractModified(object sender, ContractModifiedArgs e)
        {
            OnContractModified(e);
        }

        #endregion

        /// <summary>
        /// The fully-qualified method signature.
        /// </summary>
        public Signature Signature
        {
            get
            {
                return new Signature(CeleriacQualifiedName, Parameters);
            }
        }

        /// <summary>
        /// The method parameters.
        /// </summary>
        public ReadOnlyCollection<Parameter> Parameters { get; private set; }

        /// <summary>
        /// A reference to the method's contracts.
        /// </summary>
        public List<ProgramContract> Contracts { get; private set; }

        /// <summary>
        /// Variables referred to by the contracts for this method.
        /// </summary>
        private readonly SortedSet<string> sourceVariables;

        /// <summary>
        /// Fields and parameters referred to by the contracts for this method; does not include derived expressions
        /// such as <c>Contract.OldValue</c>.
        /// </summary>
        /// <seealso cref="ProgramElement.Variables"/>
        public IEnumerable<string> SourceVariables
        {
            get
            {
                return sourceVariables;
            }
        }

        /// <summary>
        /// Variables and derived variables referred to by the contracts for this method.
        /// </summary>
        private readonly SortedSet<String> variables;

        /// <summary>
        /// Fields and parameters referred to by the contracts for this method.
        /// </summary>
        /// <seealso cref="ProgramElement.SourceVariables"/>
        public IEnumerable<string> Variables
        {
            get
            {
                return variables;
            }
        }

        /// <summary>
        /// Contract filters for the method.
        /// </summary>
        private readonly List<ContractFilter> filters;

        /// <summary>
        /// A read-only view of the active invariant filters.
        /// </summary>
        public ReadOnlyCollection<ContractFilter> Filters
        {
            get
            {
                return filters.AsReadOnly();
            }
        }

        /// <summary>
        /// The number of method preconditions (<c>Contract.Requires</c> statements).
        /// </summary>
        private int preCount;

        /// <summary>
        /// The number of method postconditions (<c>Contract.Ensures</c> statements).
        /// </summary>
        private int postCount;

        /// <summary>
        /// The number of object invariants (<c>Contract.Invariant</c> statements).
        /// </summary>
        private int invariantCount;

        private readonly Dictionary<ContractKind, int> unfilteredCounts;

        /// <summary>
        /// Determines the relative ordering of inserted contracts.
        /// It maps contract text to the index that text should appear in the source code. 
        /// </summary>
        private readonly Dictionary<string, int> contractIndices;

        /// <summary>
        /// The full name of the method.
        /// </summary>
        private string CeleriacQualifiedName { get; set; }

        /// <summary>
        /// <c>True</c> if an object invariant method should be created when a contract is added.
        /// </summary>
        public bool CreateInvariantMethod { get; private set; }

        /// <summary>
        /// <c>True</c> if a contract class should be created when a contract is added. I.e., the method
        /// is an interface declaration with no associated contract class.
        /// </summary>
        public bool MustCreateContractClass
        {
            get
            {
                return IsInterfaceOrAbstractMethod && ContractClassElementSource == null;
            }
        }

        /// <summary>
        /// <c>True</c> if this method is an interface or abstract method.
        /// </summary>
        public bool IsInterfaceOrAbstractMethod
        {
            get
            {
                return parent is Interface || (method != null && method.IsAbstract);
            }
        }

        /// <summary>
        /// <c>True</c> if a default constructor should be created when a contract is added.
        /// </summary>
        public bool CreateDefaultConstructor { get; private set; }

        /// <summary>
        /// <c>True</c> iff the method is an object invariant.
        /// </summary>
        public bool IsObjectInvariant { get; private set; }

        /// <summary>
        /// The source location where contracts are added.
        /// For classes, contracts are added to the method/property implementation.
        /// For interfaces, contracts are added to the contract class.
        /// </summary>
        private SourceLocation ContractLocation
        {
            get
            {
                return ContractClassElementSource ?? ElementSource;
            }
        }

        /// <summary>
        /// The source code of the method or property this represents, or
        /// <c>null</c> if this is a method that does not yet exist (such as an object invariant method). 
        /// </summary>
        private SourceLocation ElementSource
        {
            get
            {
                if (Method != null) return Method.MethodSource;
                return null;
            }
        }

        /// <summary>
        /// The source code of the corresponding contract class method (or property), or <c>null</c>
        /// if there is no corresponding contract class.
        /// </summary>
        private SourceLocation ContractClassElementSource
        {
            get
            {
                if (Method != null)
                {
                    return Method.ContractMethodSource;
                }
                if (parent.ContractClassRef != null)
                {
                    // This case is for the object invariant method for an interface.
                    // Before the invariant method is created in the contract class, this points to
                    // the contract class itself. After the invariant method is created, this will point
                    // to the object invariant method created in the contract class.
                    return parent.ContractClass.ContractSource;
                }
                
                return null;
            }
        }

        /// <summary>
        /// The underlying project model.
        /// </summary>
        private readonly ProjectModel project;

        /// <summary>
        /// The parent type of this method or property. Either a Class or an Interface. 
        /// </summary>
        private TypeModel parent;

        /// <summary>
        /// The method contracts are being applied to, or <c>null</c> if
        /// the method does not exist yet (such as an object invariant method)
        /// or if this is a property. 
        /// </summary>
        private Method method;

        /// <summary>
        /// The property contracts are being applied to, or <c>null</c> if
        /// this is a method that is not a getter/setter.
        /// </summary>
        private Property property;

        /// <summary>
        /// The method contracts are being applied to.
        /// 
        /// TODO: The distinction between method, property, and Method is confusing.
        /// In the future, this.property should be removed because we actually only 
        /// care about the getter or setter method.
        /// </summary>
        public Method Method
        {          
            get
            {
                if(method != null) 
                {
                    return method;
                }
                else if (property != null)
                {
                    return Signature.QualifiedName.Contains(".get") ? 
                        property.Getter : property.Setter;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// A list of contracts that are always true (not conditionally true).
        /// </summary>
        /// <remarks>
        /// Daikon can generate contracts that look like this when using splitters:
        /// x != y
        /// z > 3 => x != y
        /// 
        /// It does not make sense to even display the second contract, because the consequent is always true.
        /// This field helps identify such contracts and set <see cref="ProgramContract.IsTrivial"/>
        /// to true. Call <see cref="ProgramElement.InvalidateRedundantImplications"/> to perform this logic.
        /// </remarks>
        private readonly HashSet<string> AlwaysTrueContracts;

        /// <summary>
        /// Id counter for contracts. Each contract added to this has a unique non-negative id.
        /// </summary>
        private int idCounter = 1;

        /// <summary>
        /// Maps ids to contracts.
        /// </summary>
        private readonly Dictionary<int, ProgramContract> contractsById = new Dictionary<int, ProgramContract>();

        #region Constructors

        /// <summary>
        /// Creates a new program element that represents a method or object invariant method (that existed at time of parse). 
        /// </summary>
        /// <param name="project">The underlying project model</param>
        /// <param name="daikonElement">The parsed daikon information, including the list of contracts for this method</param>
        /// <param name="method">The method this represents</param>
        /// <param name="parent">The parent of this method</param>
        public ProgramElement(ProjectModel project, DaikonElement daikonElement, Method method, TypeModel parent) :
            this(project, daikonElement, parent)
        {
            this.CreateDefaultConstructor = false;
            this.CreateInvariantMethod = false;
            this.method = method;
            UpdateUserWrittenContracts();
        }

        /// <summary>
        /// Creates a new program element that represents a property.
        /// </summary>
        /// <param name="project">The underlying project model</param>
        /// <param name="daikonElement">The parsed Daikon information, including the list of contracts for this property</param>
        /// <param name="property">The property this represents</param>
        /// <param name="parent">The parent elment type of this property</param>
        public ProgramElement(ProjectModel project, DaikonElement daikonElement, Property property, TypeModel parent) :
            this(project, daikonElement, parent)
        {
            Contract.Requires(!daikonElement.IsObjectInvariant);
            this.CreateDefaultConstructor = false;
            this.CreateInvariantMethod = false;
            this.property = property;
            UpdateUserWrittenContracts();
        }

        /// <summary>
        /// Creates a new program element without a method (the method may not yet exist, 
        /// such as a missing invaraint method or missing default constructor).
        /// 
        /// If <paramref name="daikonElement"/> is an object invariant, the missing method is 
        /// an invariant method, otherwise the missing method is a default constructor. 
        /// </summary>
        /// <param name="project">The underlying project model</param>
        /// <param name="daikonElement">The daikon method this represents. Contains a list of the contracts</param>
        /// <param name="parent">The parent element this of this method/property</param>
        public ProgramElement(ProjectModel project, DaikonElement daikonElement, TypeModel parent)
        {
            Contract.Requires(project != null);
            Contract.Requires(daikonElement != null);

            this.project = project;
            this.parent = parent;
            this.CeleriacQualifiedName = daikonElement.Signature.QualifiedName;
            this.Parameters = daikonElement.Signature.Parameters;
            this.IsObjectInvariant = daikonElement.IsObjectInvariant;
            this.Contracts = new List<ProgramContract>();
            this.filters = new List<ContractFilter>();
            this.variables = new SortedSet<string>();
            this.sourceVariables = new SortedSet<string>();
            this.contractIndices = new Dictionary<string, int>();
            this.unfilteredCounts = new Dictionary<ContractKind, int>();
            this.AlwaysTrueContracts = new HashSet<string>();
            this.preCount = 0;
            this.postCount = 0;
            this.invariantCount = 0;
            ResetUnfilteredCounts();

            if (daikonElement.IsObjectInvariant)
            {
                this.CreateInvariantMethod = true;
            }
            else
            {
                this.CreateDefaultConstructor = true;
            }

            foreach (DaikonContract contract in daikonElement.Contracts)
            {
                AddProgramContract(contract);
            }
        }

        #endregion

        /// <summary>
        /// Gets a contract given its id.
        /// </summary>
        /// <param name="id">the id of the contract</param>
        /// <returns>a contract with a matching id, or null if no contract with the given id exists</returns>
        public ProgramContract FindContract(int id)
        {
            ProgramContract found;
            this.contractsById.TryGetValue(id, out found);
            return found;
        }

        /// <summary>
        /// Appends a <see cref="ProgramContract"/> to this.
        /// </summary>
        /// <param name="contract">the contract information for the contract to append</param>
        private void AddProgramContract(DaikonContract contract)
        {
            AddProgramContract(contract.contract, contract.daikonInvariant, contract.programPointType, contract.invariantType,
                contract.groupVariables, contract.filterVariables);
        }

        /// <summary>
        /// Appends a <see cref="ProgramContract"/> to this.
        /// </summary>
        /// <param name="baseContract">the contract text.</param>
        /// <param name="daikonContract">the Daikon contract text.</param>
        /// <param name="kind">the program point of the contract.</param>
        /// <param name="daikonInvariantType">the object name of the Daikon invariant</param>
        /// <param name="groupVars">the variables used as sorting for this contract.</param>
        /// <param name="vars">the variables contained in this contract.</param>
        private void AddProgramContract(string baseContract, string daikonContract, ContractKind kind,
            string daikonInvariantType, string[] groupVars, string[] vars)
        {
            int index;

            if (kind == ContractKind.Requires)
            {
                preCount++;
                index = preCount;
            }
            else if (kind == ContractKind.Ensures)
            {
                postCount++;
                index = postCount;
            }
            else
            {
                invariantCount++;
                index = invariantCount;
            }

            foreach (var groupVar in groupVars)
            {
                this.sourceVariables.Add(groupVar);
            }

            foreach (var var in vars)
            {
                this.variables.Add(var);
            }

            var contract = new ProgramContract(idCounter, baseContract, daikonContract, daikonInvariantType, groupVars, vars, kind, this, index);
            contractsById.Add(idCounter, contract);
            idCounter++;

            if (!contractIndices.ContainsKey(contract.CodeContractTextNoWhitespace))
            {
                Implication implication = contract.Implication;
                if (implication.IsUniversal)
                {
                    AlwaysTrueContracts.Add(implication.Consequent.StripWhitespace());
                }

                contractIndices.Add(contract.CodeContractTextNoWhitespace, index);
                Contracts.Add(contract);
                contract.ContractModified += HandleContractModified;
                unfilteredCounts[kind]++;
            }
        }

        internal void PostProcess()
        {
            // Add TypeOf filter by default.
            this.AddFilter(new ContractFilter(this, InvariantType.TypeOf));
            // Filter unary contracts with "this" by default.
            this.AddFilter(new ContractFilter(this, InvariantType.Unary, new[] { "this" }));
            this.IdentifySimilarContracts();
            this.InvalidateRedundantImplications();
        }

        private void IdentifySimilarContracts()
        {
            for (int i = 0; i < Contracts.Count; i++)
            {
                var first = Contracts[i];

                for (int j = i + 1; j < Contracts.Count; j++)
                {
                    var second = Contracts[j];

                    if (ExpressionComparison.AreSimilarImplications(first.Implication, second.Implication))
                    {
                        first.SimilarContracts.Add(second);
                        second.SimilarContracts.Add(first);
                    }
                    else if (first.Implication.IsOpposite(second.Implication))
                    {
                        first.IsTrivial = true;
                        second.IsTrivial = true;
                    }
                }
            }
        }

        private void InvalidateRedundantImplications()
        {
            foreach (ProgramContract contract in this.Contracts)
            {
                Implication implication = contract.Implication;
                
                // Remove contracts such as "Count != 0 ==> x > 1" when "x > 1" is always true anyways.
                if (!implication.IsUniversal)
                {
                    if (AlwaysTrueContracts.Contains(implication.Consequent.StripWhitespace()))
                    {
                        contract.IsTrivial = true;
                        unfilteredCounts[contract.Kind]--;
                    }
                }

                // Remove contracts such as "Count != 0 ==> Count != 0"
                if (implication.Antecedent.Equals(implication.Consequent))
                {
                    contract.IsTrivial = true;
                }
            }
        }    

        /// <summary>
        /// Adds and applies a <see cref="ContractFilter"/> to this.
        /// </summary>
        /// <param name="filter">the filter to add</param>
        internal void AddFilter(ContractFilter filter)
        {
            Contract.Requires(filter != null);
            Contract.Requires(filter.Element == this);
            Contract.Ensures(this.Filters.Contains(filter));

            this.ResetUnfilteredCounts();

            foreach (var contract in this.Contracts)
            {
                if (filter.Filters(contract))
                {
                    contract.FilteredBy++;
                    filter.AddFilteredContract(contract);
                }

                if (!contract.IsFiltered && !contract.IsTrivial)
                {
                    unfilteredCounts[contract.Kind]++;
                }
            }

            this.filters.Add(filter);

            // Notify this filter was added.
            OnFilterModified(new FilterModifiedArgs(filter, true));
        }

        /// <summary>
        /// Removes an active <see cref="ContractFilter"/> from this.
        /// </summary>
        /// <param name="filter">the filter to remove</param>
        internal void RemoveFilter(ContractFilter filter)
        {
            Contract.Requires(filter != null);
            Contract.Requires(filter.Element == this);
            Contract.Requires(this.Filters.Contains(filter));
            Contract.Ensures(!this.Filters.Contains(filter));

            this.ResetUnfilteredCounts();

            foreach (var contract in this.Contracts)
            {
                if (filter.Filters(contract))
                {
                    contract.FilteredBy--;
                }

                if (!contract.IsFiltered && !contract.IsTrivial)
                {
                    unfilteredCounts[contract.Kind]++;
                }
            }

            this.filters.Remove(filter);

            // Notify this filter was removed.
            OnFilterModified(new FilterModifiedArgs(filter, false));
        }

        /// <summary>
        /// Inserts <paramref name="contract"/> into the source code as a Code Contract.
        /// </summary>
        /// <param name="contract">the contract to insert</param>
        internal void InsertContract(ProgramContract contract)
        {
            Contract.Requires(contract != null);
            Contract.Requires(Contracts.Contains(contract));
            Contract.Requires(!contract.Inserted);
            Contract.Ensures(!MustCreateContractClass);
            Contract.Ensures(!CreateDefaultConstructor);
            Contract.Ensures(!CreateInvariantMethod);

            if (MustCreateContractClass && parent.IsGeneric)
            {
                throw new NotSupportedException(MSG_CANNOT_INSERT_GENERIC_CONTRACT_CLASS);
            }

            if (MustCreateContractClass)
            {
                project.CreateContractClass(parent);
            }

            if (IsInterfaceOrAbstractMethod && CreateInvariantMethod)
            {
                // Create the missing interface invariant method inside the contract class.
                Contract.Assume(!MustCreateContractClass);
                project.CreateMissingMethodIfNecessary(
                    sourceLocation: ContractClassElementSource,
                    element: this,
                    modelParent: parent);
            }
            else if (CreateInvariantMethod || CreateDefaultConstructor)
            {
                // Create the missing class invariant method or default construct inside the parent class. 
                project.CreateMissingMethodIfNecessary(
                    sourceLocation: (parent as Class).ClassSource,
                    element: this,
                    modelParent: parent);
            }

            ContractLocation.InsertContract(contract, contractIndices);
        }

        /// <summary>
        /// Removes <paramref name="contract"/> from the source code.
        /// </summary>
        /// <param name="contract">the contract to remove</param>
        internal void DeleteContract(ProgramContract contract)
        {
            Contract.Requires(contract != null);
            Contract.Requires(Contracts.Contains(contract));
            Contract.Requires(contract.Inserted);
            ContractLocation.DeleteContract(contract);
        }

        /// <summary>
        /// Updates this program element to include its missing method.
        /// The missing method was either an invariant method or default constructor.
        /// </summary>
        /// <param name="newMethod">The method that was missing</param>
        /// <param name="newParent">The parent of the missing method</param>
        internal void AddMissingMethod(Method newMethod, TypeModel newParent)
        {
            Contract.Requires(newMethod != null);
            Contract.Requires(newParent != null);
            Contract.Requires(CreateInvariantMethod == false || CreateDefaultConstructor == false);
            Contract.Ensures(CreateInvariantMethod == false);
            Contract.Ensures(CreateDefaultConstructor == false);

            this.method = newMethod;
            this.property = null;
            this.parent = newParent;
            this.CreateDefaultConstructor = false;
            this.CreateInvariantMethod = false;
        }

        /// <summary>
        /// Update contracts to reflect whether or not they were written by the user.
        /// </summary>
        private void UpdateUserWrittenContracts() 
        {
            // Check for existing contracts and documentation. 
            if (ContractLocation != null)
            {
                var existingContracts = ContractLocation.Contracts.ToList();
                foreach (var contract in Contracts)
                {
                    if (existingContracts.Contains(contract.CodeContractText))
                    {
                        contract.IsUserWritten = true;
                    }
                }
            }
        }

        private void ResetUnfilteredCounts()
        {
            unfilteredCounts[ContractKind.Requires] = 0;
            unfilteredCounts[ContractKind.Ensures] = 0;
            unfilteredCounts[ContractKind.Invariant] = 0;
        }

        /// <summary>
        /// Gets the remaining number of unfiltered contracts of a particular kind (i.e. Requires, Ensures, Invariant).
        /// </summary>
        /// <param name="kind">the contract kind</param>
        /// <returns>the number of unfiltered contracts of a particular kind</returns>
        public int RemainingContractCount(ContractKind kind)
        {
            return unfilteredCounts[kind];
        }

        public override string ToString()
        {
            return IsObjectInvariant ? CeleriacQualifiedName : (CeleriacQualifiedName + "(" + string.Join(", ", Parameters) + ")");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnvDTE80;
using EnvDTE;
using System.Diagnostics.Contracts;
using System.Diagnostics;
using ContractGenerator.Contracts;

namespace ContractGenerator 
{
    /// <summary>
    /// The contract inserter model for a Visual Studio project. A project consists
    /// of contract classes and implementation classes. Each class can have an associated object
    /// invariant method; each method within a class has associated contracts.
    /// </summary>
    public class ProjectModel 
    {
        public string ProjectName { get; private set; }
        public string ProjectExecutablePath { get; private set; }

        /// <summary>
        /// Pure method information.
        /// </summary>
        public ICollection<PurityEntry> PureMethods { get; private set; }

        /// <summary>
        /// Contract classes for interfaces in the project.
        /// </summary>
        public ICollection<ContractClass> ContractClasses { get; private set; }

        /// <summary>
        /// Classes and interfaces in the project.
        /// </summary>
        public TypeCollection Types { get; private set; }
 
        /// <summary>
        /// Maps from signatures to the contracts for that signature.
        /// </summary>
        /// <remarks>Populated after contract generation</remarks>
        private Dictionary<Signature, ProgramElement> programElements;

        public IEnumerable<ProgramElement> ProgramElements
        {           
            get
            {
                return programElements.Values;
            }
        }

        /// <summary>
        /// Maps from signatures to the signature of the element that contains contracts for that signature.
        /// </summary>
        /// <remarks>
        /// Contract class methods/properties have no associated program element. Thus we map from the contract class
        /// method/property signature to the declaration method/property signature to retrieve the contracts
        /// via <see cref="programElements"/>.
        /// </remarks>
        private readonly Dictionary<Signature, Signature> links;

        // For debug mode.
        public IEnumerable<Signature> GetLinkSignatures(string filter)
        {
            if(string.IsNullOrEmpty(filter))
            {
                return links.Keys;
            }
            else
            {
                return links.Keys.Where(l => l.QualifiedName.Contains(filter));
            }
        }

        /// <summary>
        /// Gets the <see cref="ProgramElement"/> associated with <paramref name="signature"/>.
        /// </summary>
        /// <param name="signature">the method or property signature</param>
        /// <returns>The corresponding <see cref="ProgramElement"/>, or null if no mapping exists.</returns>
        [Pure]
        public ProgramElement GetProgramElement(Signature signature)
        {
            ProgramElement element = null;
            Signature key;
            if (links.TryGetValue(signature, out key))
            {
                programElements.TryGetValue(key, out element);
            }
            return element;
        }

        [Pure]
        public ProgramElement GetProgramElement(Signature signature, out bool proxy)
        {
            ProgramElement element = null;
            Signature key;
            proxy = false;
            if (links.TryGetValue(signature, out key))
            {
                proxy = !key.Equals(signature);
                programElements.TryGetValue(key, out element);
            }
            return element;
        }

        /// <summary>
        /// Create an empty project model with the given name and associated executable.
        /// </summary>
        /// <param name="projectName">the name of the project</param>
        /// <param name="projectExecutablePath">the path to the project's executable (or <c>null</c>)</param>
        public ProjectModel(string projectName, string projectExecutablePath) 
        {
            ProjectName = projectName;
            ProjectExecutablePath = projectExecutablePath;
            PureMethods = new List<PurityEntry>();
            ContractClasses = new List<ContractClass>();
            Types = new TypeCollection();
            programElements = new Dictionary<Signature, ProgramElement>();
            links = new Dictionary<Signature, Signature>();
        }

        /// <summary>
        /// Adds <paramref name="element"/> to the model. <paramref name="signature"/> gets linked to itself.
        /// </summary>
        /// <param name="signature">the element signature</param>
        /// <param name="element">the element to add</param>
        private void AddProgramElement(Signature signature, ProgramElement element)
        {
            Contract.Requires(signature != null);
            Contract.Requires(element != null);
            Contract.Requires(signature.Equals(element.Signature));
            if (!links.ContainsKey(signature))
            {
                links.Add(signature, signature);
                programElements.Add(signature, element);
            }
            else
            {
                // TODO: This case occurs in Boogie - how?
            }
        }

        /// <summary>
        /// Links the keys of <paramref name="linksToAdd"/> to their lookup value.
        /// </summary>
        /// <param name="linksToAdd">the signature links to add</param>
        /// <remarks>
        /// Use this method to link contract class method/property signatures to their implementation.
        /// </remarks>
        public void AddLinks(Dictionary<Signature, Signature> linksToAdd)
        {
            if (linksToAdd != null)
            {
                foreach (Signature linkFrom in linksToAdd.Keys)
                {
                    Contract.Assert(!this.links.ContainsKey(linkFrom));
                    Signature linkTo = linksToAdd[linkFrom];
                    this.links.Add(linkFrom, linkTo);
                }
            }
        }

        /// <summary>
        /// Match the Celeriac/Daikon inferred contracts to the project model. Clears any previous
        /// contract information.
        /// </summary>
        /// <param name="daikonMethodContracts">Mapping from method signature to method contracts.</param>
        public void CreateMethodContracts(Dictionary<Signature, DaikonElement> daikonMethodContracts)
        {
            Contract.Requires(daikonMethodContracts != null);

            HashSet<Signature> matched = new HashSet<Signature>();
            programElements = new Dictionary<Signature, ProgramElement>();

            foreach (TypeModel type in Types)
            {
                DaikonElement match;
                ProgramElement invariantContracts = null;
                Signature typeSignature = new Signature(type.QualifiedName, new List<Parameter>());
                if (daikonMethodContracts.TryGetValue(typeSignature, out match))
                {
                    Contract.Assume(match.IsObjectInvariant);
                    matched.Add(typeSignature);

                    Class classType = type as Class;
                    if (classType != null)
                    {
                        if (classType.InvariantMethods.Count > 0)
                        {
                            // An invariant method exists. Link to the first one parsed.
                            Method invariantMethod = classType.InvariantMethods.First();
                            invariantContracts = new ProgramElement(this, match, invariantMethod, type);
                        }
                    }

                    if (invariantContracts == null)
                    {
                        // The invariant method does not exist. 
                        invariantContracts = new ProgramElement(this, match, type);
                    }

                    AddProgramElement(invariantContracts.Signature, invariantContracts);
                }

                // Match functions.
                foreach (Method m in type.Methods)
                {
                    if (daikonMethodContracts.TryGetValue(m.Signature, out match))
                    {
                        matched.Add(m.Signature);
                        ProgramElement mContracts = new ProgramElement(this, match, m, type);
                        AddProgramElement(mContracts.Signature, mContracts);
                    }
                }

                // Match properties. 
                foreach (Property p in type.Properties)
                {
                    if (p.Getter != null)
                    {
                        if (daikonMethodContracts.TryGetValue(p.Getter.Signature, out match))
                        {
                            matched.Add(p.Getter.Signature);
                            ProgramElement getterContracts = new ProgramElement(this, match, p, type);
                            AddProgramElement(getterContracts.Signature, getterContracts);
                        }
                    }
                    if (p.Setter != null)
                    {
                        // TWS: why did this original use a signature with no arguments?
                        if (daikonMethodContracts.TryGetValue(p.Setter.Signature, out match))
                        {
                            matched.Add(p.Setter.Signature);
                            ProgramElement setterContracts = new ProgramElement(this, match, p, type);
                            AddProgramElement(setterContracts.Signature, setterContracts);
                        }
                    }
                }
            }

            // Do a second pass searching for missing default constructors.
            //  
            // Contracts can be generated for functions that do not exist in the source code. For example,
            // a default constructor can exist without being written if no other constructors are present.
            // If this is the case, there is no way we could have parsed it, so we go back through 
            // all class code elements and compare them to unmatched entries in the contract file, which 
            // should be the only unmatched entries remaining. 
            foreach (Class type in Types.OfType<Class>())
            {
                var defaultCtor = type.ClassRef.DefaultConstructorQualifiedName();

                DaikonElement match;
                Signature typeSignature = new Signature(defaultCtor, new List<Parameter>());
                if (daikonMethodContracts.TryGetValue(typeSignature, out match))
                {
                    if (!matched.Contains(typeSignature))
                    {
                        // Add a program element that represents the missing default constructor. 
                        Contract.Assume(!match.IsObjectInvariant);
                        AddProgramElement(typeSignature, new ProgramElement(this, match, type));
                    }
                }
            }

            Debug.WriteLine(string.Empty);
            Debug.WriteLine("Unmatched errors:");
            Debug.WriteLine("(signatures with contracts but did not get matched to source code)");

            foreach (Signature sig in daikonMethodContracts.Keys)
            {
                if (!matched.Contains(sig))
                {
                    Debug.WriteLine(sig);
                }
            }
        }

        /// <summary>
        /// Create a contract class for interface <paramref name="type"/> below the interface in the source code
        /// and update the interface contract source bindings.
        /// </summary>
        /// <param name="type">The Visual Studio code model interface</param>
        public void CreateContractClass(TypeModel type)
        {
            Contract.Requires(type != null);
            Contract.Requires(!type.IsGeneric, "Adding contract classes for generic interfaces is not supported");

            var typeRef = type.TypeRef;
            Func<CodeFunction, bool> IsMethodImplementation = m => type is Class && !m.MustImplement;

            // Contract class will be insterted directly below the interface
            var editPoint = typeRef.EndPoint.CreateEditPoint();
            var interfaceNamespace = (CodeNamespace)editPoint.CodeElement[vsCMElement.vsCMElementNamespace];

            // Construct contract class
            var nameNoGenerics = typeRef.DefaultContractClassName(); // do we have to start with a unique name to avoid conflicts?
            var baseName = typeRef is CodeClass ? typeRef.Name : null;
            var interfaceName = typeRef is CodeInterface ? typeRef.Name : null;

            var contractClassRef = interfaceNamespace.AddClass(nameNoGenerics, typeRef, baseName, interfaceName, typeRef.Access);
            contractClassRef.AddAttribute("ContractClassFor", typeRef.TypeOfExpression(), -1);
            contractClassRef.IsAbstract = true;

            // Add a back reference
            typeRef.AddAttribute("ContractClass", (contractClassRef as CodeType).TypeOfExpression(), -1);

            // For each method and property in the interface, we need to create those elements in the contract class.
            foreach (CodeElement e in typeRef.Members)
            {
                if (e.Kind == vsCMElement.vsCMElementFunction) // method
                {
                    var mRef = e as CodeFunction;

                    if (IsMethodImplementation(mRef))
                    {
                        continue;
                    }

                    // Create the method
                    CodeFunction2 m = (CodeFunction2)contractClassRef.AddFunction(mRef.Name, vsCMFunction.vsCMFunctionFunction, mRef.Type.AsString, -1, mRef.Access, null);
                    if (type is Class)
                    {
                        m.OverrideKind = vsCMOverrideKind.vsCMOverrideKindOverride;
                    }

                    // Copy the parameters
                    foreach (CodeParameter p in mRef.Parameters)
                    {
                        m.AddParameter(p.Name, p.Type.AsString, -1);
                    }
                }
                else if (e.Kind == vsCMElement.vsCMElementProperty) // property
                {
                    var pRef = e as CodeProperty2;

                    var getter = (pRef.Getter != null && !IsMethodImplementation(pRef.Getter)) ? pRef.Getter.Name : null;
                    var setter = (pRef.Setter != null && !IsMethodImplementation(pRef.Setter)) ? pRef.Setter.Name : null;

                    if (getter == null && setter == null)
                    {
                        continue;
                    }

                    var p = (CodeProperty2)contractClassRef.AddProperty(getter, setter, pRef.Type.AsString, -1, pRef.Access, null);
                    if (type is Class)
                    {
                        p.OverrideKind = vsCMOverrideKind.vsCMOverrideKindOverride;
                    }
                }
            }

            // Update the Code Model
            var contractClass = ContractClass.Wrap(contractClassRef);
            Dictionary<Signature, Signature> newLinks;
            type.LinkContractClass(contractClass, out newLinks);
            AddLinks(newLinks);
        }

        /// <summary>
        /// If <paramref name="element"/> is missing either an invariant method or default constructor, creates 
        /// the invariant method or default constructor.
        /// </summary>
        /// <param name="sourceLocation">the location to create the missing method</param>
        /// <param name="element">The element representing the missing method to create (if necessary)</param>
        /// <param name="modelParent">The type (interface or class) to add the new method too</param>
        public void CreateMissingMethodIfNecessary(SourceLocation sourceLocation, ProgramElement element, TypeModel modelParent)
        {
            Contract.Requires(element != null);
            Contract.Requires(modelParent != null);
            Contract.Requires(element.CreateInvariantMethod.Implies(element.IsObjectInvariant));
            //Contract.Requires(element.CreateDefaultConstructor.Implies(!element.IsInterfaceOrAbstractMethod));
            Contract.Ensures(element.CreateInvariantMethod == false);
            Contract.Ensures(element.CreateDefaultConstructor == false);

            if (element.CreateInvariantMethod || element.CreateDefaultConstructor)
            {
                EditPoint editPoint = sourceLocation.Start.CreateEditPoint();
                CodeClass clazz = (CodeClass)editPoint.CodeElement[vsCMElement.vsCMElementClass];

                CodeFunction newMethodRef = null;

                // The default constructor is missing.
                if (element.CreateDefaultConstructor)
                {
                    newMethodRef = clazz.AddFunction(clazz.DefaultConstructorName(), vsCMFunction.vsCMFunctionConstructor,
                        vsCMTypeRef.vsCMTypeRefVoid, 0, vsCMAccess.vsCMAccessPublic, null);
                }

                // The class invariant function is missing. 
                if (element.CreateInvariantMethod)
                {
                    newMethodRef = clazz.AddFunction("ObjectInvariant", vsCMFunction.vsCMFunctionFunction,
                        vsCMTypeRef.vsCMTypeRefVoid, 0, vsCMAccess.vsCMAccessPrivate, null);
                    newMethodRef.AddAttribute("ContractInvariantMethod", null, -1);
                }

                Contract.Assume(newMethodRef != null);
                // The new method is now created. Now we must properly update the model.
                // Classes: The new method is added directly to the class it is contained in.
                // Interfaces: The new method is added to the contract class it is contained in.            

                Method newMethod;
                Class parentClass = modelParent as Class;
                Interface parentInterface = modelParent as Interface;
                Contract.Assume(parentClass != null || parentInterface != null);
                Contract.Assume(parentClass == null || parentInterface == null);

                if (parentClass != null)
                {
                    newMethod = new Method(newMethodRef, parentClass.ClassRef as CodeType);
                    // Update the project model: add the new method to the class. 
                    parentClass.Methods.Add(newMethod);

                    // Link method signature to the element that contains contracts for this method.
                    programElements[newMethod.Signature] = element;
                }
                else
                {
                    //Contract.Assert(element.IsInterfaceMethod && element.CreateInvariantMethod);
                    newMethod = new Method(parentInterface.InterfaceRef as CodeType, newMethodRef);
                    Signature signature = new Signature(newMethodRef.FullName, newMethodRef.GetParameters().ToList());
                    programElements[signature] = element;
                    parentInterface.ContractClass.Methods.Add(newMethod.Signature, newMethod);
                }

                // Now that the missing function as been creating, this program method now
                // represents the new method. 
                element.AddMissingMethod(newMethod, modelParent);
            }
        }
    }
}

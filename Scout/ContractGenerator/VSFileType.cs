﻿using System;

namespace ContractGenerator
{

    // Project type and GUID information found at:
    // - http://www.mztools.com/articles/2008/MZ2008017.aspx
    // - https://teamreview.svn.codeplex.com/svn/1.1.1/2005/TeamReview/Constants.cs
    // - https://yphp.svn.codeplex.com/svn/YoungPHPStudio/PhpProject/ProjectBase/EnvDTEConstants.cs
    // - http://msdn.microsoft.com/en-us/library/bb166496(v=vs.100).aspx
    // - http://msdn.microsoft.com/en-us/library/z4bcch80(v=vs.80).aspx

    /// <summary>
    /// Enumeration of possible file types within a Visual Studio solution.
    /// This is not a complete list.
    /// </summary>
    public enum VSFileType
    {
        WindowsCSharp,
        WindowsVBDotNet, // Not used.
        WindowsCPlusPlus, // Not used. 
        ProjectSolutionFolder,
        ProjectItemSolutionFolder,
        PhysicalFile,
        PhysicalFolder,
        VirtualFolder,
        Unknown
    }

    static class GuidToVSType
    {
        /// <summary>
        /// Converts <paramref name="guid"/> to its corresponding Visual Studio file type.
        /// </summary>
        /// <param name="guid">The guid string to convert</param>
        /// <returns>The file type represented by the guid</returns>
        public static VSFileType Convert(string guid)
        {
            if (new Guid("FAE04EC0-301F-11D3-BF4B-00C04F79EFBC").Equals(new Guid(guid)))
                return VSFileType.WindowsCSharp;

            if (new Guid("66A26720-8FB5-11D2-AA7E-00C04F688DDE").Equals(new Guid(guid)))
                return VSFileType.ProjectSolutionFolder;

            if (new Guid("66A26722-8FB5-11D2-AA7E-00C04F688DDE").Equals(new Guid(guid)))
                return VSFileType.ProjectItemSolutionFolder;

            if (new Guid("6BB5F8EE-4483-11D3-8BCF-00C04F8EC28C").Equals(new Guid(guid)))
                return VSFileType.PhysicalFile;

            if (new Guid("6BB5F8EF-4483-11D3-8BCF-00C04F8EC28C").Equals(new Guid(guid)))
                return VSFileType.PhysicalFolder;

            if (new Guid("6BB5F8F0-4483-11D3-8BCF-00C04F8EC28C").Equals(new Guid(guid)))
                return VSFileType.VirtualFolder;

            return VSFileType.Unknown;
        }
    }
}

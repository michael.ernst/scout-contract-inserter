﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.IO;
using System.Security.Permissions;
using System.Diagnostics.Contracts;

namespace ContractGenerator
{
    /// <summary>
    /// Enumeration of possible flags to Daikon.
    /// </summary>
    public enum DaikonArguments
    {
        [Flag("java"), NumArgs(0)]
        Java,
        [Flag("-Xmx512m")]
        MoreMemory,
        [Flag("-cp "), NumArgs(1)]
        ClassPath,
        [Flag("daikon.Daikon"), NumArgs(0)]
        DaikonMain,
        [Flag("daikon.PrintInvariants"), NumArgs(0)]
        DaikonPrintInvariants,
        [Flag("--format=csharpcontract"), NumArgs(0)]
        FormartCSharp,
        [Flag("--config="), NumArgs(1)]
        ConfigurationFile,
        [Flag("--output="), NumArgs(1)]
        OutputFile,
        [Flag("--conf_limit "), NumArgs(1)]
        ConfidenceLimit,
        [Flag("-o "), NumArgs(1)]
        InvariantsFileName,
        [Flag("--no_text_output"), NumArgs(0)]
        NoTextOutput,
        [Flag(""), NumArgs(1)]
        InputFile,
        [Flag("--print_csharp_metadata")]
        PrintCSharpMetadata,

        /// <summary>
        /// Option flag to preface a Daikon options that set Daikon variables.
        /// </summary>
        [Flag("--config_option"), NumArgs(0)]
        ConfigurationOption,

        /// <summary>
        /// Undo optimizations, most importantly breaking the equality sets. As a side-effect, implication
        /// invariants are not output.
        /// NOTE: you must include the <see cref="DaikonArguments.ConfigurationOption"/> before this option.
        /// </summary>
        [Flag("daikon.Daikon.undo_opts=true"), NumArgs(0)]
        UndoOptimizations,

        /// <summary>
        /// NOTE: you must include the <see cref="DaikonArguments.ConfigurationOption"/> before this option.
        /// </summary>
        [Flag("daikon.PrintInvariants.print_implementer_entry_ppts="), NumArgs(1)]
        PrintImplementerEntryPointsPpt
    }

    /// <summary>
    /// Enumeration of possible flags to Celeriac.
    /// </summary>
    public enum CeleriacArguments
    {
        [Flag("--std-visibility"), NumArgs(0)]
        StdVisibility,
        [Flag("--output-location="), NumArgs(1)]
        OutputLocation,
        [Flag("--purity-file="), NumArgs(1)]
        PurityFile,
        [Flag("--infer-purity="), NumArgs(1)]
        InferPurity,
        [Flag("--is-property-flags"), NumArgs(0)]
        UsePropertyFlags,
        [Flag("--is-enum-flags"), NumArgs(0)]
        UseEnumFlags,
        [Flag("--is-readonly-flags"), NumArgs(0)]
        UseReadonlyFlags,
        [Flag("--enum-underlying-values"), NumArgs(0)]
        UseEnumUnderlyingValues,
        [Flag("--linked-lists="), NumArgs(1)]
        LinkedLists,
        [Flag("--friendly-dec-types="), NumArgs(1)]
        FriendlyDecTypes,
        [Flag("--link-object-invariants"), NumArgs(0)]
        LinkObjectInvariants,
        [Flag(""), NumArgs(1)]
        ExecutableFile
    }

    /// <summary>
    /// Enumeration of possible flags to SEAL.
    /// </summary>
    public enum SealArguments
    {
        [Flag("/in "), NumArgs(1)]
        InputFile,
        [Flag("/outdir "), NumArgs(1)]
        OutputDirectory
    }

    /// <summary>
    /// Runs Celeriac and Daikon processes.
    /// </summary>
    public class FrontEndLoader : IDisposable
    {
        public event EventHandler<GenerateStatusEvent> GenerateStatusChange;

        private void UpdateStatus(GenerateStatus status, string message)
        {
            EventHandler<GenerateStatusEvent> handler = GenerateStatusChange;
            if (handler != null)
            {
                handler(this, new GenerateStatusEvent(status, message));
            }
        }

        private readonly ProcessRunner Celeriac;
        private readonly ProcessRunner Daikon;
        private readonly ProcessRunner DaikonPrintInvariants;
        private readonly string configurationError;
        private readonly Log log;
        private readonly LoaderConfiguration configuration;
        private readonly string projectDirectory;

        private const string JAVA = "java"; // Java must be on the Windows path for now.
        private static readonly string BASE = Path.GetDirectoryName(typeof(FrontEndLoader).Assembly.Location);
        private static readonly string DAIKON_JAR = Path.Combine(BASE, "binaries\\daikon\\daikon.jar").Quote();
        private static readonly string SEAL = Path.Combine(BASE, "binaries\\seal\\Bin\\Checker.exe").Quote();
        private static readonly string SEAL_HOME = Path.Combine(BASE, "binaries\\seal");
        private static readonly string CONFIG_FILE = Path.Combine(BASE, "config\\settings.txt");

        public static bool USE_DAIKON_SPLITTERS = true;

        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        public FrontEndLoader(LoaderConfiguration configuration, IEnumerable<PurityEntry> projectParsedPurityEntries, Log logger)
        {
            configurationError = string.Empty;
            log = logger;

            this.configuration = configuration;
            this.projectDirectory = configuration.ProjectDirectory;

            string executablePath = configuration.ExecutablePath;
            string executableFolderPath = configuration.ExecutableDirectory;

            string executingDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            Contract.Assume(executingDirectory != null);
            string celeriacPath = configuration.CeleriacExecutablePath.Quote();
            string celeriacExePath = executablePath;
            string celeriacOutputPath = Path.Combine(projectDirectory, Resources.DtraceFile).Quote();
            string purityPath = Path.Combine(projectDirectory, Resources.PureMethodsFile);
            string daikonConfigPath = Path.Combine(executingDirectory, CONFIG_FILE).Quote();
            string daikonContractsPath = Path.Combine(projectDirectory, Resources.ContractsFile).Quote();
            string daikonInvariantsPath = Path.Combine(projectDirectory, Resources.InvariantsFile).Quote();

            // Write the the parsed purity entries to parsed.pure.
            PurityEntry.CreatePurityFile(Path.Combine(projectDirectory, "parsed.pure"), projectParsedPurityEntries);
              
            //
            // Seal & Celeriac
            //
            if (!configuration.SkipCeleriac)
            {
                if (string.IsNullOrEmpty(celeriacPath))
                {
                    configurationError = Resources.PathToCeleriacNotSetError;
                    return;
                }
               
                // Assembly a = Assembly.LoadFile(configuration.ExecutablePath);
                // Cannot run SEAL on .NET version 4.5
                // bool canRunSeal = !a.ImageRuntimeVersion.Contains("4.5");

                // if (!canRunSeal)
                // {
                //     UpdateStatus(GenerateStatus.OTHER, Resources.SkippingPurityAnalysis);
                // }

                // bool runSeal = this.configuration.RunSealPurityAnalysis && canRunSeal;
                bool runSeal = this.configuration.RunSealPurityAnalysis;
                var parsedPurityEntries = new HashSet<PurityEntry>(projectParsedPurityEntries);
                HashSet<PurityEntry> sealPurityEntries;

                if (runSeal)
                {
                    // Run SEAL.
                    UpdateStatus(GenerateStatus.Seal, Resources.StatusSeal);
                    sealPurityEntries = RunSeal(executablePath, projectDirectory);
                    parsedPurityEntries.UnionWith(sealPurityEntries);
                }
                else
                {
                    // If we don't run SEAL, look for a file named seal.pure in the project directory.
                    sealPurityEntries = 
                        PurityEntry.ReadPurityFile(Path.Combine(projectDirectory, "seal.pure"));
                    parsedPurityEntries.UnionWith(sealPurityEntries);
                }

                parsedPurityEntries.UnionWith(sealPurityEntries);
                var newEntries = sealPurityEntries.Intersect(parsedPurityEntries);

                // Write to debug if SEAL did not detect methods marked as pure. Sanity check.
                if (newEntries.Count() != 0)
                {
                    Debug.WriteLine("Parsed some purity entries that SEAL did not discover:");
                    foreach (var entry in newEntries)
                    {
                        Debug.WriteLine(entry.ToString());
                    }
                }

                // Create the purity file.
                PurityEntry.CreatePurityFile(purityPath, parsedPurityEntries);
                purityPath = purityPath.Quote();

                Celeriac = new ProcessRunner("Celeriac", celeriacPath);
                Celeriac.StartInfo.WorkingDirectory = executableFolderPath;
                Celeriac.AddArgument(CeleriacArguments.StdVisibility);
                Celeriac.AddArgument(CeleriacArguments.UsePropertyFlags);
                Celeriac.AddArgument(CeleriacArguments.LinkObjectInvariants);
                Celeriac.AddArgument(CeleriacArguments.InferPurity, "true");
                Celeriac.AddArgument(CeleriacArguments.UseReadonlyFlags);
                Celeriac.AddArgument(CeleriacArguments.OutputLocation, celeriacOutputPath);
                Celeriac.AddArgument(CeleriacArguments.PurityFile, purityPath);
                Celeriac.AddArgument(CeleriacArguments.ExecutableFile, celeriacExePath);

                Debug.WriteLine(
                    string.Format("Celeriac Command:\n{0}\n", Celeriac.Command));
            }

            //
            // Daikon Main
            //
            if (!configuration.SkipDaikonMain)
            {
                Daikon = new ProcessRunner("Daikon", JAVA);
                Daikon.AddArgument(DaikonArguments.MoreMemory);
                Daikon.AddArgument(DaikonArguments.ClassPath, DAIKON_JAR);
                Daikon.AddArgument(DaikonArguments.DaikonMain);
                Daikon.AddArgument(DaikonArguments.NoTextOutput);
                Daikon.AddArgument(DaikonArguments.ConfigurationOption);
                Daikon.AddArgument(DaikonArguments.UndoOptimizations);
                Daikon.AddArgument(DaikonArguments.ConfigurationOption);
                Daikon.AddArgument(DaikonArguments.PrintImplementerEntryPointsPpt, "false");
                Daikon.AddArgument(DaikonArguments.ConfidenceLimit, ".75");
                Daikon.AddArgument(DaikonArguments.FormartCSharp);
                Daikon.AddArgument(DaikonArguments.InvariantsFileName, daikonInvariantsPath);
                Daikon.AddArgument(DaikonArguments.ConfigurationFile, daikonConfigPath);

                // Add the generated splitter files as input.
                foreach (var splitterFile in configuration.SplitterFiles)
                {
                    if (USE_DAIKON_SPLITTERS)
                    {
                        if (!splitterFile.Contains("Util"))
                        {
                            Daikon.AddArgument(DaikonArguments.InputFile, splitterFile.Quote());
                        }
                    }
                }

                // Use an existing dtrace file.
                if (configuration.SkipCeleriac && !configuration.SkipDaikonMain)
                {
                    foreach (var trace in configuration.Files.TraceFiles)
                    {
                        Daikon.AddArgument(DaikonArguments.InputFile, trace);
                    }
                }
                // Generate a new dtrace file. 
                else
                {
                    string daikonDtracePath = Path.Combine(projectDirectory, Resources.DtraceFile);
                    Daikon.AddArgument(DaikonArguments.InputFile, daikonDtracePath);
                }

                Debug.WriteLine(
                    string.Format("daikon.Main Command:\n{0}\n", Daikon.Command));
            }

            //
            // Daikon Print Invariants
            //
            if (!configuration.SkipDaikonPrintInvariants)
            {
                DaikonPrintInvariants = new ProcessRunner("Daikon Print Invariants", JAVA);
                DaikonPrintInvariants.AddArgument(DaikonArguments.ClassPath, DAIKON_JAR);
                DaikonPrintInvariants.AddArgument(DaikonArguments.DaikonPrintInvariants);
                DaikonPrintInvariants.AddArgument(DaikonArguments.FormartCSharp);
                DaikonPrintInvariants.AddArgument(DaikonArguments.PrintCSharpMetadata);
                DaikonPrintInvariants.AddArgument(DaikonArguments.OutputFile, daikonContractsPath);
                DaikonPrintInvariants.AddArgument(DaikonArguments.ConfigurationFile, daikonConfigPath);
                DaikonPrintInvariants.AddArgument(DaikonArguments.InputFile, daikonInvariantsPath);

                Debug.WriteLine(
                    string.Format("daikon.PrintInvariants Command:\n{0}\n", DaikonPrintInvariants.Command));
            }
        }

        ~FrontEndLoader()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Celeriac != null)
                {
                    Celeriac.Dispose();
                }

                if (Daikon != null)
                {
                    Daikon.Dispose();
                }

                if (DaikonPrintInvariants != null)
                {
                    DaikonPrintInvariants.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Runs SEAL (side effect/purity analysis) on an assembly and it's dependencies.
        /// 
        /// TODO: include more details here about SEAL and the reference dependency resolver.
        /// </summary>
        /// <param name="assemblyPath">the assembly to analyze</param>
        /// <param name="baseDirectory">the directory to place SEAL's output</param>
        /// <returns>the set of purity entries discovered by SEAL</returns>
        private static HashSet<PurityEntry> RunSeal(string assemblyPath, string baseDirectory)
        {
            string executingDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            Contract.Assume(executingDirectory != null);

            // Forrest - Remember the old SEALHOME value because I've been running SEAL manually. 
            // It is set back to the old value after SEAL executes.
            string oldSealHome = Environment.GetEnvironmentVariable(Resources.SealEnvironmentVariable);
            // Set SEALHOME to the seal directory included with the inserter.
            Environment.SetEnvironmentVariable(Resources.SealEnvironmentVariable, Path.Combine(executingDirectory, SEAL_HOME), EnvironmentVariableTarget.User);

            // Get the non-Microsoft dependencies of the assembly we are analyzing from most dependent to least dependent.
            IEnumerable<string> orderedAssemblies = AssemblyReferenceResolver.ReferenceResolver.Resolve(
                assemblyLocation: assemblyPath,
                includeMicrosoftAssemblies: false);

            // Set up SEAL arguments.
            using (ProcessRunner Seal = new ProcessRunner("Seal", SEAL))
            {
                foreach (string assembly in orderedAssemblies)
                {
                    Seal.AddArgument(SealArguments.InputFile, assembly.Quote());
                }

                string sealOutputDirectory = Path.Combine(baseDirectory, "seal");

                // Output directory must exist or SEAL will crash.
                if (!Directory.Exists(sealOutputDirectory))
                {
                    Directory.CreateDirectory(Path.Combine(baseDirectory, "seal"));
                }
                else
                {
                    foreach (FileInfo file in new DirectoryInfo(sealOutputDirectory).GetFiles())
                    {
                        file.Delete();
                    }
                }

                Seal.AddArgument(SealArguments.OutputDirectory, sealOutputDirectory.Quote());

                // Run SEAL.
                RunProcessResults sealResults = Seal.RunProcess();

                HashSet<PurityEntry> purityEntries = new HashSet<PurityEntry>();

                if (!sealResults.Success)
                {
                    // TODO: Notify via event that SEAL failed?
                    // MessageBox.Show("Purity analysis (SEAL) failed.");
                }
                else
                {
                    // If SEAL succeeds, it will output a .pure file for each assembly analyzed, so
                    // loop through each .pure file and each entry and add the entry to the set.
                    foreach (string file in Directory.GetFiles(sealOutputDirectory))
                    {
                        foreach (string purityEntry in File.ReadAllLines(file))
                        {
                            purityEntries.Add(PurityEntry.FromString(purityEntry));
                        }
                    }
                }

                // Set SEALHOME back to what is what before.
                Environment.SetEnvironmentVariable(Resources.SealEnvironmentVariable, oldSealHome);
                return purityEntries;
            }
        }

        /// <summary>
        /// Runs Celeriac, Daikon Main, and/or Daikon Print Invariants, depending on which configuration files were given.
        /// </summary>
        /// <returns>
        /// A boolean and a string tuple. 
        /// If the boolean is true, the string contains the path to the contract file.
        /// If the boolean is false, the string contains an error message.
        /// </returns>
        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        public Tuple<bool, string> CreateContractsFile()
        {
            if (!string.IsNullOrEmpty(this.configurationError))
            {
                this.log.WriteHeader();
                this.log.WriteLine(this.configurationError);
                return new Tuple<bool, string>(false, configurationError);
            }

            if (!this.configuration.SkipCeleriac)
            {
                // Run Celeriac.
                UpdateStatus(GenerateStatus.Celeriac, Resources.StatusCeleriac);
                RunProcessResults celeriacRunResults = this.Celeriac.RunProcess();

                if (!celeriacRunResults.Success ||
                    !string.IsNullOrEmpty(celeriacRunResults.StdError) ||
                    celeriacRunResults.StdOut.Equals(Resources.CeleriacEnvironmentVariableUndefined) ||
                    celeriacRunResults.Exception != null)
                {
                    WriteErrorResultsToLog(this.log, this.Celeriac, celeriacRunResults);
                    return new Tuple<bool, string>(false, celeriacRunResults.StdOut);
                }
            }

            if (!this.configuration.SkipDaikonMain)
            {
                // Run Daikon main. This generates a .invariants file.
                UpdateStatus(GenerateStatus.DaikonCalculate, Resources.StatusDaikon);
                RunProcessResults daikonRunResults = this.Daikon.RunProcess();

                if (!daikonRunResults.Success || !string.IsNullOrEmpty(daikonRunResults.StdError))
                {
                    WriteErrorResultsToLog(this.log, this.Daikon, daikonRunResults);
                    return new Tuple<bool, string>(false, daikonRunResults.StdError);
                }
            }

            if (!this.configuration.SkipDaikonPrintInvariants)
            {
                // Run Daikon print invariants. This generates a .contracts file.
                UpdateStatus(GenerateStatus.DaikonPrint, Resources.StatusDaikonPrint);
                RunProcessResults daikonRunResults = this.DaikonPrintInvariants.RunProcess();

                if (!daikonRunResults.Success || !string.IsNullOrEmpty(daikonRunResults.StdError))
                {
                    WriteErrorResultsToLog(this.log, this.DaikonPrintInvariants, daikonRunResults);
                    return new Tuple<bool, string>(false, daikonRunResults.StdError);
                }

                return new Tuple<bool, string>(true, Path.Combine(this.projectDirectory, Resources.ContractsFile));
            }
            else
            {
                return new Tuple<bool, string>(true, this.configuration.Files.ContractFile);
            }
        }

        private static void WriteErrorResultsToLog(Log log, ProcessRunner process, RunProcessResults results)
        {
            log.WriteHeader();
            log.WriteLine("Errors running " + process.Name + ":");
            log.WriteLine(process.Name + " command: " + process.Command);
            log.WriteLine("Standard output: " + results.StdOut);
            log.WriteLine("Standard error: " + results.StdError);

            if (results.Exception != null)
            {
                log.WriteLine("Exception: " + results.Exception);
            }
        }
    }
}

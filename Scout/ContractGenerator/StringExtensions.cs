﻿using System.Diagnostics.Contracts;
using System.Text;
using System.Text.RegularExpressions;

namespace ContractGenerator
{
    public static class StringExtensions
    {
        /// <summary>
        /// Tests if the string is equal to the empty string.
        /// </summary>
        /// <param name="str">the string</param>
        /// <returns>true if the string is equal to the empty string</returns>
        [Pure]
        public static bool IsEmpty(this string str)
        {
            Contract.Ensures(str != null);
            return str.Equals(string.Empty);
        }

        /// <summary>
        /// Returns the string formed by repeating <paramref name="str"/> <paramref name="n"/> times.
        /// </summary>
        /// <param name="str">the string to repeat</param>
        /// <param name="n">the number of times to repeat</param>
        /// <returns>the string formed by repeating <paramref name="str"/> <paramref name="n"/> times</returns>
        [Pure]
        public static string Repeat(this string str, int n)
        {
            Contract.Requires(str != null);
            Contract.Requires(n >= 0);
            Contract.Ensures(Contract.Result<string>() != null);
            Contract.Ensures(Contract.Result<string>().Length == str.Length * n);

            if (n == 0)
            {
                return string.Empty;
            }

            var result = new StringBuilder(n * str.Length);
            for (int i = 0; i < n; i++)
            {
                result.Append(str);
            }
            return result.ToString();
        }

        /// <summary>
        /// Returns <paramref name="str"/> surrounded by quotes.
        /// </summary>
        /// <param name="str">The string to quote.</param>
        /// <returns>the string surrounded by quotes.</returns>
        [Pure]
        public static string Quote(this string str)
        {
            Contract.Requires(str != null);
            Contract.Ensures(Contract.Result<string>() != null);
            Contract.Ensures(Contract.Result<string>().StartsWith(""));
            Contract.Ensures(Contract.Result<string>().EndsWith(""));
            return "\"" + str + "\"";
        }

        /// <summary>
        /// Removes all whitespace from <paramref name="str"/>.
        /// </summary>
        /// <param name="str">the string whose whitespace to remove</param>
        /// <returns>the string with whitespace removed</returns>
        [Pure]
        public static string StripWhitespace(this string str)
        {
            Contract.Requires(str != null);
            Contract.Ensures(!Contract.Result<string>().Contains(" "));
            return Regex.Replace(str, @"\s+", "");
        }

        /// <summary>
        /// Removes characters following two forward slashes in the string.
        /// </summary>
        /// <param name="str">the string whose comments to strip</param>
        /// <returns>the string with comments removed.</returns>
        [Pure]
        public static string StripComments(this string str)
        {
            Contract.Requires(str != null);
            Contract.Ensures(Contract.Result<string>() != null);
            const string re = @"(@(?:""[^""]*"")+|""(?:[^""\n\\]+|\\.)*""|'(?:[^'\n\\]+|\\.)*')|//.*|/\*(?s:.*?)\*/";
            return Regex.Replace(str, re, "$1");
        }

        /// <summary>
        /// Removes both comments and whitespace from <paramref name="str"/>.
        /// </summary>
        /// <param name="str">the string whose comments and whitespace to remove</param>
        /// <returns>the string with comments and whitespace removed</returns>
        [Pure]
        public static string StripCommentsAndWhitespace(this string str)
        {
            Contract.Requires(str != null);
            Contract.Ensures(Contract.Result<string>() != null);
            return str.StripComments().StripWhitespace();
        }

        /// <summary>
        /// Removes all instances of an expression from the string.
        /// </summary>
        /// <param name="str">the string</param>
        /// <param name="remove">the expression to remove</param>
        /// <returns>the new string</returns>
        [Pure]
        public static string Remove(this string str, string remove)
        {
            Contract.Requires(str != null);
            Contract.Requires(remove != null);
            Contract.Ensures(!Contract.Result<string>().Contains(remove));
            return str.Replace(remove, string.Empty);
        }

        /// <summary>
        /// Gets the leading whitespace and tab characers of the string.
        /// </summary>
        /// <param name="str">the string</param>
        /// <returns>the leading whitespace and tab characters of the string</returns>
        [Pure]
        public static string GetLeadingWhitespaceAndTabs(this string str)
        {
            Contract.Requires(str != null);
            Contract.Ensures(Contract.Result<string>() != null);
            Contract.Ensures(Contract.Result<string>().Length <= str.Length);

            char[] characters = str.ToCharArray();
            string result = string.Empty;

            for (int i = 0; i < characters.Length; i++)
            {
                char c = characters[i];
                if (c == ' ' || c == '\t')
                {
                    result += c;
                }
                else
                {
                    break;
                }
            }

            return result;
        }
    }
}

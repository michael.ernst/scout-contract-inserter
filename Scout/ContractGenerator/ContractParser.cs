﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ContractGenerator
{
    /// <summary>
    /// A serializable method descriptor, including the method's signature and whether or not it is an object invariant.
    /// The object invariant flag is included since the method signature will be the qualified name of the type for object
    /// invariants.
    /// </summary>
    [Serializable]
    public struct DaikonMethodDescriptor
    {
        public Signature method;
        public bool isObjectInvariant;

        public DaikonMethodDescriptor(Signature method, bool isObjectInvariant)
        {
            this.method = method;
            this.isObjectInvariant = isObjectInvariant;
        }
    }

    /// <summary>
    /// A serializable representation of a Daikon contract.
    /// </summary>
    [Serializable]
    public class DaikonContract
    {
        public DaikonMethodDescriptor method;
        public string contract;
        public string daikonInvariant;
        public ContractKind programPointType;
        public string invariantType;
        public string[] groupVariables;
        public string[] filterVariables;

        public DaikonContract(
            Signature methodSignature, bool isObjectInvariant, string contract, string daikonContract, ContractKind programPointType,
            string invariantType, string[] groupVariables, string[] filterVariables)
        {
            this.method = new DaikonMethodDescriptor(methodSignature, isObjectInvariant);
            this.contract = contract;
            this.daikonInvariant = daikonContract;
            this.programPointType = programPointType;
            this.invariantType = invariantType;
            this.groupVariables = groupVariables;
            this.filterVariables = filterVariables;
        }
    }

    /// <summary>
    /// A collection of Daikon contracts that belong to the same method. 
    /// Required in the construction of <see cref="ProgramElement"/>
    /// </summary>
    public class DaikonElement
    {

        public Signature Signature 
        { 
            get 
            {
                return new Signature(QualifiedName, Parameters);
            }
        }

        public string QualifiedName { get; private set; }
        public ReadOnlyCollection<Parameter> Parameters { get; private set; }
        public bool IsObjectInvariant { get; private set; }
        public List<DaikonContract> Contracts { get; private set; }

        public DaikonElement(string qualifiedName, ReadOnlyCollection<Parameter> parameters, bool isObjectInvariant)
        {
            Contract.Requires(qualifiedName != null);
            Contract.Requires(parameters != null);

            QualifiedName = qualifiedName;
            Parameters = parameters;
            IsObjectInvariant = isObjectInvariant;
            Contracts = new List<DaikonContract>();
        }

        public DaikonElement(string qualifiedName, List<Parameter> parameters, bool isObjectInvariant) :
            this(qualifiedName, new ReadOnlyCollection<Parameter>(parameters), isObjectInvariant)
        {
            // NOP
        }
    }

    public static class ContractParser
    {
        private const String impliesTemplate = "({0}).Implies({1})";
        private const String daikonImpliesTemplate = "{0} ==> {1}";

        private static readonly Regex enterOrExitConditionalRegex = new Regex("(.*)\\((.*)\\):::(ENTER|EXIT)(.*);condition=\"(.*)\"");
        private static readonly Regex enterOrExitRegex = new Regex("(.*)\\((.*)\\):::(ENTER|EXIT)(.*)");
        private static readonly Regex objectRegex = new Regex("(.*):::(OBJECT)");
        private static readonly Regex ctor = new Regex("(.*)\\.\\.ctor"); // Normal constructor.
        private static readonly Regex cctor = new Regex("(.*)\\.\\.cctor"); // Static constructor. 
        private static readonly Regex genericTypeNameSuffix = new Regex("`[0-9]");

        /// <summary>
        /// Contextual information about the current program point being parsed. Only used in ParseInvariants.
        /// </summary>
        private class ParsingContext
        {
            public Signature Signature { get; private set; }
            public ContractKind ProgramPointType { get; private set; }
            public string Conditional { get; private set; }
            public string DaikonConditional { get; private set; }
            public bool ContractIsConditional
            {
                get { return Conditional != null; }
            }

            public ParsingContext(Signature signature, ContractKind programPointType, string conditional = null, string daikonConditional = null)
            {
                this.Signature = signature;
                this.ProgramPointType = programPointType;
                this.Conditional = conditional;
                this.DaikonConditional = daikonConditional;
            }
        }

        /// <summary>
        /// Parses a .contract file generated by Daikon that used the --format=csharpcontract flag. 
        /// The result is a dictionary that maps method signatures to Daikon inferred contracts.
        /// 
        /// For example, the file parsed has a format that looks something like this:
        /// A(param A, param B):::ENTER
        /// Contract 1
        /// Additional Info
        /// Contract 2
        /// Additional Info
        /// 
        /// B(param A):::ENTER
        /// Contract 3
        /// Additional Info
        /// Contract 4
        /// Additional Info
        /// 
        /// B(param A):::EXIT
        /// Contract 5
        /// Additional Info
        /// 
        /// B(param A):::EXIT37
        /// Contract 6
        /// Additional Info
        /// 
        /// B(param A):::EXIT;condition=cond
        /// Contract 7
        /// Additional Info
        /// 
        /// If this was the contract file, there would be two entries in the resulting dictionary that looks like this:
        /// A(param A, param B) => Contract 1, 2
        /// B(param A) => Contracts 3, 4, 5, cond => 7
        /// 
        /// Note that contracts under specific enter or exit points are not parsed. 
        /// Also note that conditional program point invariants are added to the program point with the same 
        /// signature but no condition. For example, Contract 7 is added to the method with signature B(param A).
        /// </summary>
        /// <param name="contractFile">the path to the file generated by Daikon</param>
        /// <returns>a mapping from method signature to the Daikon inferred contracts</returns>
        public static Dictionary<Signature, DaikonElement> ParseInvariants(string contractFile)
        {
            var result = new Dictionary<Signature, DaikonElement>();
            String[] lines = File.ReadAllLines(contractFile);

            ParsingContext context = null;
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];

                Match enterOrExitMatch = Match.Empty;
                Match objectMatch = Match.Empty;

                // Optimization to avoid computing regex.
                if (context == null)
                {
                    enterOrExitMatch = enterOrExitRegex.Match(line);
                }

                // Optimization to avoid computing regex.
                if (context == null && !enterOrExitMatch.Success)
                {
                    // Only perform this regex if the enterOrExitMatch fails.
                    objectMatch = objectRegex.Match(line);
                }      

                if (enterOrExitMatch.Success)
                {
                    // Method header match.
                    string methodName = enterOrExitMatch.Groups[1].Value;
                    string[] parameters = ReplaceCommasInBrackets(enterOrExitMatch.Groups[2].Value).Split(',');
                    string programPoint = enterOrExitMatch.Groups[3].Value;
                    string returnPoint = enterOrExitMatch.Groups[4].Value;

                    string condition = null;
                    string daikonCondition = null;

                    Match enterOrExitConditionalMatch = enterOrExitConditionalRegex.Match(line);
                    if (enterOrExitConditionalMatch.Success)
                    {
                        condition = enterOrExitConditionalMatch.Groups[5].Value;
                        daikonCondition = condition;

                        // Fix the c-sharp condition.
                        if (condition.Equals("return == true") || condition.Equals("not(return == false)"))
                        {
                            condition = "Contract.Result<bool>()";
                        }
                        else if (condition.Equals("return == false") ||condition.Equals("not(return == true)"))
                        {
                            condition = "!Contract.Result<bool>()";
                        }
                        else if (condition.StartsWith("not"))
                        {
                            // Remove the 'not' and replace it with the negation operator.
                            condition = "!" + condition.Remove(0, 3);
                        }
                    }

                    if (string.IsNullOrEmpty(returnPoint) || returnPoint.StartsWith(";"))
                    {
                        methodName = ModifyConstructorName(methodName);
                        methodName = CorrectOperatorOverLoading(methodName);
                        var theParameters = ParseParameters(parameters, line);

                        // In the Visual Studio code model, setters have no parameters. So to get a correct match we clear 
                        // the parameters. Note that the variable name ending in ".set" is not a perfect indication this is a setter.
                        if (methodName.EndsWith(".set") && parameters.Count() == 1)
                        {
                            theParameters.Clear();
                        }

                        DaikonElement element = new DaikonElement(
                            qualifiedName: methodName, 
                            parameters: theParameters, 
                            isObjectInvariant: false);

                        context = new ParsingContext(element.Signature, programPoint.GetContractKind(), condition, daikonCondition);

                        if (!result.ContainsKey(element.Signature))
                        {
                            result[element.Signature] = element;
                        }
                    }
                }
                else if (objectMatch.Success)
                {
                    // Object invariant header match.
                    string className = objectMatch.Groups[1].Value;
                    string programPoint = objectMatch.Groups[2].Value;

                    DaikonElement objectElement = new DaikonElement(className, new List<Parameter>(), true);
                    Contract.Assert(!result.ContainsKey(objectElement.Signature));
                    context = new ParsingContext(objectElement.Signature, programPoint.GetContractKind());
                    result[objectElement.Signature] = objectElement;
                }
                else if (line.StartsWith("=") || line.Equals("Exiting Daikon."))
                {
                    context = null;
                }
                else if (!string.IsNullOrEmpty(line.Trim()))
                {
                    if (context == null)
                    {
                        // This occurs when processing the junk output at the start of the file.
                        continue;
                    }

                    // Get the class or method to append the contract to.
                    DaikonElement element = result[context.Signature];

                    // Get the contract and additional information about the contract. 

                    // The contract as a c-sharp expression.
                    string contract = line;
                    // The contract as a daikon expression. Less verbose and thus easier to read.
                    string daikonContract = lines[i + 1];
                    // The class name of the Daikon invariant, such as 'daikon.inv.unary.scalar.OneOfScalar'
                    string invariantType = lines[i + 2];
                    // The important variables that make up the c-sharp expression. 
                    // Contracts are grouped by which variables they contain in the user interface.
                    string[] groupVariables = lines[i + 3].TrimEnd().Split(' ');
                    // The variables this contract can be filtered by. 
                    // The user interface allows for filtering of all contracts by certain variables. 
                    string[] filterVariables = lines[i + 4].TrimEnd().Split(' ');
                    // An asterisk character indicates the end of the data for this contract.
                    Contract.Assert(lines[i + 5].Equals("*"));

                    // If this contract was listed under a conditional program point (via a Splitter), then
                    // manually create the conditional statement that would apply to the unconditional program point.
                    if (context.ContractIsConditional)
                    {
                        contract = string.Format(impliesTemplate, context.Conditional, contract);
                        daikonContract = string.Format(daikonImpliesTemplate,context.DaikonConditional, daikonContract);
                    }

                    // Skip past the lines we just parsed.
                    i += 5;

                    // Create the new contract.
                    DaikonContract newContract = new DaikonContract(
                        methodSignature: element.Signature,
                        isObjectInvariant: element.IsObjectInvariant,
                        contract: contract,
                        daikonContract: daikonContract,
                        programPointType: context.ProgramPointType,
                        invariantType: invariantType,
                        groupVariables: groupVariables,
                        filterVariables: filterVariables);

                    // Add the contract.
                    element.Contracts.Add(newContract);
                }
            }

            return result;
        }

        /// <summary>
        /// Creates a mapping from methods to contracts from a list of contract information.
        /// </summary>
        /// <param name="contracts">The contracts</param>
        /// <returns>a mapping from methods to contracts from a list of contract information</returns>
        public static Dictionary<Signature, DaikonElement> ParseInvariants(List<DaikonContract> contracts)
        {
            Contract.Requires(contracts != null);

            var result = new Dictionary<Signature, DaikonElement>();
            foreach (var g in contracts.GroupBy(c => c.method))
            {
                var m = new DaikonElement(g.Key.method.QualifiedName, g.Key.method.Parameters, g.Key.isObjectInvariant);
                result.Add(g.Key.method, m);
                foreach (var c in g)
                {
                    m.Contracts.Add(c);
                }
            }
            return result;
        }

        /// <summary>
        /// Fixes compiler generated operator overloading strings in <paramref name="funcName"/> so it 
        /// matches the full name found in the Visual Studio code model elements. 
        /// </summary>
        /// <param name="funcName">the string to correct</param>
        /// <returns>the corrected string</returns>
        private static string CorrectOperatorOverLoading(string funcName)
        {
            // List of compiler generated operator overloading names:
            // http://forums.devx.com/showthread.php?55322-Operator-Overloading.....C-can-do-it....&p=208952#post208952
            // TODO: Complete for all possible operator overloadings.

            if(funcName.Contains("op_Addition"))
                return funcName.Replace(".op_Addition", ".operator +");

            if (funcName.Contains("op_Subtraction"))
                return funcName.Replace(".op_Subtraction", ".operator -");

            if (funcName.Contains("op_Multiply"))
                return funcName.Replace(".op_Multiply", ".operator *");

            if (funcName.Contains("op_Divide"))
                return funcName.Replace(".op_op_Divide", ".operator /");

            return funcName;
        }

        /// <summary>
        /// Modifies the Daikon constructor names to match the style used by the Visual Studio code model.
        /// </summary>
        /// <param name="constructor">The Daikon constructor name</param>
        /// <returns>The modified constructor name</returns>
        private static string ModifyConstructorName(string constructor)
        {
            if (!constructor.EndsWith("..ctor") && !constructor.EndsWith("..cctor"))
            {
                return constructor;
            }

            Match ctorMatch;
            int sub = 6;

            if (constructor.EndsWith("..ctor"))
            {
                ctorMatch = ctor.Match(constructor);
            }
            else
            {
                sub = 7;
                ctorMatch = cctor.Match(constructor);
            }

            string[] split = ctorMatch.Groups[1].Value.Split('.');
            string classname = split[split.Count() - 1];
            constructor = constructor.Substring(0, constructor.Count() - sub) + "." + classname;
            return constructor;
        }

        public static List<Parameter> ParseParameters(string[] parameters, string line = "")
        {
            var result = new List<Parameter>();

            if (!string.IsNullOrEmpty(parameters[0])) // Only if there are parameters to add.
            {
                foreach (string _parameter in parameters)
                {
                    // During parse we replaced commas inside brackets with the @ symbol
                    // so that parameters could be split by command without worrying about generics.
                    // Reverse this.
                    string parameter = _parameter.Replace('@', ',').Trim();

                    try
                    {
                        int splitIndex = Extensions.IndexOfFirstSpaceNotInGenericBrackets(parameter);
                        string celeriacType = parameter.Substring(0, splitIndex);
                        string variableName = parameter.Substring(splitIndex + 1, parameter.Length - splitIndex - 1);
                        string codeModelType = genericTypeNameSuffix.Replace(celeriacType, string.Empty);
                        result.Add(new Parameter(codeModelType, variableName));
                    }
                    catch (Exception)
                    {
                        throw new ContractGenerationException(
                            string.Format("Parser error: failed to process parameters of line \"{0}\" in the contract file", line));
                    }
                }
            }

            return result;
        }

        // Replace any commas inside <> and [] with the @ character. 
        private static string ReplaceCommasInBrackets(string str)
        {
            int inside = 0;
            char[] characters = str.ToCharArray();

            for (int i = 0; i < characters.Length; i++)
            {
                char c = characters[i];
                if (c == '<' || c == '[') inside++;
                else if (c == '>' || c == ']') inside--;
                else if (characters[i] == ',')
                {
                    if (inside != 0)
                    {
                        characters[i] = '@';
                    }
                }
            }
            return new string(characters);
        }
    }
}

﻿using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Text.RegularExpressions;

namespace ContractGenerator
{
    public class Log : IDisposable
    {
        private static readonly string space = " ";
        private static readonly string tab = space.Repeat(4);
        private static readonly string contractTag = "contract";
        private static readonly string filterTag = "filter";
        private static readonly string selectionTag = "selection";
        private static readonly string groupingTag = "grouping";
        private static readonly string timestampTag = "timestamp";
        private static readonly string methodTag = "method";
        private static readonly string kindTag = "kind";
        private static readonly string contractTextTag = "text";
        private static readonly string statusTag = "status";
        private static readonly string contractClassificationTag = "classification";
        private static readonly string variablesTag = "variables";
        private static readonly string variableTag = "var";
        private static readonly string cDataTemplate = "<![CDATA[{0}]]>";

        private readonly StreamWriter stream;

        public string FilePath { get; private set; }

        public Log(string path)
        {
            FilePath = path;
            stream = File.Exists(path) ? 
                File.AppendText(path) : new StreamWriter(path);
        }

        #region IDisposable

        ~Log() 
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (stream != null)
                {
                    stream.Flush();
                    stream.Close();
                    stream.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        private void PrintTag(string tag, bool open, int indentLevel)
        {
            Contract.Requires(!string.IsNullOrEmpty(tag));

            string indentation = indentLevel == 0 ?
                string.Empty : tab.Repeat(indentLevel);

            string format = open ?
                "{0}<{1}>" : "{0}</{1}>";
            string xmlTag = string.Format(format, indentation, tag);
            WriteLine(xmlTag);
        }

        private void PrintXmlElement(string tag, string content, int indentLevel)
        {
            Contract.Requires(!string.IsNullOrEmpty(tag));
            Contract.Requires(tag != null);
            Contract.Requires(indentLevel >= 0);

            string indentation = indentLevel == 0 ?
                string.Empty : tab.Repeat(indentLevel);

            string xmlElement = string.Format
                ("{0}<{1}>{2}</{1}>", indentation, tag, content);
            WriteLine(xmlElement);
        }

        public void LogSelection(string methodName)
        {
            string currentTime = DateTime.Now.ToString();
            string wrappedMethodName = string.Format(cDataTemplate, methodName);
            PrintTag(selectionTag, true, 0);
            PrintXmlElement(methodTag, wrappedMethodName, 1);
            PrintTag(selectionTag, false, 0);
        }

        public void LogContract(ProgramContract contract, bool inserted)
        {
            Contract.Requires(contract != null);

            string currentTime = DateTime.Now.ToString();
            string methodName = string.Format(cDataTemplate, contract.ContainingElement.Signature.ToString());
            string kind = contract.Kind.ToString();
            string contractText = string.Format(cDataTemplate, contract.ContractText);
            string classification = contract.SemanticClassification;
            string status = inserted.ToString();

            PrintTag(contractTag, true, 0);
            PrintXmlElement(timestampTag, currentTime, 1);
            PrintXmlElement(methodTag, methodName, 1);
            PrintXmlElement(kindTag, kind, 1);
            PrintXmlElement(contractTextTag, contractText, 1);
            PrintXmlElement(contractClassificationTag, classification, 1);
            PrintXmlElement(statusTag, status, 1);
            PrintTag(contractTag, false, 0);
        }

        public void LogFilter(ContractFilter filter, bool added)
        {
            Contract.Requires(filter != null);

            string currentTime = DateTime.Now.ToString();
            string methodName = string.Format(cDataTemplate, filter.Element.Signature.ToString());
            string kind = filter.InvariantType.ToString();
            string status = added.ToString();

            PrintTag(filterTag, true, 0);
            PrintXmlElement(timestampTag, currentTime, 1);
            PrintXmlElement(methodTag, methodName, 1);
            PrintXmlElement(kindTag, kind, 1);
            PrintXmlElement(statusTag, status, 1);
            PrintTag(variablesTag, true, 1);

            foreach (string variable in filter.Variables)
            {
                PrintXmlElement(variableTag, variable, 2);
            }

            PrintTag(variablesTag, false, 1);
            PrintTag(filterTag, false, 0);
        }

        public void LogGrouping(string groupName)
        {
            Contract.Requires(!string.IsNullOrEmpty(groupName));
            PrintTag(groupingTag, true, 0);
            PrintXmlElement(kindTag, groupName, 1);
            PrintTag(groupingTag, false, 0);
        }

        public void WriteLine(string line = "")
        {
            Contract.Requires(line != null);
            stream.WriteLine(line);
            stream.Flush();
        }

        public void WriteHeader()
        {
            stream.WriteLine("-----".Repeat(12));
            stream.WriteLine(DateTime.Now);
            stream.WriteLine();
            stream.Flush();
        }

        public void Close()
        {
            stream.Close();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;
using System.Text.RegularExpressions;
using System.Diagnostics.Contracts;
using System.Xml.Linq;
using ContractGenerator.Contracts;

namespace ContractGenerator
{
    /// <summary>
    /// Represents the source and XML documentation of a method in the project.
    /// </summary>
    public class SourceLocation
    {
        [ContractInvariantMethod]
        private void Invariants()
        {
            Contract.Invariant(Start != null);
            Contract.Invariant(End != null);
            Contract.Invariant(Source != null);
            Contract.Invariant(XmlDoc != null);
        }

        /// <summary>
        /// The start point of the source code.
        /// </summary>
        public TextPoint Start { get; private set; }

        /// <summary>
        /// The end point of the source code.
        /// </summary>
        public TextPoint End { get; private set; }

        /// <summary>
        /// Gets the lines of the source code.
        /// </summary>
        /// <remarks>Similar to the Source property except common leading whitespace is not trimmed. </remarks>
        private List<string> Lines
        {
            get
            {
                Contract.Ensures(Contract.Result<List<string>>() != null);
                var lines = new List<string>();
                EditPoint editPoint = Start.CreateEditPoint();

                for (int i = Start.Line; i <= End.Line; i++)
                {
                    string line = editPoint.GetLines(i, i + 1);
                    lines.Add(line);
                }

                return lines;
            }
        }

        /// <summary>
        /// Gets the first line of the source code.
        /// </summary>
        private string FirstLine
        {
            get
            {
                Contract.Ensures(Contract.Result<string>() != null);
                EditPoint editPoint = Start.CreateEditPoint();
                int startLine = Start.Line;
                return editPoint.GetLines(startLine, startLine + 1);
            }
        }

        /// <summary>
        /// True if the soure code uses tabs for spacing. 
        /// We assume this is true if the first character of the first line is a tab ('\t').
        /// </summary>
        private bool UsesTabSpacing
        {
            get
            {
                string firstLine = FirstLine;
                return firstLine.Length != 0 && firstLine[0] == '\t';
            }
        }

        /// <summary>
        /// Gets the contracts of the source code.
        /// </summary>
        public IEnumerable<string> Contracts
        {
            get
            {
                var contracts = new List<string>();

                foreach (string lines in Lines)
                {
                    string line = lines.Trim();
                    // Perform a split by semicolon, but not inside quotes. 
                    string[] statements = Regex.Split(line, ";(?=(?:[^']*'[^']*')*[^']*$)");
                    contracts.AddRange(from statement in statements where statement.IsContractStatement() select statement + ";");
                }
                return contracts;
            }
        }

        /// <summary>
        /// Gets the lines of the source code with common leading whitespace trimmed. 
        /// TODO: Issue #87
        /// </summary>
        public string Source
        {
            get
            {
                Contract.Ensures(Contract.Result<string>() != null);

                int offset = Start.LineCharOffset;
                StringBuilder source = new StringBuilder();

                // Find smallest whitespace offset.
                foreach (string line in Lines)
                {
                    if (!string.IsNullOrEmpty(line.Trim()))
                    {
                        int numWhitespace = line.Length - line.TrimStart(' ').Length;
                        offset = Math.Min(offset, numWhitespace + 1);
                    }
                }

                foreach (string line in Lines)
                {
                    if (!string.IsNullOrEmpty(line.Trim()))
                    {
                        string remove = line.Remove(0, offset - 1);
                        source.AppendLine(remove);
                    }
                    else
                    {
                        source.AppendLine(line);
                    }
                }

                return source.ToString();
            }
        }

        /// <summary>
        /// Gets the text of the documentation comment as valid Xml.
        /// </summary>
        public string XmlDoc
        {
            get
            {
                Contract.Ensures(Contract.Result<string>() != null);

                string xmlDoc = XmlDocRaw;
                xmlDoc = xmlDoc.Replace("<doc>\r\n", "");
                xmlDoc = xmlDoc.Replace("</doc>\n", "");
                return xmlDoc;
            }
        }

        /// <summary>
        /// Gets or sets the text of the containing CodeElements documentation comment.
        /// Note: this does not return a valid Xml document: use XmlDoc instead. 
        /// </summary>
        private string XmlDocRaw
        {
            get
            {
                Contract.Ensures(Contract.Result<string>() != null);

                CodeElement element = CodeElement;
                string result = string.Empty;

                if (element == null)
                    return string.Empty;
                if (element.Kind == vsCMElement.vsCMElementFunction)
                    result = ((CodeFunction)element).DocComment + "\n";
                if (element.Kind == vsCMElement.vsCMElementProperty)
                    result = ((CodeProperty)element).DocComment + "\n";

                // result = SecurityElement.Escape(result);
                // TODO: Fix problems with illegal characters in XML documentation.    
                result = result.Replace("&", "&amp;");
                return result;
            }
            set
            {
                CodeElement element = CodeElement;

                if (element == null)
                    return;
                if (element.Kind == vsCMElement.vsCMElementFunction)
                    ((CodeFunction)element).DocComment = value;
                if (element.Kind == vsCMElement.vsCMElementProperty)
                    ((CodeProperty)element).DocComment = value;
            }
        }

        /// <summary>
        /// The name of the containing <see cref="ProjectItem"/>.
        /// </summary>
        public string CodeFile
        {
            get
            {
                return CodeElement.ProjectItem.Name;
            }
        }

        /// <summary>
        /// The corresponding method or property element, <c>null</c> if the source location
        /// does not correspond to a method or property.
        /// </summary>
        private CodeElement CodeElement
        {
            get
            {
                EditPoint ep = Start.CreateEditPoint();
                
                try
                {
                    return ep.CodeElement[vsCMElement.vsCMElementFunction];   
                }
                catch { }

                try
                {
                    return ep.CodeElement[vsCMElement.vsCMElementProperty];
                }
                catch { }

                return null;
            }
        }

        public SourceLocation(CodeType typeRef) : this(typeRef.GetStartPoint(), typeRef.GetEndPoint())
        {
            Contract.Requires(typeRef != null);
        }

        public SourceLocation(CodeFunction functionRef) : this(functionRef.GetStartPoint(), functionRef.GetEndPoint()) 
        {
            Contract.Requires(functionRef != null);
        }

        public SourceLocation(CodeInterface interfaceRef) : this(interfaceRef.GetStartPoint(), interfaceRef.GetEndPoint()) 
        {
            Contract.Requires(interfaceRef != null);
        }

        public SourceLocation(CodeClass classRef) : this(classRef.GetStartPoint(), classRef.GetEndPoint()) 
        {
            Contract.Requires(classRef != null);  
        }

        public SourceLocation(CodeProperty propertyRef) : this(propertyRef.GetStartPoint(), propertyRef.GetEndPoint()) 
        {
            Contract.Requires(propertyRef != null);
        }

        public SourceLocation(TextPoint start, TextPoint end)
        {
            Contract.Requires(start != null);
            Contract.Requires(end != null);

            this.Start = start;
            this.End = end;
        }

        private static string codeContractsUsingStatement = "using System.Diagnostics.Contracts;";
        private static string extensionLibraryUsingStatement = "using CSharpDaikonLib;";
        private static string linqUsingStatement = "using System.Linq;";
        private static string systemUsingStatement = "using System;";

        /// <summary>
        /// Inserts the using statements required by the contract.
        /// 
        /// If the document contains using statements, the new using statements are inserted after the 
        /// first set of using statements found. If the document does not contain any using statements, 
        /// the new using statements are inserted at the start of the document.
        /// </summary>
        /// <param name="contract">the contract that was just inserted</param>
        private void InsertMissingUsingStatements(ProgramContract contract)
        {
            Contract.Requires(contract != null);

            var doc = Start.Parent;
            Contract.Assert(doc != null);
            var start = doc.StartPoint;
            var end = doc.EndPoint;
            EditPoint editPoint = start.CreateEditPoint();
            EditPoint editPointOfLastUsing = null;
            bool usingExtensionLibary = false;
            bool usingContracts = false;
            bool usingLinq = false;
            bool usingSystem = false;

            // Loop through the using statements. Note that it is not sufficient to stop
            // searching once we find a namespace because using statements can be declared within namespaces.
            for (int i = start.Line; i <= end.Line; i++)
            {
                string line = editPoint.GetLines(i, i + 1).Trim();

                if (!line.StartsWith("using") && !string.IsNullOrEmpty(line) && editPointOfLastUsing != null)
                {
                    break;
                }
                else if (line.StartsWith("using"))
                {
                    usingContracts |= line.Equals(codeContractsUsingStatement);
                    usingExtensionLibary |= line.Equals(extensionLibraryUsingStatement);
                    usingLinq |= line.Equals(linqUsingStatement);
                    usingSystem |= line.Equals(systemUsingStatement);
                    editPointOfLastUsing = editPoint.CreateEditPoint();
                }
                editPoint.LineDown();
            }

            EditPoint insertionPoint;
            string template;

            // If we saw using statements, insert after the last statement, otherwise
            // insert at the start of the document.
            if(editPointOfLastUsing != null)
            {
                insertionPoint = editPointOfLastUsing;
                insertionPoint.EndOfLine();
                string lastUsingLine = editPoint.GetLines(editPointOfLastUsing.Line, editPointOfLastUsing.Line + 1);
                // Base the indentation off of the indentation of the previous using statement.
                string indent = lastUsingLine.GetLeadingWhitespaceAndTabs();
                template = Environment.NewLine + indent + "{0}";
            }
            else
            {
                insertionPoint = start.CreateEditPoint();
                template = "{0}" + Environment.NewLine;
            }

            // Insert using statements as required.
            if(!usingContracts)
            {
                insertionPoint.Insert(string.Format(template, codeContractsUsingStatement));
                insertionPoint.EndOfLine();
            }

            if(contract.RequiresSystemReference && !usingSystem)
            {
                insertionPoint.Insert(string.Format(template, systemUsingStatement));
                insertionPoint.EndOfLine();
            }

            if(contract.RequiresLinqReference && !usingLinq)
            {
                insertionPoint.Insert(string.Format(template, linqUsingStatement));
                insertionPoint.EndOfLine();
            }

            if(contract.RequiresExtensionLibraryReference && !usingExtensionLibary)
            {
                insertionPoint.Insert(string.Format(template, extensionLibraryUsingStatement));
                insertionPoint.EndOfLine();
            }
        }

        /// <summary>
        /// Returns <c>true</c> iff documentation exists for the given contract.
        /// </summary>
        /// <param name="contract">a non-null contract</param>
        /// <returns><c>true</c> iff documentation exists for the given contract</returns>
        public bool DocumentationExists(ProgramContract contract)
        {
            Contract.Requires(contract != null);

            string raw = XmlDocRaw;
            var doc = XDocument.Parse(raw);
            var node = doc.Descendants(contract.Kind.XmlDocKey()).SingleOrDefault(x => (x.Attribute("text")).Value.Equals(contract.DaikonContractText));

            return node != null;
        }

        /// <summary>
        /// Inserts <paramref name="contract"/> as Xml documentation. 
        /// </summary>
        /// <param name="contract">The contract to insert as documentation</param>
        public void InsertContractDocumentation(ProgramContract contract)
        {
            Contract.Requires(contract != null);

            string newDocComment = InsertDocumentationNode(XmlDocRaw, contract.DaikonContractText, contract.Kind.XmlDocKey());
            XmlDocRaw = newDocComment;
        }

        /// <summary>
        /// Inserts <paramref name="text"/> as a new Xml node into <paramref name="xml"/>.
        /// <paramref name="xmlSearchKey"/> determines the placement of the node if other
        /// contracts exist as documentation. For example, requires contracts are inserted
        /// before the first ensures contract. 
        /// 
        /// <paramref name="xmlSearchKey"/> must be one of "invariant", "ensures", or "requires". 
        /// </summary>
        /// <param name="xml">The current Xml documentation</param>
        /// <param name="text">The text of the added Xml node</param>
        /// <param name="xmlSearchKey">The search key (the name) to associate with the added Xml node.
        /// Determines the location of the added node.</param>
        /// <returns>The new Xml documentation</returns>
        private static string InsertDocumentationNode(string xml, string text, string xmlSearchKey)
        {
            Contract.Requires(xml != null);
            Contract.Requires(!String.IsNullOrEmpty(text));
            Contract.Requires(xmlSearchKey.OneOf("invariant", "ensures", "requires"));
            Contract.Ensures(Contract.Result<string>() != null);

            XDocument doc = XDocument.Parse(xml);
            var lastRequires = doc.Descendants("requires").LastOrDefault();
            var firstEnsures = doc.Descendants("ensures").FirstOrDefault();

            // Create the new XML element to add. 
            var node = new XElement(xmlSearchKey, new XAttribute("text", text));
            node.Value = text;

            if ((lastRequires == null && firstEnsures == null) || xmlSearchKey.Equals("invariants") || xmlSearchKey.Equals("ensures"))
            {
                doc.Root.Add(node);
            }
            else if (firstEnsures != null)
            {
                firstEnsures.AddBeforeSelf(node);
            }
            else if (firstEnsures == null && lastRequires != null)
            {
                lastRequires.AddAfterSelf(node);
            }

            return doc.ToString();
        }

        /// <summary>
        /// Removes the Xml documentation for <paramref name="contract"/> if it exists. 
        /// </summary>
        /// <param name="contract">The contract whose Xml documentation to remove</param>
        public void DeleteContractDocumentation(ProgramContract contract)
        {
            Contract.Requires(contract != null);

            string newDocComment = RemoveDocumentationNode(XmlDocRaw, contract.DaikonContractText, contract.Kind.XmlDocKey());
            XmlDocRaw = newDocComment;
        }

        /// <summary>
        /// Removes the Xml node with text <paramref name="text"/> and name <paramref name="xmlSearchKey"/>
        /// if it exists in <paramref name="xml"/>.
        /// 
        /// <paramref name="xmlSearchKey"/> must be one of "invariant", "ensures", or "requires". 
        /// </summary>
        /// <param name="xml">The Xml to remove the documentation from</param>
        /// <param name="text">The text of the Xml node to remove</param>
        /// <param name="xmlSearchKey">The name of the Xml node to remove</param>
        /// <returns></returns>
        private static string RemoveDocumentationNode(string xml, string text, string xmlSearchKey)
        {
            Contract.Requires(xml != null);
            Contract.Requires(!String.IsNullOrEmpty(text));
            Contract.Requires(xmlSearchKey.OneOf("invariant", "ensures", "requires"));
            Contract.Ensures(Contract.Result<string>() != null);

            XDocument doc = XDocument.Parse(xml);
            var node = doc.Descendants(xmlSearchKey).SingleOrDefault(x => (x.Attribute("text")).Value.Equals(text));

            if (node != null)
            {
                node.Remove();
            }

            return doc.ToString();
        }

        /// <summary>
        /// Inserts <paramref name="contract"/> as text into the source code.
        /// </summary>
        /// <param name="contract">The contract whose text to insert</param>
        /// <param name="contractIndices">Indicates where this contract should be inserted relative to other contracts in the same method</param>
        public void InsertContract(ProgramContract contract, Dictionary<string, int> contractIndices)
        {
            Contract.Requires(contract != null);
            Contract.Requires(contractIndices != null);
            Contract.Requires(contractIndices.ContainsKey(contract.CodeContractText.StripWhitespace()));

            EditPoint source = Start.CreateEditPoint();
            int indent = Start.DisplayColumn; 
            string whitespace = " ".Repeat(indent - 1);
            string contractExpression = Environment.NewLine + whitespace + "    " + contract.CodeContractText + " // Daikon";

            if(UsesTabSpacing)
            {
                contractExpression = contractExpression.Replace("    ", "\t");
            }

            // Get the line number where this contract should be inserted. 
            int insertionLine = FindInsertionLine(contract, contractIndices);

            if (insertionLine >= 0)
            {
                var lines = Lines;
                for (int i = 0; i < lines.Count() - 1; i++)
                {
                    if (i == insertionLine)
                    {
                        source.EndOfLine();
                        source.Insert(contractExpression);
                        break;
                    }
                    source.LineDown();
                }
            }
            else
            {
                source.FindPattern("{", 0, source);
                source.WordRight();
                if (source.AtEndOfLine)
                {
                    source.Insert(contractExpression);
                }
                else
                {
                    // TODO: Issue #112 - contracts cannot be inserted onto auto-implemented properties.
                    source.Insert(contractExpression + Environment.NewLine + whitespace + "    ");
                }
            }


            InsertMissingUsingStatements(contract);
        }

        /// <summary>
        /// Deletes the text of <paramref name="contract"/> from the source code.
        /// </summary>
        /// <param name="contract">The contract whose text to delete</param>
        public void DeleteContract(ProgramContract contract)
        {
            Contract.Requires(contract != null);

            // TODO: Issue #19 - contract deletion can fail when contracts are not one per line.
            var lines = Lines;
            EditPoint source = Start.CreateEditPoint();

            for (int i = 0; i < lines.Count(); i++)
            {
                string line = lines[i];
                if (contract.CodeContractTextNoWhitespace.Equals(line.StripCommentsAndWhitespace()))
                {
                    // Move to the start of the line, then delete it. We add 1 to delete the newline.
                    source.StartOfLine();
                    Contract.Assert(source.AtStartOfLine);
                    source.Delete(source.LineLength + 1);
                    break;
                }
                source.LineDown();
            }
        }

        /// <summary>
        /// Returns the line number in the method where <paramref name="contract"/> should be inserted.
        /// Ordering relative to other contracts inserted in the method is determined by <paramref name="contractIndices"/>,
        /// which maps contracts to the index that contract should appear. 
        /// </summary>
        /// <param name="contract">The contract whose insertion line number to determine</param>
        /// <param name="contractIndices">Indicates where this contract should be inserted relative to other contracts in the same method.</param>
        /// <returns>The line number in the method where <paramref name="contract"/> should be inserted.
        /// If -1 is returned then contract may be inserted as the first contract in source.</returns>
        private int FindInsertionLine(ProgramContract contract, Dictionary<string, int> contractIndices)
        {
            Contract.Requires(contract != null);
            Contract.Requires(contractIndices != null);
            Contract.Requires(contractIndices.ContainsKey(contract.CodeContractText.StripWhitespace()));
            Contract.Ensures(Contract.Result<int>() >= -1);

            // TODO: Clean up SourceLocation.FindInsertionLine
            int index = contract.InsertionIndex;
            ContractKind programPoint = contract.Kind;
            var lines = Lines;
            int lastRequiresLineNum = -1;

            var tuples = new List<Tuple<int, int>>();
            for (int i = 0; i < lines.Count(); i++)
            {
                string line = lines[i].StripComments().StripWhitespace();
                if (line.IsContractStatement())
                {
                    if(programPoint == line.GetContractKind()) 
                    {
                        int currIndex;
                        if (contractIndices.TryGetValue(line, out currIndex))
                        {
                            tuples.Add(new Tuple<int, int>(currIndex, i));
                        }        
                    }

                    if (line.GetContractKind() == ContractKind.Requires)
                    {
                        lastRequiresLineNum = i;
                    }
                }
            }

            int count = tuples.Count();
            if (count == 0)
            {
                if (lastRequiresLineNum >= 0 && programPoint == ContractKind.Ensures)
                {
                    // Put ensures after requires. 
                    return lastRequiresLineNum;
                }
                else
                {
                    return -1;
                }
            }
            else if (count == 1)
            {
                int singleIndex = tuples[0].Item1;
                int lineNum = tuples[0].Item2;
                if (index < singleIndex)
                {
                    return lineNum - 1;
                }
                else if (index > singleIndex)
                {
                    return lineNum;
                }
                else
                {
                    return -1;
                }
            }
            else // count >= 2
            {
                int endn = -1;
                for (int i = 0; i < tuples.Count()-1; i++)
                {
                    int i1 = tuples[i].Item1;
                    int ln1 = tuples[i].Item2;
                    int i2 = tuples[i + 1].Item1;
                    int ln2 = tuples[i + 1].Item2;

                    if (index < i1 && index < i2)
                    {
                        return ln1 - 1;
                    }
                    else if (index > i1 && index < i2)
                    {
                        return ln2 - 1;
                    }
                    else if (index > i1 && index > i2)
                    {
                        endn = ln2;
                    }
                }
                return endn;
            }
        }
    }
}

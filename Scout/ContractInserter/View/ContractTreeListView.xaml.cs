﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using ContractGenerator;

namespace UniversityofWashington.ContractInserter
{
    /// <summary>
    /// Interaction logic for ContractTreeListView.xaml
    /// </summary>
    public partial class ContractTreeListView
    {
        public static readonly DependencyProperty ProjectModelProperty = DependencyProperty.Register
            ("ProjectModel", typeof(ProjectModel), typeof(ContractTreeListView));

        /// <summary>
        /// The project model for the currently generated project.
        /// </summary>
        /// <remarks>
        /// This binds to ViewModel.ProjectModel and is used to retrieve the <see cref="ProgramElement"/>
        /// of the clicked row to display the filtering menu. 
        /// </remarks>
        public ProjectModel ProjectModel
        {
            get 
            { 
                return (ProjectModel)GetValue(ProjectModelProperty); 
            }
            set 
            { 
                SetValue(ProjectModelProperty, value); 
            }
        }

        /// <summary>
        /// The currently highlighted rows.
        /// </summary>
        private readonly List<ContractRow> highlightedRows;

        public ContractTreeListView()
        {
            InitializeComponent();
            highlightedRows = new List<ContractRow>();
        }

        // When the text of a row is right clicked, and that row represents a contract, display the contextual filtering dialog.
        private void TextBlock_MouseRightButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            object data = (sender as TextBlock).DataContext;
            ContractRow row = (ContractRow)data;

            if (data != null && ProjectModel != null && row.IsContract)
            {
                // Get the program contract associated with the clicked row.
                ProgramContract contract = ProjectModel.GetProgramElement(row.Signature).FindContract(row.Id);
                Point clickLocation = e.GetPosition(this);
                ContextualContractFilterDialog contextFilter = new ContextualContractFilterDialog(contract);
                Point displayLocation = this.PointToScreen(clickLocation);
                contextFilter.Show((int)displayLocation.X, (int)displayLocation.Y);   
            }
        }

        private void HighlightOnMouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ContractRow row;

            if (sender is TextBlock)
            {
                var textBlock = (TextBlock)sender;
                row = (ContractRow)textBlock.DataContext;
            }
            else
            {
                var checkBox = (System.Windows.Controls.CheckBox)sender;
                row = (ContractRow)checkBox.DataContext;
            }

            unhighlightRows();

            if (row.IsContract)
            {
                highlightedRows.Add(row);
                row.BackgroundColor = new SolidColorBrush(Colors.LightGray);

                foreach (ContractRow _row in row.IdenticalRows)
                {
                    highlightedRows.Add(_row);
                    _row.BackgroundColor = new SolidColorBrush(Colors.LightGray);
                }

                foreach (ContractRow _row in row.SimilarRows)
                {
                    highlightedRows.Add(_row);
                    _row.BackgroundColor = new SolidColorBrush(Colors.LightBlue);
                }
            }
        }

        private void UnhighlightOnMouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            unhighlightRows();
        }

        private void unhighlightRows()
        {
            foreach (ContractRow _row in highlightedRows)
            {
                _row.BackgroundColor = new SolidColorBrush(Colors.White);
            }

            highlightedRows.Clear();
        }
    }
}

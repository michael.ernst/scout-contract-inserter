﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace UniversityofWashington.ContractInserter
{
    /// <summary>
    /// Interaction logic for HyperLinkText.xaml
    /// </summary>
    public partial class HyperLinkText
    {     
        #region Dependency Properties

        public static readonly DependencyProperty TextBeforeHyperLinkProperty =
            DependencyProperty.Register("TextBeforeHyperLink", typeof(string), typeof(HyperLinkText), new PropertyMetadata(string.Empty));

        public static readonly DependencyProperty HyperLinkTextProperty =
            DependencyProperty.Register("TextHyperLink", typeof(string), typeof(HyperLinkText), new PropertyMetadata(string.Empty));

        public static readonly DependencyProperty TextAfterHyperLinkProperty =
            DependencyProperty.Register("TextAfterHyperLink", typeof(string), typeof(HyperLinkText), new PropertyMetadata(string.Empty));

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(HyperLinkText), new PropertyMetadata(null));

        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register("CommandParameter", typeof(object), typeof(HyperLinkText), new PropertyMetadata(string.Empty));

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(string), typeof(HyperLinkText), new PropertyMetadata(string.Empty));

        #endregion

        #region Backing Properties

        /// <summary>
        /// The text prior to the hyper-link.
        /// </summary>
        public string TextBeforeHyperLink
        {      
            get { return (string)GetValue(TextBeforeHyperLinkProperty); }
            set { SetValue(TextBeforeHyperLinkProperty, value); }
        }

        /// <summary>
        /// The text of the hyper-link.
        /// </summary>
        public string TextHyperLink
        {
            get { return (string)GetValue(HyperLinkTextProperty); }
            set { SetValue(HyperLinkTextProperty, value); }
        }

        /// <summary>
        /// The text after the hyper-link.
        /// </summary>
        public string TextAfterHyperLink
        {
            get { return (string)GetValue(TextAfterHyperLinkProperty); }
            set { SetValue(TextAfterHyperLinkProperty, value); }
        }

        /// <summary>
        /// The hyper-link command.
        /// </summary>
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        /// <summary>
        /// The hyper-link string command parameter.
        /// </summary>
        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        /// <summary>
        /// The orientation of the control.
        /// </summary>
        public string Orientation
        {
            get { return (string)GetValue(CommandParameterProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        #endregion

        public HyperLinkText()
        {           
            this.InitializeComponent();
            this.Layout.DataContext = this;
            this.Orientation = "Horizontal";
        }
    }
}

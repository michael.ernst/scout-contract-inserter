﻿using System.Diagnostics.Contracts;
using System.Windows;
using System.Windows.Controls;

namespace UniversityofWashington.ContractInserter
{
    /// <summary>
    /// Interaction logic for FilterListView.xaml
    /// </summary>
    public partial class FilterListView
    {
        public FilterListView()
        {
            InitializeComponent();
        }

        private void MenuItemDelete_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = (MenuItem)e.Source;
            ContextMenu contextMenu = menuItem.CommandParameter as ContextMenu;
            Contract.Assert(contextMenu != null);
            ListViewItem item = (ListViewItem)contextMenu.PlacementTarget;
            FilterRowViewModel filterRow = ((FilterRowViewModel)(item.Content));
            filterRow.Filter.Remove();
        }
    }
}

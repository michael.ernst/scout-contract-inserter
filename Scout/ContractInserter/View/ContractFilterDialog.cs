﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using ContractGenerator;

namespace UniversityofWashington.ContractInserter
{
    public class ContextualContractFilterDialog : ContextMenuStrip
    {
        public ProgramContract contract { get; private set; }

        public ContextualContractFilterDialog(ProgramContract contract)
        {
            this.contract = contract;
            ShowCheckMargin = false;
            ShowImageMargin = false;

            ProgramElement element = contract.ContainingElement;
            int varCount = contract.Variables.Count();
            string[] variables = contract.Variables.ToArray();

            Items.Add(new HtmlMenuStripItem(Resources.FilterOptionsHeader, false, this));

            if (contract.IsTypeOfInvariant)
            {
                Items.Add(new FilterOption(element, InvariantType.TypeOf, new string[] { }, this));
            }

            if (varCount == 1)
            {
                Items.Add(new FilterOption(element, InvariantType.All, variables, this));
                Items.Add(new FilterOption(element, InvariantType.Unary, variables, this));
            }
            else if (varCount == 2)
            {
                string[] s1 = { variables[0] };
                string[] s2 = { variables[1] };
                Items.Add(new FilterOption(element, InvariantType.All, s1, this));
                Items.Add(new FilterOption(element, InvariantType.All, s2, this));
                Items.Add(new FilterOption(element, InvariantType.Binary, variables, this));
            }
            else
            {
                Debug.Assert(varCount == 3);
                string[] s1 = { variables[0], variables[1] };
                string[] s2 = { variables[0], variables[2] };
                string[] s3 = { variables[1], variables[2] };
                Items.Add(new FilterOption(element, InvariantType.BinaryAndTernary, s1, this));
                Items.Add(new FilterOption(element, InvariantType.BinaryAndTernary, s2, this));
                Items.Add(new FilterOption(element, InvariantType.BinaryAndTernary, s3, this));
                Items.Add(new FilterOption(element, InvariantType.Ternary, variables, this));
            }
        }
    }

    public class RemoveContractFilterDialog : ContextMenuStrip
    {
        public RemoveContractFilterDialog(ContractFilter filter)
        {
            Items.Add(new RemoveFilterOption(this, filter));
        }
    }

    public class RemoveFilterOption : HtmlMenuStripItem
    {
        private readonly ContractFilter filter;
        private static readonly string text = Resources.RemoveFilterMessage;

        public RemoveFilterOption(ContextMenuStrip parent, ContractFilter filter) : base (parent)
        {
            this.filter = filter;
            Label.Text = text;
            Click += RemoveFilterOption_Click;
        }

        private void RemoveFilterOption_Click(object sender, EventArgs e)
        {
            filter.Remove();
        }
    }

    public class FilterOption : HtmlMenuStripItem
    {
        private readonly string text;
        private readonly List<ContractFilter> filtersToApply;
        
        public FilterOption(ProgramElement element, InvariantType invariantType, IEnumerable<string> variables, ContextMenuStrip parent) : base(parent)
        {
            filtersToApply = new List<ContractFilter>();
            string[] colors = { "red", "blue", "green" };
            string separator = Resources.CommaSeparator;

            if (invariantType == InvariantType.TypeOf)
            {
                text = Resources.FilterTypeOfMessage;
            }
            else
            {
                if (invariantType == InvariantType.Binary || invariantType == InvariantType.Ternary)
                {
                    text = string.Format(Resources.FilterBinaryOrTernaryMessageStart, invariantType.ToString().ToLower());
                    separator = Resources.AndSeparator;
                }
                else if (invariantType == InvariantType.BinaryAndTernary)
                {
                    text = Resources.FilterBinaryAndTernaryMessageStart;
                    separator = Resources.AndSeparator;
                }
                else if (invariantType == InvariantType.Unary || invariantType == InvariantType.All)
                {
                    text = string.Format(Resources.FilterUnaryOrAllMessageStart, invariantType.ToString().ToLower());
                }

                for (int i = 0; i < variables.Count(); i++)
                {
                    text += "<span style=color:" + colors[i] + ">" + variables.ElementAt(i) + "</span>";

                    if (i != variables.Count() - 1)
                    {
                        text += separator;
                    }
                }
            }

            if (invariantType != InvariantType.BinaryAndTernary)
            {
                filtersToApply.Add(new ContractFilter(element, invariantType, variables));
            }
            else
            {
                filtersToApply.Add(new ContractFilter(element, InvariantType.Binary, variables));
                filtersToApply.Add(new ContractFilter(element, InvariantType.Ternary, variables));
            }

            Label.Text = text;
            Click += OnClick;
        }

        private void OnClick(object sender, EventArgs e)
        {
            foreach (var filter in filtersToApply)  filter.Apply();
            if (ColorOnHover) parent.Close();
        }
    }

    public class HtmlMenuStripItem : ToolStripControlHost
    {
        public bool ColorOnHover { get; private set; }

        protected ContextMenuStrip parent;

        public HtmlLabel Label
        {
            get
            {
                return Control as HtmlLabel;
            }
        }

        private static System.Windows.Forms.HtmlLabel Create()
        {
            var label = new HtmlLabel();
            label.AutoScroll = false;
            label.AutoSize = true;
            label.MinimumSize = label.Size;
            return label;
        }

        public HtmlMenuStripItem(ContextMenuStrip parent) : this(string.Empty, true, parent) { }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HtmlMenuStripItem(string text, bool colorOnHover, ContextMenuStrip parent): base(Create())
        {
            this.parent = parent;
            this.ColorOnHover = colorOnHover;
            Visible = true;
            Enabled = true;
            Label.Text = text;
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            if (ColorOnHover) this.BackColor = Color.LightBlue;
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            if (ColorOnHover) this.BackColor = Color.White;
        }
    }

}

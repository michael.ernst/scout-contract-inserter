﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.IO; // so we can say local.Resources
using System.Linq;
using System.Timers;
using System.Windows.Forms; // for the load contracts dialog window.
using ContractGenerator;
using ContractGenerator.Contracts;
using EnvDTE;
using Microsoft.VisualStudio.PlatformUI.OleComponentSupport;
using Microsoft.VisualStudio.Shell;
using local = UniversityofWashington.ContractInserter;

namespace UniversityofWashington.ContractInserter
{
    public enum GroupingMethod
    {
        Variable, 
        Semantic,
        AntecedentThenVariable,
        AntecedentThenSemantic
    }

    /// <summary>
    /// Interaction logic for ContractInserter.xaml
    /// </summary>
    public partial class ContractInserter
    {
        /// <summary>
        /// Whether or not to use debug mode.
        /// </summary>
        private const bool debugMode = false;

        /// <summary>
        /// The top level view model object. 
        /// </summary>
        public ViewModel ViewModel { get; set; }

        Signature currentMethodSignature;
        Log eventLog;
        readonly BackgroundWorker generateWorker;
        delegate void Action();

        public ContractInserter()
        {
            InitializeComponent();

            // Create the view model.
            ViewModel = new ViewModel(
                new RelayCommand(ExpandContractLists), 
                new RelayCommand(SelectFileCommand), 
                new RelayCommand(NavigateToSelectionCommand));

            ViewModel.DebugMode = debugMode;

            // Set up the background thread for contract generation.
            generateWorker = new BackgroundWorker();
            generateWorker.DoWork += performContractGeneration;
            generateWorker.RunWorkerCompleted += contractGenerationCompleted;

            // Bind the view model to the top level user interface element.
            toolWindow.DataContext = ViewModel;

            // Start the updater timer. 
            System.Timers.Timer updater = new System.Timers.Timer(400);
            updater.Elapsed += RunUpdater;

            updater.Start();
        }

        // This is called periodically when the update timer ticks, calling Update.
        private void RunUpdater(object sender, ElapsedEventArgs e)
        {
            // Perform the update on the ui thread.
            this.Dispatcher.Invoke(new Action(this.Update), new object[] { });
        }

        public void Update()
        {
            DoSelection(auto: true);
        }

        public void DoSelection(bool auto)
        {
            if (ViewModel.DebugMode && auto)
            {
                return;
            }

            if (this.ViewModel.ProjectModel == null)
            {
                // A successful generation has not occured.
                ViewModel.CurrentSelection = null;
                return;
            }

            Signature signature;
            CodeElement chosenElement;
            CodeFunction currentMethod = null;
            CodeClass currentClass = null;
            CodeInterface currentInterface = null;
            CodeProperty currentProperty = null;

            currentMethod = (CodeFunction)getCodeElementAtCursor(vsCMElement.vsCMElementFunction);
            currentClass = (CodeClass)getCodeElementAtCursor(vsCMElement.vsCMElementClass);
            currentProperty = (CodeProperty)getCodeElementAtCursor(vsCMElement.vsCMElementProperty);
            currentInterface = (CodeInterface)getCodeElementAtCursor(vsCMElement.vsCMElementInterface);

            CodeElement containingType = currentClass == null ?
                currentInterface as CodeElement : currentClass as CodeElement;

            if (currentMethod != null)
            {
                string reflectiveName;

                bool isObjectInvariant = currentMethod.IsClassInvariantMethod();

                // Cursor is in a method.
                if (currentClass as CodeType != null)
                {
                    if (!isObjectInvariant)
                    {
                        reflectiveName = ContractGenerator.Extensions.ReflectiveFullName(currentMethod, containingType as CodeType);
                    }
                    else
                    {
                        reflectiveName = ContractGenerator.Extensions.ReflectiveFullName(currentClass);
                    }
                }
                else
                {
                    // When does this case occur ?
                    reflectiveName = currentMethod.FullName;
                }

                signature = new Signature(reflectiveName, currentMethod.GetParameters().ToList());
                chosenElement = (CodeElement)currentMethod;
            }
            else if (currentProperty != null)
            {
                // Cursor is in a property. Unfortunately the Visual Studio code model does
                // not tell us if we are in the getter, setter, or neither, so we must do this manually.
                string propertyName;

                // Select the getter or setter of the property.
                if (currentProperty.HasGetter() && !currentProperty.HasSetter())
                {
                    propertyName = Extensions.ReflectiveFullName(currentProperty.Getter, containingType as CodeType);
                    chosenElement = (CodeElement)currentProperty.Getter;
                }
                else if (!currentProperty.HasGetter() && currentProperty.HasSetter())
                {
                    propertyName = Extensions.ReflectiveFullName(currentProperty.Setter, containingType as CodeType);
                    chosenElement = (CodeElement)currentProperty.Setter;
                }
                else
                {
                    // There is both a getter and a setter.
                    // If the current cursor is before the start of the setter, select the getter.
                    // Otherwise select the setter.
                    var cursorPosition = getCurrentCursorPosition();
                    int line = cursorPosition.Item1;
                    int offset = cursorPosition.Item2;

                    int setterLine = currentProperty.Setter.StartPoint.Line;
                    int setterOffset = currentProperty.Setter.StartPoint.LineCharOffset;

                    if (line < setterLine || (line == setterLine && offset <= setterOffset))
                    {
                        propertyName = Extensions.ReflectiveFullName(currentProperty.Getter, containingType as CodeType);
                        chosenElement = (CodeElement)currentProperty.Getter;
                    }
                    else
                    {
                        propertyName = Extensions.ReflectiveFullName(currentProperty.Setter, containingType as CodeType);
                        chosenElement = (CodeElement)currentProperty.Setter;
                    }
                }
                signature = new Signature(propertyName, new List<Parameter>());
            }
            else if (currentClass != null)
            {
                // Cursor is in a class.
                string reflectiveClassName = ContractGenerator.Extensions.ReflectiveFullName(currentClass);
                signature = new Signature(reflectiveClassName, new List<Parameter>());
                chosenElement = (CodeElement)currentClass;
            }
            else if (currentInterface != null)
            {
                // Cursor is in an interface.
                signature = new Signature(currentInterface.FullName, new List<Parameter>());
                chosenElement = (CodeElement)currentInterface;
            }
            else
            {
                ViewModel.CurrentSelection = null;
                return;
            }

            if (currentMethodSignature == null || !currentMethodSignature.Equals(signature) || ViewModel.CurrentSelection == null)
            {
                // Update the current signature.
                currentMethodSignature = signature;
                bool proxy; // true if the clicked method contains contracts for another method (ie. a contract class method).
                ProgramElement clickedElement = this.ViewModel.ProjectModel.GetProgramElement(signature, out proxy);
                ViewModel.CurrentSelection = new Selection(clickedElement, chosenElement, proxy);
            }
        }

        public void ExpandContractLists(object dummyParam)
        {
            list1._treeList.ConditionallyExpandNodes(10);
            list2._treeList.ConditionallyExpandNodes(10);

            if (ViewModel.CurrentSelection.ProjectModelCodeElement.IsObjectInvariant)
            {
                list1TabItem.IsSelected = true;
            }
            else if (ViewModel.ListOneViewModel.HasNoContracts)
            {
                list2TabItem.IsSelected = true;
            }
            else if (ViewModel.ListTwoViewModel.HasNoContracts)
            {
                list1TabItem.IsSelected = true;
            }
        }

        public void Generate(EnvDTE.Solution solution, EnvDTE.Project projectToGenerate)
        {
            Contract.Requires(solution != null);
            Contract.Requires(projectToGenerate != null);

            // Perform contract generation in a background thread. This stops Visual Studio 
            // from freezing and lets the user interface be responsive.
            GenerateArguments args = new GenerateArguments(solution, projectToGenerate, new LoaderFiles());
            generateWorker.RunWorkerAsync(args);
        }

        public void Load(EnvDTE.Solution solution, EnvDTE.Project projectToGenerate)
        {
            Contract.Requires(solution != null);
            Contract.Requires(projectToGenerate != null);

            LoaderFiles loaderFiles = GetLoaderFiles();
            if (loaderFiles != null)
            {
                GenerateArguments args = new GenerateArguments(solution, projectToGenerate, loaderFiles);
                generateWorker.RunWorkerAsync(args);
            }
        }
    
        private void performContractGeneration(object sender, DoWorkEventArgs e)
        {
            ViewModel.SetNoContractsGenerated();

            GenerateArguments args = (GenerateArguments)e.Argument;
            var generator = new ContractGenerator.ContractGenerator();
            // Register for generation status changes.
            generator.GenerateStatusChange += generator_GenerateStatusChange;

            try
            {
                ProjectModel projectModel;
                generator.Generate(args.Solution, args.Project, args.LoaderFiles, out projectModel);

                string directory = Path.GetDirectoryName(args.Project.FullName);
                string dateAndTime = string.Format("{0:yy.MM.dd.HH.mm.ss}", DateTime.Now);
                string logName = string.Format("{0}.{1}.events.log", args.Project.Name, dateAndTime);
                string logPath = Path.Combine(directory, logName);
                eventLog = new Log(logPath);
                ViewModel.SetSuccess(args.Project, projectModel, eventLog);
            }
            catch(ContractGenerationException exception)
            {
                // Make new lines appear properly in WPF textblocks.
                string error = exception.Message.Replace("\\n", Environment.NewLine);
                ViewModel.SetError(error, exception.LogFilePath);
            }
        }

        private void contractGenerationCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            bool success = ViewModel.Status == GenerateStatus.Success;
            Contract.Assert(success.Implies(ViewModel.ProjectModel != null));
            Contract.Assert(success.Implies(ViewModel.CurrentProject != null));
            Contract.Assert(success.Implies(ViewModel.EventLog != null));
        }

        // Callback when the contract generation status changes.
        private void generator_GenerateStatusChange(object sender, GenerateStatusEvent status)
        {
            Contract.Requires(status != null);

            if (status.Status != GenerateStatus.Success &&
                status.Status != GenerateStatus.Error) // Set these status via ViewModel.SetSuccess and ViewModel.SetError
            {
                ViewModel.Status = status.Status;
                ViewModel.StatusMessage += "\n" + status.Message + "...";
                Dispatcher.Invoke(new Action(ScrollToBottomOfStatusMessages), new object[] { });
            }
        }

        public void ScrollToBottomOfStatusMessages()
        {
            statusMessageScroller.ScrollToBottom();
        }

        private void NavigateToSelection(CodeElement element)
        {
            Contract.Requires(element != null);

            Window window = element.ProjectItem.Open(Constants.vsViewKindCode);
            window.Activate();
            TextSelection selection = window.Document.Selection as TextSelection;
            Contract.Assert(selection != null);
            selection.MoveToPoint(element.StartPoint);
        }

        public void NavigateToSelectionCommand(object _codeElement)
        {
            CodeElement codeElement = (CodeElement)_codeElement;
            if (codeElement != null)
            {
                NavigateToSelection(codeElement);
            }
        }

        /// <summary>
        /// Prompts for invariant, contract or trace files.
        /// </summary>
        /// <returns>the selected generation files, or <c>null</c> if no files were selected</returns>
        private static LoaderFiles GetLoaderFiles()
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.CheckFileExists = true;
                dialog.CheckPathExists = true;
                dialog.Multiselect = true;
                dialog.Title = local.Resources.LoadContractsDialogTitle;
                dialog.Filter = local.Resources.LoadContractsDialogFilter;

                LoaderFiles userFiles = new LoaderFiles();

                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    if (dialog.FileNames.Length == 1)
                    {
                        var file = dialog.FileNames[0];
                        if (file.EndsWith(".invariants") || file.EndsWith(".inv.gz"))
                        {
                            userFiles.InvariantFile = file;
                        }
                        else if (file.EndsWith(".contracts"))
                        {
                            userFiles.ContractFile = file;
                        }
                        else if (file.EndsWith(".dtrace"))
                        {
                            userFiles.TraceFiles = new List<string>(new[] { file });
                        }
                    }
                    else
                    {
                        var files = new List<string>(dialog.FileNames);
                        // Make sure DECLs files come first.
                        files.Sort(
                            (lhs, rhs) => String.Compare(System.IO.Path.GetExtension(lhs), System.IO.Path.GetExtension(rhs), StringComparison.Ordinal));
                        userFiles.TraceFiles = files;
                    }
                }

                return result == DialogResult.OK ? userFiles : null;
            }
        }

        /// <summary>
        /// Gets the Visual Studio code model element at the current position of the cursor.
        /// </summary>
        /// <param name="scope">
        /// Determines the type of element to return. For example, if scope is 
        /// vsCMElement.vsCMElementClass, then whatever class the cursor is in will be 
        /// returned as a CodeElement.
        /// </param>
        /// <returns>the Visual Studio code model element at the current position of the cursor in the
        /// active document, or <c>null</c> if no element within the given scope exists</returns>
        [DebuggerStepThrough]
        private static CodeElement getCodeElementAtCursor(vsCMElement scope)
        {
            try
            {
                DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
                // Get the currently active document.
                Document activeDocument = dte.ActiveDocument;
                Contract.Assert(activeDocument != null);
                // Get the selection point in the document.
                EnvDTE.TextSelection currentSelection = (EnvDTE.TextSelection)activeDocument.Selection;
                TextPoint currentPoint = currentSelection.ActivePoint;

                // Try to extract the code element for the particular scope.
                CodeElement selectedElement =
                    activeDocument.ProjectItem.FileCodeModel.CodeElementFromPoint(
                    currentPoint, scope);

                return selectedElement;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the line number and character offset of the current cursor position in the active document.
        /// </summary>
        /// <returns>A tuple of ints, where the first int is the line number and the second int is the offset</returns>
        [DebuggerStepThrough]
        private static Tuple<int, int> getCurrentCursorPosition()
        {
            try
            {
                DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
                Contract.Assert(dte != null);
                // Get the currently active document.
                Document activeDocument = dte.ActiveDocument;
                // Get the selection point in the document.
                EnvDTE.TextSelection currentSelection = (EnvDTE.TextSelection)activeDocument.Selection;
                TextPoint currentPoint = currentSelection.ActivePoint;
                return new Tuple<int, int>(currentPoint.Line, currentPoint.LineCharOffset);
            }
            catch
            {
                return new Tuple<int, int>(0, 0);
            }
        }

        public void SelectFileCommand(object param)
        {
            SelectFileInExplorer((string)param);
        }

        /// <summary>
        /// Opens the Windows file explorer with <paramref name="file"/> selected.
        /// </summary>
        /// <param name="file">the file to select</param>
        private static void SelectFileInExplorer(string file)
        {
            Contract.Requires(!string.IsNullOrEmpty(file));

            string args = string.Format("/Select, {0}", file);
            ProcessStartInfo pfi = new ProcessStartInfo("Explorer.exe", args);
            System.Diagnostics.Process.Start(pfi);
        }

        /// <summary>
        /// Arguments required for Code Contract generation.
        /// </summary>
        private class GenerateArguments
        {
            /// <summary>
            /// The solution that contains the project being generated for.
            /// </summary>
            public Solution Solution { get; private set; }

            /// <summary>
            /// The project to generate Code Contracts for.
            /// </summary>
            public Project Project { get; private set; }

            /// <summary>
            /// User supplied invariant, contract, or trace files.
            /// </summary>
            public LoaderFiles LoaderFiles { get; private set; }

            public GenerateArguments(Solution solution, Project project, LoaderFiles loaderFiles)
            {
                this.Solution = solution;
                this.Project = project;
                this.LoaderFiles = loaderFiles;
            }
        }

        // Debug mode option.
        // Manually toggles a selection.
        private void SelectionDebug(object sender, System.Windows.RoutedEventArgs e)
        {
            DoSelection(auto : false);
        }

        // Debug mode option.
        // Print signatures that have contracts to debug.
        private void PrintSignaturesDebug(object sender, System.Windows.RoutedEventArgs e)
        {
            string filter = this.sigFilter.Text;

            Debug.WriteLine(string.Empty);
            Debug.WriteLine("Signatures with contracts:");

            if (string.IsNullOrEmpty(filter))
            {
                Debug.WriteLine("(no filter)");
            }
            else
            {
                Debug.WriteLine("(filter: " + filter + ")");
            }
            
            foreach (Signature sig in ViewModel.ProjectModel.GetLinkSignatures(filter))
            {
                Debug.WriteLine(sig);
            }
        }
    }
}
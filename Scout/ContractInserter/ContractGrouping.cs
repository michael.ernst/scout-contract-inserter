﻿using ContractGenerator;
using ContractGenerator.Classification;

namespace UniversityofWashington.ContractInserter
{
    public class ContractGrouping
    {
        public ContractRowRoot Requires { get; private set; }
        public ContractRowRoot Ensures { get; private set; }
        public ContractRowRoot Invariants { get; private set; }

        public ContractGrouping(ProgramElement element, GroupingMethod method)
        {
            dynamic dict;

            if (method == GroupingMethod.Variable)
            {
                dict = ContractGrouper.GroupByVariable(element);
            }
            else if(method == GroupingMethod.Semantic)
            {
                dict = ContractGrouper.GroupBySemanticType(element);
            }
            else if(method == GroupingMethod.AntecedentThenVariable)
            {
                dict = ContractGrouper.GroupByAntecedentThenVariable(element);
            }
            else
            {
                dict = ContractGrouper.GroupByAntecedentThenSemanticType(element);
            }

            Requires = new ContractRowRoot(element, dict[ContractKind.Requires]);
            Ensures = new ContractRowRoot(element, dict[ContractKind.Ensures]);
            Invariants = new ContractRowRoot(element, dict[ContractKind.Invariant]);
        }
    }
}

﻿using System.Globalization;

namespace UniversityofWashington.ContractInserter
{
    public class FilterRowViewModel
    {
        public ContractGenerator.ContractFilter Filter { get; private set; }

        public string FilterName { get; private set; }

        public int NumberFiltered { get; private set; }

        public string NumberFilteredString
        {
            get
            {
                return NumberFiltered.ToString(CultureInfo.InvariantCulture);
            }
        }

        public string FilteredVariables { get; private set; }

        public FilterRowViewModel(ContractGenerator.ContractFilter filter)
        {
            Filter = filter;
            FilterName = filter.InvariantType.ToString();
            FilteredVariables = filter.FilteredVariablesString;
            NumberFiltered = filter.FilteredContracts.Count;
        }         
    }
}

﻿using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Windows.Input;

namespace UniversityofWashington.ContractInserter
{
    public class RelayCommand : ICommand
    {
        private readonly Action<object> execute;
        private readonly Predicate<object> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">the execution logic</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        { }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">the execution logic</param>
        /// <param name="canExecute">the execution status logic</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute = null)
        {
            Contract.Requires(execute != null);
            this.execute = execute;
            this.canExecute = canExecute;
        }

        /// <summary>
        /// True if this command can be executed with the given parameters.
        /// </summary>
        /// <param name="parameters">the executation parameters</param>
        /// <returns>true if this command can be executed with the given parameters</returns>
        [DebuggerStepThrough]
        public bool CanExecute(object parameters)
        {
            return canExecute == null || canExecute(parameters);
        }

        /// <summary>
        /// Execute this command
        /// </summary>
        /// <param name="parameters">the parameters to pass</param>
        public void Execute(object parameters)
        {
            execute(parameters);
        }
    }
}

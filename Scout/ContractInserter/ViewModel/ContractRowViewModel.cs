﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Windows.Media;
using Aga.Controls.Tree;
using ContractGenerator;
using ContractGenerator.Classification;

namespace UniversityofWashington.ContractInserter
{
    public sealed class ContractRowRoot : ITreeModel
    {
        #region Object Invariants
        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(Root != null);
        }
        #endregion

        #region Events

        public event EventHandler<ContractInsertionEventArgs> ContractInsertionEvent;

        private void RaiseViewModelContractEvent(ContractInsertionEventArgs args)
        {
            EventHandler<ContractInsertionEventArgs> handler = ContractInsertionEvent;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        // Forward events from the individual rows. 
        void contractRow_ViewModelContractEvent(object sender, ContractInsertionEventArgs e)
        {
            RaiseViewModelContractEvent(e);
        }

        #endregion

        public static int NoContractsRowId = -1;
        public static int AllContractsFilteredRowId = -2;

        /// <summary>
        /// The root row. This will not be displayed (only its children will), so its text does not matter.
        /// </summary>
        public ContractRow Root { get; private set; }

        public bool HasNoContracts
        {
            get
            {
                ContractRow row = Root.Children.First(x => x.Id == NoContractsRowId);
                return row.IsVisible;
            }
        }

        // One level of grouping.
        public ContractRowRoot(ProgramElement element, SortedDictionary<string, List<ProgramContract>> groupedContracts)
        {
            Root = ContractRow.CreateRootRow();

            var noContractsAvailable = 
                ContractRow.CreateMessageRow(Resources.NoContractsAvailableMessage, false, NoContractsRowId);

            var allContractsFiltered = 
                ContractRow.CreateMessageRow(Resources.AllContractsFilteredMessage, false, AllContractsFilteredRowId);

            Root.AddChild(noContractsAvailable);
            Root.AddChild(allContractsFiltered);

            // If there are no contracts, display the no contracts available row.
            if (groupedContracts.Keys.Count == 0)
            {
                noContractsAvailable.IsVisible = true;
                return;
            }

            // A mapping from contract id to the row(s) that contract represents.
            var associations = new Dictionary<int, List<ContractRow>>();

            foreach (string group in groupedContracts.Keys)
            {
                var groupRow = ContractRow.CreateGroupRow(group); 

                foreach (ProgramContract contract in groupedContracts[group])
                {
                    if (!contract.IsTrivial)
                    {
                        var contractRow = ContractRow.CreateContractRow(
                            displayText : contract.DaikonContractText, 
                            insertionText : contract.CodeContractText, 
                            inserted : contract.Inserted, 
                            visible : !contract.IsFiltered,
                            signature : element.Signature,
                            id: contract.Id);

                        contractRow.ContractInsertionEvent += contractRow_ViewModelContractEvent;
                        groupRow.AddChild(contractRow);

                        if (!associations.ContainsKey(contract.Id))
                        {
                            associations[contract.Id] = new List<ContractRow>();
                        }
                        associations[contract.Id].Add(contractRow);
                    }
                }

                // Do not display a group row if all its contracts are filtered.
                if (!groupRow.HasVisibleChildren) groupRow.IsVisible = false;
                Root.AddChild(groupRow);
            }

            if (!Root.HasVisibleChildren) allContractsFiltered.IsVisible = true;

            CreateIdenticalAndSimilarRows(element, associations);
        }

        // Two levels of grouping.
        public ContractRowRoot(ProgramElement element, Dictionary<string, SortedDictionary<string, List<ProgramContract>>> contracts)
        {
            Root = ContractRow.CreateRootRow();

            var noContractsAvailable =
                ContractRow.CreateMessageRow(Resources.NoContractsAvailableMessage, false, NoContractsRowId);

            var allContractsFiltered =
                ContractRow.CreateMessageRow(Resources.AllContractsFilteredMessage, false, AllContractsFilteredRowId);

            Root.AddChild(noContractsAvailable);
            Root.AddChild(allContractsFiltered);

            if (contracts.Keys.Count == 0)
            {
                // If there are no contracts, display the no contracts available message in a single row with no children.
                noContractsAvailable.IsVisible = true;
                return;
            }

            // A mapping from contract id to the row(s) that contract represents.
            var associations = new Dictionary<int, List<ContractRow>>();

            foreach (string antecedent in contracts.Keys)
            {
                ContractRow firstGroup;
                if (antecedent.Equals(Implication.TrueAntecedent))
                {
                    firstGroup = ContractRow.CreateGroupRow("Always");
                }
                else
                {
                    string antecedentAsDaikon = Extensions.ImitateDaikon(antecedent);
                    firstGroup = ContractRow.CreateGroupRow(antecedentAsDaikon); 
                }

                foreach (string group in contracts[antecedent].Keys)
                {
                    var secondGroup = ContractRow.CreateGroupRow(group);

                    foreach (ProgramContract contract in contracts[antecedent][group])
                    {
                        if (!contract.IsTrivial)
                        {
                            bool inserted = contract.Inserted;
                            bool visible = !contract.IsFiltered;

                            ContractRow contractRow;
                            if (antecedent.Equals(Implication.TrueAntecedent))
                            {
                                contractRow = ContractRow.CreateContractRow(
                                    displayText: contract.DaikonContractText, 
                                    insertionText: contract.CodeContractText,
                                    inserted: inserted, 
                                    visible: visible, 
                                    signature: element.Signature, 
                                    id: contract.Id);
                            }
                            else
                            {
                                contractRow = ContractRow.CreateContractRow(
                                    displayText: contract.Implication.DaikonConsequent, 
                                    insertionText: contract.CodeContractText, 
                                    inserted: inserted, 
                                    visible: visible, 
                                    signature: element.Signature, 
                                    id: contract.Id);
                            }

                            contractRow.ContractInsertionEvent += contractRow_ViewModelContractEvent;
                            secondGroup.AddChild(contractRow);

                            if (!associations.ContainsKey(contract.Id))
                            {
                                associations[contract.Id] = new List<ContractRow>();
                            }
                            associations[contract.Id].Add(contractRow);
                        }
                    }

                    firstGroup.AddChild(secondGroup);
                    // Do not display a group row if all its contracts are filtered.
                    if (!secondGroup.HasVisibleChildren) secondGroup.IsVisible = false;
                }

                Root.AddChild(firstGroup);
                // Do not display a group row if all its contracts are filtered.
                if (!firstGroup.HasVisibleChildren) firstGroup.IsVisible = false;
            }

            if (!Root.HasVisibleChildren) allContractsFiltered.IsVisible = true;

            CreateIdenticalAndSimilarRows(element, associations);
        }

        public void Update(ContractRowRoot other)
        {
            Contract.Requires(other != null);
            Root.Update(other.Root);
        }

        private static void CreateIdenticalAndSimilarRows(ProgramElement element, Dictionary<int, List<ContractRow>> associations)
        {
            foreach (ProgramContract contract in element.Contracts)
            {
                if (associations.ContainsKey(contract.Id))
                {
                    var identicalRows = associations[contract.Id];

                    // Link identical rows together.
                    foreach (var identical in identicalRows)
                    {
                        foreach (var _identical in identicalRows)
                        {
                            if (identical != _identical)
                            {
                                identical.AddIdenticalRow(_identical);
                            }
                        }
                    }

                    // Get the rows representing contracts similar to contract.
                    var similarRows = contract.SimilarContracts
                        .Where(_contract => associations.ContainsKey(_contract.Id))
                        .Select(_contract => associations[_contract.Id])
                        .SelectMany(x => x)
                        .ToList();

                    // Add similar rows to each identical row.
                    foreach (var _identical in identicalRows)
                    {
                        foreach (var _similar in similarRows)
                        {
                            _identical.AddSimilarRow(_similar);
                        }
                    }
                }
            }
        }

        #region ITreeModel Methods
        System.Collections.IEnumerable ITreeModel.GetChildren(object parent)
        {
            if (parent == null)
            {
                parent = Root;
            }

            return (parent as ContractRow).Children;
        }

        bool ITreeModel.HasChildren(object parent)
        {
            return (parent as ContractRow).Children.Any();
        }
        #endregion
    }

    /// <summary>
    /// Represents a row in the contract tree list view.
    /// </summary>
    public class ContractRow : INotifyBase
    {
       
        #region Object Invariants
        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(Id > 0 || Signature == null && InsertionText.Equals(string.Empty));
            Contract.Invariant(DisplayText != null);
            Contract.Invariant(InsertionText != null);
            Contract.Invariant(Children != null);
            Contract.Invariant(IdenticalRows != null);
        }
        #endregion

        #region Events

        // Clients subscribe to this to be notified of modifications to this.
        public event EventHandler<ContractInsertionEventArgs> ContractInsertionEvent;

        private void RaiseViewModelContractEvent(ContractInsertionEventArgs args)
        {
            EventHandler<ContractInsertionEventArgs> handler = ContractInsertionEvent;
            if (handler != null)
            {
                handler(this, args);
            }
        }
        #endregion

        /// <summary>
        /// If true, modifying <see cref="IsChecked"/> will not fire a view model event.
        /// </summary>
        private bool fireInsertionEvent;

        /// <summary>
        /// The Id of the contract this row represents.
        /// If this row represents a contracts, then Id > 0.
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// The signature of the method this contract belongs to or
        /// <c>null</c> if this row does not represent a contract.
        /// </summary>
        /// <remarks>
        /// The signature along with <see cref="Id"/> can identify the <see cref="ProgramContract"/>
        /// this row represents in the model.
        /// </remarks>
        public Signature Signature { get; private set; }

        /// <summary>
        /// The text to insert into the source code if this row is inserted as a contract.
        /// </summary>
        public string InsertionText { get; private set; }

        private string displayText;

        /// <summary>
        /// The text to display.
        /// </summary>
        public string DisplayText
        {
            get
            {
                return displayText;
            }
            set
            {
                displayText = value;
                NotifyPropertyChanged();
            }
        }

        private bool isVisible;

        /// <summary>
        /// True if this row is visible.
        /// </summary>
        public bool IsVisible
        {
            get
            {
                return isVisible;
            }
            set
            {
                isVisible = value;
                NotifyPropertyChanged();
            }
        }

        public bool isChecked;

        /// <summary>
        /// True if this row is checked (the contract is represents is inserted).
        /// </summary>
        public bool IsChecked
        {
            get
            {
                return isChecked;
            }
            set
            {
                isChecked = value;
                NotifyPropertyChanged();

                if (fireInsertionEvent)
                {
                    List<ContractRow> links = new List<ContractRow>();
                    links.AddRange(IdenticalRows);
                    links.AddRange(SimilarRows);
                    foreach (var link in links)
                    {
                        // We want other rows which have the same contract to have the same status for their checkbox,
                        // but we only want to insert/delete the contract once. So turn off the insertion fire event for
                        // the linked rows to change the check box but not fire an event.
                        link.fireInsertionEvent = false;
                        link.IsChecked = value;
                        link.fireInsertionEvent = true;
                    }

                    // Raise an event when the user toggles insertion for this contract.
                    var args = new ContractInsertionEventArgs(value, Id, Signature);
                    RaiseViewModelContractEvent(args);
                }
            }
        }

        private readonly List<ContractRow> children;

        /// <summary>
        /// The child nodes/rows.
        /// </summary>
        public ReadOnlyCollection<ContractRow> Children
        {
            get
            {
                return children.AsReadOnly();
            }
        }

        private readonly List<ContractRow> identicalRows;

        /// <summary>
        /// Other rows that represent the same contract as this.
        /// For example, a contract may appear twice under different groupings.
        /// </summary>
        public ReadOnlyCollection<ContractRow> IdenticalRows
        {
            get
            {
                return identicalRows.AsReadOnly();
            }
        }

        private readonly List<ContractRow> similarRows;

        /// <summary>
        /// Other rows that represent similar contracts as this. 
        /// Similar contracts are such that if one is inserted, it does not make sense to insert the others.
        /// 
        /// For example,
        /// A > B and A >= B are similar, but it does not make sense to have both as Code Contracts.
        /// </summary>
        public ReadOnlyCollection<ContractRow> SimilarRows
        {
            get
            {
                return similarRows.AsReadOnly();
            }
        }

        private Brush backgroundColor;

        /// <summary>
        /// The background color of the row.
        /// </summary>
        public Brush BackgroundColor
        {
            get
            {
                return backgroundColor;
            }
            set
            {
                backgroundColor = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// True if this row represents a contract (i.e. its Id is greater than zero).
        /// </summary>
        public bool IsContract
        {
            get
            {
                return Id > 0;
            }
        }

        /// <summary>
        /// True if this row can be expanded (i.e. it is a grouping row).
        /// </summary>
        public bool IsExpandable
        {
            get
            {
                return children.Count != 0 ||
                    Id == ContractRowRoot.NoContractsRowId ||
                    Id == ContractRowRoot.AllContractsFilteredRowId;
            }
        }

        /// <summary>
        /// True if this row has visible children.
        /// </summary>
        public bool HasVisibleChildren
        {
            get
            {
                bool visibleChildren = false;
                foreach (ContractRow child in Children)
                {
                    visibleChildren |= child.isVisible;
                }
                return visibleChildren;
            }
        }

        public static ContractRow CreateRootRow()
        {
            return new ContractRow(
                displayText: string.Empty,
                insertionText: string.Empty,
                isChecked: false,
                isVisible: true,
                signature: null,
                id: 0);
        }

        public static ContractRow CreateMessageRow(string message, bool visible, int id)
        {
            return new ContractRow(
                displayText: message,
                insertionText: string.Empty,
                isChecked: false,
                isVisible: visible,
                signature: null,
                id: id);
        }

        public static ContractRow CreateGroupRow(string group)
        {
            return new ContractRow(
                displayText: group,
                insertionText: string.Empty,
                isChecked: false,
                isVisible: true,
                signature: null,
                id: 0);
        }

        public static ContractRow CreateContractRow(string displayText, string insertionText, bool inserted, bool visible, Signature signature, int id)
        {
            return new ContractRow(
                displayText: displayText,
                insertionText: insertionText,
                isChecked: inserted,
                isVisible: visible,
                signature: signature,
                id: id);        
        }

        /// <summary>
        /// Creates a new row.
        /// </summary>
        /// <param name="displayText">the text to display</param>
        /// <param name="insertionText">the text to insert if this row is checked</param>
        /// <param name="isChecked">true if this row should be initially checked</param>
        /// <param name="isVisible">true if this row is initially visible</param>
        /// <param name="signature">the signature of the element containing the contract, or null if this row does not represent a contract</param>
        /// <param name="id">the Id of this row. If this row represents a contracts, this is a unique 
        /// non-negative number that maps to the contract Id in the model.
        /// </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        // see: http://social.msdn.microsoft.com/Forums/en-US/f5832d74-ff17-4ca0-b69c-d579e1f54506/code-analysis-in-visual-studio-2010-generates-a-ca2214-warning-caused-by-code-contracts?forum=codecontracts
        private ContractRow(string displayText, string insertionText, bool isChecked, bool isVisible, Signature signature, int id)
        {
            this.DisplayText = displayText;
            this.InsertionText = insertionText;
            this.Signature = signature;
            this.Id = id;
            this.isChecked = isChecked;
            this.IsVisible = isVisible;
            this.backgroundColor = new SolidColorBrush(Colors.White);
            this.children = new List<ContractRow>();
            this.identicalRows = new List<ContractRow>();
            this.similarRows = new List<ContractRow>();
            this.fireInsertionEvent = true;
        }

        /// <summary>
        /// Updates this row with the state of <paramref name="update"/>.
        /// </summary>
        /// <param name="update">the row whose state to update to</param>
        public void Update(ContractRow update)
        {
            Contract.Requires(update != null);
            Contract.Ensures(isVisible == update.isVisible);

            IsVisible = update.IsVisible;
            foreach (var child in Children)
            {
                ContractRow row = update.Children.First(x => x.DisplayText.Equals(child.DisplayText));
                child.Update(row);
            }
        }

        public void AddChild(ContractRow child)
        {
            Contract.Requires(child != null);
            Contract.Ensures(Children.Contains(child));
            this.children.Add(child);
        }

        public void AddIdenticalRow(ContractRow row)
        {
            Contract.Requires(row != null);
            Contract.Requires(this.InsertionText.Equals(row.InsertionText));
            Contract.Ensures(this.IdenticalRows.Contains(row));
            this.identicalRows.Add(row);
        }

        public void AddSimilarRow(ContractRow row)
        {
            Contract.Requires(row != null);
            Contract.Ensures(this.SimilarRows.Contains(row));
            this.similarRows.Add(row);
        }
    }

    public class ContractInsertionEventArgs : EventArgs
    {
        public bool InsertContract { get; private set; }
        public int ContractId { get; private set; }
        public Signature ElementSignature { get; private set; }

        public ContractInsertionEventArgs(bool insertContract, int contractId, Signature elementSignature)
        {
            this.InsertContract = insertContract;
            this.ContractId = contractId;
            this.ElementSignature = elementSignature;
        }
    }
}

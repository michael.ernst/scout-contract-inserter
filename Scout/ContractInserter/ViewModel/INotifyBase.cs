﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace UniversityofWashington.ContractInserter
{
    public class INotifyBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Notify that a particular property value has changed.
        /// </summary>
        /// <param name="propertyName">the name of the property whose value has changed.
        /// By default it is the name of the property this is called in.</param>
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

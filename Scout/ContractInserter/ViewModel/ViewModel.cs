﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Windows.Input;
using ContractGenerator;
using ContractGenerator.Contracts;
using EnvDTE;

namespace UniversityofWashington.ContractInserter
{

    /// <summary>
    /// Represents the elements at the cursor in the active document window.
    /// </summary>
    public class Selection
    {
        public ProgramElement ProjectModelCodeElement { get; private set; }
        public CodeElement VisualStudioCodeModelElement { get; private set; }
        public bool IsContractClassMethod { get; private set; }

        public Selection(ProgramElement programElement, CodeElement codeElement, bool isContractClassMethod)
        {
            ProjectModelCodeElement = programElement;
            VisualStudioCodeModelElement = codeElement;
            IsContractClassMethod = isContractClassMethod;
        }

        /// <summary>
        /// <c>True</c> if this selection has available Code Contracts.
        /// </summary>
        public bool HasContracts
        {
            get
            {
                return ProjectModelCodeElement != null && VisualStudioCodeModelElement != null;
            }
        }

        /// <summary>
        /// <c>True</c> if this selection has available Code Contracts that are object invariants.
        /// </summary>
        public bool HasInvariantContracts
        {
            get
            {
                return HasContracts && ProjectModelCodeElement.IsObjectInvariant;
            }
        }
    }

    public class ViewModel : INotifyBase
    {
        [ContractInvariantMethod]
        private void Invariants()
        {
            Contract.Invariant((!ContractsGenerated).Implies(ProjectModel == null));
            Contract.Invariant((!ContractsGenerated).Implies(CurrentProject == null));
            Contract.Invariant((!ContractsGenerated).Implies(EventLog == null));
            Contract.Invariant((ContractsGenerated).Implies(ProjectModel != null));
            Contract.Invariant((ContractsGenerated).Implies(CurrentProject != null));
            Contract.Invariant((ContractsGenerated).Implies(EventLog != null));
            Contract.Invariant((ProjectModel == null) == (CurrentProject == null));
            Contract.Invariant((CurrentProject == null) == (EventLog == null));
            Contract.Invariant((!ContractGenerationError).Implies(string.IsNullOrEmpty(ContractGenerationErrorMessage)));
            Contract.Invariant((!ContractGenerationError).Implies(string.IsNullOrEmpty(ErrorLogFilePath)));
            Contract.Invariant(NavigateToFileCommand != null);
            Contract.Invariant(NavigateToSelectionCommand != null);
            Contract.Invariant(expandContractLists != null);
        }

        #region Non-derived Fields

        /// <summary>
        /// Backing field for <see cref="Status"/>.
        /// </summary>
        private GenerateStatus status;

        /// <summary>
        /// The status of generation.
        /// </summary>
        public GenerateStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                if (status != value && value != GenerateStatus.Other)
                {
                    status = value;
                    NotifyPropertyChanged();
                    // Update associated derived fields.
                    NotifyPropertyChanged("GeneratingContracts");
                    NotifyPropertyChanged("NoContractsGenerated");
                    NotifyPropertyChanged("ContractsGenerated");
                    NotifyPropertyChanged("ContractGenerationError");
                }
            }
        }

        /// <summary>
        /// Backing field for <see cref="StatusMessage"/>
        /// </summary>
        private string statusMessage;

        /// <summary>
        /// The content of the status message box to display during generation.
        /// </summary>
        public string StatusMessage
        {
            get
            {
                return statusMessage;
            }
            set
            {
                statusMessage = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Backing field for <see cref="ListOneViewModel"/>.
        /// </summary>
        private ContractRowRoot listOneViewModel;

        /// <summary>
        /// The view model for requires and invariant contracts.
        /// </summary>
        /// <remarks>
        /// Instance of ITreeModel.
        /// Binds to Aga.Controls.Tree.TreeList.Model dependency property.
        /// Updated by calling ViewModel.UpdateContractLists
        /// </remarks>
        public ContractRowRoot ListOneViewModel
        {
            get
            {
                return listOneViewModel;
            }
            private set
            {
                listOneViewModel = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Backing field for <see cref="ListTwoViewModel"/>.
        /// </summary>
        private ContractRowRoot listTwoViewModel;

        /// <summary>
        /// The view model for ensures contracts.
        /// </summary>
        /// <remarks>
        /// Instance of ITreeModel.
        /// Binds to Aga.Controls.Tree.TreeList.Model dependency property.
        /// Updated by calling ViewModel.UpdateContractLists
        /// </remarks>
        public ContractRowRoot ListTwoViewModel
        {
            get
            {
                return listTwoViewModel;
            }
            private set  
            {
                listTwoViewModel = value;
                NotifyPropertyChanged();
            }
        }
        
        /// <summary>
        /// Backing field for <see cref="currentSelection"/>.
        /// </summary>
        private Selection currentSelection;

        /// <summary>
        /// The current cursor selection, or null if no element is selected.
        /// </summary>
        public Selection CurrentSelection
        {
            get
            {
                return currentSelection;
            }
            set
            {
                currentSelection = value;
                OverwriteContractLists();
                // Update associated derived fields.
                NotifyPropertyChanged("ShowContractLists");
                NotifyPropertyChanged("FirstTabHeaderName");
                NotifyPropertyChanged("SecondTabHeaderName");
                NotifyPropertyChanged("DisplaySecondTab");
                NotifyPropertyChanged("CurrentSelectionCodeElement");
                NotifyPropertyChanged("CurrentElementCodeElement");
                NotifyPropertyChanged("CurrentElementMessage");
                NotifyPropertyChanged("CurrentElementName");
                NotifyPropertyChanged("CurrentElement");
                NotifyPropertyChanged("ContractClassElementSelected");
                NotifyPropertyChanged("CurrentElementContractClassMessage");
                NotifyPropertyChanged("CurrentElementOverrideElement");
                NotifyPropertyChanged("CurrentElementInterfaceElement");
                NotifyPropertyChanged("ContractClassElement");
                NotifyPropertyChanged("WillCreateContractClass");
                NotifyPropertyChanged("ShowContractClassMessage");
                NotifyPropertyChanged("CurrentSelectionIsContractClassMethod");
                NotifyPropertyChanged("Filters");
                NotifyPropertyChanged("FilterTabHeaderName");

                if (currentSelection != null && currentSelection.HasContracts)
                {
                    EventLog.LogSelection(currentSelection.ProjectModelCodeElement.Signature.ToString());
                }
            }
        }

        /// <summary>
        /// Backing field for <see cref="CurrentProject"/>.
        /// </summary>
        private Project currentProject;

        /// <summary>
        /// The project that is currently generated, or null if contracts are not generated for any project.
        /// </summary>
        public Project CurrentProject
        {
            get
            {
                return currentProject;
            }
            private set
            {
                currentProject = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("CurrentProjectMessage");
                NotifyPropertyChanged("CurrentProjectName");
                NotifyPropertyChanged("CurrentProjectDirectory");
            }
        }

        /// <summary>
        /// Backing field for <see cref="ProjectModel"/>
        /// </summary>
        private ProjectModel projectModel;

        /// <summary>
        /// The contract model for the currently generated project.
        /// </summary>
        public ProjectModel ProjectModel
        {
            get
            {
                return projectModel;
            }
            private set
            {
                if (value != null)
                {
                    foreach (ProgramElement element in value.ProgramElements)
                    {
                        // Do not display implication whose consequent is always true
                        // regardless of the antecedent. 
                        element.FiltersModified += FiltersModifiedAction;
                    }

                    projectModel = value;
                }
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Backing field for <see cref="ContractGenerationErrorMessage"/>.
        /// </summary>
        private string contractGenerationErrorMessage;

        /// <summary>
        /// Message to display when there is an error during contract generation.
        /// </summary>
        public string ContractGenerationErrorMessage
        {
            get
            {
                return contractGenerationErrorMessage;
            }
            private set
            {
                contractGenerationErrorMessage = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Backing field for <see cref="ErrorLogFilePath"/>.
        /// </summary>
        private string errorLogFilePath;

        /// <summary>
        /// The contract ErrorLogFilePath error log path.
        /// </summary>
        public string ErrorLogFilePath
        {
            get
            {
                return errorLogFilePath;
            }
            private set
            {
                errorLogFilePath = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// The user event log.
        /// </summary>
        public Log EventLog { get; private set; }

        /// <summary>
        /// Backing field for <see cref="GroupingMethod"/>.
        /// </summary>
        private GroupingMethod groupingMethod;

        /// <summary>
        /// The way to group displayed Code Contracts.
        /// </summary>
        public GroupingMethod GroupingMethod
        {
            get
            {
                return groupingMethod;
            }
            private set
            {
                if (groupingMethod != value)
                {
                    groupingMethod = value;
                    OverwriteContractLists();
                    NotifyPropertyChanged();

                    if (EventLog != null)
                    {
                        EventLog.LogGrouping(groupingMethod.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Backing field for <see cref="NavigateToFileCommand"/>.
        /// </summary>
        private ICommand navigateToFileCommand;

        /// <summary>
        /// The command to open the Windows file explorer and select a specific file.
        /// This command expects a string parameter that is the file to select.
        /// </summary>
        public ICommand NavigateToFileCommand
        {
            get
            {
                return navigateToFileCommand;
            }
            private set
            {
                navigateToFileCommand = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Backing field for <see cref="NavigateToSelectionCommand"/>.
        /// </summary>
        private ICommand navigateToSelectionCommand;

        /// <summary>
        /// The command to navigate to a specific code element in Visual Studio.
        /// The command expects a parameter which is the CodeElement to navigate to.
        /// </summary>
        public ICommand NavigateToSelectionCommand
        {
            get
            {
                return navigateToSelectionCommand;
            }
            private set
            {
                navigateToSelectionCommand = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Expands all nodes in the contract lists.
        /// This command expects no parameters.
        /// </summary>
        private readonly ICommand expandContractLists;

        /// <summary>
        /// Backing field for <see cref="DebugMode"/>.
        /// </summary>
        private bool debugMode;

        /// <summary>
        /// Whether or not we are in debug mode.
        /// </summary>
        public bool DebugMode
        {
            get
            {
                return debugMode;
            }
            set
            {
                debugMode = value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        public ViewModel(
            ICommand expandContractLists,
            ICommand navigateToFileCommand, 
            ICommand navigateToSelectionCommand)
        {
            this.groupingMethod = GroupingMethod.Variable;
            this.expandContractLists = expandContractLists;
            this.NavigateToFileCommand = navigateToFileCommand;
            this.NavigateToSelectionCommand = navigateToSelectionCommand;
            this.DebugMode = false;
            SetNoContractsGenerated();
        }

        #region Methods

        public void SetSuccess(Project newCurrentProject, ProjectModel newProjectModel, Log newEventLog)
        {
            Contract.Requires(newCurrentProject != null);
            Contract.Requires(newProjectModel != null);
            Contract.Requires(newEventLog != null);

            Status = GenerateStatus.Success;
            CurrentProject = newCurrentProject;
            ProjectModel = newProjectModel;
            EventLog = newEventLog;
        }

        public void SetError(string generationErrorMessage, string errorLogPath)
        {
            Contract.Requires(generationErrorMessage != null);

            Status = GenerateStatus.Error;
            currentSelection = null;
            currentProject = null;
            projectModel = null;
            EventLog = null;
            ContractGenerationErrorMessage = generationErrorMessage;

            if (errorLogPath == null)
            {
                ErrorLogFilePath = string.Empty;
            }
            else
            {
                ErrorLogFilePath = errorLogPath;
            }
        }

        /// <summary>
        /// Reset the view model state such that no project has been generated for.
        /// </summary>
        public void SetNoContractsGenerated()      
        {
            Status = GenerateStatus.NothingGenerated;
            currentSelection = null;
            currentProject = null;
            projectModel = null;
            EventLog = null;
            ErrorLogFilePath = string.Empty;
            ContractGenerationErrorMessage = string.Empty;
            StatusMessage = string.Empty;
        }

        // Occurs when a filter is added or removed from the current selection.
        private void FiltersModifiedAction(object sender, FilterModifiedArgs args)
        {
            EventLog.LogFilter(args.Filter, args.Added);
            UpdateContractLists();     
            NotifyPropertyChanged("Filters");
            NotifyPropertyChanged("FilterTabHeaderName");
            NotifyPropertyChanged("FirstTabHeaderName");
            NotifyPropertyChanged("SecondTabHeaderName");
        }

        // Occurs when a check box is checked/unchecked.
        private void HandleContractInsertionEvent(object sender, ContractInsertionEventArgs args)
        {
            Contract.Assert(projectModel != null);
            ProgramElement element = projectModel.GetProgramElement(args.ElementSignature);
            Contract.Assert(element != null);
            ProgramContract contract = element.FindContract(args.ContractId);
            Contract.Assert(contract != null);

            if (args.InsertContract)
            {
                bool notifyUpdate = element.MustCreateContractClass;
                contract.Insert();

                if (notifyUpdate)
                {
                    NotifyPropertyChanged("ContractClassElement");
                    NotifyPropertyChanged("ContractClassElementMessage");
                    NotifyPropertyChanged("WillCreateContractClass");
                    NotifyPropertyChanged("ShowContractClassMessage");
                }
            }
            else
            {
                contract.Delete();
            }

            EventLog.LogContract(contract, args.InsertContract);
        }

        /// <summary>
        /// Update the display of the current contract lists.
        /// This is required when a filter is added or removed.
        /// </summary>
        private void UpdateContractLists()
        {
            if (CurrentSelection != null && CurrentSelection.HasContracts)
            {
                // Group the contracts of the selected elements by the selected grouping method.
                ContractGrouping grouping = new ContractGrouping(CurrentSelection.ProjectModelCodeElement, GroupingMethod);

                if (!CurrentSelection.HasInvariantContracts)
                {
                    ListOneViewModel.Update(grouping.Requires);
                    ListTwoViewModel.Update(grouping.Ensures);
                }
                else
                {
                    ListOneViewModel.Update(grouping.Invariants);
                    ListTwoViewModel = null;
                }
            }
        }

        /// <summary>
        /// Complete refresh of the contract lists.
        /// This is required when the user selects a new element, or a new grouping method is chosen.
        /// </summary>
        private void OverwriteContractLists()
        {
            if (CurrentSelection != null && CurrentSelection.HasContracts)
            {
                ProgramElement element = CurrentSelection.ProjectModelCodeElement;
                // Group the contracts of the selected elements by the selected grouping method.
                ContractGrouping grouping = new ContractGrouping(CurrentSelection.ProjectModelCodeElement, GroupingMethod);
                // Set what happens when a contract is inserted or deleted. 
                grouping.Requires.ContractInsertionEvent += HandleContractInsertionEvent;
                grouping.Ensures.ContractInsertionEvent += HandleContractInsertionEvent;
                grouping.Invariants.ContractInsertionEvent += HandleContractInsertionEvent;

                if (!CurrentSelection.HasInvariantContracts)
                {
                    ListOneViewModel = grouping.Requires;
                    ListTwoViewModel = grouping.Ensures;
                }
                else
                {
                    ListOneViewModel = grouping.Invariants;
                    ListTwoViewModel = null;
                }

                // Command expects no parameters.
                expandContractLists.Execute(null);
            }
        }

        #endregion

        /// <summary>
        /// Derived fields are getter only properties derived from the settable properties of the view model.
        /// </summary>
        #region Derived Fields

        public bool ContractsExist
        {
            get
            {
                return currentSelection != null && currentSelection.HasContracts;
            }
        }

        # region Status Booleans

        /// <summary>
        /// True if no contracts have been generated. 
        /// </summary>
        public bool NoContractsGenerated
        {
            get
            {
                return status == GenerateStatus.NothingGenerated;
            }
        }

        /// <summary>
        /// True if contracts are currently being generated.
        /// </summary>
        public bool GeneratingContracts
        {
            get
            {
                return status != GenerateStatus.Success && status != GenerateStatus.NothingGenerated && status != GenerateStatus.Error;
            }
        }

        /// <summary>
        /// True if contracts have finished generating.
        /// </summary>
        public bool ContractsGenerated
        {
            get
            {
                return status == GenerateStatus.Success;
            }
        }

        /// <summary>
        /// True if there was an error during contract generation and no other generation is occuring.
        /// </summary>
        public bool ContractGenerationError
        {
            get
            {
                return status == GenerateStatus.Error;
            }
        }

        #endregion 

        /// <summary>
        /// The string to display on the first tab of the contract lists views.
        /// </summary>
        public string FirstTabHeaderName
        {
            get
            {
                if (!ContractsExist)
                {
                    // Does not matter.
                    return string.Empty;
                }

                if (!currentSelection.HasInvariantContracts)
                {
                    int unfilteredRequires = currentSelection.ProjectModelCodeElement.RemainingContractCount(ContractKind.Requires);
                    return string.Format(Resources.PreconditionTab, unfilteredRequires);
                }
                else
                {
                    int unfilteredInvaraints = currentSelection.ProjectModelCodeElement.RemainingContractCount(ContractKind.Invariant);
                    return string.Format(Resources.ObjectInvariantsTab, unfilteredInvaraints);
                }
            }
        }

        /// <summary>
        /// The string to display on the second tab of the contract list views.
        /// </summary>
        public string SecondTabHeaderName
        {
            get
            {
                if (!ContractsExist)
                {
                    // Does not matter.
                    return string.Empty;
                }

                int unfilteredEnsures = currentSelection.ProjectModelCodeElement.RemainingContractCount(ContractKind.Ensures);
                return string.Format(Resources.PostconditionTab, unfilteredEnsures);
            }
        }
        
        /// <summary>
        /// True if the contract lists should be displayed.
        /// </summary>
        public bool ShowContractLists
        {
            get
            {
                return ContractsExist;
            }
        }

        /// <summary>
        /// True if the second tab of the contract list views should be displayed. 
        /// The second tab is not displayed when the current element represents a class
        /// because only one tab is needed to display object invariants.
        /// </summary>
        public bool DisplaySecondTab
        {
            get
            {
                return currentSelection != null && 
                    currentSelection.HasContracts && !currentSelection.HasInvariantContracts;
            }
        }
       
        /// <summary>
        /// The message to display in addition to the name of the current element selected.
        /// </summary>
        public string CurrentElementMessage
        {
            get
            {
                if (currentSelection == null)
                {
                    return Resources.NoCurrentElement;
                }
                else if (!currentSelection.HasContracts)
                {
                    var vsElement = currentSelection.VisualStudioCodeModelElement;
                    if (vsElement.Kind == vsCMElement.vsCMElementClass || vsElement.IsClassInvariantMethod())
                    {
                        return Resources.NoContractsForObjectMessage;
                    }
                    else
                    {
                        return Resources.NoContractsForMethodMessage;
                    }
                }
                else
                {
                    return Resources.CurrentElementMessage;
                }
            }
        }

        /// <summary>
        /// The Visual Studio code model element of the current selection, or <c>null</c> if 
        /// there is no current selection.
        /// 
        /// (This is the code element the user is currently clicking on).
        /// </summary>
        public CodeElement CurrentSelectionCodeElement
        {
            get
            {
                if (currentSelection == null)
                {
                    return null;
                }
                else
                {
                    return currentSelection.VisualStudioCodeModelElement;
                }
            }
        }

        /// <summary>
        /// The Visual Studio code model element of the method the current project model element represents.
        /// This means that for contract classes, this will point to the interface/abstract method, not the contract class method.
        /// </summary>
        public CodeElement CurrentElement
        {
            get
            {
                if (currentSelection == null || !currentSelection.HasContracts ||
                    currentSelection.ProjectModelCodeElement.Method == null)
                {
                    return null;
                }
                else
                {
                    return currentSelection.ProjectModelCodeElement.Method.MethodRef as CodeElement;
                }
            }
        }

        /// <summary>
        /// The Visual Studio code model element that current project model element overrides, or <c>null</c>
        /// if the current project model element does not represent contracts for an overriden method (or property getter/setter).
        /// </summary>
        public CodeElement CurrentElementOverrideElement
        {
            get
            {
                if (currentSelection == null ||
                    !currentSelection.HasContracts ||
                    currentSelection.ProjectModelCodeElement.Method == null)
                {
                    return null;
                }
                else
                {
                    return currentSelection.ProjectModelCodeElement.Method.OverrideMethodRef as CodeElement;
                }
            }
        }

        /// <summary>
        /// The Visual Studio code model element that the current project model element implements, or <c>null</c>
        /// if the current project model element does not implement a method (or property getter/setter).
        /// </summary>
        public CodeElement CurrentElementInterfaceElement
        {
            get
            {
                if (currentSelection == null ||
                    !currentSelection.HasContracts ||
                    currentSelection.ProjectModelCodeElement.Method == null)
                {
                    return null;
                }
                else
                {
                    return currentSelection.ProjectModelCodeElement.Method.InterfaceMethodRef as CodeElement;
                }
            }
        }

        /// <summary>
        /// The Visual Studio code model element that contains the contracts for the current project model element, or <c>null</c>
        /// or null if there is no contract class method.
        /// </summary>
        public CodeElement ContractClassElement
        {
            get
            {
                if (currentSelection == null ||
                    !currentSelection.HasContracts ||
                    currentSelection.ProjectModelCodeElement.Method == null)
                {
                    return null;
                }
                else
                {
                    return currentSelection.ProjectModelCodeElement.Method.ContractMethodRef as CodeElement;
                }
            }
        }

        /// <summary>
        /// True if the current project model element will create a contract class upon contract insertion.
        /// </summary>
        public bool WillCreateContractClass
        {
            get
            {
                if (currentSelection == null ||
                    !currentSelection.HasContracts)
                {
                    return false;
                }
                else
                {
                    return currentSelection.ProjectModelCodeElement.MustCreateContractClass;
                }
            }
        }

        /// <summary>
        /// True if the current Visual Studio code model element is a contract class method (or property getter/setter).
        /// </summary>
        public bool CurrentSelectionIsContractClassMethod
        {
            get
            {
                if (currentSelection == null || !currentSelection.HasContracts)
                {
                    return false;
                }
                else
                {
                    return currentSelection.IsContractClassMethod;
                }
            }
        }

        /// <summary>
        /// True if a link to the contract class method for the current project model element should be displayed.
        /// </summary>
        public bool ShowContractClassMessage
        {
            get
            {
                if (currentSelection == null || !currentSelection.HasContracts)
                {
                    return false;
                }
                else
                {
                    return !currentSelection.IsContractClassMethod && ContractClassElement != null;
                }
            }
        }

        #region Current Project

        /// <summary>
        /// The message prior to the current project hyper link.
        /// </summary>
        public string CurrentProjectMessage
        {
            get
            {
                if (currentProject == null) return string.Empty;
                else return Resources.CurrentProjectMessage;
            }
        }

        /// <summary>
        /// The name of the current project.
        /// </summary>
        public string CurrentProjectName
        {
            get
            {
                if (currentProject == null) return string.Empty;
                else return currentProject.Name;
            }
        }

        /// <summary>
        /// The current project directory path.
        /// </summary>
        public string CurrentProjectDirectory
        {
            get
            {
                if (currentProject == null) return string.Empty;
                else return currentProject.FullName;
            }
        }

        #endregion

        #region Filtering

        /// <summary>
        /// The string to display on the filters tab.
        /// </summary>
        public string FilterTabHeaderName
        {
            get
            {
                if (currentSelection == null || !currentSelection.HasContracts)
                {
                    return string.Empty;
                }
                else
                {
                    return string.Format(Resources.FilterTab, currentSelection.ProjectModelCodeElement.Filters.Count);
                }
            }
        }

        /// <summary>
        /// The filters to display in the filter tab.
        /// </summary>
        public List<FilterRowViewModel> Filters
        {
            get
            {
                var filters = new List<FilterRowViewModel>();

                if (currentSelection != null && currentSelection.HasContracts)
                {
                    foreach (ContractFilter filter in currentSelection.ProjectModelCodeElement.Filters)
                    {
                        filters.Add(new FilterRowViewModel(filter));
                    }
                }
                return filters;
            }
        }

        #endregion 

        #endregion
    }
}

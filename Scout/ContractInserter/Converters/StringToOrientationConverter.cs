﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace UniversityofWashington.ContractInserter
{
    [ValueConversion(typeof(string), typeof(Orientation))]
    public class StringToOrientationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.Equals("Vertical"))
            {
                return Orientation.Vertical;
            }
            else
            {
                return Orientation.Horizontal;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

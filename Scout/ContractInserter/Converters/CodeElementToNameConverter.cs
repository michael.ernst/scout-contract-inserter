﻿using System;
using System.Globalization;
using System.Windows.Data;
using EnvDTE;

namespace UniversityofWashington.ContractInserter
{
    [ValueConversion(typeof(CodeElement), typeof(string))]
    public sealed class CodeElementToNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            CodeElement element = (CodeElement)value;
            if (element != null)
            {
                string name = ContractGenerator.Extensions.CodeElementDisplayName(element);
                return name;
            }
            else
            {
                return string.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}

﻿using System;
using System.Globalization;
using System.Windows.Data;
using ContractGenerator;

namespace UniversityofWashington.ContractInserter
{
    /// <summary>
    /// Converts a <see cref="GenerateStatus"/> to an integer to display as percentage of generation progress.
    /// </summary>
    [ValueConversion(typeof(GenerateStatus), typeof(int))]
    class GenerateStatusToPercentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            GenerateStatus status = (GenerateStatus)value;

            if (status == GenerateStatus.NothingGenerated)
            {
                return 0;
            }
            else if (status == GenerateStatus.Building)
            {
                return 15;
            }
            else if (status == GenerateStatus.ParsingSource)
            {
                return 30;
            }
            else if (status == GenerateStatus.Seal)
            {
                return 45;
            }
            else if (status == GenerateStatus.Celeriac)
            {
                return 60;
            }
            else if (status == GenerateStatus.DaikonCalculate)
            {
                return 75;
            }
            else if (status == GenerateStatus.DaikonPrint)
            {
                return 85;
            }
            else if (status == GenerateStatus.ParsingContracts)
            {
                return 90;
            }
            else
            {
                return 100;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

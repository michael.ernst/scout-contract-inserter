﻿using System;
using System.Globalization;
using System.Windows.Data;
using ContractGenerator;

namespace UniversityofWashington.ContractInserter
{
    /// <summary>
    /// Converts a <see cref="GenerateStatus"/> to a string.
    /// </summary>
    [ValueConversion(typeof(GenerateStatus), typeof(string))]
    public class GenerateStatusToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            GenerateStatus status = (GenerateStatus)value;

            if (status == GenerateStatus.NothingGenerated)
            {
                return Resources.GenerateStatusNothingGenerated;
            }
            if (status == GenerateStatus.Building)
            {
                return Resources.GenerateStatusBuilding;
            }
            else if (status == GenerateStatus.ParsingSource)
            {
                return Resources.GenerateStatusParsing;
            }
            else if (status == GenerateStatus.Seal)
            {
                return Resources.GenerateStatusSeal;
            }
            else if (status == GenerateStatus.Celeriac)
            {
                return Resources.GenerateStatusCeleriac;
            }
            else if (status == GenerateStatus.DaikonCalculate)
            {
                return Resources.GenerateStatusDaikonCalculate;
            }
            else if (status == GenerateStatus.DaikonPrint)
            {
                return Resources.GenerateStatusDaikonPrint;
            }
            else if (status == GenerateStatus.ParsingContracts)
            {
                return Resources.GenerateStatusParsingContracts;
            }
            else
            {
                return Resources.GenerateStatusFinished;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

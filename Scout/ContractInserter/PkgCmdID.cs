﻿// PkgCmdID.cs
// MUST match PkgCmdID.h

namespace UniversityofWashington.ContractInserter
{
    static class PkgCmdIDList
    {
        public const uint ShowContractInserterToolWindowCmd = 0x101;
        public const uint GenerateCodeContractsCmd = 0x0100;
        public const uint LoadCodeContractsCmd = 0x0110;
    };
}
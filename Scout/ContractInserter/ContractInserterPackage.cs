﻿using System;
using System.Runtime.InteropServices;
using System.ComponentModel.Design;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio;
using EnvDTE;
using System.Diagnostics.Contracts;

namespace UniversityofWashington.ContractInserter
{
    /// <summary>
    /// This is the class that implements the package exposed by this assembly.
    ///
    /// The minimum requirement for a class to be considered a valid package for Visual Studio
    /// is to implement the IVsPackage interface and register itself with the shell.
    /// This package uses the helper classes defined inside the Managed Package Framework (MPF)
    /// to do it: it derives from the Package class that provides the implementation of the 
    /// IVsPackage interface and uses the registration attributes defined in the framework to 
    /// register itself and its components with the shell.
    /// </summary>
    // This attribute tells the PkgDef creation utility (CreatePkgDef.exe) that this class is
    // a package.
    [PackageRegistration(UseManagedResourcesOnly = true)]
    // This attribute is used to register the information needed to show this package
    // in the Help/About dialog of Visual Studio.
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
    // This attribute is needed to let the shell know that this package exposes some menus.
    [ProvideMenuResource("Menus.ctmenu", 1)]
    // This attribute registers a tool window exposed by this package.
    [ProvideToolWindow(typeof(ContractInserterFrame))]
    [Guid(GuidList.guidContractInserterPkgString)]
    public sealed class ContractInserterPackage : Package
    {
        /// <summary>
        /// Default constructor of the package.
        /// Inside this method you can place any initialization code that does not require 
        /// any Visual Studio service because at this point the package object is created but 
        /// not sited yet inside Visual Studio environment. The place to do all the other 
        /// initialization is the Initialize method.
        /// </summary>
        // ReSharper disable once EmptyConstructor
        public ContractInserterPackage()
        {
        }

        /// <summary>
        /// This function is called when the user clicks the menu item that shows the 
        /// tool window. See the Initialize method to see how the menu item is associated to 
        /// this function using the OleMenuCommandService service and the MenuCommand class.
        /// </summary>
        private void ShowContractInserter(object sender, EventArgs e)
        {
            // Get the instance number 0 of this tool window. This window is single instance so this instance
            // is actually the only one.
            // The last flag is set to true so that if the tool window does not exists it will be created.
            ToolWindowPane window = this.FindToolWindow(typeof(ContractInserterFrame), 0, true);
            if ((null == window) || (null == window.Frame))
            {
                throw new NotSupportedException(Resources.CanNotCreateWindow);
            }
            IVsWindowFrame windowFrame = (IVsWindowFrame)window.Frame;
            Microsoft.VisualStudio.ErrorHandler.ThrowOnFailure(windowFrame.Show());
        }

        /// <summary>
        /// Gets the DTE object after a project has been selected to generate/load contracts for.
        /// </summary>
        /// <returns>the DTE object for the solution</returns>
        private DTE GetDTE()
        {
            Contract.Ensures(Contract.Result<DTE>() != null);
            Contract.Ensures(Contract.Result<DTE>().SelectedItems != null);
            Contract.Ensures(Contract.Result<DTE>().SelectedItems.Count == 1);
            Contract.Ensures(Contract.Result<DTE>().SelectedItems.MultiSelect == false);
            DTE dte = Package.GetGlobalService(typeof(DTE)) as DTE;
            return dte;
        }

        /// <summary>
        /// This function is called when the user hits the generate code contracts 
        /// button on the context menu of a project node in the solution explorer.
        /// </summary>
        private void GenerateCodeContracts(object sender, EventArgs e)
        {
            DTE dte = GetDTE();
            SelectedItem item = dte.SelectedItems.Item(1); // For some reason Item(0) is null.
            Project projectToGenerate = item.Project;
            Contract.Assert(projectToGenerate != null);

            // Get the tool window that contains the contract inserter interface.
            ToolWindowPane window = this.FindToolWindow(typeof(ContractInserterFrame), 0, true);
            // Get the contract inserter interface.
            ContractInserter contractInserter = (ContractInserter) window.Content;
            contractInserter.Generate(dte.Solution, projectToGenerate);
        }

        private void LoadCodeContracts(object sender, EventArgs e)
        {
            DTE dte = GetDTE();
            SelectedItem item = dte.SelectedItems.Item(1); // For some reason Item(0) is null.
            Project projectToGenerate = item.Project;
            Contract.Assert(projectToGenerate != null);

            // Get the tool window that contains the contract inserter interface.
            ToolWindowPane window = this.FindToolWindow(typeof(ContractInserterFrame), 0, true);
            // Get the contract inserter interface.
            ContractInserter contractInserter = (ContractInserter)window.Content;
            contractInserter.Load(dte.Solution, projectToGenerate);
        }

        /////////////////////////////////////////////////////////////////////////////
        // Overridden Package Implementation
        #region Package Members

        /// <summary>
        /// Initialization of the package; this method is called right after the package is sited, so this is the place
        /// where you can put all the initialization code that rely on services provided by VisualStudio.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            // Add our command handlers for menu (commands must exist in the .vsct file)
            OleMenuCommandService mcs = GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if ( null != mcs )
            {
                // Create the command for the tool window
                CommandID toolwndCommandID = new CommandID(GuidList.guidContractInserterCmdSet, (int)PkgCmdIDList.ShowContractInserterToolWindowCmd);
                MenuCommand menuToolWin = new MenuCommand(ShowContractInserter, toolwndCommandID);
                mcs.AddCommand(menuToolWin);

                CommandID generateButtonCommandID = new CommandID(GuidList.guidContractInserterCmdSet, (int)PkgCmdIDList.GenerateCodeContractsCmd);
                MenuCommand generateContractsButtonCmd = new MenuCommand(GenerateCodeContracts, generateButtonCommandID);
                mcs.AddCommand(generateContractsButtonCmd);

                CommandID loadButtonCommandID = new CommandID(GuidList.guidContractInserterCmdSet, (int)PkgCmdIDList.LoadCodeContractsCmd);
                MenuCommand loadContractsButtonCmd = new MenuCommand(LoadCodeContracts, loadButtonCommandID);
                mcs.AddCommand(loadContractsButtonCmd);
            }
        }
        #endregion
    }
}

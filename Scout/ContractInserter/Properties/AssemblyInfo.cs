﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ContractInserter")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("University of Washington")]
[assembly: AssemblyProduct("ContractInserter")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]   
[assembly: ComVisible(false)]     
[assembly: CLSCompliant(false)]
[assembly: NeutralResourcesLanguage("en-US")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: InternalsVisibleTo("ContractInserter_IntegrationTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100b3620f24da820760dd4b37d48728becc2f9f437a91e7bcaf6d89d54ad11c4d05b083df473277e57259f7afe7b05530dfcbc2e4748e33d59cba8eb7a96508891120ffa6a065848b656a8573a34bd954908b2f8065b98094fd1c11528f7a7f0b1d2bc28e32dae7797ca405bf11b8cb3c83b99fcdf05fd7977250b08c6b253d87bd")]
[assembly: InternalsVisibleTo("ContractInserter_UnitTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100b3620f24da820760dd4b37d48728becc2f9f437a91e7bcaf6d89d54ad11c4d05b083df473277e57259f7afe7b05530dfcbc2e4748e33d59cba8eb7a96508891120ffa6a065848b656a8573a34bd954908b2f8065b98094fd1c11528f7a7f0b1d2bc28e32dae7797ca405bf11b8cb3c83b99fcdf05fd7977250b08c6b253d87bd")]

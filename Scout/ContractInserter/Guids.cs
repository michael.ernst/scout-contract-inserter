﻿// Guids.cs
// MUST match guids.h
using System;

namespace UniversityofWashington.ContractInserter
{
    static class GuidList
    {
        public const string guidContractInserterPkgString = "e3182501-a109-4443-8f36-42095cc96b0f";
        public const string guidContractInserterCmdSetString = "707a8b32-09bc-41f6-8b23-1405bd59fb1f";
        public const string guidToolWindowPersistanceString = "06bed63b-c248-4d30-ba7c-dc99b1f72084";

        public static readonly Guid guidContractInserterCmdSet = new Guid(guidContractInserterCmdSetString);
    };
}
﻿using ContractGenerator.Classification;
using Roslyn.Compilers.CSharp;
using System;
using Xunit;

namespace Tests
{
    public class ExpressionSemanticsTester
    {
        /// <summary>
        /// Tests the Negator.TryNegate method. This method attempts to apply a negation to an abritrary c# expression.
        /// </summary>
        [Fact]
        public void TestTryNegate()
        {
            var expression = Syntax.ParseExpression("y >= x");
            var negate = Negator.TryNegate(expression);
            Assert.Equal("y < x", negate.ToFullString());

            expression = Syntax.ParseExpression("y == x");
            negate = Negator.TryNegate(expression);
            Assert.Equal("y != x", negate.ToFullString());

            expression = Syntax.ParseExpression("y > x");
            negate = Negator.TryNegate(expression);
            Assert.Equal("y <= x", negate.ToFullString());

            expression = Syntax.ParseExpression("y < x");
            negate = Negator.TryNegate(expression);
            Assert.Equal("y >= x", negate.ToFullString());

            expression = Syntax.ParseExpression("y <= x");
            negate = Negator.TryNegate(expression);
            Assert.Equal("y > x", negate.ToFullString());

            expression = Syntax.ParseExpression("!x");
            negate = Negator.TryNegate(expression);
            Assert.Equal("x", negate.ToFullString());

            expression = Syntax.ParseExpression("x");
            negate = Negator.TryNegate(expression);
            Assert.Equal("!x", negate.ToFullString());

            expression = Syntax.ParseExpression("!(x)");
            negate = Negator.TryNegate(expression);
            Assert.Equal("x", negate.ToFullString());

            expression = Syntax.ParseExpression("(!(x))");
            negate = Negator.TryNegate(expression);
            Assert.Equal("x", negate.ToFullString());

            expression = Syntax.ParseExpression("!(x.y.z())");
            negate = Negator.TryNegate(expression);
            Assert.Equal("x.y.z()", negate.ToFullString());

            expression = Syntax.ParseExpression("(x.y.z() > 0)");
            negate = Negator.TryNegate(expression);
            Assert.Equal("x.y.z() <= 0", negate.ToFullString());

            expression = Syntax.ParseExpression("Contract.Result<bool>()");
            negate = Negator.TryNegate(expression);
            Assert.Equal("!Contract.Result<bool>()", negate.ToFullString());

            expression = Syntax.ParseExpression("!(Contract.Result<bool>())");
            negate = Negator.TryNegate(expression);
            Assert.Equal("Contract.Result<bool>()", negate.ToFullString());

            expression = Syntax.ParseExpression("!Contract.Result<bool>()");
            negate = Negator.TryNegate(expression);
            Assert.Equal("Contract.Result<bool>()", negate.ToFullString());
        }

        /// <summary>
        /// Tests the Negator.TryApplyNegation method. This method attempts to apply negations found in an abritary c# expression.
        /// </summary>
        [Fact]
        public void TryApplyNegation()
        {
            var expression = Syntax.ParseExpression("!(y >= x)");
            var negate = Negator.TryApplyNegation(expression);
            Assert.Equal("y < x", negate.ToFullString());

            expression = Syntax.ParseExpression("!(x)");
            negate = Negator.TryApplyNegation(expression);
            Assert.Equal("!x", negate.ToFullString());

            expression = Syntax.ParseExpression("(!((!x))");
            negate = Negator.TryApplyNegation(expression);
            Assert.Equal("x", negate.ToFullString());

            expression = Syntax.ParseExpression("(!((!!x))");
            negate = Negator.TryApplyNegation(expression);
            Assert.Equal("!x", negate.ToFullString());

            expression = Syntax.ParseExpression("(((!x))");
            negate = Negator.TryApplyNegation(expression);
            Assert.Equal("!x", negate.ToFullString());

            expression = Syntax.ParseExpression("(!(!x)");
            negate = Negator.TryApplyNegation(expression);
            Assert.Equal("x", negate.ToFullString());

            expression = Syntax.ParseExpression("(not(!x)");
            negate = Negator.TryApplyNegation(expression);
            Assert.Equal("x", negate.ToFullString());

            expression = Syntax.ParseExpression("(not(!(x > 3))");
            negate = Negator.TryApplyNegation(expression);
            Assert.Equal("x > 3", negate.ToFullString());

            expression = Syntax.ParseExpression("(not(!(not((x > 3))))");
            negate = Negator.TryApplyNegation(expression);
            Assert.Equal("x <= 3", negate.ToFullString());

            expression = Syntax.ParseExpression("not(_return == true)");
            negate = Negator.TryApplyNegation(expression);
            Assert.Equal("_return != true", negate.ToFullString());

            expression = Syntax.ParseExpression("!(_return == true)");
            negate = Negator.TryApplyNegation(expression);
            Assert.Equal("_return != true", negate.ToFullString());

            expression = Syntax.ParseExpression("!(x.y.z())");
            negate = Negator.TryApplyNegation(expression);
            Assert.Equal("!x.y.z()", negate.ToFullString());

            expression = Syntax.ParseExpression("!(x.y.z() > 0)");
            negate = Negator.TryApplyNegation(expression);
            Assert.Equal("x.y.z() <= 0", negate.ToFullString());
        }

        /// <summary>
        /// Tests the ExpressionSimplifer.SimplifyBooleanEquality method. This method turns verbose boolean equality statements
        /// such as "x == true" into simpler equivalent expressions, such as "x".
        /// </summary>
        [Fact]
        public void TestSimplifyBooleanEquality()
        {
            var expression = Syntax.ParseExpression("x == true");
            var simplify = ExpressionSimplifier.SimplifyBooleanEquality(expression);
            Assert.Equal("x", simplify.ToFullString().Trim());

            expression = Syntax.ParseExpression("x != true");
            simplify = ExpressionSimplifier.SimplifyBooleanEquality(expression);
            Assert.Equal("!x", simplify.ToFullString().Trim());

            expression = Syntax.ParseExpression("x == false");
            simplify = ExpressionSimplifier.SimplifyBooleanEquality(expression);
            Assert.Equal("!x", simplify.ToFullString().Trim());

            expression = Syntax.ParseExpression("x != false");
            simplify = ExpressionSimplifier.SimplifyBooleanEquality(expression);
            Assert.Equal("x", simplify.ToFullString().Trim());
        }

        /// <summary>
        /// Tests the ExpressionSimplifer.SimplifyNegationForIdentifiers method.
        /// </summary>
        [Fact]
        public void TestSimplifyNegationForIdentifiers()
        {
            var expression = Syntax.ParseExpression("!(x)");
            var simplify = ExpressionSimplifier.SimplifyNegationForIdentifiers(expression);
            Assert.Equal("!x", simplify.ToFullString().Trim());

            expression = Syntax.ParseExpression("(!(x))");
            simplify = ExpressionSimplifier.SimplifyNegationForIdentifiers(expression);
            Assert.Equal("!x", simplify.ToFullString().Trim());

            expression = Syntax.ParseExpression("(x)");
            simplify = ExpressionSimplifier.SimplifyNegationForIdentifiers(expression);
            Assert.Equal("x", simplify.ToFullString().Trim());
        }
        
        /// <summary>
        /// Tests the ExpressionComparison.AreSimilar method.
        /// </summary>
        [Fact]
        public void TestExpressionsAreSimilar()
        {
            bool fact = ExpressionComparison.AreSimilar("y > x || x == 1", "y > x || x == 1");
            Assert.False(fact);

            fact = ExpressionComparison.AreSimilar("y > x", "y < x");
            Assert.True(fact);
        }

        /// <summary>
        /// Tests the ExpressionComparison.AreEquivalentBinaryExpressions method.
        /// </summary>
        [Fact]
        public void TestBinaryExpressionEquivalence()
        {
            bool fact = ExpressionComparison.AreEquivalentBinaryExpressions("x > y", "y < x");
            Assert.True(fact);

            fact = ExpressionComparison.AreEquivalentBinaryExpressions("x <= y", "y >= x");
            Assert.True(fact);

            fact = ExpressionComparison.AreEquivalentBinaryExpressions("x != y", "y != x");
            Assert.True(fact);

            bool fiction = ExpressionComparison.AreEquivalentBinaryExpressions("x > y", "y <= x");
            Assert.False(fiction);
        }
    }
}

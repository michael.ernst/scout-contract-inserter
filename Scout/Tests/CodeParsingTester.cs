﻿using System;
using Xunit;
using ContractGenerator.Classification;
using System.Collections.Generic;
using ContractGenerator;

namespace Tests
{
    public class CodeParsingTester
    {
        /// <summary>
        /// Tests Extensions.FixGenericArtifacts, which is responsible for correcting generic brackets and removing 
        /// the number of generic arguments from an expression.
        /// </summary>
        [Fact]
        public void TestFixGenericArtifacts()
        {
            string before;
            string after;

            before = "System.Tuple`2[int, string]";
            after = Extensions.FixGenericArtifacts(before);
            Assert.Equal("System.Tuple<int, string>", after);

            before = "Contract.Result<System.Collections.Generic.List`1[System.Tuple`2[Lucene.Net.Documents.Document,System.Single]]>()";
            after = Extensions.FixGenericArtifacts(before);
            Assert.Equal("Contract.Result<System.Collections.Generic.List<System.Tuple<Lucene.Net.Documents.Document,System.Single>>>()", after);

            before = "Contract.Result<System.Collections.Generic.List`1[System.Tuple`2[Lucene.Net.Documents.Document[],String[]]]>()";
            after = Extensions.FixGenericArtifacts(before);
            Assert.Equal("Contract.Result<System.Collections.Generic.List<System.Tuple<Lucene.Net.Documents.Document[],String[]>>>()", after);
        }

        /// <summary>
        /// Tests the parsing of parameters from generated contract files.
        /// </summary>
        [Fact]
        public void TestContractParserParameterParsing()
        {
            var parameters = ContractParser.ParseParameters(
                new[] 
                { 
                    "System.Collections.Generic.IDictionary`2<TKey@ TValue> Dictionary"
                });

            Assert.Equal(1, parameters.Count);
            var parameter = parameters[0];
            Assert.Equal("System.Collections.Generic.IDictionary<TKey, TValue>", parameter.QualifiedTypeName);
            Assert.Equal("IDictionary<TKey, TValue>", parameter.SimpleTypeName);
            Assert.Equal("Dictionary", parameter.Name);

            parameters = ContractParser.ParseParameters(
                new[] 
                { 
                    "T Value",
                    "T Max",
                    "T Min",
                    "System.Collections.Generic.IComparer`1<T> Comparer"
                });
            Assert.Equal(4, parameters.Count);

            var parameter1 = parameters[0];
            Assert.Equal("T", parameter1.QualifiedTypeName);
            Assert.Equal("T", parameter1.SimpleTypeName);
            Assert.Equal("Value", parameter1.Name);

            var parameter2 = parameters[1];
            Assert.Equal("T", parameter2.QualifiedTypeName);
            Assert.Equal("T", parameter2.SimpleTypeName);
            Assert.Equal("Max", parameter2.Name);

            var parameter3 = parameters[2];
            Assert.Equal("T", parameter3.QualifiedTypeName);
            Assert.Equal("T", parameter3.SimpleTypeName);
            Assert.Equal("Min", parameter3.Name);

            var parameter4 = parameters[3];
            Assert.Equal("System.Collections.Generic.IComparer<T>", parameter4.QualifiedTypeName);
            Assert.Equal("IComparer<T>", parameter4.SimpleTypeName);
            Assert.Equal("Comparer", parameter4.Name);

            parameters = ContractParser.ParseParameters(
                new[]
                {
                    "System.Func`2<System.Collections.Generic.KeyValuePair`2<T1@ T2>@ T3> OrderBy"
                });

            parameter = parameters[0];
            Assert.Equal("System.Func<System.Collections.Generic.KeyValuePair<T1, T2>, T3>", parameter.QualifiedTypeName);
            // TODO: Parameter.SimpleTypeName for this generic type?
            // Assert.Equal<string>("IDictionary<TKey, TValue>", parameter.SimpleTypeName);
            Assert.Equal("OrderBy", parameter.Name);
        }

        /// <summary>
        /// Tests the counting of the number of generic arguments for type names.
        /// </summary>
        [Fact]
        public void TestGenericParameterCount()
        {
            int genericArgCount;
            const string type1 = "Utilities.DataTypes.TagDictionary<Key, Value>";
            genericArgCount = type1.GenericParameterCount();
            Assert.Equal(2, genericArgCount);

            const string type2 = "Utilities.DataTypes.TagDictionary<Key, Value>.TaggedItem<TKey, TValue, int>";
            genericArgCount = type2.GenericParameterCount();
            Assert.Equal(3, genericArgCount);

            const string type3 = "TagDictionary<Key, Value>";
            genericArgCount = type3.GenericParameterCount();
            Assert.Equal(2, genericArgCount);

            const string type4 = "Utilities.DataTypes.Bag<T>";
            genericArgCount = type4.GenericParameterCount();
            Assert.Equal(1, genericArgCount);

            const string type5 = "System.String";
            genericArgCount = type5.GenericParameterCount();
            Assert.Equal(0, genericArgCount);

            const string type6 = "System.Func<System.Collections.Generic.KeyValuePair<T1, T2>, T3>";
            genericArgCount = type6.GenericParameterCount();
            Assert.Equal(2, genericArgCount);
        }

        /// <summary>
        /// Tests the MergeKeys extension method for dictionaries that map to lists.
        /// This routine is used during antecedent contract grouping.
        /// </summary>
        [Fact]
        public void TestMergeKeys()
        {
            var dict = new Dictionary<string, List<int>>();

            Func<string, string, bool> similarButUnequalTest = (first, second) =>
                !first.Equals(second) && ExpressionComparison.AreEquivalentBinaryExpressions(first, second);

            var merge = dict.MergeKeys(similarButUnequalTest);

            Assert.True(merge.Keys.Count == 0);

            dict["x > y"] = new List<int>() { 1, 2, 3 };

            merge = dict.MergeKeys(similarButUnequalTest);

            Assert.True(merge.Keys.Count == 1);
            Assert.Equal(merge["x > y"], new List<int> { 1, 2, 3 });

            dict["result == false"] = new List<int>() { 0 };
            dict["y < x"] = new List<int>() { 4, 5, 6 };

            merge = dict.MergeKeys(similarButUnequalTest);

            Assert.True(merge.Keys.Count == 2);

            foreach (string key in merge.Keys)
            {
                Assert.True(key.Equals("x > y") || key.Equals("result == false"));

                if (key.Equals("x > y"))
                {
                    Assert.Equal(merge[key], new List<int> { 1, 2, 3, 4, 5, 6 });
                }

                if (key.Equals("result == false"))
                {
                    Assert.Equal(merge[key], new List<int> { 0 });
                }
            }
        }
    }
}

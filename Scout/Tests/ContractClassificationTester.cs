﻿using Xunit;
using ContractGenerator.Classification;
using Roslyn.Compilers.CSharp;
using ContractGenerator;

namespace Tests
{
    public class ContractClassificationTester
    {
        [Fact]
        public void TestClassification()
        {
            bool nullness = SemanticClassifier.IsNullnessCheck(Syntax.ParseExpression("x != null"));
            Assert.True(nullness);

            string classification;
            classification = SemanticClassifier.Classify("n == null");
            Assert.Equal("Nullness", classification);

            classification = SemanticClassifier.Classify("n != null");
            Assert.Equal("Nullness", classification);

            classification = SemanticClassifier.Classify("n.OneOf(1, 2, 3)");
            Assert.Equal("Expr. Comparison", classification);

            classification = SemanticClassifier.Classify("Contract.ForAll(list, x => x != null)");
            Assert.Equal("Nullness", classification);

            classification = SemanticClassifier.Classify("Contract.ForAll(list, x => x != null)");
            Assert.Equal("Nullness", classification);

            classification = SemanticClassifier.Classify("Contract.ForAll(list, x => x != null)");
            Assert.Equal("Nullness", classification);

        }

        [Fact]
        public void TestNullnessClassification()
        {
            bool nullness;

            nullness = SemanticClassifier.IsNullnessCheck(Syntax.ParseExpression("x != null"));
            Assert.True(nullness);

            nullness = SemanticClassifier.IsNullnessCheck(Syntax.ParseExpression("x == null"));
            Assert.True(nullness);

            nullness = SemanticClassifier.IsNullnessCheck(Syntax.ParseExpression("Contract.ForAll(list, x => x != null)"));
            Assert.True(nullness);
        }

        [Fact]
        public void TestBoundsClassification()
        {
            bool isBoundsCheck;

            isBoundsCheck = SemanticClassifier.IsBoundedCheck(Syntax.ParseExpression("IndexWriter.MaxDoc() >= 0"));
            Assert.True(isBoundsCheck);

            isBoundsCheck = SemanticClassifier.IsBoundedCheck(
                Syntax.ParseExpression("Contract.Result<ArchiMetrics.Common.Metrics.ITypeDefinition>().Assembly.Length >= 0"));
            Assert.True(isBoundsCheck);
        }

        [Fact]
        public void TestConstantClassification()
        {
            bool isConstantCheck;

            isConstantCheck = SemanticClassifier.IsConstantCheck(Syntax.ParseExpression("x == 10"));
            Assert.True(isConstantCheck);

            isConstantCheck = SemanticClassifier.IsConstantCheck(Syntax.ParseExpression("x.getInt() == 10"));
            Assert.True(isConstantCheck);

            isConstantCheck = SemanticClassifier.IsConstantCheck(Syntax.ParseExpression("x.Int == 10"));
            Assert.True(isConstantCheck);

            isConstantCheck = SemanticClassifier.IsConstantCheck(Syntax.ParseExpression("language.Equals(\"C#\")"));
            Assert.True(isConstantCheck);

            isConstantCheck = SemanticClassifier.IsConstantCheck(Syntax.ParseExpression("language.ToString().Equals(\"C#\")"));
            Assert.True(isConstantCheck);
        }

    }
}

﻿using ContractGenerator.Classification;
using Roslyn.Compilers.CSharp;
using System;
using Xunit;

namespace Tests
{
    public class ImplicationTester
    {
        [Fact]
        public void TestGeneratedImplications()
        {
            Implication implication;

            implication = new Implication("(s == 0).Implies(Contract.OldValue(s) == 0); // Daikon");
            Assert.Equal("s == 0", implication.Antecedent);
            Assert.Equal("Contract.OldValue(s) == 0", implication.Consequent);
            Assert.True(!implication.IsUniversal);
            Assert.True(implication.DaikonComponentsUnset);

            implication = new Implication("(s.isEmpty()).Implies(s.Count() == 0);");
            Assert.Equal("s.isEmpty()", implication.Antecedent);
            Assert.Equal("s.Count() == 0", implication.Consequent);
            Assert.True(!implication.IsUniversal);
            Assert.True(implication.DaikonComponentsUnset);

            implication = new Implication("(returnn == true).Implies(this.Balance <= Contract.OldValue(this.Balance))");
            Assert.Equal("returnn", implication.Antecedent);
            Assert.Equal("Balance <= Contract.OldValue(Balance)", implication.Consequent);
            Assert.True(!implication.IsUniversal);
            Assert.True(implication.DaikonComponentsUnset);

            implication = new Implication("!(x > 3) == y < 4");
            Assert.Equal("true", implication.Antecedent);
            Assert.Equal("x <= 3 == y < 4", implication.Consequent);
            Assert.True(implication.IsUniversal);
            Assert.True(implication.DaikonComponentsUnset);

            implication = new Implication(
                contract: "(!(Contract.Result<bool> == true)).Implies(x != null)",
                daikon: "not(return == true) ==> x != null");

            Assert.Equal("!Contract.Result<bool>", implication.Antecedent);
            Assert.Equal("x != null", implication.Consequent);
            Assert.Equal("!return", implication.DaikonAntecedent);
            Assert.Equal("x != null", implication.DaikonConsequent);

            implication = new Implication(
                contract: "(this._hasIndexChanged == false) == (Contract.OldValue(this._hasIndexChanged) == false)",
                daikon: "(this._hasIndexChanged == false)  <==>  (orig(this._hasIndexChanged) == false)");

            Assert.Equal("true", implication.Antecedent);
            Assert.Equal("!_hasIndexChanged == !Contract.OldValue(_hasIndexChanged)", implication.Consequent);
            Assert.Equal("true", implication.DaikonAntecedent);
            Assert.Equal("!_hasIndexChanged == !orig(_hasIndexChanged)", implication.DaikonConsequent);

            implication = new Implication(
                contract: "(!(_criteria.SearchByProgramElementType)).Implies(this._criteria != null)",
                daikon: "not(_criteria.SearchByProgramElementType) ==> this._criteria != null");

            Assert.Equal("!_criteria.SearchByProgramElementType", implication.Antecedent);
            Assert.Equal("_criteria != null", implication.Consequent);
            Assert.Equal("!_criteria.SearchByProgramElementType", implication.DaikonAntecedent);
            Assert.Equal("_criteria != null", implication.DaikonConsequent);
        }

        [Fact]
        public void TestWildImplications()
        {
            Implication implication;
            string contract;
            string daikon;

            implication = new Implication("s != 0 || Contract.OldValue(s) == 0");
            Assert.Equal(implication.Antecedent, "s == 0");
            Assert.Equal(implication.Consequent, "Contract.OldValue(s) == 0");

            contract = "!(this.Balance == 0) || Contract.OldValue(this.Balance) == 0";
            daikon = "!(this.Balance == 0) || orig(this.Balance) == 0";
            implication = new Implication(contract, daikon);

            Assert.Equal(implication.Antecedent, "Balance == 0");
            Assert.Equal(implication.Consequent, "Contract.OldValue(Balance) == 0");
        }

        [Fact]
        public void TestIsOpposite()
        {
            Implication first;
            Implication second;
            Implication third;

            first = new Implication("(Contract.Result<bool>()).Implies(Contract.OldValue(this) != null)");
            second = new Implication("(!Contract.Result<bool>()).Implies(Contract.OldValue(this) != null)");
            third = new Implication("((!Contract.Result<bool>())).Implies(Contract.OldValue(this) != null)");
            Assert.True(first.IsOpposite(second));
            Assert.True(second.IsOpposite(first));
            Assert.True(first.IsOpposite(third));
            Assert.True(third.IsOpposite(first));
            Assert.False(second.IsOpposite(third));
            Assert.False(third.IsOpposite(second));
        }
    }
}

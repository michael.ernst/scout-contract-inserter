﻿The splitter visualizer is a small interface for viewing the generated Daikon splitter (.spinfo) files for an entire directory or a single file. 

Directory: generated splitter files will appear in the /splitters folder of that directory

File: generated splitters for that file will appear in the text box of the interface
﻿using System.IO;
using System.Windows;

namespace SplitterVisualizer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void GenerateSplittersForDirectory(object sender, RoutedEventArgs e)
        {
            string directory = this.directoryPath.Text;
            directory = directory.Replace("\"", string.Empty);

            if (Directory.Exists(directory))
            {
                ContractGenerator.Splitters.SplitterGenerator.GenerateSplittersForDirectory(directory);
                ContractGenerator.Splitters.SplitterGenerator.ConcatSplittersForDirectory("C:\\Users\\Forrest\\Desktop\\splitters");
            }
        }

        private void GenerateSplittersForFile(object sender, RoutedEventArgs e)
        {
            string filePath = this.filePath.Text;
            filePath = filePath.Replace("\"", string.Empty);

            if (!File.Exists(filePath))
            {
                MessageBox.Show(
                    string.Format("File '{0}' does not exist.", filePath));
            }
            else if (!filePath.EndsWith(".cs"))
            {
                MessageBox.Show(
                    string.Format("'{0}' is not a c# source file.", filePath));
            }
            else
            {
                string splitters = ContractGenerator.Splitters.SplitterGenerator.GenerateSplitters(filePath);
                this.output.Text = splitters;
            }
        }
    }
}

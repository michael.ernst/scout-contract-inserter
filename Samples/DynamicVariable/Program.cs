﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace DynamicVariable
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic x = 1;
            TakeDynamic(x);
            x = 2;
            TakeDynamic(x);
            x = 3;
            TakeDynamic(x);
        }

        static dynamic TakeDynamic(dynamic arg)
        {
            dynamic y = 10;
            if (arg == 1)
            {
                y = 5.2;
            }
            else if (arg == 2)
            {
                y = "Dynamic!";
            }
            return y;
        }
    }
}

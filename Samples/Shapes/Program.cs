﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    public class Rectangle
    {
        public virtual int Width { get; set; }
        public virtual int Height { get; set; }

        public Rectangle()
        {
        }

        public Rectangle(int width, int height)
        {
            SetSize(width, height);
        }

        public virtual void SetSize(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public virtual void SetSizeTwo(int width, int height)
        {
            Width = width;
            Height = height;
        }
    }

    public class Square : Rectangle
    {
        private int width;

        public override int Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }

        public override int Height { get; set; }

        public Square() { }

        public Square(int size)
        {
            Width = size;
            Height = size;
        }
    }

    public class AnotherSquare : Square
    {
        public AnotherSquare(int size)
        {
            SetSize(size, size);
        }

        public override void SetSize(int width, int height)
        {
            base.SetSize(width, height);
        }

        public override void SetSizeTwo(int width, int height)
        {
            Width = width;
            Height = height;
        }
    }

    public class Program
    {
        static Random r = new Random();
        static void Main(string[] args)
        {
            Square s = new Square(3);

            for (int i = 1; i < 20; i++)
            {
                s = new Square(i);
                AnotherSquare h = new AnotherSquare(i);
                int f = r.Next(10) + 1;
                s.SetSize(f * s.Width, f * s.Height);
                h.SetSize(s.Width, s.Height);
                h.SetSizeTwo(s.Width, s.Height);
                Square a = new Square(i);
                a.SetSizeTwo(f * a.Width, f * a.Height);
            }
        }
    }
}

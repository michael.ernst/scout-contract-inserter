﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.Contracts;

namespace Utilities.DataTypes.Comparison
{
    public class GenericComparer<T> : IComparer<T> where T : IComparable
    {
        [Pure]
        public bool IsValueTypeOrNullableGenericType
        {
            get
            {
                return !typeof(T).IsValueType ||
                    (typeof(T).IsGenericType && typeof(T).GetGenericTypeDefinition().IsAssignableFrom(typeof(Nullable<>)));
            }
        }

        [Pure]
        public bool EqualsDefaultType(T x)
        {
            return Object.Equals(x, default(T));
        }

        [Pure]
        public IComparable<T> TempComparable(T x)
        {
            return x as IComparable<T>;
        }

        [Pure]
        public int CompareTo(T x, T y)
        {
            return x.CompareTo(y);
        }

        public int _Compare(T x, T y)
        {
            if (IsValueTypeOrNullableGenericType)
            {
                if (EqualsDefaultType(x))
                    return EqualsDefaultType(y) ? 0 : -1;
                if (EqualsDefaultType(y))
                    return -1;
            }

            if (x.GetType() != y.GetType())
                return -1;

            if (TempComparable(x) != null)
            {
                return TempComparable(x).CompareTo(y);
            }

            return CompareTo(x, y);
        }

        public int Compare(T x, T y)
        {
            if (!typeof(T).IsValueType || 
                (typeof(T).IsGenericType && typeof(T).GetGenericTypeDefinition().IsAssignableFrom(typeof(Nullable<>))))
            {
                if (Object.Equals(x, default(T)))
                    return Object.Equals(y, default(T)) ? 0 : -1;
                if (Object.Equals(y, default(T)))
                    return -1;
            }

            if (x.GetType() != y.GetType())
                return -1;

            IComparable<T> TempComparable = x as IComparable<T>;
            if (TempComparable != null)
                return TempComparable.CompareTo(y);

            return x.CompareTo(y);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            GenericComparer<string> StringComparer = new GenericComparer<string>();
            StringComparer.Compare("A", "A");
            StringComparer.Compare("A", "B");
            StringComparer.Compare("B", "A");
            StringComparer.Compare("C", "C");

            GenericComparer<int> IntComparer = new GenericComparer<int>();
            IntComparer.Compare(-55, -45);
            IntComparer.Compare(-45, -55);
            IntComparer.Compare(0, 0);
            IntComparer.Compare(0, 10);
        }
    }
}

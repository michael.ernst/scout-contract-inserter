﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Diagnostics;

/*
 - An implementation of the GaleShaply stable matching algorithm.
 - 
 - Uses enumeration and no properties.
 */
namespace StableMatching
{
    public enum Gender { M, F }

    public class PreferenceList
    {
        private Person[] preferences;
        public int rank;


        public PreferenceList(Person[] prefs)
        {
            preferences = prefs;
            rank = 0;
        }

        public Person GetNextPreference()
        {
            Person pref = preferences[rank];
            rank++;
            return pref;
        }

        public int GetRankingOf(Person p)
        {
            int rank = 1;
            while (preferences[rank - 1] != p) rank++;
            return rank;
        }
    }

    public class Person
    {
        [ContractInvariantMethod()]
        private void Invariants()
        {
            Contract.Invariant(this.gender.ToString().Equals("F") || this.gender.ToString().Equals("M")); // Daikon        
        }

        public int id;
        public Person match;
        public Gender gender;
        public PreferenceList preferences;
        private static Random r;

        public Person(int id, Gender gender)
        {
            this.id = id;
            this.gender = gender;
            this.preferences = null;
            this.match = null;
            r = new Random();
        }

        // Create and randomize the preference list. 
        public void SetPreferences(Person[] p)
        {
            preferences = new PreferenceList(p.Reverse().ToArray());
        }

        // Core of the GaleShaply stable matching algorithm.
        // This person performs a proposal to the next best person on their preference list. 
        // Returns the newly freed person if there was a break up to add back to the free stack.
        public Person Propose()
        {
            Person w = preferences.GetNextPreference();
            if (w.match == null)
            {
                match = w;
                w.match = this;
                return null;
            }
            else
            {
                if (!w.Prefers(this, w.match))
                {
                    Person newFree = w.match;
                    w.match.match = null;
                    match = w;
                    w.match = this;
                    return newFree;
                }
                else
                {
                    return this;
                }
            }
        }

        // Returns true if this person prefers a to b, given a and b are valid preferences. 
        private Boolean Prefers(Person a, Person b)
        {
            int ra = preferences.GetRankingOf(a);
            int rb = preferences.GetRankingOf(b);
            return ra > rb;
        }
    }

    public class StableMatchingWithEnums
    {
        static void Main(string[] args)
        {
            int[] sizes = { 4 };
            int iterations = 1;

            foreach (int s in sizes)
            {
                Person[] m = new Person[s];
                Person[] f = new Person[s];
                Stack<Person> free = new Stack<Person>();

                for (int j = 1; j <= iterations; j++)
                {
                    free.Clear();

                    // Create s males and s females.
                    for (int i = 1; i <= s; i++)
                    {
                        m[i - 1] = new Person(i, Gender.M);
                        f[i - 1] = new Person(i, Gender.F);
                    }

                    // For each person, set their prefrences, and add males to free stack. 
                    for (int i = 1; i <= s; i++)
                    {
                        m[i - 1].SetPreferences(f);
                        f[i - 1].SetPreferences(m);
                        free.Push(m[i - 1]);
                    }

                    // Run GaleShapley stable matching algorithm.
                    while (free.Count() != 0)
                    {
                        Person freed = free.Pop().Propose();
                        if (freed != null)
                        {
                            free.Push(freed);
                        }
                    }
                }
            }
        }
    }
}

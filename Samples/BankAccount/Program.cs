﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAccount
{
    [ContractClass(typeof(AbstractIAccountContract))]
    public abstract class AbstractIAccount
    {
        public abstract double Balance { get; set; }
        public abstract void Deposit(double amount);
        public abstract bool Withdraw(double amount);
    }

    [ContractClassFor(typeof(AbstractIAccount))]
    public abstract class AbstractIAccountContract : AbstractIAccount
    {
        public override double Balance
        {
            get
            {
                return default(double);
            }
            set
            {
            }
        }

        public override void Deposit(double amount)
        {

        }

        public override bool Withdraw(double amount)
        {
            return default(bool);
        }
    }

    public class AbstractAccount : AbstractIAccount
    {
        public override double Balance { get; set; }

        public AbstractAccount()
        {
            Balance = 0;
        }

        public override void Deposit(double amount)
        {
            if (amount < 0.0)
                throw new ArgumentOutOfRangeException();
            Balance += amount;
        }

        public override bool Withdraw(double amount)
        {
            if (amount < 0)
                throw new ArgumentOutOfRangeException();
            if (amount > Balance)
                return false;
            Balance -= amount;
            return true;
        }
    }

    [ContractClass(typeof(IAccountContract))]
    public interface IAccount
    {
        double Balance { get; }
        void Deposit(double amount);
        bool Withdraw(double amount);
    }

    [ContractClassFor(typeof(IAccount))]
    public abstract class IAccountContract : IAccount
    {
        public double Balance
        {
            get
            {
                return default(double);
            }
        }

        public void Deposit(double amount)
        {
            Contract.Requires(this.Balance != amount); // Daikon        

        }

        public bool Withdraw(double amount)
        {
            return default(bool);
        }
    }

    public class Account : IAccount
    {
        public double Balance { get; private set; }

        public Account()
        {
            Balance = 0;
        }

        public void Deposit(double amount)
        {
            if (amount < 0.0)
                throw new ArgumentOutOfRangeException();
            Balance += amount;
        }

        public bool Withdraw(double amount)
        {
            if (amount < 0)
                throw new ArgumentOutOfRangeException();
            if (amount > Balance)
                return false;
            Balance -= amount;
            return true;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Account myAccount = new Account();
            AbstractAccount myAccount2 = new AbstractAccount();

            for (int iterations = 0; iterations < 200; iterations++)
            {
                double deposit = new Random().Next(100);
                myAccount.Deposit(deposit + iterations);
                myAccount.Deposit(deposit);
                myAccount.Deposit(iterations);
                myAccount2.Deposit(deposit + iterations);
                myAccount2.Deposit(deposit);
                myAccount2.Deposit(iterations);
                Console.WriteLine(myAccount.Balance);
                Console.WriteLine(myAccount2.Balance);
            }

            for (int iterations = 0; iterations < 100; iterations++)
            {
                double withdraw = new Random().Next(500);
                myAccount.Withdraw(withdraw);
                myAccount.Withdraw(iterations);
                myAccount2.Withdraw(withdraw);
                myAccount2.Withdraw(iterations);
                Console.WriteLine(myAccount.Balance);
                Console.WriteLine(myAccount2.Balance);
            }

            for (int iterations = 0; iterations < 1000; iterations++)
            {
                double withdraw = new Random().Next(500);
                myAccount.Withdraw(withdraw);
                myAccount.Withdraw(iterations);
                myAccount2.Withdraw(withdraw);
                myAccount2.Withdraw(iterations);
                Console.WriteLine(myAccount.Balance);
                Console.WriteLine(myAccount2.Balance);
            }
        }
    }
}

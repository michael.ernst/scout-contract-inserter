﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

/*
 * An implementation of the Gale-Shaply stable matching algorithm.
 * 
 * Does not use an enumeration but uses properties with private setters.
 */
namespace StableMatchingWithProperties
{
    public class PreferenceList
    {
        private Person[] preferences;
        public int rank { get; private set; }

        public PreferenceList(Person[] prefs)
        {
            preferences = prefs;
            rank = 0;
        }

        public Person GetNextPreference()
        {
            Person pref = preferences[rank];
            rank++;
            // Debug.Assert(rank <= preferences.Length);
            return pref;
        }

        public int GetRankingOf(Person p)
        {
            // Debug.Assert(preferences.Contains(p));
            int rank = 1;
            while (preferences[rank - 1] != p) rank++;
            return rank;
        }
    }

    public class Person
    {
        public int id { get; private set; }
        public Person match { get; private set; }
        public int gender { get; private set; }
        public PreferenceList preferences { get; private set; }
        private static Random r;

        public Person(int id, int gender)
        {
            this.id = id;
            this.gender = gender;
            this.preferences = null;
            this.match = null;
            r = new Random();
        }

        // Create and randomize the preference list. 
        public void SetPreferences(Person[] p)
        {
            preferences = new PreferenceList(p.Reverse().ToArray());
        }

        // Core of the Gale-Shaply stable matching algorithm.
        // This person performs a proposal to the next best person on their preference list. 
        // Returns the newly freed person if there was a break up to add back to the free stack.
        public Person Propose()
        {
            Person w = preferences.GetNextPreference();
            // Debug.Assert(gender != w.gender);
            if (w.match == null)
            {
                match = w;
                w.match = this;
                return null;
            }
            else
            {
                if (!w.Prefers(this, w.match))
                {
                    Person newFree = w.match;
                    w.match.match = null;
                    match = w;
                    w.match = this;
                    return newFree;
                }
                else
                {
                    return this;
                }
            }
        }

        // Returns true if this person prefers a to b, given a and b are valid preferences. 
        private Boolean Prefers(Person a, Person b)
        {
            int ra = preferences.GetRankingOf(a);
            int rb = preferences.GetRankingOf(b);
            return ra > rb;
        }
    }

    public class Run
    {
        static void Main(string[] args)
        {
            int[] sizes = { 10 };
            int iterations = 10;

            foreach (int s in sizes)
            {
                Person[] m = new Person[s];
                Person[] f = new Person[s];
                Stack<Person> free = new Stack<Person>();

                for (int j = 1; j <= iterations; j++)
                {
                    free.Clear();

                    // Create s males and s females.
                    for (int i = 1; i <= s; i++)
                    {
                        m[i - 1] = new Person(i, 1);
                        f[i - 1] = new Person(i, 2);
                    }

                    // For each person, set their prefrences, and add males to free stack. 
                    for (int i = 1; i <= s; i++)
                    {
                        m[i - 1].SetPreferences(f);
                        f[i - 1].SetPreferences(m);
                        free.Push(m[i - 1]);
                    }

                    // Run Gale-Shapley stable matching algorithm.
                    while (free.Count() != 0)
                    {
                        Person freed = free.Pop().Propose();
                        if (freed != null)
                        {
                            free.Push(freed);
                        }
                    }
                }
            }
        }
    }
}

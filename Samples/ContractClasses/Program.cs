using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace ContractClasses
{
    [ContractClass(typeof(AbstractIAccountContract))]
    public abstract class AbstractIAccount
    {
        public abstract double Balance { get; set;  }
        public abstract void Deposit(double amount);
        public abstract bool Withdraw(double amount);

        public virtual double GetBalance()
        {
            return this.Balance;
        }
    }

    [ContractClassFor(typeof(AbstractIAccount))]
    public abstract class AbstractIAccountContract : AbstractIAccount
    {
        public override double Balance
        {
            get
            {
                return default(double);
            }
            set
            {
            }
        }

        public override void Deposit(double amount)
        {

        }

        public override bool Withdraw(double amount)
        {
            Contract.Ensures(this.Balance != Contract.OldValue(this.Balance)); // Daikon        
            return default(bool);
        }
    }

    public class AbstractAccount : AbstractIAccount
    {
        [ContractInvariantMethod()]
        private void ObjectInvariant()
        {
            Contract.Invariant(this != null); // Daikon        
        }
    
        public override double Balance { get; set; }

        public AbstractAccount()
        {   
            Balance = 0;
        }

        public override void Deposit(double amount)
        {
            if (amount < 0.0)
                throw new ArgumentOutOfRangeException();
            Balance += amount;
        }

        public override bool Withdraw(double amount)
        {      
            if (amount < 0)
                throw new ArgumentOutOfRangeException();
            if (amount > Balance)
                return false;
            Balance -= amount;
            return true;
        }
    }

    public interface IAccount
    {
        double Balance { get; }
        void Deposit(double amount);
        bool Withdraw(double amount);
    }

    public class Account : IAccount
    {
        public double Balance { get; private set; }

        public Account()
        {   
            Balance = 0;
        }

        public void Deposit(double amount)
        {
            if (amount < 0.0)
                throw new ArgumentOutOfRangeException();
            Balance += amount;
        }

        public bool Withdraw(double amount)
        {      
            if (amount < 0)
                throw new ArgumentOutOfRangeException();
            if (amount > Balance)
                return false;
            Balance -= amount;
            return true;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            Account myAccount = new Account();
            AbstractAccount myAccount2 = new AbstractAccount();

            for (int iterations = 0; iterations < 30; iterations++)
            {
                double deposit = new Random().Next(1000);
                myAccount.Deposit(deposit);
                myAccount2.Deposit(deposit);
                Console.WriteLine(myAccount.Balance);
            }

            for (int iterations = 0; iterations < 15; iterations++)
            {
                double withdraw = new Random().Next(150);
                myAccount.Withdraw(withdraw);
                myAccount2.Withdraw(withdraw);
                Console.WriteLine(myAccount.Balance);
            }
        }
    }
}

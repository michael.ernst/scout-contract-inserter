﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultidimensionalArrays
{
    public static class Program
    {
        public static T[] Concat<T>(this T[] Array1, params T[][] Additions)
        {
            T[] Result = new T[Array1.Length + Additions.Sum(x => x.Length)];
            int Offset = Array1.Length;
            Array.Copy(Array1, 0, Result, 0, Array1.Length);
            for (int x = 0; x < Additions.Length; ++x)
            {
                Array.Copy(Additions[x], 0, Result, Offset, Additions[x].Length);
                Offset += Additions[x].Length;
            }
            return Result;
        }

        public static int[] ConcatIntArrays(this int[] Array1, params int[][] Additions)
        {
            int[] Result = new int[Array1.Length + Additions.Sum(x => x.Length)];
            int Offset = Array1.Length;
            Array.Copy(Array1, 0, Result, 0, Array1.Length);
            for (int x = 0; x < Additions.Length; ++x)
            {
                Array.Copy(Additions[x], 0, Result, Offset, Additions[x].Length);
                Offset += Additions[x].Length;
            }
            return Result;
        }

        public static void TripleJagged(int[][][] triple)
        {
           
        }

        static void Main(string[] args)
        {

        }
    }
}

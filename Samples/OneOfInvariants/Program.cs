﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace OneOfInvariants
{
    class Program
    {
        /*
         * Designed to invoke many of the OneOf Daikon invariants. 
         * 
         * The following should be enabled/disabled via configuration:
         * daikon.inv.unary.string.OneOfString.enabled = 1
         * daikon.inv.unary.stringsequence.EltOneOfString.enabled = 1
         * daikon.inv.unary.stringsequence.OneOfStringSequence.enabled = 1

         * daikon.inv.unary.scalar.OneOfFloat.enabled = 1
         * daikon.inv.unary.sequence.EltOneOfFloat.enabled = 1
         * daikon.inv.unary.sequence.OneOfFloatSequence.enabled = 1

         * daikon.inv.unary.scalar.OneOfScalar.enabled = 1
         * daikon.inv.unary.sequence.EltOneOf.enabled = 1
         * daikon.inv.unary.sequence.OneOfSequence.enabled = 0
         * 
         */
        static void Main(string[] args)
        {
            Int32[] a = { };
            Int32[] b = { 1 };
            Int32[] c = { 2, 2, 2 };
            Int32[] d = { 1, 2, 3 };
            Int64[] e = { Int64.MaxValue - 1, 0, 1 };

            IntZeroElts(a);
            IntSingleElt(b);
            IntUniqueElt(c);
            IntManyDifferentElts(d);
            IntOverFlow(e);

            double[] af = { };
            double[] bf = { 1.0 };
            double[] cf = { 2.0, 2.0, 2.0 };
            double[] df = { 1.0, 2.0, 3.0, 1.0 };
            double[] de = { double.NaN, 1.0, double.NaN, 4.8 };

            FloatZeroElts(af);
            FloatSingleElt(bf);
            FloatUniqueElt(cf);
            FloatManyDifferentElts(df);
            FloatNaN(de);

            String[] astr = { };
            String[] bstr = { "A" };
            String[] cstr = { "B", "B", "B" };
            String[] dstr = { "A", "B", "C", "A" };
            String[] estr = { null, "A", String.Empty, "B", null };

            StringZeroElts(astr);
            StringSingleElt(bstr);
            StringUniqueElt(cstr);
            StringManyDifferentElts(dstr);

            NetTcpStyleUriParser[] objectsNonNull = { new NetTcpStyleUriParser(), new NetTcpStyleUriParser(), new NetTcpStyleUriParser() };
            Object[] objectsNull = { null, null, null };
            Boolean[] boolsTrue = { true, true, true };
            Boolean[] boolsFalse = { false, false, false };

            BoolsTrue(boolsTrue);
            BoolsFalse(boolsFalse);
            NullObjects(objectsNull);
            NonNullObjects(objectsNonNull);

            Int32[] sub1 = { 1, 2, 3, 4, 5, 6 };
            Int32[] sub2 = { 7, 8, 1, 2, 5, 6, 7, 8 };
            SubSequenceTest(sub1, sub2);
        }

        // a.Length == 0
        static void IntZeroElts(Int32[] a)
        {
        }

        // Contract.ForAll(a, x => x == 1)
        static void IntSingleElt(Int32[] a)
        {
        }

        // Contract.ForAll(a, x => x == 2)
        static void IntUniqueElt(Int32[] a)
        {
        }

        // Contract.ForAll(a, x => new int[] { 1, 2, 3 }.Contains(x))
        static void IntManyDifferentElts(Int32[] a)
        {
        }

        // Contract.ForAll(e, x => new int[] { 0, 1, 9223372036854775806L }.Contains(x))
        static void IntOverFlow(Int64[] e)
        {
        }

        // a.Length == 0
        static void FloatZeroElts(double[] a)
        {
        }

        // Contract.ForAll(a, x => x == 1.0)
        static void FloatSingleElt(double[] a)
        {
        }

        // Contract.ForAll(a, x => x == 2.0)
        static void FloatUniqueElt(double[] a)
        {
        }

        // Contract.ForAll(a, x => new double[] { 1.0, 2.0, 3.0 }.Contains(x))
        static void FloatManyDifferentElts(double[] a)
        {
        }

        // Contract.ForAll(a, x => new double[] { 1.0, 4.8, Double.NaN }.Contains(x))
        static void FloatNaN(double[] a)
        {
        }

        // s.Length == 0
        static void StringZeroElts(String[] s)
        {
        }

        // Contract.ForAll(s, x => x != null)
        // Contract.ForAll(s, x => x.Equals("A"))
        static void StringSingleElt(String[] s)
        {
        }

        // Contract.ForAll(s, x => x != null)
        // Contract.ForAll(s, x => x.Equals("B"))
        static void StringUniqueElt(String[] s)
        {
        }

        // Contract.ForAll(s, x => new string[] { "A", "B", "C" }.Contains(x))
        static void StringManyDifferentElts(String[] s)
        {
        }

        // Contract.ForAll(a, x => x == true)
        static void BoolsTrue(Boolean[] a)
        {
        }

        //Contract.ForAll(a, x => x == false)
        static void BoolsFalse(Boolean[] a)
        {
        }

        //Contract.ForAll(s, x => x == null)
        static void NullObjects(Object[] s)
        {
        }

        // Contract.ForAll(csharplib.toTypeArray(s), x => x.Equals("System.NetTcpStyleUriParser"))
        static void NonNullObjects(Object[] s)
        {
        }

        static void SubSequenceTest(Int32[] a, Int32[] b)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Collections
{
    /*
     * Tests a variety of different collections and objects.
     */
    class Program
    {
        static void Main(string[] args)
        {
            var g = new List<Int32>();
            var b = new HashSet<Int32>();
            var d = new HashSet<Int32>();
            b.Add(1);
            b.Add(2);
            b.Add(3);
            d.Add(4);
            d.Add(5);
            d.Add(6);

            LinkedList<Int32> a = new LinkedList<Int32>();
            a.AddLast(10);
            a.AddLast(20);
            a.AddLast(30);
            LinkedList<Int32> a2 = new LinkedList<Int32>();
            a2.AddLast(20);
            a2.AddLast(30);
            a2.AddLast(40);

            TakeLinkedList(a, a2);
            // TakeSet(b, d);

            var c = new UriBuilder();
            TakeUri(c);

            var j = new LinkedList<String[]>();
            x(j);
            takeString("hello");
            takeStringArray(new[] { "hello", "world" });

            var n = new LinkedList<LinkedList<Int32>>[10];
            //TakeLinkedListOfLinkedListOfInt(n);

            takeString(0);
            takeString(1);
            takeString(2);
            takeString(3);
            takeString(4);
            takeString(5);
        }

        static int takeString(int s)
        {     
            if (s > 0)
            {
                return s + 1;
            }
            else
            {
                return 10;
            }
        }

        static String takeString(String s)
        {
            return s;
        }

        static String[] takeStringArray(String[] s)
        {
            return s;
        }

        static LinkedList<String[]> x(LinkedList<String[]> d)
        {
            if (d.GetType() == typeof(System.Collections.Generic.LinkedList<System.String[]>))
            {

            }
            return null;
        }

        static void TakeUri(Object c)
        {
        }

        static Boolean TakeLinkedList(LinkedList<Int32> list, LinkedList<Int32> list2)
        {
            bool result = true;
            if (list.GetType() == typeof(System.Collections.Generic.LinkedList<System.Int32>))
            {
                for (int i = 0; i < list.Count(); i++)
                {
                    if (list.ElementAt(i) <= list2.ElementAt(i))
                    {
                        result = false;
                    }
                }
            }
            return result;
        }

        // TODO: this method in the purity file causes Celeriac to crash.
        static HashSet<Int32> TakeSet(HashSet<Int32> list, HashSet<Int32> list2)
        {
            return list;
        }

        // TODO: Does not work with splitters.
        //static LinkedList<LinkedList<Int32>>[] TakeLinkedListOfLinkedListOfInt(LinkedList<LinkedList<Int32>>[] advancedType)
        //{
        //    return advancedType;
        //}
    }
}

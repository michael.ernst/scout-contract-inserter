﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace FunctionBinaryInvariants
{
    /*
     * Designed to invoke FunctionBinary Daikon invariants. 
     * 
     * The following should be enabled via configuration:
     * daikon.inv.ternary.threeScalar.FunctionBinary.enabled = 1
     * daikon.inv.ternary.threeScalar.FunctionBinaryFloat.enabled = 1
     * 
     */
    class Program
    {
        static void Main(string[] args)
        {
            Int32[] a = new Int32[100];
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = i;
                int x = i + new Random().Next(10) - 5;
                if (x == 0)
                    x = 1;
                min(a[i] + 1, x);
                max(a[i] + 1, x);
                divide(a[i] + 1, x);
                mod(a[i] + 1, x);
                power(a[i] + 1, x);
                multiply(a[i] + 1, x);
                bitwiseAnd(a[i] + 1, x);
                bitwiseOr(a[i] + 1, x);
                bitwiseXor(a[i] + 1, x);
                logicalAnd(a[i] + 1, x);
                logicalOr(a[i] + 1, x);
                logicalXor(a[i] + 1, x);
                leftShift(a[i] + 1, x);
                rightShiftSigned(a[i] + 1, x);
                rightShiftUnsigned(a[i] + 1, x);
                GCD(a[i] + 1, x);
            }
        }

        static int min(int a, int b)
        {
            int c = Math.Min(a, b);
            return c;
        }

        static int max(int a, int b)
        {
            int c = Math.Max(a, b);
            return c;
        }

        static int multiply(int a, int b)
        {
            int c = a * b;
            return c;
        }

        static int divide(int a, int b)
        {
            int c = a / b;
            return c;
        }

        static int mod(int a, int b)
        {
            int c = a % b;
            return c;
        }

        static int power(int a, int b)
        {
            double c = Math.Pow((double)a, (double)b);
            int d = (int)c;
            return d;
        }

        static int bitwiseAnd(int a, int b)
        {
            int c = a & b;
            return c;
        }

        static int bitwiseOr(int a, int b)
        {
            int c = a | b;
            return c;
        }

        static int bitwiseXor(int a, int b)
        {
            int c = a ^ b;
            return c;
        }

        static int logicalAnd(int a, int b)
        {
            bool c = Convert.ToBoolean(a) && Convert.ToBoolean(b);
            return Convert.ToInt32(c);
        }

        static int logicalOr(int a, int b)
        {
            bool c = Convert.ToBoolean(a) || Convert.ToBoolean(b);
            return Convert.ToInt32(c);
        }

        static int logicalXor(int a, int b)
        {
            bool c = Convert.ToBoolean(a) ^ Convert.ToBoolean(b);
            return Convert.ToInt32(c);
        }

        static int leftShift(int a, int b)
        {
            int c = a << b;
            return c;
        }

        static int rightShiftSigned(int a, int b)
        {
            int c = a >> b;
            return c;
        }

        static Int32 rightShiftUnsigned(Int32 a, Int32 b)
        {
            Int32 c = (Int32)((UInt32)a >> b);
            return c;
        }

        static int GCD(int a, int b)
        {
            int Remainder;
            int d = a;

            while (b != 0)
            {
                Remainder = d % b;
                d = b;
                b = Remainder;
            }

            return d;
        }
    }
}

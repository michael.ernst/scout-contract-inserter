**Overview**

Scout is a Visual Studio extension for discovering and inserting Code Contracts into C# programs. It works by observing a program execution, generating likely properties, and providing an interface for inserting those properties as Code Contracts into the source code.

See the [User Manual](https://bitbucket.org/fmc3/scout/wiki/Scout%20User%20Manual) for more information.

**Developer Debug Instructions**

1. Clone this project and open the solution inside Visual Studio 2012 or 2013

2. Set ``ContractInserter`` as the start up project

3. Navigate to ``Properties -> Debug`` for the ``ContractInserter`` project:

	- Select ``Start external program`` and set it to the path of your Visual Studio 2012 or 2013 executable. For example, on my system the path is ``C:\Program Files (x86)\Common7\IDE\devenv.exe``
	- For ``Command line arguments`` enter ``/RootSuffix Exp``. This tells Visual Studio to use an experimental instance for testing rather than the actual Visual Studio installation

** Related Repositories**

* [Daikon](https://code.google.com/p/daikon/)
* [Celeriac](https://code.google.com/p/daikon-dot-net-front-end/)
* [C# Extension Library](https://code.google.com/p/daikon-code-contract-extensions/)
* [SEAL Clone](https://bitbucket.org/fmc3/seal-clone)
* [Assembly Reference Resolver](https://bitbucket.org/fmc3/assembly-reference-resolver)